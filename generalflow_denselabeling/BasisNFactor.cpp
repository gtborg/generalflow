/**
* @file    Basis2Factor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <generalflow_denselabeling/BasisNFactor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace denselabeling {

    /* **************************************************************************************** */
    void BasisNFactor::print(const string& s, const KeyFormatter& keyFormatter) const
    {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool BasisNFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const
    {
      const BasisNFactor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(velocity_, t->velocity_, tol) &&
        equal_with_abs_tol(flowSigmas_, t->flowSigmas_, tol) &&
        equal_with_abs_tol(pixSigmas_, t->pixSigmas_, tol) &&
        images_ == t->images_ && pixX_ == t->pixX_ && pixY_ == t->pixY_;
    }

    /* **************************************************************************************** */
    Vector BasisNFactor::unwhitenedError(const Values& x, boost::optional<std::vector<Matrix>&> H) const
    {
      const DenseIndex kappa = indicators_.size() - 1;

      // Set up derivative matrices
      boost::optional<Matrix&> ddWRot;
      boost::optional<vector<Matrix> > ddWk;
      if(H)
      {
        assert(kappa == H->size());
        ddWk = vector<Matrix>(kappa - 1);
        for(size_t k = 0; k < kappa - 1; ++k)
          (*ddWk)[k].swap((*H)[k]);
        ddWRot = (*H).back();
      }

      // Set up bases
      vector<Eigen::Map<const Matrix> > basesTrans;
      for(size_t k = 0; k < kappa - 1; ++k)
      {
        const Matrix& basisTrans = x.at<LieMatrix>(keys_[k]);
        basesTrans.push_back(Eigen::Map<const Matrix>(basisTrans.data(), basisTrans.rows(), basisTrans.cols()));
      }
      const Matrix& basisRot = x.at<LieMatrix>(keys_.back());

      // Call image error
      Vector e = flow::fullImageErrorImpl(*images_, pixX_, pixY_, basisRot, basesTrans, velocity_,
          flowSigmas_, pixSigmas_, indicators_, ddWRot,
          ddWk ? boost::optional<vector<Matrix>&>(*ddWk) : boost::optional<vector<Matrix>&>(),
          boost::none, visualization_);

      // Retrieve derivatives
      if(H)
        for(size_t k = 0; k < kappa - 1; ++k)
          (*ddWk)[k].swap((*H)[k]);

      return e;
    }

  }
}
