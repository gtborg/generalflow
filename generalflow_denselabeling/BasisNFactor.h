/**
* @file    BasisNFactor.h
* @brief   Factor on N bases with images as measurements
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#pragma once

#include <generalflow_denselabeling/dllexport.h>
#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

#include <boost/tuple/tuple.hpp>

namespace generalflow
{
  namespace denselabeling
  {

    /* **************************************************************************************** */
    class generalflow_denselabeling_EXPORT BasisNFactor: public gtsam::NoiseModelFactor
    {
    public:
      typedef gtsam::NoiseModelFactor Base;
      typedef BasisNFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Vector velocity_;
      gtsam::Vector flowSigmas_;
      gtsam::Vector pixSigmas_;
      gtsam::Vector indicators_;

      flow::ImagePairGrad::shared_ptr images_;
      gtsam::DenseIndex pixX_;
      gtsam::DenseIndex pixY_;

      boost::shared_ptr<flow::FullImageErrorVisualization> visualization_;

    public:

      /** Constructor
       * @param basisKey Key for basis (n x q LieMatrix)
       * @param latentKey Key for latent variable (q LieVector)
       * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
       * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
       * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
       * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
       * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
       */
      template<typename KEYS>
      BasisNFactor(gtsam::Key basisRotKey, const KEYS& basisTransKeys,
        const gtsam::Vector& velocity,
        const gtsam::Vector& flowSigmas, const gtsam::Vector& pixSigmas,
        const gtsam::Vector& indicators,
        const flow::ImagePairGrad::shared_ptr& images, gtsam::DenseIndex pixX, gtsam::DenseIndex pixY,
        const boost::shared_ptr<flow::FullImageErrorVisualization>& visualization =
        boost::shared_ptr<flow::FullImageErrorVisualization>()) :
        Base(gtsam::SharedNoiseModel()), velocity_(velocity), flowSigmas_(flowSigmas), pixSigmas_(pixSigmas),
        indicators_(indicators),
        images_(images), pixX_(pixX), pixY_(pixY), visualization_(visualization)
      {
        Base::keys_.resize(basisTransKeys.size() + 1);
        std::copy(basisTransKeys.begin(), basisTransKeys.end(), Base::keys_.begin());
        Base::keys_.back() = basisRotKey;
      }

      /** Virtual destructor */
      virtual ~BasisNFactor()
      {
      }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter =
          gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const
      {
        return (size_t)(indicators_.size() - 1);
      }

      /** Evaluate error */
      virtual gtsam::Vector unwhitenedError(const Values& x, boost::optional<std::vector<Matrix>&> H = boost::none) const;
    };

  }
}
