function varargout = basisLearningFrameVisualizer(varargin)
% BASISLEARNINGFRAMEVISUALIZER MATLAB code for basisLearningFrameVisualizer.fig
%      BASISLEARNINGFRAMEVISUALIZER, by itself, creates a new BASISLEARNINGFRAMEVISUALIZER or raises the existing
%      singleton*.
%
%      H = BASISLEARNINGFRAMEVISUALIZER returns the handle to a new BASISLEARNINGFRAMEVISUALIZER or the handle to
%      the existing singleton*.
%
%      BASISLEARNINGFRAMEVISUALIZER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BASISLEARNINGFRAMEVISUALIZER.M with the given input arguments.
%
%      BASISLEARNINGFRAMEVISUALIZER('Property','Value',...) creates a new BASISLEARNINGFRAMEVISUALIZER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before basisLearningFrameVisualizer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to basisLearningFrameVisualizer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help basisLearningFrameVisualizer

% Last Modified by GUIDE v2.5 08-Oct-2013 11:44:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @basisLearningFrameVisualizer_OpeningFcn, ...
                   'gui_OutputFcn',  @basisLearningFrameVisualizer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function str = getEstimatedVelocityString(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
str = sprintf('%s/velocity_%04d.txt', path, iter);

function vel = getEstimatedVelocity(handles)
vel = dlmread(getEstimatedVelocityString(handles));
frame = get(handles.frameSlider, 'Value');
vel = vel(frame, :);

function v = getTrueVelocity(handles, frameonly)
path = get(handles.dataPath, 'String');
path = path{1};
str = sprintf('%s/velocity.txt', path);
v = dlmread(str);
if exist('frameonly', 'var') && frameonly
    frame = get(handles.frameSlider, 'Value');
    v = v(frame, :);
end

function img = getImg1(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/img1_%04d_%06d.txt', path, iter, 5*frame);
img = dlmread(str);

function img = getImg2(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/img2_%04d_%06d.txt', path, iter, 5*frame);
img = dlmread(str);

function str = getTrueFlowString(handles)
path = get(handles.dataPath, 'String');
path = path{1};
str = sprintf('%s/flow.txt', path);

function errs = getErrors1(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/errors_1_%04d_%06d.txt', path, iter, 5*frame);
errs = dlmread(str);

function errs = getErrors2(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/errors_2_%04d_%06d.txt', path, iter, 5*frame);
errs = dlmread(str);

function ind = getIndicators(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/indicators_%04d_%06d.txt', path, iter, 5*frame);
ind = dlmread(str);
wid = size(ind, 2);
hei = size(ind, 1) / 3;
ind = permute(reshape(ind, [ 3, hei, wid ]), [ 2, 3, 1 ]);

function W = getBasis(handles, k)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
str = sprintf('%s/basis_%d_%04d.txt', ...
    path, k, iter);
W = dlmread(str);

function Dt = getTemporalDeriv(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/Dt_%04d_%06d.txt', path, iter, 5*frame);
Dt = dlmread(str);

function flow = getExpectedFlow(handles)
path = get(handles.dataPath, 'String');
path = path{1};
iter = get(handles.iterationSlider, 'Value');
frame = get(handles.frameSlider, 'Value');
str = sprintf('%s/flow_%04d_%06d.txt', path, iter, 5*frame);
flow = dlmread(str);

function setSliderLimits(handles)
set(handles.frameSlider, 'Min', 1);
set(handles.minFrame, 'String', get(handles.frameSlider, 'Min'));
vels = dlmread(getEstimatedVelocityString(handles));
set(handles.frameSlider, 'Max', size(vels, 1));
set(handles.maxFrame, 'String', get(handles.frameSlider, 'Max'));
set(handles.frameSlider, 'Value', get(handles.frameSlider, 'Min'));
set(handles.curFrame, 'String', get(handles.frameSlider, 'Value'));
nFrames = get(handles.frameSlider, 'Max') - get(handles.frameSlider, 'Min') + 1;
set(handles.frameSlider, 'SliderStep', [ 1/(nFrames-1) 5/(nFrames-1) ]);
set(handles.iterationSlider, 'Min', 0);
set(handles.iterationSlider, 'Max', 99999);
found = false;
for frame = 0:1
    set(handles.iterationSlider, 'Value', frame);
    if exist(getEstimatedVelocityString(handles), 'file')
        found = true;
        break;
    end
end
if ~found
    error 'Cannot find files'
end
set(handles.iterationSlider, 'Min', get(handles.iterationSlider, 'Value'));
set(handles.minIteration, 'String', num2str(get(handles.iterationSlider, 'Min')));
maxframe = -1;
for tryframe = frame:99999
    set(handles.iterationSlider, 'Value', tryframe);
    if ~exist(getEstimatedVelocityString(handles), 'file')
        if tryframe - maxframe > 20
            break;
        end
    else
        maxframe = tryframe;
    end
end
set(handles.iterationSlider, 'Value', get(handles.iterationSlider, 'Min'));
set(handles.curIteration, 'String', num2str(get(handles.iterationSlider, 'Value')));
set(handles.iterationSlider, 'Max', maxframe);
set(handles.maxIteration, 'String', num2str(get(handles.iterationSlider, 'Max')));
nIterations = get(handles.iterationSlider, 'Max') - get(handles.iterationSlider, 'Min') + 1;
set(handles.iterationSlider, 'SliderStep', [ 1/(nIterations-1) 5/(nIterations-1) ]);

function updateGUI(handles)
% Update sliders and labels
set(handles.iterationSlider, 'Value', round(get(handles.iterationSlider, 'Value')));
set(handles.frameSlider, 'Value', round(get(handles.frameSlider, 'Value')));
set(handles.curIteration, 'String', num2str(get(handles.iterationSlider, 'Value')));
set(handles.curFrame, 'String', num2str(get(handles.frameSlider, 'Value')));
% Update images
try
    img1 = getImg1(handles);
    axes(handles.img1);
    imshow(img1);
catch
end
try
    img2 = getImg2(handles);
    axes(handles.img2);
    imshow(img2);
catch
end
% Update true flow
try
    trueFlow = dlmread(getTrueFlowString(handles));
    wid = sqrt(size(trueFlow,2) / 2);
    frame = get(handles.frameSlider, 'Value');
    axes(handles.trueFlow);
    inspector.drawFlow(trueFlow(frame, :)', wid, wid);
catch
end
% Update expected flow
try
    expectedFlow = getExpectedFlow(handles);
    axes(handles.expectedFlow);
    inspector.drawFlow(expectedFlow);
catch
end
% Update errors
try
    errs1 = getErrors1(handles);
    errs2 = getErrors2(handles);
    maxerr = max(1e-20, max(max(max(abs(errs1), abs(errs2)))));
    axes(handles.errors1);
    imshow(inspector.colorize(errs1 / maxerr));
    axes(handles.errors2);
    imshow(inspector.colorize(errs2 / maxerr));
    set(handles.errorsMax, 'String', num2str(maxerr));
catch
end
% Update indicators
try
    axes(handles.indicators);
    imshow(getIndicators(handles));
catch
end
% Update temporal deriv
try
    axes(handles.temporalDeriv);
    Dt = getTemporalDeriv(handles);
    maxDt = max(max(abs(Dt)));
    imshow(inspector.colorize(Dt / maxDt));
    set(handles.maxTemporalDeriv, 'String', num2str(maxDt));
catch
end
% Update labels
try
    set(handles.trueVelocityLabel, 'String', mat2str(getTrueVelocity(handles, true)));
catch
end
try
    set(handles.velocityLabel, 'String', mat2str(getEstimatedVelocity(handles)));
catch
end
try
    set(handles.basis1MagLabel, 'String', num2str(max(max(abs(getBasis(handles, 1))))));
    set(handles.basis2MagLabel, 'String', num2str(max(max(abs(getBasis(handles, 2))))));
catch
end

handles = guidata(handles.mainWindow);
handles.pathIsNew = false;
guidata(handles.mainWindow, handles);


% --- Executes just before basisLearningFrameVisualizer is made visible.
function basisLearningFrameVisualizer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to basisLearningFrameVisualizer (see VARARGIN)

% Choose default command line output for basisLearningFrameVisualizer
handles.output = hObject;

handles.mainWindow = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes basisLearningFrameVisualizer wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Fill color code
axes(handles.colorAxis);
inspector.colorTest;


% --- Outputs from this function are returned to the command line.
function varargout = basisLearningFrameVisualizer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function dataPath_Callback(hObject, eventdata, handles)
% hObject    handle to dataPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dataPath as text
%        str2double(get(hObject,'String')) returns contents of dataPath as a double
handles.pathIsNew = true;
guidata(handles.mainWindow, handles);
setSliderLimits(handles);
updateGUI(handles);


% --- Executes during object creation, after setting all properties.
function dataPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dataPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function iterationSlider_Callback(hObject, eventdata, handles)
% hObject    handle to iterationSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
updateGUI(handles);


% --- Executes during object creation, after setting all properties.
function iterationSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iterationSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function frameSlider_Callback(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
updateGUI(handles);


% --- Executes during object creation, after setting all properties.
function frameSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
