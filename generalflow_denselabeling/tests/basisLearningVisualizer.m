function varargout = basisLearningVisualizer(varargin)
% TESTBASIS2LEARNING MATLAB code for testBasis2Learning.fig
%      TESTBASIS2LEARNING, by itself, creates a new TESTBASIS2LEARNING or raises the existing
%      singleton*.
%
%      H = TESTBASIS2LEARNING returns the handle to a new TESTBASIS2LEARNING or the handle to
%      the existing singleton*.
%
%      TESTBASIS2LEARNING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTBASIS2LEARNING.M with the given input arguments.
%
%      TESTBASIS2LEARNING('Property','Value',...) creates a new TESTBASIS2LEARNING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testBasis2Learning_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testBasis2Learning_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testBasis2Learning

% Last Modified by GUIDE v2.5 09-Oct-2013 14:41:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @basisLearningVisualizer_OpeningFcn, ...
                   'gui_OutputFcn',  @basisLearningVisualizer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function str = getFormatString(handles)
if get(handles.modeTestBasis2Learning, 'Value')
    str = '%s/basis_%d_%04d.txt';
elseif get(handles.modeLearnTemplates, 'Value')
    str = '%s/basis_%d_%04d.txt';
end

function ppcares = getTrueBasis1(handles)
path = get(handles.outputPath, 'String');
path = path{1};
str = sprintf('%s/basis_1.txt', ...
    path);
ppcares.W = dlmread(str);
wid = sqrt(size(ppcares.W,1)/2);
ppcares.basissize = [ 0 0 0 wid wid ];

function ppcares = getTrueBasis2(handles)
path = get(handles.outputPath, 'String');
path = path{1};
str = sprintf('%s/basis_2.txt', ...
    path);
ppcares.W = dlmread(str);
wid = sqrt(size(ppcares.W,1)/2);
ppcares.basissize = [ 0 0 0 wid wid ];


% --- Executes just before basisLearningVisualizer is made visible.
function basisLearningVisualizer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testBasis2Learning (see VARARGIN)

% Choose default command line output for testBasis2Learning
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes testBasis2Learning wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Fill color code
axes(handles.colorAxis);
inspector.colorTest;

% --- Outputs from this function are returned to the command line.
function varargout = basisLearningVisualizer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function outputPath_Callback(hObject, eventdata, handles)
% hObject    handle to outputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outputPath as text
%        str2double(get(hObject,'String')) returns contents of outputPath as a double
setPath = get(hObject, 'String');
setPath = setPath{1};

if exist(setPath, 'file')

    % Load true bases
    try
    axes(handles.trueBasis1);
    inspector.drawBasis(getTrueBasis1(handles));
    axes(handles.trueBasis2);
    inspector.drawBasis(getTrueBasis2(handles));
    catch e
    end

    for frame = 0:1
        if exist(sprintf(getFormatString(handles), ...
                setPath, 1, frame), 'file')
            break;
        end
    end
    if frame > 1
        error 'Cannot find files'
    end
    set(handles.minFrame, 'String', num2str(frame));
    set(handles.frameSlider, 'Min', frame);
    maxframe = -1;
    for tryframe = frame:99999
        if ~exist(sprintf(getFormatString(handles), ...
                setPath, 1, tryframe), 'file')
            if tryframe - maxframe > 20
                break;
            end
        else
            maxframe = tryframe;
        end
    end
    set(handles.maxFrame, 'String', num2str(maxframe));
    set(handles.frameSlider, 'Max', maxframe);
    set(handles.frameSlider, 'SliderStep', [ 1/(maxframe-frame) 5/(maxframe-frame) ]);
    set(handles.frameSlider, 'Value', frame);
end


% --- Executes during object creation, after setting all properties.
function outputPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function frameSlider_Callback(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.frameSlider, 'Value', round(get(handles.frameSlider, 'Value')));
set(handles.curFrame, 'String', num2str(get(handles.frameSlider, 'Value')));

path = get(handles.outputPath, 'String');
path = path{1};
try
    ppcares1.W = dlmread(sprintf(getFormatString(handles), ...
        path, 1, get(handles.frameSlider, 'Value')));
    wid = sqrt(size(ppcares1.W, 1) / 2);
    ppcares1.basissize = [ 0 0 0 wid wid ];
    axes(handles.basisAxis1);
    inspector.drawBasis(ppcares1);
catch
end
try
    ppcares2.W = dlmread(sprintf(getFormatString(handles), ...
        path, 2, get(handles.frameSlider, 'Value')));
    wid = sqrt(size(ppcares2.W, 1) / 2);
    ppcares2.basissize = [ 0 0 0 wid wid ];
    axes(handles.basisAxis2);
    inspector.drawBasis(ppcares2);
catch
end


% --- Executes during object creation, after setting all properties.
function frameSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frameSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
