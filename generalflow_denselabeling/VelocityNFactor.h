/**
* @file    Velocity2Factor.h
* @brief   Factor on velocity with 2 bases and images as measurements
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#pragma once

#include <generalflow_denselabeling/dllexport.h>
#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace generalflow {
  namespace denselabeling {

    class generalflow_denselabeling_EXPORT VelocityNFactor : public gtsam::NonlinearFactor {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef VelocityNFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

      static const DenseIndex Kappa = 2;

    private:

      gtsam::Matrix basisRot_;
      std::vector<gtsam::Matrix> basesTrans_;
      gtsam::Vector flowSigmas_;
      gtsam::Vector pixSigmas_;
      gtsam::Matrix indicators_;
      boost::shared_ptr<flow::ImagePairGrad> images_;
      boost::shared_ptr<flow::FullImageErrorVisualization> visualization_;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      VelocityNFactor(gtsam::Key latentKey, const gtsam::Matrix& basisRot, const std::vector<gtsam::Matrix>& basesTrans,
        const gtsam::Vector& flowSigmas, const gtsam::Vector& pixSigmas,
        const gtsam::Matrix& indicators, const boost::shared_ptr<flow::ImagePairGrad>& images,
        const boost::shared_ptr<flow::FullImageErrorVisualization>& visualization = boost::shared_ptr<flow::FullImageErrorVisualization>()) :
        Base(gtsam::ListOfOne(latentKey)), basisRot_(basisRot), basesTrans_(basesTrans),
        flowSigmas_(flowSigmas), pixSigmas_(pixSigmas),
        indicators_(indicators), images_(images), visualization_(visualization) {}

      gtsam::Key latentKey() const { return Base::keys_[0]; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 2; }

      /// Basis 1
      const gtsam::Matrix& basisRot() const { return basisRot_; }

      /// Basis 1
      const std::vector<gtsam::Matrix>& basesTrans() const { return basesTrans_; }

      /// Sigmas
      const Vector& flowSigmas() const { return flowSigmas_; }

      /// Sigmas
      const Vector& pixSigmas() const { return pixSigmas_; }

      /// Indicators
      const gtsam::Matrix& indicators() const { return indicators_; }

      /// Images
      const boost::shared_ptr<flow::ImagePairGrad>& images() const { return images_; }

      /// Visualization
      const boost::shared_ptr<flow::FullImageErrorVisualization>& visualization() const { return visualization_; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;
    };

  }
}
