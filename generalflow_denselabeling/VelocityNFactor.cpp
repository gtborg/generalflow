/**
* @file    Velocity2Factor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <tbb/tbb.h>

#include <generalflow_denselabeling/VelocityNFactor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <boost/assign/list_of.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace denselabeling {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    void VelocityNFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool VelocityNFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const VelocityNFactor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(basisRot_, t->basisRot_, tol) &&
        basesTrans_ == t->basesTrans_ &&
        equal_with_abs_tol(flowSigmas_, t->flowSigmas_, tol) &&
        equal_with_abs_tol(pixSigmas_, t->pixSigmas_, tol) &&
        images_->equals(*t->images_, tol);
    }

    /* **************************************************************************************** */
    namespace
    {
      struct AccumulateErrorChunk
      {
        const VelocityNFactor& velocityFactor;
        const LieVector& latent;
        double error;

        AccumulateErrorChunk(const VelocityNFactor& velocityFactor, const LieVector& latent) :
          velocityFactor(velocityFactor), latent(latent), error(0.0) {}

        AccumulateErrorChunk(const AccumulateErrorChunk& original, tbb::split) :
          velocityFactor(original.velocityFactor), latent(original.latent), error(0.0) {}

        void join(const AccumulateErrorChunk& other)
        {
          error += other.error;
        }

        void operator()(const tbb::blocked_range<size_t>& range)
        {
          const DenseIndex imHeight = velocityFactor.images()->curFrame().rows();

          for(size_t pix = range.begin(); pix != range.end(); ++pix)
          {
            DenseIndex i = pix % imHeight;
            DenseIndex j = pix / imHeight;

            // Get blocks
            Eigen::Block<const Matrix> WRot = velocityFactor.basisRot().middleRows(2*pix, 2);
            vector<Eigen::Block<const Matrix> > WTrans;
            for(size_t k = 0; k < velocityFactor.basesTrans().size(); ++k)
              WTrans.push_back(velocityFactor.basesTrans()[k].middleRows(2*pix, 2));

            // Compute sqrt error
            const auto indicators = velocityFactor.indicators().col(pix);
            const Vector e = flow::fullImageErrorImpl(*velocityFactor.images(), j, i, WRot, WTrans, latent,
                velocityFactor.flowSigmas(), velocityFactor.pixSigmas(), indicators,
                boost::none, boost::none, boost::none);

            // Update error
            error += e.squaredNorm();
          }
        }
      };
    }

    /* **************************************************************************************** */
    double VelocityNFactor::error(const Values& x) const
    {
      AccumulateErrorChunk errorAccumulator(*this, x.at<LieVector>(latentKey()));
      const size_t nPix = images_->curFrame().cols() * images_->curFrame().rows();
      tbb::parallel_reduce(tbb::blocked_range<size_t>(0, nPix), errorAccumulator);
      return 0.5 * errorAccumulator.error;
    }

    /* **************************************************************************************** */
    namespace
    {
      struct AccumulateHessianChunk
      {
        const VelocityNFactor& velocityFactor;
        const LieVector& latent;
        Matrix hessian;

        AccumulateHessianChunk(const VelocityNFactor& velocity2Factor, const LieVector& latent) :
          velocityFactor(velocity2Factor), latent(latent),
          hessian(Matrix::Zero(latent.size()+1, latent.size()+1)) {}

        AccumulateHessianChunk(const AccumulateHessianChunk& original, tbb::split) :
          velocityFactor(original.velocityFactor), latent(original.latent),
          hessian(Matrix::Zero(latent.size()+1, latent.size()+1)) {}

        void join(const AccumulateHessianChunk& other)
        {
          hessian += other.hessian;
        }

        void operator()(const tbb::blocked_range<size_t>& range)
        {
          const int imHeight = velocityFactor.images()->curFrame().rows();

          Matrix ddy;

          for(size_t pix = range.begin(); pix != range.end(); ++pix)
          {
            int i = pix % imHeight;
            int j = pix / imHeight;

            // Get blocks
            Eigen::Block<const Matrix> WRot = velocityFactor.basisRot().middleRows(2*pix, 2);
            vector<Eigen::Block<const Matrix> > WTrans;
            for(size_t k = 0; k < velocityFactor.basesTrans().size(); ++k)
              WTrans.push_back(velocityFactor.basesTrans()[k].middleRows(2*pix, 2));

            // Compute sqrt error
            const Vector e = flow::fullImageErrorImpl(*velocityFactor.images(), j, i, WRot, WTrans, latent,
                velocityFactor.flowSigmas(), velocityFactor.pixSigmas(),
                Vector(velocityFactor.indicators().col(pix)), boost::none, boost::none, ddy,
                velocityFactor.visualization());

            // Update hessian (only the upper triangle, which is all that GTSAM uses)
            // The factors of 2 here are because this is the Hessian without the 1/2 factor applied in error().
            hessian.topLeftCorner(latent.size(), latent.size()).selfadjointView<Eigen::Upper>()
                .rankUpdate(ddy.transpose());
            hessian.col(latent.size()).head(latent.size()) -= ddy.transpose() * e;
            hessian(latent.size(), latent.size()) += e.squaredNorm();

          }
        }
      };
    }

    /* **************************************************************************************** */
    boost::shared_ptr<GaussianFactor> VelocityNFactor::linearize(const Values& x) const
    {
      // Values for convenience
      const LieVector& latent = x.at<LieVector>(latentKey());
      const DenseIndex q = latent.size();
      const int imWidth = images_->curFrame().cols();
      const int imHeight = images_->curFrame().rows();
      const size_t nPix = imWidth * imHeight;

      // Allocate Hessian (matrix is uninitialized)
      HessianFactor::shared_ptr hessian = boost::make_shared<HessianFactor>(
        ListOfOne(latentKey()), SymmetricBlockMatrix(boost::assign::list_of(q)(1)));

      // Loop over all pixels doing updates on the Hessian
      AccumulateHessianChunk hessianAccumulator(*this, latent);
      tbb::parallel_reduce(tbb::blocked_range<size_t>(0, nPix), hessianAccumulator);

      // Copy matrix into HessianFactor, dividing by two to account for the 1/2 factor in error()
      hessian->info() = hessianAccumulator.hessian;

      return hessian;
    }
  }
}
