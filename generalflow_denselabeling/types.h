/**
* @file    types.h
* @brief   Shared types in denselabeling
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#pragma once

#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <gtsam/base/Matrix.h>

namespace generalflow
{
  namespace denselabeling
  {
    struct Sigmas {
      double flowSigmaF;
      double flowSigmaB1;
      double flowSigmaB2;
      double pixSigmaF;
      double pixSigmaB1;
      double pixSigmaB2;

      bool equals(const Sigmas& other, double tol = 1e-9) const {
        return abs(flowSigmaF - other.flowSigmaF) < tol &&
          abs(flowSigmaB1 - other.flowSigmaB1) < tol &&
          abs(flowSigmaB2 - other.flowSigmaB2) < tol &&
          abs(pixSigmaF - other.pixSigmaF) < tol &&
          abs(pixSigmaB1 - other.pixSigmaB1) < tol &&
          abs(pixSigmaB2 - other.pixSigmaB2) < tol;
      }
    };

    typedef boost::tuple<gtsam::Matrix, gtsam::Matrix, gtsam::Matrix> ClassPriors;
  }
}

