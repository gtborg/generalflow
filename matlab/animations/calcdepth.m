function depth = calcdepth(flow)

width = 64;
height = size(flow, 1) / width / 2;

xs = reshape(repmat((1:width)', 1, height), width*height, 1);
ys = reshape(repmat(1:height, width, 1), width*height, 1);

depth = zeros(height, width);

for i=1:(width*height)
    x = xs(i);
    y = ys(i);
    y = y - (height / 2);
    if x > 32
        x = x - 32;
    end
    x = x - (width / 4);
    
    if x == 0 && y == 0
        invdepth = 1;
    elseif x == 0
        invdepth = flow(i+width*height)/y;
    elseif y == 0
        invdepth = flow(i)/x;
    else
        invdepth = mean([ flow(i)/x, flow(i+width*height)/y ]);
    end
    depth(ys(i), xs(i)) = invdepth;
end

end
