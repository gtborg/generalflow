import cam.*

% sphere, dome, or urban
shape = 'urban';

cube = { [
    -1 -1 -1 1
    1 -1 -1 1
    1 -1 1 1
    -1 -1 1 1 ]'
    [
    -1 1 -1 1
    1 1 -1 1
    1 1 1 1
    -1 1 1 1 ]'
    [
    -1 -1 -1 1
    -1 1 -1 1
    -1 1 1 1
    -1 -1 1 1 ]'
    [
    1 -1 -1 1
    1 1 -1 1
    1 1 1 1
    1 -1 1 1 ]'
    [
    -1 -1 -1 1
    1 -1 -1 1
    1 1 -1 1
    -1 1 -1 1 ]'
    [
    -1 -1 1 1
    1 -1 1 1
    1 1 1 1
    -1 1 1 1 ]'
    };

rectangle = { [
    -1 0 -1 1
    1 0 -1 1 ]'
    [
    1 0 -1 1 
    1 0 1 1 ]'
    [
    1 0 1 1
    -1 0 1 1 ]'
    [
    -1 0 1 1
    -1 0 -1 1 ]'
    };

rectface = { [
    -1 0 -1 1
    1 0 -1 1
    1 0 1 1
    -1 0 1 1 ]'
    };

cambox = { [
    0 3 0 1
    -3 8 3 1 ]'
    [
    0 3 0 1
    3 8 3 1 ]'
    [
    0 3 0 1
    3 8 -3 1 ]'
    [
    0 3 0 1
    -3 8 -3 1 ]'
    };

car = [
    transobj(trans([0 0 -3])*scale([1.5, 3, 1]), cube)
    transobj(trans([0 2 0])*scale([.5, 1, .5]), cube) ];

screen = [
    transobj(trans([0 8 0])*scale([3 1 3]), rectangle)
    cambox ];
screenface = [
    transobj(trans([0 8 0])*scale([3 1 3]), rectface) ];

[ sphx sphy sphz ] = sphere(100);
sphx = sphx * 100;
sphy = sphy * 100;
sphz = sphz * 100;

[ plax play ] = meshgrid(-100:100);
plaz = zeros(size(plax));

[ wally wallz ] = meshgrid(-100:100, -4.9:5.1);
wallx = zeros(size(wally));


if strcmp(shape, 'sphere')
    rots = zeros(100, 3);
    tran = zeros(100, 3);
    for i = 1:50
        rots(i,:) = [ 0 0 -pi/200 * sin(i*pi/50) ];
    end
    for i = 50:100
        rots(i,:) = [ 0 0 pi/200 * sin((i-50)*pi/50) ];
    end
    for i = 1:50
        rots(i,:) = [ pi/200 * sin(i*pi/50) 0 0 ];
    end
    for i = 51:100
        rots(i,:) = [ -pi/200 * sin((i-50)*pi/50) 0 0 ];
    end
    for i = 1:50
        rots(i,:) = [ 0 -pi/200 * sin(i*pi/50) 0 ];
    end
    for i = 51:100
        rots(i,:) = [ 0 pi/200 * sin((i-50)*pi/50) 0 ];
    end
    for i = 1:300
        rots(i,:) = [ pi/300 * cos(2*i*pi/300) -pi/300 * sin(2*i*pi/300) -pi/300 * sin(2*i*pi/300) ];
    end
end
if strcmp(shape, 'dome')
    rots = zeros(100, 3);
    tran = zeros(100, 3);
    for i = 1:100
        sca = sin(i*pi/100);
        tran(i,:) = [ sca*.07*sin(i*pi/400) sca*.07*cos(i*pi/400) 0 ];
        rots(i,:) = [ 0 0 -sca*pi/4/100 ];
    end
end
if strcmp(shape, 'urban')
    rots = zeros(100, 3);
    tran = zeros(100, 3);
    for i = 1:100
        sca = sin(i*pi/100);
        tran(i,:) = [ 0 sca*.15 0 ];
    end
end



ppcaresI = struct;
if strcmp(shape, 'sphere')
    ppcaresI.W = [ sampfromflow(makeflowsphere(rotz(pi/300))) sampfromflow(makeflowsphere(rotx(pi/300))) sampfromflow(makeflowsphere(roty(pi/300))) ];
end
if strcmp(shape, 'dome')
    ppcaresI.W = [ sampfromflow(makeflowdome(rotz(pi/300))) sampfromflow(makeflowdome(trans([0 .07 0]))) sampfromflow(makeflowdome(trans([.07 0 0]))) ];
end
if strcmp(shape, 'urban')
    ppcaresI.W = sampfromflow(makeflowdome(trans([0 0 .07])));
end
q = size(ppcaresI.W, 2);
ppcaresI.Ex = zeros(q, 1);
ppcaresI.mu = zeros(size(ppcaresI.W, 1), 1);
ppcaresI.sig2v = 1;
ppcaresI.sig2f = 100;


mat = eye(4);
rot = [ 0 0 0 ];
tra = [ 0 0 0 ];

mainfig = figure;
basefig = figure;

for n = 1:length(rots)

    set(0, 'CurrentFigure', mainfig);
    clf
    mesh(sphx, sphy, sphz);
    hold on;
    axis vis3d
    if strcmp(shape, 'dome') || strcmp(shape, 'urban')
        mesh(plax, play, plaz - 5);
    end
    if strcmp(shape, 'urban')
        mesh(wallx - 7, wally, wallz);
        mesh(wallx + 7, wally, wallz);
    end

    
    oldmat = mat;
    rot = rot + rots(n,:);
    tra = tra + tran(n,:);
    mat = trans(tra) * rotx(rot(1)) * roty(rot(2)) * rotz(rot(3));
    
    lookat = mat * [ 0 6 0 1 ]';

    camproj('perspective');
    set(gca, 'CameraPosition', [ -5 -5 + tra(2)*.8 3 ]);
    %set(gca, 'CameraPosition', [ 0 0 0 ]);
    set(gca, 'CameraTarget', lookat(1:3)');
    set(gca, 'CameraViewAngle', 40);
    %set(gcf, 'RendererMode', 'manual');
    set(gcf, 'Renderer', 'zbuffer');

    for i = 1:length(car)
        face = mat*car{i};
        fill3(face(1,:), face(2,:), face(3,:), 'Blue');
    end
    for i = 1:length(screenface)
        face = mat*screenface{i};
        fill3(face(1,:), face(2,:), face(3,:), 'White');
    end
    for i = 1:length(screen)
        segment = mat*screen{i};
        line(segment(1,:), segment(2,:), segment(3,:), 'Color', 'Black');
    end

    if strcmp(shape, 'sphere')
        flow = makeflowsphere((mat / oldmat)^3);
    end
    if strcmp(shape, 'dome')
        flow = makeflowdome((mat / oldmat)^3);
    end
    if strcmp(shape, 'urban')
        flow = makeflowurban((mat / oldmat)^3);
    end
    flowsamp = sampfromflow(flow);
    est = gmppca.gmestimate(ppcaresI, flowsamp);
    flow = transobj(trans([0 8 0])*scale([3 1 3]), flow);
    for i = 1:length(flow)
        segment = mat*flow{i};
        line(segment(1,:), segment(2,:), segment(3,:), 'Color', 'Black');
    end
    
    drawnow;
    
%     saveas(gcf, sprintf('comb/%04d.png', n));
    
    set(0, 'CurrentFigure', basefig);
    clf;
    axis vis3d
    camproj('perspective');
    set(gca, 'CameraPosition', [ 0 -8 0 ]);
    %set(gca, 'CameraPosition', [ 0 0 0 ]);
    set(gca, 'CameraTarget', [ 0 5 0 ]);
    set(gca, 'CameraViewAngle', 30);
    
    line([-10 -10], [10 10], [-10 -10]);
    
    if strcmp(shape, 'sphere')
        flow = makeflowsphere(rotz(pi/25*est.Ex(1)));
    end
    if strcmp(shape, 'dome')
        flow = makeflowdome(rotz(pi/25*est.Ex(1)));
    end
    if strcmp(shape, 'urban')
        flow = makeflowurban(trans([0 0 .84]*est.Ex(1)));
    end
    flow = transobj(trans([-1.25 0 1.25]), flow);
    for i = 1:length(flow)
        segment = flow{i};
        line(segment(1,:), segment(2,:), segment(3,:), 'Color', 'Black');
    end

    if ~strcmp(shape, 'urban')
        if strcmp(shape, 'sphere')
            flow = makeflowsphere(rotx(pi/25*est.Ex(2)));
        end
        if strcmp(shape, 'dome')
            flow = makeflowdome(trans([0 .84 0]*est.Ex(2)));
        end
        flow = transobj(trans([-1.25 0 -1.25]), flow);
        for i = 1:length(flow)
            segment = flow{i};
            line(segment(1,:), segment(2,:), segment(3,:), 'Color', 'Black');
        end
        
        if strcmp(shape, 'sphere')
            flow = makeflowsphere(roty(pi/25*est.Ex(3)));
        end
        if strcmp(shape, 'dome')
            flow = makeflowdome(trans([.84 0 0]*est.Ex(3)));
        end
        flow = transobj(trans([1.25 0 0]), flow);
        for i = 1:length(flow)
            segment = flow{i};
            line(segment(1,:), segment(2,:), segment(3,:), 'Color', 'Black');
        end
    end

    %saveas(gcf, sprintf('planbase/%04d.png', n));

    
    %print('-r300', '-dpng', sprintf('%04d.png', n));

end
