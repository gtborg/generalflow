function samp = sampfromflow(flow)

samp = zeros(numel(flow)*2, 1);

for i = 1:numel(flow)
    samp(i) = flow{i}(1,2) - flow{i}(1,1);
    samp(numel(flow) + i) = flow{i}(3,2) - flow{i}(3,1);
end

end
