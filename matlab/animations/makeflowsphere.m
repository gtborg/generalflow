function flow = makeflowsphere(rot)

step = .1;

scale = 1;

flow = cell((2/step+1) * (2/step+1), 1);

i = 1;
for x = -1:step:1
    for y = -1:step:1
        P = [ x; 1; y; 0 ];
        Pd = inv(rot) * P;
        dx = Pd(1)/Pd(2) - x;
        dy = Pd(3)/Pd(2) - y;
        flow{i} = [ x 0 y 1; x+scale*dx 0 y+scale*dy 1 ]';
        i = i + 1;
    end
end

end
