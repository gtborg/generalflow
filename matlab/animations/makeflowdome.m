function flow = makeflowdome(mat)

step = .1;

scale = 1;

flow = cell((2/step+1) * (2/step+1), 1);

i = 1;
for x = -1:step:1
    for y = -1:step:1
        if y >= 0
            P = [ x; 1; y; 0 ];
        else
            p = planeline([0;0;0], [x;1;y], [0;0;-3], [0;0;1]);
            P = [ p(1); p(2); p(3); 1 ];
        end
        Pd = inv(mat) * P;
        dx = Pd(1)/Pd(2) - x;
        dy = Pd(3)/Pd(2) - y;
        flow{i} = [ x 0 y 1; x+scale*dx 0 y+scale*dy 1 ]';
        i = i + 1;
    end
end

end
