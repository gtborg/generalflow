function is = planeline(l1, l2, p, n)
% finds the intersection of a plane and a line

u = dot(n, p - l2) / dot(n, l2 - l1);
is = l1 + u*(l2-l1);

end
