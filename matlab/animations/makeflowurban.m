function flow = makeflowurban(mat)

step = .1;

scale = 1;

flow = cell((2/step+1) * (2/step+1), 1);

left = -7;
right = 7;
wallheight = 5;
ground = -5;

i = 1;
for x = -1:step:1
    for y = -1:step:1
        pground = planeline([0;0;0], [x;1;y], [0;0;ground], [0;0;1]);
        pleft = planeline([0;0;0], [x;1;y], [left;0;0], [1;0;0]);
        pright = planeline([0;0;0], [x;1;y], [right;0;0], [1;0;0]);
        if all(isfinite(pground)) && pground(1) > left && pground(1) < right && pground(2) > 0
            P = [ pground(1); pground(2); pground(3); 1 ];
        elseif all(isfinite(pleft)) && pleft(3) > ground && pleft(3) < wallheight && pleft(2) > 0
            P = [ pleft(1); pleft(2); pleft(3); 1 ];
        elseif all(isfinite(pright)) && pright(3) > ground && pright(3) < wallheight && pright(2) > 0
            P = [ pright(1); pright(2); pright(3); 1 ];
        else
            P = [ x; 1; y; 0 ];
        end
        Pd = inv(mat) * P;
        dx = Pd(1)/Pd(2) - x;
        dy = Pd(3)/Pd(2) - y;
        flow{i} = [ x 0 y 1; x+scale*dx 0 y+scale*dy 1 ]';
        i = i + 1;
    end
end

end
