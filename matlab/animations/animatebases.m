function animatebases(ppcares, width)

estalls = cell(size(ppcares.W, 2), 1);
for i = 1:size(ppcares.W, 2)
    estalls{i} = struct();
    estalls{i}.t = zeros(size(ppcares.W, 1), size(ppcares.Ex, 2));
end


for j = 1:size(ppcares.W, 2)
    estalls{j}.t = ppcares.W(:,j) * ppcares.Ex(j, :);
    annotate('', [ sprintf('B%d', j) '/%04d.png' ], sprintf('B%d.sh', j), ppcares, estalls{j}, [], width, 'dense');
end
