function [ factors, fgBasis, fgLatent, fgSigmas, ...
    basisValues, latentValues, sigmaValues ] = createAllLearningFactors( ...
    measurements, firstFrames, lastFrames, gridWidth, gridHeight, q, ...
    inlierPrior, ys, initialS, motionSigma, flowSmoothSigma)
%CREATEALLLEARNINGFACTORS Creates all factors and initializations
%needed for optimizing for a basis.

frameInterval = 1;

% Create initializations
fprintf('  Random basis initialization...\n');
basisValues = basisAlignment.createBasisInitialization(gridWidth, gridHeight, q);
sigmaValues = gtsam.Values;
sigmaValues.insert(gtsam.symbol('S',0), gtsam.LieVector(initialS'));

% Add smoothness field
fprintf('  Basis flow smoothness...\n');
factors.smoothnessFactors = basisAlignment.createSmoothnessFactors(gridWidth, gridHeight, q, flowSmoothSigma);

% Add basis priors
fprintf('  Basis flow priors...\n');
basisPriorNoise = gtsam.noiseModel.Isotropic.Sigma(q*2, 1.0);
factors.basisPriorFactors = gtsam.NonlinearFactorGraph;
for i = 0:(gridWidth-1)
    for j = 0:(gridHeight-1)
        pix = i*gridHeight + j;
        factors.basisPriorFactors.add(gtsam.PriorFactorLieMatrix( ...
            gtsam.symbol('V',pix), gtsam.LieMatrix(zeros(2,q)), basisPriorNoise));
    end
end

% Latent variable smoothness factors
fprintf('  Latent variable smoothness factors...\n');
factors.motionFactors = basisAlignment.createMotionFactors(firstFrames, lastFrames, frameInterval, q, motionSigma);

% Latent variable priors
fprintf('  Latent variable priors...\n');
[ factors.latentPriorFactors, latentValues ] = basisAlignment.createLatentPriorFactors(firstFrames, lastFrames, frameInterval, q, ys);

% Add constraints to prevent sigmas from going near or less than zero
factors.sigmaPriorFactors = gtsam.NonlinearFactorGraph;
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 0, 0.005, true));
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 1, 0.005, true));

% Sigma priors
factors.sigmaPriorFactors.add(gtsam.PriorFactorLieVector( ...
    gtsam.symbol('S',0), gtsam.LieVector(initialS'), ...
    gtsam.noiseModel.Diagonal.Precisions([1e3 1e3]')));

fprintf('  Flow factors...\n');
[ factors.basisFactors, factors.latentFactors, factors.sigmaFactors ] = ...
    learningSparseFlow.createFlowFactors(measurements, ...
    firstFrames, lastFrames, frameInterval, inlierPrior, ...
    basisValues, latentValues, sigmaValues);

% Three factor graphs
fgBasis = gtsam.NonlinearFactorGraph;
fgLatent = gtsam.NonlinearFactorGraph;
fgSigmas = gtsam.NonlinearFactorGraph;

fgBasis.push_back(factors.smoothnessFactors);
% fgBasis.push_back(factors.basisPriorFactors);
fgSigmas.push_back(factors.sigmaPriorFactors);

for imgSet = 1:numel(firstFrames)
    fgBasis.push_back(factors.basisFactors{imgSet});
    fgLatent.push_back(factors.latentFactors{imgSet});
    fgLatent.push_back(factors.latentPriorFactors{imgSet});
    fgLatent.push_back(factors.motionFactors{imgSet});
    fgSigmas.push_back(factors.sigmaFactors{imgSet});
end

end

