% End-to-end test for sparse flow learning

import basisflow.*

% Create expected basis flows
V1u = [
    -1 1
    -1 1
    ];
V1v = [
    -1 -1
    1 1
    ];
V2u = [
    1 1
    1 1
    ];
V2v = [
    0 0
    0 0
    ];

Vexpected = [
    reshape([reshape(V1u,4,1) reshape(V1v,4,1)]', 8,1) ...
    reshape([reshape(V2u,4,1) reshape(V2v,4,1)]', 8,1)
    ];

% Simulate flow measurements
% simulatedV = [
%     0 1 1   1   0.8
%     0 0 0.1 0.2 0.3
%     ];
simulatedV = zeros(2,100);
for t = 2:100
    simulatedV(:,t) = [ cos(t/20 * 2*pi); sin(t/20 * 2*pi) ];
end

measurements = { { cell(1, size(simulatedV,2)) } };
outliers = 0;
missing = 0;
for t = 2:size(simulatedV,2)
    measurements{1}{t} = SparseFlowField(2, 2, 2, 2);
    u = Vexpected * simulatedV(:,t);
    uu = reshape(u(1:2:end), 2,2);
    uv = reshape(u(2:2:end), 2,2);
    if mod(t, 5) == 0
        uu(1,1) = -5; % Outlier
        outliers = outliers + 1;
    end
    rand2 = round(rand(1,2));
    for i = 0:1
        for j = 0:1
            if ~mod(t,2) == 1 || i ~= rand2(1) || j ~= rand2(2)
                measurements{1}{t}.push_back(SparseFlowFieldMeasurement( ...
                    j, i, uu(i+1, j+1), uv(i+1, j+1)));
            else
                missing = missing + 1;
            end
        end
    end
end
fprintf('%d (of %d) outliers\n', outliers, 4*(size(simulatedV,2)-1));
fprintf('%d missing\n', missing);

% Parameters
firstFrames = [ 1 ];
lastFrames = [ size(simulatedV,2) ];
gridWidth = size(V1v,2);
gridHeight = size(V1v,1);
q = size(Vexpected, 2);
inlierPrior = 0.9;
ys = { simulatedV };
initialS = [ 0.01 50.0 ];
motionSigma = 100000.0;
flowSmoothSigma = 100000.0;

% Solve
[ factors, fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit ] = ...
    learningSparseFlow.createAllLearningFactors( ...
    measurements, firstFrames, lastFrames, gridWidth, gridHeight, q, ...
    inlierPrior, ys, initialS, motionSigma, flowSmoothSigma);

[ result, detail ] = basisAlignment.optimizeAllGraphs(...
    fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit, ...
    gridWidth, gridHeight, inlierPrior);

inspector.colorTest;


