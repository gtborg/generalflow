function [ basisFactors, latentFactors, sigmaFactors ] = createFlowFactors( ...
    measurements, firstFrameNums, lastFrameNums, ...
    frameInterval, inlierPrior, basisValues, latentValues, sigmaValues)
%CREATEIMAGEFACTORS Creates basis, latent variable, and sigma factors for
%all frames.

import gtsam.*

for imgSet = 1:numel(firstFrameNums)

    % Create basis and sigma factors (on all frames and all pixels)

    basisFactors{imgSet} = gtsam.NonlinearFactorGraph;
    latentFactors{imgSet} = gtsam.NonlinearFactorGraph;
    sigmaFactors{imgSet} = gtsam.NonlinearFactorGraph;

    for frame = (firstFrameNums(imgSet)+frameInterval):frameInterval:lastFrameNums(imgSet)
        fprintf('Building fg frame %d\n', frame);

        % Basis flow flow factors
        basisFactors{imgSet}.push_back(basisflow.OnlyBasisFlowFactor.CreateWholeFrame( ...
            measurements{imgSet}{frame}, gtsam.symbol('V',0), generalflow.vKey('y',imgSet,frame), gtsam.symbol('S',0), ...
            latentValues, sigmaValues, inlierPrior));
    
        % Sigma flow factors
        % sigmaFactors.add( ...
        %     basisflow.OnlySigmaImageFactor(gtsam.symbol('V',0), generalflow.vKey('y',imgSet,0), gtsam.symbol('S',0), ...
        %     basisValues, latentValues, inlierPrior, frameNums, frameCache));

        % Latent variable flow factors
        latentFactors{imgSet}.push_back(basisflow.OnlyLatentFlowFactor.CreateWholeFrame( ...
            measurements{imgSet}{frame}, gtsam.symbol('V',0), generalflow.vKey('y',imgSet,frame), gtsam.symbol('S',0), ...
            basisValues, sigmaValues, inlierPrior));
    end
end

end

