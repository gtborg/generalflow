function [measurements,superpixelLabels] = extractFlow(imgFormats, firstFrames, lastFrames, gridWidth, gridHeight, minEigThreshold, prescale, calibration)
%EXTRACTFLOW Extract flow for several image directories

measurements = cell(1, numel(imgFormats));

for k = 1:numel(imgFormats)
    
    if ischar(imgFormats{k})
        imagefile = @(framenum) sprintf(imgFormats{k}, framenum);
    else
        imagefile = imgFormats{k};
    end
    
    % Preallocate measurements array
    measurements{k} = cell(1, lastFrames(k));
    
    % Load first frame (0 flag makes greyscale)
    prevFrame = cv.imread(imagefile(firstFrames(k)));
    tmp = imread(imagefile(firstFrames(k)));
    imgWidth = size(tmp, 2);
    imgHeight = size(tmp, 1);
    
    % Loop over frames
    for i = firstFrames(k) + 1 : lastFrames(k)
        if mod(i,100) == 0
            fprintf('Computing flow, frame %d\n', i);
        end
        
        % Load current frame (0 flag makes greyscale)
        curFrame = cv.imread(imagefile(i));
        
        % Compute flow
        % Prescale image
        if prescale ~= 1.0
            if ~exist('prevFrameScaled', 'var')
                prevFrameScaled = cv.Mat;
                cv.resize(prevFrame, prevFrameScaled, cv.Size, prescale, prescale);
            end
            curFrameScaled = cv.Mat;
            cv.resize(curFrame, curFrameScaled, cv.Size, prescale, prescale);
            if exist('calibrationScaled', 'var') && ~isempty(calibrationScaled)
                distCoeffs = calibration.k;
                calibrationScaled = gtsam.Cal3DS2( ...
                    calibration.fx * prescale, calibration.fy * prescale, ...
                    calibration.skew, calibration.px * prescale, calibration.py * prescale, ...
                    distCoeffs(1), distCoeffs(2), distCoeffs(3), distCoeffs(4));
            end
        else
            prevFrameScaled = prevFrame;
            curFrameScaled = curFrame;
        end
        
        % TVL1:  tau, lambda, theta, nscales, warps, epsilon, iterations
        tic;
        tau = 0.10;
        lambda = 0.10;
        theta = 0.3;
        epsilon = 0.01;
        nscales = 8;
        nwarps = 8;
        iterations = 15;
        useSuperpixels = true;
        if exist('calibrationScaled', 'var') && ~isempty(calibrationScaled)
            measurements{k}{i} = generalflow.flow.computeFlowTVL1( ...
                prevFrameScaled, curFrameScaled, gridWidth, gridHeight, useSuperpixels, true, ...
                tau, lambda, theta, nscales, nwarps, epsilon, iterations, ...
                calibrationScaled);
        else
            measurements{k}{i} = generalflow.flow.computeFlowTVL1( ...
                prevFrameScaled, curFrameScaled, gridWidth, gridHeight, useSuperpixels, true, ...
                tau, lambda, theta, nscales, nwarps, epsilon, iterations);
        end
        toc;
        if useSuperpixels
            superpixelLabels{k}{i} = generalflow.flow.getLastSuperpixelLabels;
        else
            superpixelLabels{k}{i} = [];
        end
        
        % Shift frame
        prevFrame = curFrame;
        prevFrameScaled = curFrameScaled;
    end
end

end

