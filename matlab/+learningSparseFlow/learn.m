function result = learn(imgDirs, q, inlierPrior, ys, trainingRanges, measurements)
%JOINTOPTIMIZE Optimize for a basis from one or more image sequences

import gtsam.*

initialS = [ 2.0 35.0 ];
motionSigma = 1000000.0;
flowSmoothSigma = 1.0;

% Get image filename format
fprintf('Finding images...\n');
for i = 1:numel(imgDirs)
    [ imgFormats{i}, firstFrames(i), lastFrames(i) ] = ...
        dataio.detectImageSequenceFormat(imgDirs{i});
    if exist('trainingRanges', 'var') && ~isempty(trainingRanges{i})
        firstFrames(i) = trainingRanges{i}(1);
        lastFrames(i) = trainingRanges{i}(2);
    end
end

% Get image size
img = imread(sprintf(imgFormats{1}, firstFrames(1)));
imWidth = size(img,2);
imHeight = size(img,1);
gridWidth = round(imWidth / 20);
gridHeight = round(imHeight / 20);

if ~exist('ys', 'var')
    ys = [];
end

% Extract flow
if ~exist('measurements', 'var') || isempty(measurements)
    measurements = learningSparseFlow.extractFlow( ...
        imgFormats, firstFrames, lastFrames, gridWidth, gridHeight, 1e-3);
    assignin('base', 'measurements', measurements);
end

% Create factor graphs
fprintf('Building graph...\n');
[ factors, fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit ] = ...
    learningSparseFlow.createAllLearningFactors( ...
    measurements, firstFrames, lastFrames, gridWidth, gridHeight, q, ...
    inlierPrior, ys, initialS, motionSigma, flowSmoothSigma);

[ result, detail ] = basisAlignment.optimizeAllGraphs(...
    fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit, ...
    gridWidth, gridHeight, inlierPrior);
result.detail = detail;

inspector.colorTest;

% Convert to old format
flow = [];
for i = 1:numel(imgDirs)
    flowi = inf(gridWidth*gridHeight*2, lastFrames(i) - firstFrames(i) + 1);
    for t = (firstFrames(i)+1):lastFrames(i)
        flowi(:,t-firstFrames(i)+1) = reshape(measurements{i}{t}.asPairedField(), gridWidth*gridHeight*2, 1);
    end
    flow = [ flow flowi ];
end
opts = gmppca.gmppca2_opts;
opts.updateSigv = 1;
opts.inlierPrior = inlierPrior;
ppcares = gmppca.gmppca2(flow, q, opts);
ppcares.basissize = [ size(ppcares.W,2) 2 size(ppcares.W,1)/2 gridWidth gridHeight ];

inspector.drawBasis(ppcares);

result.gmppca = ppcares;

end

