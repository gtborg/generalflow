function latent = inference(imgDir, subspace, frameRange)
%INFERENCE Infer velocities

import gtsam.*
import basisflow.*

[ imgFormat, firstFrame, lastFrame ] = ...
    dataio.detectImageSequenceFormat(imgDir);
if exist('frameRange', 'var') && ~isempty(frameRange)
    firstFrame = frameRange(1);
    lastFrame = frameRange(2);
end

% Get image size
img = imread(sprintf(imgFormat, firstFrame));
imWidth = size(img,2);
imHeight = size(img,1);
gridWidth = subspace.width;
gridHeight = subspace.height;

inlierPrior = subspace.inlierPrior;

% Create values
basisValues = dataio.valuesFromSubspace(subspace);
latentValues = Values;
latentValues.insert(symbol('y',0), LieVector(zeros(subspace.dimLatent, 1)));
sigmaValues = Values;
sigmaValues.insert(symbol('S',0), LieVector([sqrt(subspace.inlierSigma2); sqrt(subspace.outlierSigma2)]));

% Loop over frames
latent = zeros(subspace.dimLatent, frameRange(2));

% Load first frame (0 flag makes greyscale)
prevFrame = cv.imread(sprintf(imgFormat, firstFrame), 0);

for i = firstFrame + 1 : lastFrame

    fprintf('Frame %d\n', i);

    % Load current frame (0 flag makes greyscale)
    curFrame = cv.imread(sprintf(imgFormat, i), 0);
    
    % Compute flow
    measurement = basisflow.sparseFlow.computeFlow( ...
        prevFrame, curFrame, gridWidth, gridHeight, 1e-3);
    
    % Create factors
    graph = NonlinearFactorGraph;
    graph.push_back(basisflow.OnlyLatentFlowFactor.CreateWholeFrame( ...
        measurement, symbol('V',0), symbol('y',0), symbol('S',0), ...
        basisValues, sigmaValues, inlierPrior));
    
    % Optimize
    params = gtsam.GaussNewtonParams;
    params.setVerbosity('ERROR');
    params.setLinearSolverType('MULTIFRONTAL_QR');
    optLatent = gtsam.GaussNewtonOptimizer(graph, latentValues, params);
    optLatent.optimize;
    latentValues.update(optLatent.values);
    
    latentValues.at(symbol('y',0)).disp;
    latent(:,i) = latentValues.at(symbol('y',0)).vector;
    
    % Shift frame
    prevFrame = curFrame;
end

end

