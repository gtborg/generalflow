function [ subspace, latent ] = subspaceFromValues(values, firstKey, inlierPrior, imWidth, imHeight)
%PPCARESFROMVALUES Summary of this function goes here
%   Detailed explanation goes here

q = size(values.at(firstKey).matrix, 2);

% Basis
W = zeros(2*imHeight*imWidth, q);
for pix=0:(imHeight*imWidth-1)
    W(1+2*pix : 1+2*pix+1, :) = values.at(firstKey + pix).matrix;
end

if values.exists(gtsam.symbol('S', 0));
    sigmas = values.at(gtsam.symbol('S', 0)).vector;
else
    sigmas = [0;0];
end

if numel(sigmas) == 4
    subspace = basisflow.Subspace(W, sigmas(1)^2, sigmas(2)^2, sigmas(3)^2, sigmas(4)^2, ...
        inlierPrior, imWidth, imHeight);
elseif numel(sigmas) == 2
    subspace = basisflow.Subspace(W, sigmas(1)^2, sigmas(2)^2, ...
        inlierPrior, imWidth, imHeight);
else
    error('Incorrect number of elements in sigmas vector');
end

% Latent variables
keys = gtsam.KeyVector(values.keys);

% Find highest y index
lasty = [];
lastImgSet = 0;
for i=0:(keys.size-1)
    key = keys.at(i);
    if generalflow.vKeyChr(key) == 'y'
        imgSet = generalflow.vKeyImgSet(key);
        if imgSet > lastImgSet
            lastImgSet = imgSet;
            lasty(imgSet) = 0;
        end
        if generalflow.vKeyFrameNum(key) > lasty(imgSet)
            lasty(imgSet) = generalflow.vKeyFrameNum(key);
        end
    end
end

latent = cell(1,lastImgSet);
for i=1:lastImgSet
    latent{i} = Inf(q,lasty(i));
end

for i=0:(keys.size-1)
    key = keys.at(i);
    if generalflow.vKeyChr(key) == 'y'
        latent{generalflow.vKeyImgSet(key)}(:,generalflow.vKeyFrameNum(key)) = values.at(key).vector;
    end
end

end

