function subspace = subspaceFromPpcares(ppcares)
%SUBSPACEFROMPPCARES Convert a ppcares (MATLAB object) to a Subspace (C++
%object)

if isfield(ppcares, 'sig2vi')
    subspace = basisflow.Subspace(ppcares.W, ppcares.sig2v, ppcares.sig2f, ...
        ppcares.sig2vi, ppcares.sig2fi, ppcares.inlierPrior, ...
        ppcares.basissize(4), ppcares.basissize(5));
else
    subspace = basisflow.Subspace(ppcares.W, ppcares.sig2v, ppcares.sig2f, ...
        ppcares.inlierPrior, ppcares.basissize(4), ppcares.basissize(5));
end

end

