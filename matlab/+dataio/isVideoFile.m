function isVideo = isVideoFile(filename)
%ISVIDEOFILE Summary of this function goes here
%   Detailed explanation goes here

video_extensions = { '.mov', '.avi', '.mp4', '.m4v', '.mj2', '.mpg', '.wmv', '.asf', '.asx' };

% Split filename
[ ~, ~, extension ] = fileparts(filename);

% Check extension
isVideo = ~isempty(find(cellfun(@(vidext) strcmpi(vidext, extension), video_extensions), 1));

end

