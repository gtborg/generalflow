function values = valuesFromSubspace(subspace, firstKey)
%VALUESFROMSUBSPACE Summary of this function goes here
%   Detailed explanation goes here

if ~exist('firstKey', 'var') || isempty(firstKey)
    firstKey = gtsam.symbol('V',0);
end

values = gtsam.Values;

W = subspace.basis;
for pix=0:(subspace.width*subspace.height-1)
    values.insert(firstKey + pix, gtsam.LieMatrix(W(1+2*pix : 1+2*pix+1, :)));
end

end

