function calibrations = readOpenCVcalibration(intrinsics_yml)
%READOPENCVCALIBRATION Read an OpenCV intrinsics file in intrinsics_yml and
%  output an array of either gtsam.Cal3_S2 or gtsam.Cal3DS2, depending on
%  whether radial distortion parameters are present in the file.  An OpenCV
%  intrinsics file may contain multiple camera calibrations, thus the
%  returned calibrations are an array of them.

    function rest = match(line, start)
        % Match start to the beginning of line, ignoring leading whitespace.
        % Returns the rest of the line, or zero if the start could not be
        % matched.
        [~,pos] = textscan(line, '%*[ \t]');
        lineRest = line(pos+1:end);
        if numel(lineRest) >= numel(start) && strcmp(lineRest(1:numel(start)), start)
            rest = lineRest(numel(start)+1:end);
        else
            rest = 0;
        end
    end

try
    
    Ms = {};
    Ds = {};
    
    state = 'start';
    mid = 1;
    fid = fopen(intrinsics_yml, 'rt');
    line = fgetl(fid);
    while ischar(line);
        if strcmp(state, 'start')
            % Look for YAML header
            rest = match(line, '%YAML:');
            if ischar(rest)
                state = 'matrix';
            end
        elseif strcmp(state, 'matrix')
            % Look for matrix start
            whichmatrix = '';
            rest1 = match(line, 'M');
            if ischar(rest1)
                whichmatrix = 'M';
                rest = rest1;
            end
            rest2 = match(line, 'D');
            if ischar(rest2)
                whichmatrix = 'D';
                rest = rest2;
            end
            if ~isempty(whichmatrix)
                % Read matrix
                mid = str2double(rest(1));
                state = 'rows';
            end
        elseif strcmp(state, 'rows')
            rest = match(line, 'rows: ');
            if ischar(rest)
                rows = str2double(rest);
                state = 'cols';
            end
        elseif strcmp(state, 'cols')
            rest = match(line, 'cols: ');
            if ischar(rest)
                cols = str2double(rest);
                state = 'data';
            end
        elseif strcmp(state, 'data')
            rest = match(line, 'data: ');
            if ischar(rest)
                datastr = rest(2:end); % Remove first [
                if datastr(end) == ']'
                    datastr = datastr(1:end-1); % Remove last ]
                    state = 'matrix'; % Done, look for next matrix
                else
                    state = 'data_continued'; % Multi-line matrix
                end
            end
        elseif strcmp(state, 'data_continued')
            datastr = [ datastr, ' ', line ];
            if datastr(end) == ']'
                datastr = datastr(1:end-1); % Remove last ]
                % Finish matrix
                data = str2num(datastr);
                data = reshape(data, cols, rows)'; % Simultaneously convert row-major
                if whichmatrix == 'M'
                    Ms{mid} = data;
                elseif whichmatrix == 'D'
                    Ds{mid} = data;
                end
                state = 'matrix'; % Done, look for next matrix
            end
        end
        
        % Next line
        line = fgetl(fid);
    end
    
    % Convert to calibrations
    for i = 1:numel(Ms)
        if ~isempty(Ms{i})
            K = Ms{i};
            if numel(Ds) >= i && ~isempty(Ds{i})
                k = Ds{i};
                calibrations{i} = gtsam.Cal3DS2( ...
                    K(1,1), K(2,2), K(1,2), K(1,3), K(2,3), ...
                    k(1), k(2), k(3), k(4));
            else
                calibrations{i} = gtsam.Cal3DS2( ...
                    K(1,1), K(2,2), K(1,2), K(1,3), K(2,3));
            end
        end
    end
    
catch e
    if fid ~= -1
        fclose(fid);
    end
    rethrow(e);
end

if fid ~= -1
    fclose(fid);
end

end

