function ppcares = read_ppcares(filename)
%READ_PPCARES Summary of this function goes here
%   Detailed explanation goes here

ppcares.W = dlmread([filename '.W']);
sigmas = dlmread([filename '.sigmas']);
ppcares.sig2v = sigmas(1);
ppcares.sig2f = sigmas(2);
ppcares.sig2vi = sigmas(3);
ppcares.sig2fi = sigmas(4);
if exist([filename '.latent'], 'file')
    ppcares.Ex = dlmread([filename '.latent'])';
end
ppcares.basissize = dlmread([filename '.basissize']);

end

