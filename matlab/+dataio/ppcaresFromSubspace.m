function ppcares = ppcaresFromSubspace(subspace)
%PPCARESFROMSUBSPACE Convert a Subspace (C++ object) to a ppcares (MATLAB
%object)

ppcares.W = subspace.basis;
ppcares.sig2v = subspace.inlierSigma2;
ppcares.sig2f = subspace.outlierSigma2;
ppcares.sig2vi = subspace.inlierPixSigma2;
ppcares.sig2fi = subspace.outlierPixSigma2;
ppcares.inlierPrior = subspace.inlierPrior;
ppcares.basissize = [ size(ppcares.W,2) 2 size(ppcares.W,1)/2 subspace.width subspace.height ];

end

