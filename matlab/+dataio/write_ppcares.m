function write_ppcares(ppcares, filename)

%q = size(ppcares.W, 2);
%w = fdims(2);
%h = fdims(1);
%Wshaped = reshape(ppcares.W, [w*h 2 q]);
%Wshaped = permute(Wshaped, [3 2 1]);
%Wshaped = reshape(Wshaped, w*h*2*q, 1);
%Wshaped = reshape(ppcares.W', w*h*2*q, 1);

%mushaped = reshape(ppcares.mu, [w*h 2]);
%mushaped = permute(mushaped, [2 1]);
%mushaped = reshape(mushaped, w*h*2, 1);
%mushaped = ppcares.mu;

dlmwrite([filename '.W'], ppcares.W, 'Precision', 15, 'Delimiter', ' ');
dlmwrite([filename '.sigmas'], [ppcares.sig2v ppcares.sig2f ppcares.sig2vi ppcares.sig2fi], 'Precision', 15, 'Delimiter', ' ');
if isfield(ppcares, 'Ex')
    dlmwrite([filename '.latent'], ppcares.Ex', 'Precision', 15, 'Delimiter', ' ');
end
if isfield(ppcares, 'Ez')
    dlmwrite([filename '.inliers'], ppcares.Ez', 'Precision', 15, 'Delimiter', ' ');
end

%dlmwrite([filename '.basissize'], ...
%    [ size(ppcares.W,2) 2 size(ppcares.W,1)/2 ], ' ');
dlmwrite([filename '.basissize'], int64(ppcares.basissize), 'Precision', 15, 'Delimiter', ' ');

end

