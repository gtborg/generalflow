function [ flow ] = concat_flow(flow1, flow2)

flow = struct('flow_vec', [ flow1.flow_vec flow2.flow_vec ], ...
    'flow_mask', [ flow1.flow_mask flow2.flow_mask ]);

end
