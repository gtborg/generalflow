function [flow,fdims] = rawflowfile(file)

fid = fopen(file);
flowr = fread(fid, 'double');
fclose(fid);

fdims = dlmread([file '.txt']);
w = fdims(2);
h = fdims(1);
n = numel(flowr) / (w*h*2);

flow = flowr;
%flow = reshape(flow, [2 w*h n]);
%flow = permute(flow, [2 1 3]);
flow = reshape(flow, [w*h*2 n]);

end

