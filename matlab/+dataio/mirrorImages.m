function mirrorImages(inputDir, outputDir)
%MIRRORIMAGES Summary of this function goes here
%   Detailed explanation goes here

[format,firstImage,lastImage] = dataio.detectImageSequenceFormat(inputDir);
[outFormat,~,~] = dataio.detectImageSequenceFormat(inputDir, outputDir);

for frame = firstImage:lastImage
    if mod(frame,100) == 0
        fprintf('Frame %d\n', frame);
    end
    img = imread(sprintf(format, frame));
    img = flipdim(img, 2); % Flip the image
    imwrite(img, sprintf(outFormat, frame));
end

end

