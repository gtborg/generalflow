function videoFromFrames(formatOrDirectory, frameRate, outputFile, skip)
%dataio.videoFromFrames formatOrDirectory [frameRate] [outputFile]

if ~exist('frameRate', 'var') || isempty(frameRate)
    frameRate = 30;
end

if isempty(find(formatOrDirectory=='%', 1, 'first'))
    if ~exist('outputFile', 'var') || isempty(outputFile)
        outputFile = [ formatOrDirectory, '.mp4' ];
    end
    
    [formatOrDirectory,firstFrame,lastFrame] = dataio.detectImageSequenceFormat(formatOrDirectory);
else
    firstFrame = 0;
end

if ~exist('skip', 'var')
    skip = [];
end

writer = VideoWriter(outputFile, 'MPEG-4');
writer.Quality = 80;
writer.FrameRate = frameRate;
open(writer);

i = firstFrame;
missed = 0;
while true
    if ~exist(sprintf(formatOrDirectory, i), 'file')
        missed = missed + 1;
        i = i + 1;
        if missed > 10
            break;
        end
        continue;
    end
    missed = 0;
    if isempty(find(i == skip, 1))
        fprintf('%i\n', i);
        img = imread(sprintf(formatOrDirectory, i));
        if size(img,2) > 1024
            img = imresize(img, 1024 / size(img,2));
        end
        writeVideo(writer, img);
    end
    i = i + 1;
end

close(writer);

end

