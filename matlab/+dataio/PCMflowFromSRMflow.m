function flowOut = PCMflowFromSRMflow(flow, width, height)
%PCMFLOWFROMSRMFLOW Convert flow in separated u,v, row-major format to
%paired column-major format

n = size(flow,2);

u = reshape(flow(1:(size(flow,1)/2), :), [ width, height, n ]);
v = reshape(flow((size(flow,1)/2+1):end, :), [ width, height, n ]);

u = permute(u, [ 2 1 3 ]);
v = permute(v, [ 2 1 3 ]);

flowOut(1:2:size(flow,1), :) = reshape(u, width*height, n);
flowOut(2:2:size(flow,1), :) = reshape(v, width*height, n);

end

