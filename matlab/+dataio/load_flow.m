function flow = load_flow(filename)

flow_raw = dlmread(filename);

length = size(flow_raw, 2);
if round(length / 3) ~= length / 3
    flow_raw = flow_raw(:,1:end-1);
end

[ flow_vec flow_mask ] = convert_flow_txt(flow_raw);

flow = struct('flow_vec', flow_vec, 'flow_mask', flow_mask);

end
