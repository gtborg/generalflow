function result = isOutOfDate(generatedFile, inputFile)
%ISOUTOFDATE Summary of this function goes here
%   Detailed explanation goes here

if exist(generatedFile, 'file')
    if ~exist('inputFile', 'var') || isempty(inputFile)
        result = false;
    else
        inputFileDir = dir(inputFile);
        generatedFileDir = dir(generatedFile);
        if inputFileDir.datenum >= generatedFileDir.datenum
            result = true;
        else
            result = false;
        end
    end
else
    result = true;
end

end

