function clipImages(inputDir, outputDir, xmin, xmax, ymin, ymax)
%CLIPIMAGES Summary of this function goes here
%   Detailed explanation goes here

[format,firstImage,lastImage] = dataio.detectImageSequenceFormat(inputDir);
[outFormat,~,~] = dataio.detectImageSequenceFormat(inputDir, outputDir);

for frame = firstImage:lastImage
    if mod(frame,100) == 0
        fprintf('Frame %d\n', frame);
    end
    img = imread(sprintf(format, frame));
    img = img(ymin:ymax, xmin:xmax);
    imwrite(img, sprintf(outFormat, frame));
end

end

