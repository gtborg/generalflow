function [ format, firstIndex, lastIndex ] = detectImageSequenceFormat(directory, newName)
%DETECTIMAGESEQUENCEFORMAT Scan a directory to detect the printf-style
%filename format for an image sequence, and the first and last frame
%numbers.

%%
image_extensions = { '.png', '.jpg', '.jpeg', '.bmp', '.pgm', '.ppm' };

%% Get list of files in directory
if ~exist(directory, 'dir')
    error('detectImageSequenceFormat:CannotList', 'Cannot list directory');
end
files = dir(directory);

%% Look for an image file
format = [];
for file = { files.name }
    % Get file parts
    [ ~, name, extension ] = fileparts(file{:});

    % See if it's an image file
    if ~isempty(find(cellfun(@(imgext) strcmpi(imgext, extension), image_extensions), 1, 'first'))
        % See if it has the format of non-numeric, numeric, then
        % extension.
        digits = name >= '0' & name <= '9';
        % Find last digit in filename
        numberEnd = find(digits, 1, 'last');
        if ~isempty(numberEnd)
            % Find previous non-digit in filename
            numberStart = find(~digits(1:numberEnd), 1, 'last');
            if isempty(numberStart)
                numberStart = 1; % All remaining characters were digits
            else
                numberStart = numberStart + 1;
            end
            
            % Get length in characters of the number
            numberLength = numberEnd - numberStart + 1;
            
            % Format replaces the number part with a number formatter
            if numberLength == 1
                fmtSpec = '%d';
            else
                fmtSpec = sprintf('%%0%dd', numberLength);
            end
            if ~exist('newName','var') || isempty(newName)
                newName = directory;
            end
            format = [ newName '/' [ name(1:numberStart-1) fmtSpec name(numberEnd+1:end) extension ] ];
            
            firstIndex = str2double(name(numberStart:numberEnd));
                            
            % Find last frame
            lastIndex = firstIndex;
            while 1
                if exist(sprintf(format, lastIndex), 'file')
                    lastIndex = lastIndex + 1;
                else
                    lastIndex = lastIndex - 1;
                    break;
                end
            end
            
            break
        end
    end
end

%% Return or error
if isempty(format)
    error('detectImageSequenceFormat:NoMatching', 'Could not find any files that look like an image sequence');
end

end

