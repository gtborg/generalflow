function nframes = framesFromVideo(inputFiles, outputFormat)
%FRAMESFROMVIDEO Summary of this function goes here
%   Detailed explanation goes here

framenum = 1;

if ~iscell(inputFiles)
    inputFiles = { inputFiles };
end

if ~exist('outputFormat', 'var') || isempty(outputFormat)
    [ ~, name, ~ ] = fileparts(inputFiles{1});
    if exist(name, 'file')
        error([ 'Directory "' name '" already exists, either remove it or explicitly specify the output filename format.']);
    end
    mkdir(name);
    outputFormat = [ name '/%08d.jpg' ];
end

for i = 1:length(inputFiles)
    reader = VideoReader(inputFiles{i});
    nFrames = reader.NumberOfFrames;
    nframes(i) = nFrames;

    for f = 1:nFrames
        img = read(reader, f);
        img = rgb2gray(img);
        imwrite(img, sprintf(outputFormat, framenum), 'Quality', 95);
        framenum = framenum + 1;
        
        if mod(framenum, 100) == 0
            fprintf('  Frame %d\n', framenum);
        end
    end
end

end

