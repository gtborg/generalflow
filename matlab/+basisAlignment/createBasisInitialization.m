function values = createBasisInitialization(imWidth, imHeight, q)
%CREATEBASISINITIALIZATION Create a random basis initialization

values = gtsam.Values;
for i = 0:(imHeight-1)
    for j = 0:(imWidth-1)
        pix = j*imHeight + i;
        values.insert(gtsam.symbol('V', pix), gtsam.LieMatrix(rand(2,q)));
    end
end

end

