function factors = createMotionFactors(firstFrameNums, lastFrameNums, frameInterval, q, motionSigma)
%CREATEMOTIONFACTORS Creates smooth motion factors between adjacent latent
%variables.

import gtsam.*

factors = cell(1, numel(firstFrameNums));
lastLastFrame = 0;
for imgSet = 1:numel(firstFrameNums)
    factors{imgSet} = NonlinearFactorGraph;
    
    % Create a zero-precision "dummy" factor tying the disconnected image
    % sets together.
    if imgSet > 1
        factors{imgSet}.add(gtsam.BetweenFactorLieVector( ...
            generalflow.vKey('y',imgSet-1,lastLastFrame), ...
            generalflow.vKey('y',imgSet,firstFrame(imgSet)), LieVector(zeros(q,1)), ...
            gtsam.noiseModel.Isotropic.Precision(q, 0.0)));
    end

    for frame = (firstFrameNums(imgSet)+frameInterval):frameInterval:lastFrameNums(imgSet)
        ykey = generalflow.vKey('y', imgSet, frame);
        factors{imgSet}.add(gtsam.BetweenFactorLieVector(generalflow.vKey('y',imgSet,frame-frameInterval), ykey, ...
           LieVector(zeros(q,1)), noiseModel.Isotropic.Sigma(q, motionSigma*frameInterval)));
    end

    lastLastFrame = frame;
end

end

