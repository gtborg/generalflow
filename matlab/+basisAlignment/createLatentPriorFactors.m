function [ factors, values ] = createLatentPriorFactors(firstFrameNums, lastFrameNums, frameInterval, q, ys)
%CREATELATENTPRIORFACTORS Create priors on latent variables given
%partially-labeled motion.

import gtsam.*

factors = cell(1, numel(firstFrameNums));
values = Values;
for imgSet = 1:numel(firstFrameNums)
    factors{imgSet} = NonlinearFactorGraph;
    values.insert(generalflow.vKey('y',imgSet,firstFrameNums(imgSet)), gtsam.LieVector(rand(q,1)));

    for frame = (firstFrameNums(imgSet)+frameInterval):frameInterval:lastFrameNums(imgSet)
        ykey = generalflow.vKey('y', imgSet, frame);

        % Latent variable prior if we have it
        if ~isempty(ys) && ~isempty(ys{imgSet}) && all(isfinite(ys{imgSet}(:,frame)))
            factors{imgSet}.add(gtsam.PriorFactorLieVector( ...
                ykey, gtsam.LieVector(ys{imgSet}(:,frame)), ...
                gtsam.noiseModel.Isotropic.Sigma(q, 0.1)));
            values.insert(ykey, gtsam.LieVector(ys{imgSet}(:,frame)));
        else
            factors{imgSet}.add(gtsam.PriorFactorLieVector( ...
              ykey, gtsam.LieVector(zeros(q,1)), ...
              gtsam.noiseModel.Isotropic.Sigma(q, 1.0)));
            values.insert(ykey, gtsam.LieVector(rand(q,1)));
        end
    end
end

end
