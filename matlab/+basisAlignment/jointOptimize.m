function result = jointOptimize(imgDirs, q, pyrLevel, inlierPrior, ys)
%JOINTOPTIMIZE Optimize for a basis from one or more image sequences

import gtsam.*

initialS = [ 0.05 0.5 0.01 0.01 ];
motionSigma = 0.01;
flowSmoothSigma = 0.01;
frameInterval = 1;

% Get image filename format
fprintf('Finding images...\n');
for i = 1:numel(imgDirs)
    [ imgFormats{i}, firstFrames(i), lastFrames(i) ] = dataio.detectImageSequenceFormat(imgDirs{i});
end

%firstFrames(1) = 928;
%lastFrames(1) = 2857;

% Get image size
img = imread(sprintf(imgFormats{1}, firstFrames(1)));
imWidth = size(img,2);
imHeight = size(img,1);
pimWidth = imWidth / (2^pyrLevel);
pimHeight = imHeight / (2^pyrLevel);

if ~exist('ys', 'var')
    ys = [];
end

% Create factor graphs
fprintf('Building graph...\n');
[ factors, fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit ] = ...
    basisAlignment.createAllBasisOptimizationFactors( ...
    imgFormats, firstFrames, lastFrames, imWidth, imHeight, q, pyrLevel, ...
    inlierPrior, ys, initialS, motionSigma, flowSmoothSigma, frameInterval);

[ result, ~ ] = basisAlignment.optimizeAllGraphs(...
    fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit, ...
    pimWidth, pimHeight, inlierPrior);

inspector.colorTest;

end

