function [ result, details ] = optimizeAllGraphs( ...
    fgBasis, fgLatent, fgSigmas, basisInit, latentInit, sigmaInit, ...
    imWidth, imHeight, inlierPrior)
%OPTIMIZEALLGRAPHS Summary of this function goes here
%   Detailed explanation goes here

maxIterations = 5;

import gtsam.*

basisValues = Values(basisInit);
latentValues = Values(latentInit);
sigmaValues = Values(sigmaInit);

% Optimize
fprintf('Optimizing...\n');
%params = gtsam.DoglegParams;
params = gtsam.GaussNewtonParams;
params.setVerbosity('ERROR');
params.setLinearSolverType('MULTIFRONTAL_QR');
%params.setVerbosityDL('VERBOSE');
%     params.setOrdering(ordering);
%opt = gtsam.DoglegOptimizer(fg, initial, params);
optBasis = gtsam.GaussNewtonOptimizer(fgBasis, basisValues, params);
optLatent = gtsam.GaussNewtonOptimizer(fgLatent, latentValues, params);
optSigmas = gtsam.GaussNewtonOptimizer(fgSigmas, sigmaValues, params);
details = struct;
for iter=1:maxIterations
    % Iterate
    fprintf('Iteration %d, basis\n', iter);
    optBasis.iterate;
    basisValues.update(optBasis.values);
    fprintf('Iteration %d, latent\n', iter);
    optLatent.iterate;
    latentValues.update(optLatent.values);
    if iter > 0
        try
            fprintf('Iteration %d, sigmas\n', iter);
            optSigmas.iterate;
            sigmaValues.update(optSigmas.values);
        catch
            fprintf('Caught error, could not optimize sigmas\n');
        end
        sigmaValues.disp;
    end

    combinedValues = gtsam.Values;
    combinedValues.insert(basisValues);
    combinedValues.insert(latentValues);
    combinedValues.insert(sigmaValues);
    details(iter).vals = combinedValues;
    [ details(iter).subspace, details(iter).latent ] = ...
        dataio.subspaceFromValues(combinedValues, inlierPrior, imWidth, imHeight);
    details(iter).errors = [ optBasis.error; optLatent.error; optSigmas.error ];

    if mod(iter,1) == 0
        ppcares.W = details(iter).subspace.basis;
        ppcares.basissize(4:5) = [ imWidth imHeight ];
        inspector.drawBasis(ppcares);
    end
end

result = details(end);

end

