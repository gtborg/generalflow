function [ factors, fgBasis, fgLatent, fgSigmas, ...
    basisValues, latentValues, sigmaValues ] = createAllBasisOptimizationFactors( ...
    imgFormats, firstFrames, lastFrames, imWidth, imHeight, q, pyrLevel, ...
    inlierPrior, ys, initialS, motionSigma, flowSmoothSigma, frameInterval)
%CREATEALLBASISOPTIMIZATIONFACTORS Creates all factors and initializations
%needed for optimizing for a basis.

imDownScale = 2 ^ pyrLevel;
pimWidth = imWidth / imDownScale;
pimHeight = imHeight / imDownScale;

% Add smoothness field
fprintf('  Basis flow smoothness...\n');
factors.smoothnessFactors = basisAlignment.createSmoothnessFactors(pimWidth, pimHeight, q, flowSmoothSigma);

% Create initializations
fprintf('  Random basis initialization...\n');
basisValues = basisAlignment.createBasisInitialization(pimWidth, pimHeight, q);
sigmaValues = gtsam.Values;
sigmaValues.insert(gtsam.symbol('S',0), gtsam.LieVector(initialS'));

% Add constraints to prevent sigmas from going near or less than zero
factors.sigmaPriorFactors = gtsam.NonlinearFactorGraph;
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 0, 0.005, true));
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 1, 0.005, true));
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 2, 0.005, true));
factors.sigmaPriorFactors.add(basisflow.ScalarInequality( ...
    gtsam.symbol('S',0), 3, 0.005, true));

% Sigma priors
% factors.sigmaPriorFactors.add(gtsam.PriorFactorLieVector( ...
%     gtsam.symbol('S',0), gtsam.LieVector(initialS'), ...
%     gtsam.noiseModel.Diagonal.Precisions([0 1e14 0 1e14]')));

% Latent variable smoothness factors
fprintf('  Latent variable smoothness factors...\n');
factors.motionFactors = basisAlignment.createMotionFactors(firstFrames, lastFrames, frameInterval, q, motionSigma);

% Latent variable priors
fprintf('  Latent variable priors...\n');
[ factors.latentPriorFactors, latentValues ] = basisAlignment.createLatentPriorFactors(firstFrames, lastFrames, frameInterval, q, ys);

fprintf('  Image factors...\n');
[ factors.basisFactors, factors.latentFactors, factors.sigmaFactors ] = ...
    basisAlignment.createImageFactors(imgFormats, pimWidth, pimHeight, pyrLevel, ...
    firstFrames, lastFrames, frameInterval, inlierPrior, ...
    basisValues, latentValues, sigmaValues);

% Three factor graphs
fgBasis = gtsam.NonlinearFactorGraph;
fgBasis.push_back(factors.basisFactors);
fgBasis.push_back(factors.smoothnessFactors);

fgLatent = gtsam.NonlinearFactorGraph;
for imgSet = 1:numel(imgFormats)
    fgLatent.push_back(factors.latentFactors{imgSet});
    fgLatent.push_back(factors.latentPriorFactors{imgSet});
    fgLatent.push_back(factors.motionFactors{imgSet});
end

fgSigmas = gtsam.NonlinearFactorGraph;
fgSigmas.push_back(factors.sigmaFactors);
fgSigmas.push_back(factors.sigmaPriorFactors);

end

