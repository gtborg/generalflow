function M = optimizeBasis(M, ySegments, absoluteConstraints, relativeConstraints)
%OPTIMIZEBASIS Summary of this function goes here
%   Detailed explanation goes here

import gtsam.*

% If planar motion, augment M to 6DOF motion
if size(M, 1) == 3
    M = [
        zeros(2, size(M,2))
        M(3,:)
        M(1,:)
        M(2,:)
        zeros(1, size(M,2)) ];
    planar = 1;
else
    planar = 0;
end

% Integrate velocity for initialization
initial = Values;
nextIndex = 1;
for ySeg = ySegments
    y = ySeg{:};
    vel = M * y;
    initial.insert(symbol('x',nextIndex), Pose3); % First pose at origin
    for i=2:size(y,2)
        prevKey = symbol('x', i + nextIndex - 1 - 1);
        curKey = symbol('x', i + nextIndex - 1);
        pose = initial.at(prevKey).compose(Pose3.Expmap(vel(:,i)));
        initial.insert(curKey, pose);
    end
    nextIndex = nextIndex + size(y,2);
end
nPoses = nextIndex - 1;
clear nextIndex
initial.insert(symbol('M', 0), LieMatrix(M));

% Create factors
fg = NonlinearFactorGraph;
fg.add(PriorFactorLieMatrix(symbol('M',0), LieMatrix(zeros(size(M))), ...
    noiseModel.Isotropic.Precision(Pose3.Dim*Pose3.Dim, 1e-16)));

% If planar motion, constrain other rows of M
if planar
    fg.add(PriorFactorLieMatrix(symbol('M',0), LieMatrix(zeros(size(M))), ...
        noiseModel.Diagonal.Precisions([ ...
        repmat(1e6, size(M,2), 1)
        repmat(1e6, size(M,2), 1)
        zeros(size(M,2), 1)
        zeros(size(M,2), 1)
        zeros(size(M,2), 1)
        repmat(1e6, size(M,2), 1) ])));
end

% Add optical flow factors
nextIndex = 1;
for ySeg = ySegments
    y = ySeg{:};
    for i=2:size(y,2)
        prevKey = symbol('x', i + nextIndex - 1 - 1);
        curKey = symbol('x', i + nextIndex - 1);
        fg.add(basisflow.BasisAlignmentFactorPose3(prevKey, curKey, ...
            symbol('M',0), y(:,i), noiseModel.Isotropic.Precision(Pose3.Dim, 1e4)));
    end
    nextIndex = nextIndex + size(y,2);
end
clear nextIndex

% Add constraint factors
for i=1:size(absoluteConstraints,1)
    c = absoluteConstraints(i,:);
    if size(c,2) == 4
        c = [ c(1) 0 0 c(4) c(2) c(3) 0 ];
    end
    precisions = 1e8 * ones(Pose3.Dim, 1);
    precisions(~isfinite(c(2:end))) = 0;
    c(~isfinite(c)) = 0;
    fg.add(PriorFactorPose3(symbol('x',c(1)), ...
        Pose3(Rot3.RzRyRx(c(2), c(3), c(4)), Point3(c(5), c(6), c(7))), ...
        noiseModel.Diagonal.Precisions(precisions)));
end
for i=1:size(relativeConstraints,1)
    c = relativeConstraints(i,:);
    if size(c,2) == 5
        c = [ c(1) c(2) 0 0 c(5) c(3) c(4) 0 ];
    end
    precisions = 1e8 * ones(Pose3.Dim, 1);
    precisions(~isfinite(c(3:end))) = 0;
    c(~isfinite(c)) = 0;
    fg.add(BetweenFactorPose3(symbol('x',c(1)), symbol('x',c(2)), ...
        Pose3(Rot3.RzRyRx(c(3), c(4), c(5)), Point3(c(6), c(7), c(8))), ...
        noiseModel.Diagonal.Precisions(precisions)));
end

% Add planar motion constraints if planar motion
if planar
    for i=1:nPoses
        fg.add(PriorFactorPose3(symbol('x',i), Pose3, ...
            noiseModel.Diagonal.Precisions([1e4; 1e4; 0; 0; 0; 1e4])));
    end
end

% Plot initialization
axis equal
hold on
gtsam_utils.plot3DTrajectory(initial, 'b');
export_fig('traj000.png');
drawnow

% Optimize
dlParams = LevenbergMarquardtParams;
dlParams.setVerbosityLM('LAMBDA');
dlParams.setVerbosity('ERROR');
dlParams.setRelativeErrorTol(0.0);
dlParams.setAbsoluteErrorTol(0.1);
nlo = LevenbergMarquardtOptimizer(fg, initial, dlParams);
if 1
    soln = nlo.optimize();
    
    % Plot solution
    gtsam_utils.plot3DTrajectory(soln, 'g');
    initial.at(symbol('M',0)).print('Initial M: ');
    soln.at(symbol('M',0)).print('Final M: ');
else
    for i = 1:10
        nlo.iterate();
        soln = nlo.values();
        clf; axis equal
        gtsam_utils.plot3DTrajectory(soln, 'g');
        export_fig(sprintf('traj%03d.png', nlo.iterations()));
    end
end

M = soln.at(symbol('M',0)).matrix();

end

