function factors = createSmoothnessFactors(imWidth, imHeight, q, flowSmoothSigma)
%CREATESMOOTHNESSFACTORS Creates factors between adjacent pixels in the
%basis flows to prefer smoothness.  q is the number of basis flows (latent
%variable dimensionality), and flowSmoothSigma is the sigma of the Gaussian
%pairwise smoothness terms.

import gtsam.*

factors = gtsam.NonlinearFactorGraph;

for i = 0:(imHeight-1)
    for j = 0:(imWidth-1)
        for k = 1:q
            pix = j*imHeight + i;
            if j >= 1
                for xy = 1:2
                    precsM = zeros(2,q);
                    precsM(xy,k) = 1/(flowSmoothSigma^2);
                    precs = reshape(precsM', 2*q, 1);
                    p = gtsam.BetweenFactorLieMatrix( ...
                        symbol('V',pix), symbol('V',(j-1)*imHeight+i), ...
                        LieMatrix(zeros(2,q)), noiseModel.Diagonal.Precisions(precs));
                    factors.add(p);
                end
            end
            if i >= 1
                for xy = 1:2
                    precsM = zeros(2,q);
                    precsM(xy,k) = 1/(flowSmoothSigma^2);
                    precs = reshape(precsM', 2*q, 1);
                    p = gtsam.BetweenFactorLieMatrix( ...
                        symbol('V',pix), symbol('V',j*imHeight+(i-1)), ...
                        LieMatrix(zeros(2,q)), noiseModel.Diagonal.Precisions(precs));
                    factors.add(p);
                end
            end
        end
    end
end


end

