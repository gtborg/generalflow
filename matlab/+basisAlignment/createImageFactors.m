function [ basisFactors, latentFactors, sigmaFactors ] = createImageFactors( ...
    imgFormats, imWidth, imHeight, pyrLevel, firstFrameNums, lastFrameNums, frameInterval, inlierPrior, ...
    basisValues, latentValues, sigmaValues)
%CREATEIMAGEFACTORS Creates basis, latent variable, and sigma factors for
%all frames.

import gtsam.*

basisFactors = gtsam.NonlinearFactorGraph;
sigmaFactors = gtsam.NonlinearFactorGraph;
for imgSet = 1:numel(firstFrameNums)

    % Create frame cache
    frameCache = basisflow.FrameCache(imgFormats{imgSet}, 2^(pyrLevel-2)-1, imWidth, imHeight, 2e9);

    % Create basis and sigma factors (on all frames and all pixels)
    frameNums = basisflow.IntVector;
    for frame = (firstFrameNums(imgSet)+frameInterval):frameInterval:lastFrameNums(imgSet)
        frameNums.push_back(frame);
    end
    basisFactors.add( ...
        basisflow.OnlyBasisImageFactor(gtsam.symbol('V',0), generalflow.vKey('y',imgSet,0), gtsam.symbol('S',0), ...
        latentValues, sigmaValues, inlierPrior, frameNums, frameCache));
    sigmaFactors.add( ...
        basisflow.OnlySigmaImageFactor(gtsam.symbol('V',0), generalflow.vKey('y',imgSet,0), gtsam.symbol('S',0), ...
        basisValues, latentValues, inlierPrior, frameNums, frameCache));

    latentFactors{imgSet} = gtsam.NonlinearFactorGraph;

    for frame = (firstFrameNums(imgSet)+frameInterval):frameInterval:lastFrameNums(imgSet)
        fprintf('Building fg frame %d\n', frame);
        ykey = generalflow.vKey('y', imgSet, frame);

        % Pixel latent variable factor
        latentFactors{imgSet}.add( ...
            basisflow.OnlyLatentImageFactor(gtsam.symbol('V',0), ykey, gtsam.symbol('S',0), ...
            basisValues, sigmaValues, inlierPrior, frame, frameCache));
    end
end


end

