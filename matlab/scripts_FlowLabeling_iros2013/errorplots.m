function errorplots(velocities, states, handlabels, firstFrame, lastFrame)
%ERRORPLOTS Summary of this function goes here
%   Detailed explanation goes here

velocities_gt = [ zeros(3,1) states(:,2:end)-states(:,1:end-1) ];

velocities = velocities(:,firstFrame:lastFrame);
velocities_gt = velocities_gt(:,firstFrame:lastFrame);

clf;
%figure('color','white');
[ax,h1,h2] = plotyy((firstFrame:lastFrame)-firstFrame, handlabels(firstFrame:lastFrame,:), ...
    (firstFrame:lastFrame)-firstFrame, abs(velocities(3,:) - velocities_gt(3,:)));

legend('Extra obstacles', 'Missed obstacles', 'Yaw-rate estimation error');

xlabel('Frame number');

set(get(ax(1),'Ylabel'),'String','Number of objects mislabeled');
set(get(ax(2),'Ylabel'),'String','\omega_z yaw-rate estimation error, rad/s');
maxerrs = max(max(handlabels(firstFrame:lastFrame,:)));
set(ax(1), 'Ylim', [-0.5,2*maxerrs+0.5]);

curlim = get(ax(2), 'Ylim');
set(ax(2), 'Ylim', [-curlim(2),curlim(2)]);

end

