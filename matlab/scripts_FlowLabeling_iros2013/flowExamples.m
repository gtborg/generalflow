fx = 982.71605;
fy = 981.345549;
px = 685.94996;
py = 271.896184;

scale = 0.25;

calibrationScaled = gtsam.Cal3_S2(fx * scale, fy * scale, 0, px * scale, py * scale);

pitch = -7/180*pi;


f = flowsim.flowGroundPlane( ...
    [0;0;0.002;.25*cos(pitch);0;.25*sin(pitch)], ...
    Pose3(Rot3.RzRyRx(0,pitch,0), Point3(0,0,-5)), calibrationScaled, ...
    1380 * scale, 480 * scale, 1380 * scale, 480 * scale);

figure; inspector.drawFlow(.25*f);

f2 = flowsim.flowInfinity( ...
    [0;0;0.002;.25*cos(pitch);0;.25*sin(pitch)], ...
    calibrationScaled, ...
    1380 * scale, 480 * scale, 1380 * scale, 480 * scale);

figure; inspector.drawFlow(0.5*f2);

