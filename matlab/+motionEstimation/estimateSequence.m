function velocities = estimateSequence(imgDir, subspace, inlierPrior)
%ESTIMATESEQUENCE Estimate velocity (latent variables) on an image sequence

% Get image filename format
fprintf('Finding images...\n');
[ imgFormat, firstFrame, lastFrame ] = dataio.detectImageSequenceFormat(imgDir);

lastFrame = 300;

% Get image size
img = imread(sprintf(imgFormat, firstFrame));
imWidth = size(img,2);
imHeight = size(img,1);
scale = imWidth / subspace.width;
pixelSkip = max(0, (scale / 4) - 1);

% Set up values
basisValues = dataio.valuesFromSubspace(subspace);
sigmaValues = gtsam.Values;
sigmaValues.insert(gtsam.symbol('S',0), gtsam.LieVector( ...
    [ sqrt(subspace.inlierSigma2()); sqrt(subspace.outlierSigma2()); ...
    sqrt(subspace.inlierPixSigma2()); sqrt(subspace.outlierPixSigma2()) ]));

% Estimate
velocities = zeros(size(subspace.basis,2), lastFrame);
unitModel = gtsam.noiseModel.Unit.Create(size(velocities,1));
constrain2 = gtsam.noiseModel.Diagonal.Precisions([0;1e7;0]);
for frame = (firstFrame+1) : lastFrame
    fprintf('Frame %d\n', frame);
    ykey = gtsam.symbol('y', frame);
    
    frameCache = basisflow.FrameCache(imgFormat, pixelSkip, subspace.width, subspace.height, 10e6);

    fg = gtsam.NonlinearFactorGraph;
    fg.add(motionEstimation.createImageFactor(frameCache, ykey, inlierPrior, basisValues, sigmaValues));
    fg.at(0).inspection(true);
    fg.add(gtsam.PriorFactorLieVector(ykey, gtsam.LieVector(zeros(size(velocities,1),1)), unitModel));
    fg.add(gtsam.PriorFactorLieVector(ykey, gtsam.LieVector(zeros(size(velocities,1),1)), constrain2));

    init = gtsam.Values;
    init.insert(ykey, gtsam.LieVector(velocities(:,frame-1)));

    params = gtsam.GaussNewtonParams;
    params.setVerbosity('Error');
    opt = gtsam.GaussNewtonOptimizer(fg, init, params);
    for i = 1:1
        opt.iterate;
        fprintf('error = %g\n', opt.error);
    end
    soln = opt.values;
    
    velocities(:,frame) = soln.at(ykey).vector;
end

end

