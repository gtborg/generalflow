function [ factor ] = createImageFactor(frameCache, ykey, inlierPrior, basisValues, sigmaValues)
%CREATEIMAGEFACTOR Create a factor on the latent variable of a single
%frame.

% Pixel latent variable factor
factor = basisflow.OnlyLatentImageFactor(gtsam.symbol('V',0), ykey, gtsam.symbol('S',0), ...
    basisValues, sigmaValues, inlierPrior, generalflow.vKeyFrameNum(ykey), frameCache);

end

