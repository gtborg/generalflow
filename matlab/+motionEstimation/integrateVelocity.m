function trajectory = integrateVelocity(velocities, timestep)
%INTEGRATEVELOCITY Integrate velocities into a trajectory

if size(velocities,1) == 3
    trajectory = gtsam.Pose2;
elseif size(velocities,1) == 6
    trajectory = gtsam.Pose3;
else
    error('Velocities must have dimensionality 3 or 6');
end

for i=2:size(velocities,2)
    if size(velocities,1) == 3
        trajectory(i) = trajectory(i-1).compose(Pose2.Expmap(velocities(:,i) .* timestep));
    else
        trajectory(i) = trajectory(i-1).compose(Pose3.Expmap(velocities(:,i) .* timestep));
    end
end

end

