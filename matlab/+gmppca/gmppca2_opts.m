classdef gmppca2_opts
    %GMPPCA2_OPTS Options for gmppca2
    
    properties
        compute_mean
        updateSigv
        robust
        draw
        inlierPrior
    end
    
    methods
        function obj = gmppca2_opts()
            obj.compute_mean = 0;
            obj.updateSigv = 1;
            obj.robust = 1;
            obj.draw = 1;
            obj.inlierPrior = 0.6;
        end
    end
    
end

