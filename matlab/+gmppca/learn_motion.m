function [ Mx Mc ] = learn_motion(Ex, gtruth)

N = size(Ex, 2);
q = size(Ex, 1);
r = size(gtruth, 1);

% M = zeros(r, q+1);
M = zeros(r, q);

for k = 1:r
%     A = [ Ex' ones(N,1) ];
%     b = gtruth(k, :)';
%     
%     M(k, :) = b \ A;

    X = Ex';
    y = gtruth(k, :)';
%     [ A stats ] = robustfit(X,y);
    [ A stats ] = robustfit(X,y, 'bisquare', 4.685, 'off');
    M(k, :) = A';
    stats
end

Mc = zeros(r, 1);
Mx = M;

% Mc = M(:, 1);
% Mx = M(:, 2:end);


