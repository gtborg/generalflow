function result = ppca(t, q, mask)
%PPCA Probabilistic PCA using EM, following Tipping and Bishop, 1999.
%   Finds q principal components, from observations in t.  Entries in t
%   corresponding to non-zero entries in mask will be estimated as missing
%   parameters.

t = t';
mask = mask';
t(find(t > 30)) = 30;
t(find(t < -30)) = -30;

d = size(t, 1);
N = size(t, 2);
x = ones(q, N);
xxT = zeros(q, q, N);
maskIDs = cell(N, 1);
Si = cell(N, 1);
for i=1:N
    maskIDs{i} = find(mask(:,i));
    Si{i} = (maskIDs{i}(find(mod(maskIDs{i}, 2)==0)) ./ 2)';
end
Sj = cell(d, 1);
Sj2 = cell(d/2, 1);
for j=1:d
    Sj{j} = find(mask(j,:));
    if mod(j,2) == 0
        Sj2{j/2} = Sj{j};
    end
end
mu = mean(t, 2);
tc = t - repmat(mu, 1, N);

W = repmat(eye(q), round(d/q) + 1, 1);
W = W(1:d, :);
sig2 = 0;

    fig1 = figure('color', 'white');
    fig2 = figure('color', 'white');
    fig3 = figure('color', 'white');
    
    
delta = 10.0;
while delta > .01
    
    % Obtain expectations of xi and xi*xiT
    xPrev = x;
    for i = 1:N
        if length(maskIDs{i}) > 0
            WSi = W(maskIDs{i}, :);
            MSi = WSi'*WSi + sig2*eye(size(WSi,2));
            tcSi = tc(maskIDs{i}, i);
            invMSi = inv(MSi);
            x(:, i) = invMSi*WSi'*tcSi;
            xxT(:,:,i) = sig2*invMSi + x(:,i)*x(:,i)';
        end
    end
    %x
        
    % Estimate W
    Wprev = W;
    for j = 1:d
        a = zeros(1, q);
        %for i = Sj{j}
            a = a + tc(j, Sj{j}) * x(:, Sj{j})';
        %end
        W(j, :) = a*inv(sum(xxT(:,:,Sj{j}), 3));
    end
    %W

    % Estimate sig2
    sig2prev = sig2;
%     count = 0;
%     a = 0;
%     for i = 1:N
%         count = count + length(Si{i});
%         for j = Si{i}
%             tcij = tc(j:j+1, i);
%             Wj = W(j:j+1, :);
%             a = a + 0.5 * (tcij'*tcij) ...
%                 - tcij'*Wj*x(:,i) ...
%                 + 0.5*trace(Wj'*Wj*xxT{i});
%         end
%     end
%     sig2 = (1/count) * a;
    sig2 = 0.000001;
    
    sig2
    
    delta = norm((xPrev - x) / norm(x)) + norm((Wprev - W) / norm(W)) + abs(((sig2prev - sig2) / sig2));
    %delta = sum(sum((xPrev - x) .^ 2)) + sum(sum((Wprev - W) .^ 2)) + (sig2prev - sig2);
    
    delta
    
    set(0,'CurrentFigure',fig1);
    clf;
    draw_flow(W(:,1), 0, 45)
    
    set(0,'CurrentFigure',fig2);
    clf;
    draw_flow(W(:,2), 0, 45)
    
    set(0,'CurrentFigure',fig3);
    clf;
    plot(x');
    
    drawnow
    
end

% WTW = W' * W;
% [ V u ] = eig(WTW);
% W = W * V';

result = struct('W', W, 'x', x, 't', t, 'sig2', sig2);
