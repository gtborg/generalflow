function ppcares = gmppca2(t, q, opts)
%GMPPCA2 Summary of this function goes here
%   Detailed explanation goes here

compute_mean = opts.compute_mean;
robust = opts.robust;

t(abs(t) > 20.0) = 0.0;

%mask = logical(mask);
mask = isfinite(t);
N = size(t, 2);
d = size(t, 1);

numobs = numel(find(mask));

% Initialize W with a random projection
W = rand(d, q) - 0.5;
for k = 1:q
    W(:,k) = W(:,k) / norm(W(:,k)) * 5;
end

% Initialize mu with the full data mean.
mu = zeros(d, 1);
y = zeros(d, N);
for j = 1:d
    if compute_mean
        mu(j) = sum(t(j, :) .* mask(j, :)) / sum(mask(j, :));
    end
    y(j,:) = t(j,:) - mu(j);
end

% Initialize sigma2
sig2v = 2.0*2.0;
sig2f = 15.0*15.0;
invsig2v = 1.0 / sig2v;
invsig2f = 1.0 / sig2f;

% Initialize hidden variable expectations
Ex = zeros(q, N);
ExxT = zeros(q, q, N);
Ez = ones(d, N);

if opts.draw
    % Open progress figures
    % fig1 = figure('color', 'white');
    % fig2 = figure('color', 'white');
    % fig3 = figure('color', 'white');
    fig4 = figure('color', 'white');
    % fig5 = figure('color', 'white');
end

% lastsig2v = 0.0;
initconverged = 0;
iterations = 0;
while initconverged < 2
    
    % Update Ez
    if robust && initconverged > 0
        WTW = W'*W;
        inlierMeans = W * (WTW \ (WTW + sig2v*speye(q)) * Ex) + repmat(mu, 1,N);
        errs = t - inlierMeans;
        errs = errs .* errs;
        Linlier = sqrt(invsig2v) .* exp(-1/2 * invsig2v .* errs) * opts.inlierPrior;
        Loutlier = sqrt(invsig2f) .* exp(-1/2 * invsig2f .* t .* t) * (1-opts.inlierPrior);
        Loutlier(and(Loutlier == 0, Linlier == 0)) = 1;
        Ez = Linlier ./ (Linlier + Loutlier);
    end

    % Update Ex and ExxT
    lastEx = Ex;
%     Exvar = sqrt(var(Ex')');
%     if initconverged > 0 || min(Exvar) <= 0.0
        Exvar = ones(q,1);
%     end
    for i = 1:N
        Si = mask(:,i);
        ySi = y(Si, i);
        WSi = W(Si, :);
        if numel(ySi) >= q
            %invcovi = diag(Ez(Si, i) * invsig2v);
            invcovi = spdiags(Ez(Si, i) * invsig2v, 0, numel(ySi), numel(ySi));
%             invM = inv(WSi' * invcovi * WSi + eye(q));
%             Ex(:, i) = invM * WSi' * invcovi * ySi;
            M = WSi' * invcovi * WSi + speye(q);
            Ex(:, i) = (M \ WSi' * invcovi * ySi) ./ Exvar;
            ExxT(:, :, i) = inv(M) + Ex(:,i) * Ex(:,i)';
        else
            Ex(:, i) = zeros(q,1);
            ExxT(:, :, i) = zeros(q,q,1);
        end
    end

    
    % Update the mean and factor loadings, precompute WTW
    sumz = 0;
    WTW = zeros(q,q,d);
    for j = 1:d
        Sj = mask(j,:);
        EzSj = Ez(j,Sj);
        sumzj = sum(EzSj, 2);
        sumz = sumz + sumzj;
        assert(sumzj > 0);
        % Comment out next two lines to not compute mean.
        if compute_mean
            mu(j) = sum(EzSj .* (t(j,Sj) - W(j,:) * Ex(:,Sj)), 2) / sumzj;
            y(j,:) = t(j,:) - mu(j);
        end
        
        EzSj3 = permute(EzSj, [1,3,2]);
        EzSj3 = repmat(EzSj3, [q,q,1]);
        sumdenom = sum((invsig2v .* EzSj3) .* ExxT(:,:,Sj), 3);
        EzSj2 = repmat(EzSj, [q,1]);
        ySj2 = repmat(y(j,Sj), [q,1]);
        W(j,:) = sum((invsig2v .* EzSj2) .* Ex(:,Sj) .* ySj2, 2)' / sumdenom;
        
        WTW(:,:,j) = W(j,:)' * W(j,:);
        
        clear EzSj3 EzSj2 ySj2
    end

    % Update the inlier variance
    aij = (y .* y) - (2 * y .* (W * Ex));
    xxtCOL = reshape(permute(ExxT, [2,1,3]), [q*q, N, 1]);
    WTWROW = reshape(WTW, [q*q, d])';
%     trij = WTWROW * xxtCOL;
    assert(sumz > 0);
    numerator = aij + (WTWROW * xxtCOL);
    clear aij
    numerator = numerator .* mask;
    if opts.updateSigv
        numeratorv = numerator .* Ez;  % Ran out of memory when these were combined
        lastsig2v = sig2v;
        sig2v = sum(sum(numeratorv)) / sumz;
        invsig2v = 1 / sig2v;
        clear numeratorv
    end
    if robust && initconverged > 0
        numerator = numerator .* (1 - Ez);
        sig2f = sum(sum(numerator)) / (numobs - sumz);
        invsig2f = 1 / sig2f;
    end
    
    clear aij xxtCOL WTWROW trij numerator

    fprintf(1, 'sig2v = %g\n', sig2v);
    fprintf(1, 'sig2f = %g\n', sig2f);
    
%     if abs(lastsig2v - sig2v) < 0.1 && initconverged == 0
%         initconverged = 1;
%     elseif abs(lastsig2v - sig2v) < 0.003 && initconverged == 1
%         initconverged = 2;
%     end

%     Exvar = mean(var(Ex'));
%     Exvardiff = mean(var(lastEx')) - Exvar;
%     fprintf(1, 'Exvar = %f\n', Exvar);
    Exvardiff = 0;
    if (~opts.updateSigv || abs(lastsig2v - sig2v) < 0.3) && abs(Exvardiff) < 0.5 && initconverged == 0
        initconverged = 1;
        fprintf('Stage 2\n');
    elseif iterations > 400 || ((~opts.updateSigv || abs(lastsig2v - sig2v) < 0.0005) ...
            && (opts.updateSigv || (~opts.updateSigv && iterations > 10)) ...
            && Exvardiff >= 0 && Exvardiff < 0.001 && initconverged == 1)
        initconverged = 2;
    end
    
    iterations = iterations + 1;

    if opts.draw
        %     set(0,'CurrentFigure',fig1);
        %     clf;
        %     draw_flow(W(:,1)*5, 0, 32, ones(d,1))
        %
        %     set(0,'CurrentFigure',fig2);
        %     clf;
        %     draw_flow(W(:,2)*5, 0, 32, ones(d,1))
        %
        %      set(0,'CurrentFigure',fig3);
        %      clf;
        %      draw_flow(W(:,3)*5, 0, 32, ones(d,1))
        
        set(0,'CurrentFigure',fig4);
        clf;
        plot(Ex');
        
        %     set(0,'CurrentFigure',fig4);
        %     clf;
        %     draw_flow(t(:,215), 0, 45, EV(:,215));
        %
        %     set(0,'CurrentFigure',fig5);
        %     clf;
        %     draw_flow(W*Ex(:,215)+mu, 0, 45, ones(d,1));
        
        drawnow
    end
    
end

WTW = W' * W;
[ V D ] = eig(WTW);
Wortho = W * V';

ppcares = struct('Wortho', Wortho, 'mask', mask, 'W', W, 'mu', mu, 'Ex', Ex, 'Ez', Ez, 'sig2v', sig2v, 'sig2f', sig2f);
ppcares.inlierPrior = opts.inlierPrior;


end

