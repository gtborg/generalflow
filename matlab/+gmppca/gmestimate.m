function estimate = gmestimate(ppca_result, t)

mask = isfinite(t);
N = size(t, 2);
d = size(t, 1);
q = size(ppca_result.Ex, 1);

% Initialize hidden variable expectations
Ex = zeros(q, N);
%ExxT = zeros(q, q, N);
Ez = ones(d, N);

W = ppca_result.W;
mu = ppca_result.mu;
y = t - repmat(mu, 1,N);
tsq = t .* t;
sig2v = ppca_result.sig2v;
invsig2v = 1 / ppca_result.sig2v;
invsig2f = 1 / ppca_result.sig2f;

initconverged = 0;
lastx = zeros(q, N);
while initconverged < 2
    % Update Ez
    if initconverged > 0
        WTW = W'*W;
        inlierMeans = W * (WTW \ (WTW + sig2v*speye(q)) * Ex);
        errs = y - inlierMeans;
        errs = errs .* errs;
        clear inlierMeans WTW
        Linlier = sqrt(invsig2v) .* exp(-1/2 * invsig2v .* errs);
        Loutlier = sqrt(invsig2f) .* exp(-1/2 * invsig2f .* tsq);
        clear errs
        Loutlier(and(Loutlier == 0, Linlier == 0)) = 1;
        Ez = (Linlier + Loutlier);
        Ez = Linlier ./ Ez;
        clear Linlier Loutlier
    end

    % Update Ex and ExxT
    lastx = Ex;
    for i = 1:N
        Si = mask(:,i);
        ySi = y(Si, i);
        WSi = W(Si, :);
        if numel(ySi) >= q
            invcovi = spdiags(Ez(Si, i) * invsig2v, 0, numel(ySi), numel(ySi));
            %invcovi = diag(Ez(Si, i) * invsig2v);
            %invM = inv(WSi' * invcovi * WSi + eye(q));
            M = WSi' * invcovi * WSi + speye(q);
            Ex(:, i) = M \ WSi' * invcovi * ySi;
            %ExxT(:, :, i) = invM + Ex(:,i) * Ex(:,i)';
        else
            Ex(:, i) = zeros(q,1);
            %ExxT(:, :, i) = zeros(q,q,1);
        end
    end
    
    xdiff = lastx - Ex;
    xdiff = xdiff .* xdiff;
    xdiffm = sum(sum(xdiff)) / numel(xdiff);
    if initconverged == 0 && xdiffm < .1
        initconverged = 1;
    elseif initconverged == 1 && xdiffm < .001
        initconverged = 2;
    end
    
    fprintf(1, '%f\n', xdiffm);
end

WTW = W'*W;
inlierMeans = W * (WTW \ (WTW + sig2v*speye(q)) * Ex);
estimate = struct('t', inlierMeans + repmat(mu, 1,N), 'Ex', Ex, 'Ez', Ez);

end
