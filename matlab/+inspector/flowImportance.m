function J = flowImportance(frame, flow)
%SHOWFLOWIMPORTANCE Summary of this function goes here
%   Detailed explanation goes here

imgops = basisflow.ImgOps(2, 1+3*2);
[gradx, grady] = imgops.computeDerivatives(basisflow.Utils.MatOfMatrix(frame));
gradx = basisflow.Utils.MatrixOfMat(gradx);
grady = basisflow.Utils.MatrixOfMat(grady);

flowx = reshape(flow(1:2:end,1), size(frame'))';
flowy = reshape(flow(2:2:end,1), size(frame'))';

J = abs(flowx .* gradx + flowy .* grady);

end

