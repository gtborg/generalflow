function drawBasis(ppcares, pimWidth, pimHeight)
%DRAWBASIS Summary of this function goes here
%   Detailed explanation goes here

%figure;

if isa(ppcares, 'gtsam.Values')
    values = ppcares;
    clear 'ppcares'
    q = size(values.at(gtsam.symbol('V',0)).matrix, 2);
    ppcares.W = zeros(2*pimHeight*pimWidth, q);
    for pix=0:(pimHeight*pimWidth-1)
        ppcares.W(1+2*pix : 1+2*pix+1, :) = values.at(gtsam.symbol('V', pix)).matrix();
    end
    ppcares.basissize(4:5) = [ pimWidth pimHeight ];
end

q = size(ppcares.W,2);
cols = ceil(sqrt(q));
if q <= cols*(cols-1)
    rows = cols - 1;
else
    rows = cols;
end

%Wnorm = ppcares.W ./ max(max(ppcares.W));
W = ppcares.W;
W(~isfinite(W)) = nan;
n = nanstd(W, 0, 1) * 3;
Wnorm = (1/sqrt(2)) * ppcares.W ./ (repmat(n, size(ppcares.W,1), 1) + 1e-10);
%Wnorm = ppcares.W;

width = ppcares.basissize(4);
height = ppcares.basissize(5);
img = zeros(rows*height, cols*width, 3, 'uint8');

for i = 1:q
    Wimg = reshape(Wnorm(:,i), 2, ppcares.basissize(5), ppcares.basissize(4));
    Wimg = permute(Wimg, [ 2 3 1 ]);
    x = mod(i-1, cols) + 1;
    y = floor((i-1) / cols) + 1;
    img(height*(y-1)+1:height*y, width*(x-1)+1:width*x, :) = inspector.computeColor(Wimg(:,:,1), Wimg(:,:,2));
    
    % Black border
    color = permute([200,100,0], [1,3,2]);
    img(height*(y-1)+1:height*y, width*(x-1)+1, :) = repmat(color, [height,1,1]);
    img(height*(y-1)+1:height*y, width*x, :) = repmat(color, [height,1,1]);
    img(height*(y-1)+1, width*(x-1)+1:width*x, :) = repmat(color, [width,1,1]);
    img(height*y,       width*(x-1)+1:width*x, :) = repmat(color, [width,1,1]);
end

image(img); axis off; axis image;

end

