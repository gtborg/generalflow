function colorImg = colorize(img)
%COLORIZE Colorize an image, making negative values red and positive values
%green

imgPos = img;
imgPos(img < 0) = 0;
imgNeg = img;
imgNeg(img > 0) = 0;

colorImg = zeros([ size(img), 3 ]);
colorImg(:,:,1) = -imgNeg;
colorImg(:,:,2) = imgPos;

end

