function drawSuperpixelBasis(basisValues, firstBasisKey, superpixelLabels, nSegments, imgWidth, imgHeight)
%DRAWSUPERPIXELBASIS Summary of this function goes here
%   Detailed explanation goes here

q = size(basisValues.at(firstBasisKey).matrix, 2);
sparseBasis = zeros(2*nSegments, q);
for l = 0:(nSegments-1) % Fill all segments
    sparseBasis(2*l+1 : 2*l+2, :) = basisValues.at(firstBasisKey + l).matrix;
end

% Expand into full basis
fullBasis = zeros(2*imgWidth*imgHeight, q);
for k = 1:q
    fullBasis(1:2:end, k) = reshape(generalflow.flow.fillSuperpixels( ...
        superpixelLabels, sparseBasis(1:2:end, k)), ...
        imgHeight*imgWidth, 1);
    fullBasis(2:2:end, k) = reshape(generalflow.flow.fillSuperpixels( ...
        superpixelLabels, sparseBasis(2:2:end, k)), ...
        imgHeight*imgWidth, 1);
end

% Draw
ppcares.basissize = [ 0 0 0 imgWidth imgHeight ];
ppcares.W = fullBasis;
inspector.drawBasis(ppcares);

end

