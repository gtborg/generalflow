function drawFlow(flow, width, height)
%DRAWFLOW Summary of this function goes here
%   Detailed explanation goes here

if size(flow, 3) == 1
    if size(flow, 2) ~= 1
        width = size(flow, 2);
        height = size(flow, 1) / 2;
    end
    flow = reshape(flow, 2, height, width);
    flow = permute(flow, [ 2 3 1 ]);
end

colored = inspector.computeColor(flow(:,:,1), flow(:,:,2));
colored(repmat(~isfinite(flow(:,:,1)), [1,1,3])) = 0;

image(colored); axis off; axis image;

end

