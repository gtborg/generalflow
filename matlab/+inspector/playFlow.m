function playFlow(imgDir, frameRange)

    function cleanup
        % Close OpenCV window and flush event queue
        cv.destroyWindow('Flow');
        for t = 1:20
            cv.waitKey(10);  % Make sure nothing is left in the GUI event queue
        end
    end

% If Ctrl-C is pressed, or when this function exits, be sure the OpenCV
% window closes and the event queue is flushed, otherwise the OpenCV window
% will appear to hang.
cleanupObj = onCleanup(@cleanup);

[format,first,last] = dataio.detectImageSequenceFormat(imgDir);

if exist('frameRange', 'var') && ~isempty(frameRange)
    first = frameRange(1);
    last = frameRange(2);
end

for i=(first+1):last
    fprintf('Frame %d\n', i);
    prev = cv.imread(sprintf(format, i-1), 0);
    cur = cv.imread(sprintf(format, i), 0);
    flow = basisflow.sparseFlow.computeFlow(prev, cur, 1280/16, 480/16, 1e-3);
    prevc = basisflow.Utils.Gray2rgb(prev);
    basisflow.sparseFlow.drawFlow(prevc, flow, 2);
    cv.imshow('Flow', prevc);
    cv.waitKey(0);
end

end
