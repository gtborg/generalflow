function drawSuperpixelFlow(measurements, superpixelLabels, imgWidth, imgHeight, scale)
%DRAWSUPERPIXELBASIS Summary of this function goes here
%   Detailed explanation goes here

if ~exist('scale', 'var') || isempty(scale)
    scale = 1.0;
end

sparseFlow = zeros(2*measurements.size, 1);
for l = 0:(measurements.size-1) % Fill all segments
    sparseFlow(2*l+1 : 2*l+2, 1) = [ measurements.at(l).ux; measurements.at(l).uy ];
end

% Expand into full flow
fullPairedFlow = zeros(2*imgHeight, imgWidth);
fullPairedFlow(1:2:end, :) = generalflow.flow.fillSuperpixels( ...
    superpixelLabels, sparseFlow(1:2:end, 1));
fullPairedFlow(2:2:end, :) = generalflow.flow.fillSuperpixels( ...
    superpixelLabels, sparseFlow(2:2:end, 1));

% Draw
inspector.drawFlow(scale * fullPairedFlow);

end

