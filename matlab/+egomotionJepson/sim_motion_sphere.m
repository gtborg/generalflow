function flow = sim_motion_sphere(world, motion)

flow = 3*zeros(size(world.X,2), 1);

for i=1:size(world.X, 2)
    proj = eye(3) - world.s(:,i)*world.s(:,i)';
    flow((3*i-2):(3*i)) = ...
        [ -(1/norm(world.X(:,i)))*proj  -skew_symm(world.s(:,i)) ] * motion;
end