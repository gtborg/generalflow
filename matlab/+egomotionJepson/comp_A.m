function A = comp_A(world, T)

A = zeros(2*size(world.X,2), size(world.X,2));

for i=1:size(world.X,2)
    A(2*i-1, i) = dot(world.n1(:,i), T);
    A(2*i, i) = dot(world.n2(:,i), T);
end

end