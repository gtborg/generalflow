function plot_flow(world, flow, varargin)

x = zeros(3, size(world.X,2));
u = zeros(3, size(world.X,2));
for i=1:size(world.X,2)
    x(:,i) = world.s(:,i);
    %if size(world.X,1) == 2
    %    u(:,i) = world.n1(:,i)*flow(2*i-1, 1) + world.n2(:,i)*flow(2*i, 1);
    %else
        u(:,i) = flow((3*i-2):(3*i), 1);
    %end
end

figure('color', 'white');
axes(varargin{:});
quiver3(x(1,:), x(2,:), x(3,:), u(1,:), u(2,:), u(3,:));
axis equal
camproj('perspective');
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
set(gca, 'xtick', []);
set(gca, 'ytick', []);
set(gca, 'ztick', []);
xlabel('X');
ylabel('Y');
zlabel('Z');