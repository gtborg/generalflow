function world = regular_world()

n = 25;
azs = -pi:(2*pi/n):pi;
alts = -pi:(2*pi/n):pi;

world.X = [];
for alt = alts
    r = cos(alt);
    spacing = 2*pi / n;
    steps = 2*pi*r / spacing;
    skip = round(n / steps);
    for az = azs(1:skip:size(azs,2))
        world.X = [ world.X [ cos(alt)*cos(az); cos(alt)*sin(az); sin(alt) ] * 10 ];
    end
end

world.s = [];
world.n1 = [];
world.q1 = [];
world.n2 = [];
world.q2 = [];
for i=1:size(world.X, 2)
    X = world.X(:,i);
    s = X / norm(X);
    
    n1 = cross(X, [0;1;0]);
    n1 = n1 / norm(n1);
    q1 = cross(s, n1);
    n2 = cross(n1, s);
    n2 = n2 / norm(n2);
    q2 = cross(s, n2);
    
    world.s = [ world.s s ];
    world.n1 = [world.n1 n1];
    world.q1 = [world.q1 q1];
    world.n2 = [world.n2 n2];
    world.q2 = [world.q2 q2];
end

end