world = rand_world('full', 400);

prefix = [getenv('HOME') '/papers/richard/pami2010-imgs/basis-depth-'];

s = .2;

flow = sim_depth_sphere(world, [ 1; 1; ones(398,1)*.1], [1;0;0], [0;0;0]);
plot_flow(world, flow);
hold on;
quiver3(world.s(1,1:2), world.s(2,1:2), world.s(3,1:2), ...
    flow([1,4])', flow([2,5])', flow([3,6])', s, 'LineWidth', 3);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix '0.eps'], 'epsc2');
delete(gcf);

flow = sim_depth_sphere(world, [ 1; 1; ones(398,1)*.1], [1;0;0], [1;0;0]);
plot_flow(world, flow);
hold on;
quiver3(world.s(1,1:2), world.s(2,1:2), world.s(3,1:2), ...
    flow([1,4])', flow([2,5])', flow([3,6])', s, 'LineWidth', 3);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'rx.eps'], 'epsc2');
delete(gcf);

flow = sim_depth_sphere(world, [ 1; 1; ones(398,1)*.1], [1;0;0], [0;1;0]);
plot_flow(world, flow);
hold on;
quiver3(world.s(1,1:2), world.s(2,1:2), world.s(3,1:2), ...
    flow([1,4])', flow([2,5])', flow([3,6])', s, 'LineWidth', 3);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'ry.eps'], 'epsc2');
delete(gcf);

flow = sim_depth_sphere(world, [ 1; 1; ones(398,1)*.1], [1;0;0], [0;0;1]);
plot_flow(world, flow);
hold on;
quiver3(world.s(1,1:2), world.s(2,1:2), world.s(3,1:2), ...
    flow([1,4])', flow([2,5])', flow([3,6])', s, 'LineWidth', 3);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'rz.eps'], 'epsc2');
delete(gcf);
