world = rand_world('full', 400);

prefix = [getenv('HOME') '/papers/richard/pami2010-imgs/basis-motion-'];

flow = sim_motion_sphere(world, [1;0;0;0;0;0]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'x.eps'], 'epsc2');
delete(gcf);

flow = sim_motion_sphere(world, [0;1;0;0;0;0]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'y.eps'], 'epsc2');
delete(gcf);

flow = sim_motion_sphere(world, [0;0;1;0;0;0]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'z.eps'], 'epsc2');
delete(gcf);

flow = sim_motion_sphere(world, [0;0;0;1;0;0]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'rx.eps'], 'epsc2');
delete(gcf);

flow = sim_motion_sphere(world, [0;0;0;0;1;0]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'ry.eps'], 'epsc2');
delete(gcf);

flow = sim_motion_sphere(world, [0;0;0;0;0;1]);
plot_flow(world, flow);
campos([-5.531320819835609 -14.062053003037134   8.034357937556855]);
saveas(gcf, [prefix 'rz.eps'], 'epsc2');
delete(gcf);
