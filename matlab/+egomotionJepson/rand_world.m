function world = rand_world(mode, n)

if isa(mode, 'char')
    if(strcmp(mode, 'forward'))
        if nargin < 2 || isempty(n)
            n = 100;
        end
        azs = -45:(90/sqrt(n)):45;
        alts = -45:(90/sqrt(n)):45;
        
        world.X = [];
        for az = azs
            for alt = alts
                world.X = [ world.X [ sin(az); sin(alt); 1 ] * (rand*10+1) ];
            end
        end
    elseif strcmp(mode, 'full')
        world.X = [];
        if nargin < 2 || isempty(n)
            n = 200;
        end
        for i=1:n
            Xi = [ rand; rand; rand ] - [ 0.5; 0.5; 0.5 ];
            Xi = Xi * 10.0;
            %if norm(Xi) < 5.0
            %    Xi = Xi + (5*Xi/norm(Xi));
            %end
            Xi = Xi * (10.0 / norm(Xi));
            world.X = [ world.X  Xi ];
        end
    end
else
    world.X = mode;
end

world.s = [];
world.n1 = [];
world.q1 = [];
world.n2 = [];
world.q2 = [];
for i=1:size(world.X, 2)
    X = world.X(:,i);
    s = X / norm(X);
    
    n1 = cross(X, [0;1;0]);
    n1 = n1 / norm(n1);
    q1 = cross(s, n1);
    n2 = cross(n1, s);
    n2 = n2 / norm(n2);
    q2 = cross(s, n2);
    
    world.s = [ world.s s ];
    world.n1 = [world.n1 n1];
    world.q1 = [world.q1 q1];
    world.n2 = [world.n2 n2];
    world.q2 = [world.q2 q2];
end

end