function r1 = comp_residual_jepson(world, flow, T)

C = [ comp_A(world,T) comp_B(world) ];
[U,S,V] = svd(C');
C_perp1 = V(:, (size(S,1)+1):end);

r1 = flow' * C_perp1;
r1 = dot(r1,r1);

end