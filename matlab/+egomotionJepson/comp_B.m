function B = comp_B(world)

B = zeros(2*size(world.X,2), 3);

for i=1:size(world.X,2)
    B(2*i-1,:) = cross(world.s(:,i), world.n1(:,i))';
    B(2*i,:) = cross(world.s(:,i), world.n2(:,i))';
end

end