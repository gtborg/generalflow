function flow = sim_depth_sphere(world, depth, trans, rot)

flow = 3*zeros(size(world.X,2), 1);

for i=1:size(world.X, 2)
    proj = eye(3) - world.s(:,i)*world.s(:,i)';
    flow((3*i-2):(3*i)) = -proj*trans*depth(i) - cross(world.s(:,i), rot);
end