function V = skew_symm(v)
%SKEW_SYMM Summary of this function goes here
%   Detailed explanation goes here

if min(size(v)) == 1 && max(size(v)) == 3
    V = [
        0   -v(3) v(2)
        v(3) 0   -v(1)
       -v(2) v(1) 0 ];
else
    error 'Need a 3-vector';
end

end

