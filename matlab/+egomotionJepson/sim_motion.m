function flow = sim_motion(world, motion)

flow = 2*zeros(size(world.X,2), 1);

for i=1:size(world.X, 2)
    flow(2*i-1) = dot(world.n1(:,i), motion(1:3)) ...
        / norm(world.X(:,i)) + dot(cross(world.s(:,i),world.n1(:,i)), motion(4:6));
    flow(2*i) = dot(world.n2(:,i), motion(1:3)) ...
        / norm(world.X(:,i)) + dot(cross(world.s(:,i),world.n2(:,i)), motion(4:6));
end