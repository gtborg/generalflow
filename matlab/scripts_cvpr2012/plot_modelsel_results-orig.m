inl1 = dlmread([dir1 '/inliers_out.txt']);
inl2 = dlmread([dir2 '/inliers_out.txt']);
inl3 = dlmread([dir3 '/inliers_out.txt']);

%[ inl1(:,2)-inl1(:,5) inl1(:,4)-inl1(:,7) ]
%[ inl2(:,2)-inl2(:,5) inl2(:,4)-inl2(:,7) ]

confusion = zeros(3,3);
confusionm = zeros(3,3);
for target=1:2
    confusion(1,target) = get_class_accuracy(target, inl1(:,[2 5]));
    confusion(2,target) = get_class_accuracy(target, inl2(:,[2 5]));
    %confusion(3,target) = get_class_accuracy(target, inl3(:,[2 5 8]));
    confusionm(1,target) = get_class_accuracy(target, inl1(:,[4 7]));
    confusionm(2,target) = get_class_accuracy(target, inl2(:,[4 7]));
    %confusioni(3,target) = get_class_accuracy(target, inl3(:,[4 7 10]));
end
confusion
confusionm

corr1 = all([inl1(:,2) > inl1(:,5)  inl1(:,2) > inl1(:,8)], 2);
corr2 = all([inl2(:,5) > inl2(:,2)  inl2(:,5) > inl2(:,8)], 2);
corr3 = all([inl3(:,8) > inl3(:,2)  inl3(:,8) > inl3(:,5)], 2);
corr1i = all([inl1(:,4) > inl1(:,7)  inl1(:,4) > inl1(:,10)], 2);
corr2i = all([inl2(:,7) > inl2(:,4)  inl2(:,7) > inl2(:,10)], 2);
corr3i = all([inl3(:,10) > inl3(:,4)  inl3(:,10) > inl3(:,7)], 2);
[ numel(find(corr1)) / numel(corr1)  numel(find(corr1i)) / numel(corr1i) ]
[ numel(find(corr2)) / numel(corr2)  numel(find(corr2i)) / numel(corr2i) ]
[ numel(find(corr3)) / numel(corr3)  numel(find(corr3i)) / numel(corr3i) ]

l1avg = mean(abs(inl1(:,2)-inl1(:,5)));
i1avg = mean(abs(inl1(:,4)-inl1(:,7)));
l2avg = mean(abs(inl2(:,2)-inl2(:,5)));
i2avg = mean(abs(inl2(:,4)-inl2(:,7)));
l3avg = mean(abs(inl3(:,2)-inl3(:,5)));
i3avg = mean(abs(inl3(:,4)-inl3(:,7)));

figure;
hold on;
plot(inl1(:,1), [ inl1(:,2) inl1(:,5) inl1(:,8) ]);
plot(inl1(:,1), [ inl1(:,4) inl1(:,7) inl1(:,10) ], '.');
legend 0 1 2 I0 I1 I2
%ylim([0 2e4]);

figure;
hold on;
plot(inl2(:,1), [ inl2(:,2) inl2(:,5) inl2(:,8) ]);
plot(inl2(:,1), [ inl2(:,4) inl2(:,7) inl2(:,10) ], '.');
legend 0 1 2 I0 I1 I2
%ylim([0 2e4]);

figure;
hold on;
plot(inl3(:,1), [ inl3(:,2) inl3(:,5) inl3(:,8) ]);
plot(inl3(:,1), [ inl3(:,4) inl3(:,7) inl3(:,10) ], '.');
legend 0 1 2 I0 I1 I2
%ylim([0 2e4]);

%plot(inl2(:,1), l1avg/l2avg * (inl2(:,2)-inl2(:,5)), 'r');
%plot(inl2(:,1), l1avg/i2avg * (inl2(:,4)-inl2(:,7)), 'c');
%plot(0:max(inl1(end,1), inl2(end,1)), zeros(max(inl1(end,1), inl2(end,1))+1, 1), 'm');

