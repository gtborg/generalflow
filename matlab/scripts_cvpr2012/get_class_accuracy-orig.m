function frac = get_class_accuracy(target, scores)
%GET_CLASS_ACCURACY Summary of this function goes here
%   Detailed explanation goes here

targetscores = scores(:,target);
otherscores = scores;
otherscores(:,target) = [];
corr = targetscores > max(otherscores, [], 2);
frac = numel(find(corr)) / numel(corr);

end

