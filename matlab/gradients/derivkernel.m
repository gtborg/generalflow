function kernel = derivkernel(ksize)
%DERIVKERNEL Summary of this function goes here
%   Detailed explanation goes here

krad = (ksize - 1) / 2;
sigma2 = (krad / 3) ^ 2;

pos = transpose(1:ksize) - 1 - krad;
normalizer = sum(exp(-0.5 * pos.*pos / sigma2));

kernel = normalizer * exp(-0.5 * pos.*pos / sigma2) .* (-pos/sigma2);

end

