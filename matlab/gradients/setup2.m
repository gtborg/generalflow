img1 = imread([getenv('HOME') '/data/GeneralFlow.2/iSight/short1/img_054.jpg']);
img1 = double(img1) / 255.0;

% Swap red and blue channels
tmp = img1(:,:,1);
img1(:,:,1) = img1(:,:,3);
img1(:,:,3) = tmp;
clear tmp;

% Convert to grayscale
img1 = rgb2gray(img1);

img2 = imread([getenv('HOME') '/data/GeneralFlow.2/iSight/short1/img_064.jpg']);
img2 = double(img2) / 255.0;

% Swap red and blue channels
tmp = img2(:,:,1);
img2(:,:,1) = img2(:,:,3);
img2(:,:,3) = tmp;
clear tmp;

% Convert to grayscale
img2 = rgb2gray(img2);
