function errs = showShifted(img, dx, dy, blurSize)
%SHOWSHIFTED Summary of this function goes here
%   Detailed explanation goes here

width = size(img,2);
height = size(img,1);

if blurSize >= 3
    gk1 = gaussiankernel(blurSize,0);
    gk = gk1 * gk1';
    gk = gk / sum(sum(gk));
    blurimg = imfilter(img, gk);
else
    blurimg = img;
end

shifted = shiftImage(blurimg, dx, dy);

actualShifted = zeros(size(img));
xs = reshape(repmat(1:width, [height 1]), [width*height 1]);
ys = repmat(transpose(1:height), [width 1]);

xss = xs + dx;
yss = ys + dy;

xys = [ xss yss xs ys ];
xys( xys(:,1) < 1, :) = [];
xys( xys(:,1) > width, :) = [];
xys( xys(:,2) < 1, :) = [];
xys( xys(:,2) > height, :) = [];

iss = (xys(:,1)-1)*height + xys(:,2);
is = (xys(:,3)-1)*height + xys(:,4);

actualShifted(is) = blurimg(iss);

quad = [ gray2rgb(img) gray2rgb(shifted); 3*colorGradient(actualShifted - shifted) gray2rgb(actualShifted) ];

imshow(quad);

errs = actualShifted - shifted;

end

