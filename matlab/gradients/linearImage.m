function [ val didx didy ] = linearImage(img, gradx, grady, x, y, Dx, Dy, sigma)
%LINEARIMAGE Summary of this function goes here
%   Detailed explanation goes here

width = size(img,2);
height = size(img,1);

if Dx ~= 0 || Dy ~= 0
    ksize = 2*round(3*sigma) + 3;
else
    ksize = 2*round(3*sigma) + 1;
end

gkx = gaussiankernel(sigma, ksize, Dx);
gky = gaussiankernel(sigma, ksize, Dy);

krad = (ksize - 1) / 2;

if x > krad && y > krad && x <= width-krad && y <= height-krad
    didx = sum(sum(gradx(y-krad:y+krad, x-krad:x+krad) .* (gky * gkx')));
    didy = sum(sum(grady(y-krad:y+krad, x-krad:x+krad) .* (gky * gkx')));
    val = sum(sum(img(y-krad:y+krad, x-krad:x+krad) .* (gky * gkx')));
else
    didx = 0.;
    didy = 0.;
    val = 0.;
end

end

