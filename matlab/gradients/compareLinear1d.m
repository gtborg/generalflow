function compareLinear1d(sigma, ksize)
%COMPARELINEAR1D Summary of this function goes here
%   Detailed explanation goes here

sig1 = [ zeros(10,1); ones(10,1); zeros(10,1) ];
sig2 = [ zeros(9,1); ones(10,1); zeros(11,1) ];

gk = gaussiankernel(sigma, ksize,0);

sig1 = signalApply(sig1,gk,transpose(1:length(sig1)));
sig2 = signalApply(sig2,gk,transpose(1:length(sig2)));

recon1 = zeros(length(sig1),1);
for i=1:length(sig1)
    recon1(i) = signalApply(sig1, gaussiankernel(sigma, ksize+2, 1), i);
end

plot(transpose(1:length(sig1)), [ sig1 sig2 recon1 ]);
legend('sig1','sig2','recon1');

end

