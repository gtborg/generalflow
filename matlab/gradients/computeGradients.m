function [ gradX gradY ] = computeGradients(img)
%COMPUTEGRADIENTS Summary of this function goes here
%   Detailed explanation goes here

dkernel = [ -0.5 0 0.5 ];
gradX = imfilter(img, dkernel);
gradY = imfilter(img, dkernel');

end

