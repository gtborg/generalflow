%img = imread([getenv('HOME') '/data/Parrot/2011-12-18/run_Outside10/imgs/img_27074.jpg']);
img = imread([getenv('HOME') '/data/Parrot/2011-12-18/run_Outside10/imgs/img_31317.jpg']);
img = double(img) / 255.0;

% Swap red and blue channels
tmp = img(:,:,1);
img(:,:,1) = img(:,:,3);
img(:,:,3) = tmp;
clear tmp;

% Convert to grayscale
imgrgb = img;
img = rgb2gray(img);
