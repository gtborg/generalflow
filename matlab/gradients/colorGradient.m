function colorGrad = colorGradient(grad)
%COLORGRADIENT Summary of this function goes here
%   Detailed explanation goes here

colorGrad = zeros([size(grad) 3]);

colorGrad(:,:,1) = -min(grad, 0.0);
colorGrad(:,:,3) = max(grad, 0.0);

end

