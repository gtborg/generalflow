function showImageAndGrads(img, gradX, gradY)
%SHOWIMAGEANDGRADS Summary of this function goes here
%   Detailed explanation goes here

width = size(img,2);
height = size(img,1);

imshow([gray2rgb(img) 1*colorGradient(gradX); 1*colorGradient(gradY) zeros(height,width,3)])

end

