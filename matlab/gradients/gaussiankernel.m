function kernel = gaussiankernel(sigma, ksize, offset)
%GAUSSIANKERNEL Summary of this function goes here
%   Detailed explanation goes here

if sigma <= 0
    error Sigma <= 0;
end

krad = (ksize - 1) / 2;
sigma2 = sigma ^ 2;

if krad ~= round(krad) || krad <= 0
    error ksize;
end

kernel = zeros(ksize,1);
for i=1:ksize
    pos = i - 1 - krad - offset;
    kernel(i) = exp(-0.5 * pos*pos / sigma2);
end

kernel = kernel / sum(kernel);

end

