function shifted = shiftImage(img, dx, dy)
%SHIFTIMAGE Summary of this function goes here
%   Detailed explanation goes here

[gradX gradY] = computeGradients(img);

shifted = img + dx*gradX + dy*gradY;

end

