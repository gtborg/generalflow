function result = signalApply(signal, kernel, xs)
%SIGNALAPPLY Summary of this function goes here
%   Detailed explanation goes here

ksize = length(kernel);
krad = (ksize - 1) / 2;

result = zeros(size(xs));
for i=1:length(xs)
    x = xs(i);
    if x-krad >= 1 && x+krad <= length(signal)
        signalPiece = signal(x-krad:x+krad);
        result(i) = sum(kernel .* signalPiece);
    else
        result(i) = 0.0;
    end
end

end

