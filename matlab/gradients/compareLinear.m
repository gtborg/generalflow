function diff = compareLinear(img, sigma)
%COMPARELINEAR Summary of this function goes here
%   Detailed explanation goes here

ksize = 2*round(3*sigma) + 1;

[height width] = size(img);

% Compute gradients
[gradx grady] = computeGradients(img);

% Create shifted image
expectedimg = [ img(:,2:end) zeros(height,1) ];
expectedimg = imfilter(expectedimg, gaussiankernel(sigma, ksize, 0) * transpose(gaussiankernel(sigma, ksize, 0)));

% Reconstruct missing pixels
reconimg = zeros(height,width);
recongradx = zeros(height,width);
recongrady = zeros(height,width);

for j=1:width
    for i=1:height
        [val dx dy] = linearImage(img, gradx, grady, j, i, 1, 0, sigma);
        reconimg(i,j) = val;
        recongradx(i,j) = dx;
        recongrady(i,j) = dy;
    end
end

diff = reconimg - expectedimg;

imshow([ gray2rgb(img) 3*colorGradient(diff); gray2rgb(expectedimg) gray2rgb(reconimg) ]);

end

