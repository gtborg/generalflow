function showColorGrads(img, scale)
%SHOWCOLORGRADS Summary of this function goes here
%   Detailed explanation goes here

width = size(img,2);
height = size(img,1);

imggray = rgb2gray(img);

[gradX gradY] = computeGradients(img);
[gradXgray gradYgray] = computeGradients(imggray);

tiled = [
    img scale*colorGradient(gradXgray) scale*colorGradient(max(gradX, [], 3))
    scale*colorGradient(gradX(:,:,1)) scale*colorGradient(gradX(:,:,2)) scale*colorGradient(gradX(:,:,3))];

kernel = gaussiankernel(1.0, 7, 0)';
imgF = imfilter(img, kernel);
gradXF = imfilter(gradX, kernel);
gradYF = imfilter(gradY, kernel);
gradXgrayF = imfilter(gradXgray, kernel);
gradYgrayF = imfilter(gradYgray, kernel);
scale = scale * 1.7;

tiled = [ tiled
    imgF scale*colorGradient(gradXgrayF) scale*colorGradient(max(gradXF, [], 3))
    scale*colorGradient(gradXF(:,:,1)) scale*colorGradient(gradXF(:,:,2)) scale*colorGradient(gradXF(:,:,3))];

kernel = gaussiankernel(2.0, 13, 0)';
imgF = imfilter(img, kernel);
gradXF = imfilter(gradX, kernel);
gradYF = imfilter(gradY, kernel);
gradXgrayF = imfilter(gradXgray, kernel);
gradYgrayF = imfilter(gradYgray, kernel);
scale = scale * 1.7;

tiled = [ tiled
    imgF scale*colorGradient(gradXgrayF) scale*colorGradient(max(gradXF, [], 3))
    scale*colorGradient(gradXF(:,:,1)) scale*colorGradient(gradXF(:,:,2)) scale*colorGradient(gradXF(:,:,3))];

figure; imshow(tiled);
title('Horizontal gradients');


end

