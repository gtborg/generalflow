function points = computePointCloud(p, K)
%COMPUTEPOINTCLOUD Summary of this function goes here
%   Detailed explanation goes here

points = zeros(numel(p), 3);

xctr = K(1,3);
yctr = K(2,3);
f = (K(1,1)+K(2,2)) / 2;

for i=0:size(p,1)-1
    for j=0:size(p,2)-1
        x = j - xctr;
        y = i - yctr;
   
        pi = p(i+1,j+1);
        if pi > 1e-4
            X = 1/pi;
        else
            X = inf;
        end
        %if X > 1e4
        %    X = 1e4;
        %end
        Y = x*X/f;
        Z = y*X/f;
        
        points(i*size(p,2) + j + 1, :) = [ X Y Z ];
    end
end

figure;plot3(points(:,1), points(:,2), points(:,3), '.'); xlabel x; ylabel y; zlabel z;
axis equal
set(gca,'ydir','reverse');set(gca,'zdir','reverse');

end

