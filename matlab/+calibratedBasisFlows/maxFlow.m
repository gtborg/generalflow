function m = maxFlow(uv)
%MAXFLOW Summary of this function goes here
%   Detailed explanation goes here

u = uv(1:2:end);
v = uv(2:2:end);
m = sqrt(max(u.*u + v.*v));

end

