function [Fu,Fv] = basisFlow(Omega, width, height, K, distCoeffs, no_distortion)
    Fu = zeros(height, width);
    Fv = zeros(height, width);
    
    f = 0.5*(K(1,1)+K(2,2));
    C = [K(1,3) K(2,3)]';
    
    if ~no_distortion
        reverseDistCoeffs = reverseDistortion(distCoeffs);
        k = distCoeffs;
    end
    for v=1:height
        for u=1:width
            pd_pix = [u v]';
            
            pd_img = (pd_pix - C)/f;
            
            if (no_distortion)
                pn_img = pd_img;    % if no distortion
            else
                pn_img = idealFromDistorted(pd_img, reverseDistCoeffs);
            end
            
            x = pn_img(1);
            y = pn_img(2);
            
             if (~no_distortion)
                 thres = 0.5;
                 if ( pd_img(1)>thres || pd_img(1)<-thres || pd_img(1)>thres || pd_img(2)<-thres )
                     Fu(v,u) = 0;
                     Fv(v,u) = 0;
                     continue;
                 end
             end
            
            r = sqrt(x^2 + y^2);
            
%Compute Jacobian
%d(pd_pix)/dt = d(pd_pix)/d(pd_img) * d(pd_img)/d(pn_img) * d(pn_img)/dt

            Jd_pix = [f 0; 0 f];    % d(pd_pix)/d(pd_img)
            
            if (no_distortion)
                Jd_img = eye(2);    % if no distortion
            else
                J11 = 1 + k(1)*r^2 + k(2)*r^4 + 4*r*x^2*(k(1) + 2*k(2)*r^2) + 2*k(3)*y + 4*k(4)*x*(r + 1);
                J12 = 4*x*y*r^2*(k(1) + 2*k(2)*r^2) + 2*k(3)*x + 4*k(4)*r*y;
                J21 = 4*x*y*r^2*(k(1) + 2*k(2)*r^2) + 2*k(4)*y + 4*k(3)*r*x;
                J22 = 1 + k(1)*r^2 + k(2)*r^4 + 4*r*y^2*(k(1) + 2*k(2)*r^2) + 2*k(4)*x + 4*k(3)*y*(r + 1);
                Jd_img = [J11 J12; J21 J22];
            end
            
            %Jd_img;
            
            
            B = [x*y/f      -(f+x^2/f)   y;
                f+y^2/f     -(x*y)/f     -x];
            
            
            flow = Jd_pix * Jd_img* B*Omega;
            Fu(v,u) = flow(1);
            Fv(v,u) = flow(2);

        end
    end
end