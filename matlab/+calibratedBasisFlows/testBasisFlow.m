% Modify expression to add input arguments.
% Example:
%   a = [1 2 3; 4 5 6]; 
%   foo(a);

Omega = [0 1 0]';
width = 320;
height = 240;
distCoeffs = [ 0.26462978138436127
    -0.614880362867904
    0.002895194904756873
    -0.00073686434670106038];


%C = [170.45850190085955 115.75310156967717];
C = [160 120];
F = [ 219.11684355649891 219.37003067554045];
K = [F(1) 0 C(1); 0 F(2) C(2); 0 0 1];

[Fx Fy] = basisFlow(Omega, width, height, K, distCoeffs, false);


gridSize = 20;
[X,Y] = meshgrid(1:gridSize:width, 1:gridSize:height);

figure;
quiver(X,Y,Fx(1:gridSize:height, 1:gridSize:width),Fy(1:gridSize:height, 1:gridSize:width));

axis([0 320 0 240])
