function img = drawColoredFlow(uv, norm)
%DRAWCOLOREDFLOW Summary of this function goes here
%   Detailed explanation goes here

if ~exist('norm', 'var') || norm
    m = maxFlow(uv);
else
    m = 1;
end
img = computeColor(1/m * reshape(uv(1:2:end),80,60)', 1/m * reshape(uv(2:2:end),80,60)');
imshow(img);

end

