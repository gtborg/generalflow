function pixd = distort(pNormalized,  K, distCoeffs)
    pd = distortedFromIdeal(pNormalized, distCoeffs);
    pix = K*[pd; 1];
    pixd = pix(1:2);
end