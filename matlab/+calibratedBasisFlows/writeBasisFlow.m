function writeBasisFlow(w,h,K,filename)
%WRITEBASISFLOW Summary of this function goes here
%   Detailed explanation goes here

% Interleave u and v
    function Wcol = basis(vel)
        [u,v] = basisFlowUndistorted(vel, w, h, K);
        uv = [ reshape(u', w*h, 1) reshape(v', w*h, 1) ];
        Wcol = reshape(uv', size(uv,1)*2, 1);
    end

W(:,1) = basis([1;0;0; 0;0;0]);
W(:,2) = basis([0;1;0; 0;0;0]);
W(:,3) = basis([0;0;1; 0;0;0]);
%W(:,4) = basis([0;0;0; 1;0;0]);
%W(:,5) = basis([0;0;0; 0;1;0]);
%W(:,6) = basis([0;0;0; 0;0;1]);

dlmwrite([filename '.W'], W, 'delimiter', ' ', 'precision', 15);
dlmwrite([filename '.sigmas'], [0.02 1.0], 'delimiter', ' ', 'precision', 15);
dlmwrite([filename '.basissize'], [ size(W,2) 2 size(W,1)/2 ], 'delimiter', ' ', 'precision', '%d');


end

