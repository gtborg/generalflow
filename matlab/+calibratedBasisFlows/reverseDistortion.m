function a = reverseDistortion(distCoeffs)
    T = [];
    e = [];
    for x=-0.5:.02:0.5
        for y=-0.5:.02:0.5
            pd = distortedFromIdeal([x y], distCoeffs);
            pi = [x y]';
            
            ud = pd(1); vd = pd(2);
            ui = pi(1); vi = pi(2);
            r = norm(pd);
            u = [-ud*r^2, -ud*r^4, -2*ud*vd, -(r^2+2*ud^2), ui*r^4, ui*ud*r^2, ui*vd*r^2, ui*r^2];
            v = [-vd*r^2, -vd*r^4, -(r^2+2*vd^2), -2*ud*vd, vi*r^4, vi*ud*r^2, vi*vd*r^2, vi*r^2];
            T = [T; u; v];
            e = [e; ud - ui; vd - vi];
        end
    end
    
    a = pinv(T)*e;
end
