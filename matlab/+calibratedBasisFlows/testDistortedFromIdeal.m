% Modify expression to add input arguments.
% Example:
%   a = [1 2 3; 4 5 6]; 
%   foo(a);

distCoeffs = [ 0.26462978138436127    -0.614880362867904     0.002895194904756873    -0.00073686434670106038];

%distCoeffs = [-0.229 0.196 0 0];
%distCoeffs = [-0.2 0.2 0 0]
    
pd = [];
pn = [];
for x=-.7:.1:.7
    for y=-.7:.1:.7
        pd = [pd distortedFromIdeal([x y], distCoeffs)];
        pn = [pn [x y]'];
    end
end

hold on
plot(pd(1,:), pd(2,:), '.')
%pl = plot(pn(1,:), pn(2,:), '*');
%set(pl,'Color','red');
axis([-1 1 -1 1])
hold off