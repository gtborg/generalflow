% Modify expression to add input arguments.
% Example:
%   a = [1 2 3; 4 5 6]; 
%   foo(a);

distCoeffs = [ 0.26462978138436127
    -0.614880362867904
    0.002895194904756873
    -0.00073686434670106038];
    
C = [170.45850190085955 115.75310156967717];
F = [ 219.11684355649891 219.37003067554045];
K = [F(1) 0 C(1); 0 F(2) C(2); 0 0 1];

a = reverseDistortion(distCoeffs);

    
pd = [];
for x=-0.7:.1:0.7
    for y=-0.7:.1:0.7
        pd = [pd distortedFromIdeal([x y], distCoeffs)];
    end
end

pn = [];
for i=1:size(pd,2)
    pdist = pd(:,i);
    pn = [pn idealFromDistorted(pdist, a)];
end
pn
hold on
plot(pd(1,:), pd(2,:), '.')
pl = plot(pn(1,:), pn(2,:), '*');
set(pl,'Color','red');
hold off