function pd = distortedFromIdeal(p, distCoeffs)
    r = norm(p);
    x = p(1);
    y = p(2);
    xd = x*( 1 + distCoeffs(1)*r^2 + distCoeffs(2)*r^4 ) + 2*distCoeffs(3)*x*y + distCoeffs(4)*(r^2 + 2*x^2);
    yd = y*( 1 + distCoeffs(1)*r^2 + distCoeffs(2)*r^4 ) + 2*distCoeffs(4)*x*y + distCoeffs(3)*(r^2 + 2*y^2);
%     xd = x*(1 + distCoeffs(1)*r^2 + distCoeffs(2)*(r^4)) ;
%     yd = y*(1 + distCoeffs(1)*r^2 + distCoeffs(2)*(r^4)) ;
    pd = [xd yd]';
end