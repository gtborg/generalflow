function [Fu,Fv] = basisFlowUndistorted(velocity, width, height, K)
%BASISFLOWUNDISTORTED Summary of this function goes here
%   Detailed explanation goes here

    Fu = zeros(height, width);
    Fv = zeros(height, width);
    
    f = 0.5*(K(1,1)+K(2,2));
    C = [K(1,3) K(2,3)]';
    
    vrot = velocity(1:3);
    vtrans = velocity(4:6);
    
    for v=1:height
        for u=1:width
            pd_pix = [u v]';
            
            pn_img = (pd_pix - C);
                        
            x = pn_img(1);
            y = pn_img(2);
            
            %A = [f  0 x
            %     0 -f y];
            A = [x -f  0
                 y 0 -f];
            
            %B = [x*y/f      -(f+x^2/f)   y;
            %    f+y^2/f     -(x*y)/f     -x];
            B = [  y          x*y/f      -(f+x^2/f);
                   -x        f+y^2/f     -(x*y)/f];
            
            flow = A*vtrans + B*vrot;
            Fu(v,u) = flow(1);
            Fv(v,u) = flow(2);

        end
    end
end