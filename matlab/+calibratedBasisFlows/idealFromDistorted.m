function pd = idealFromDistorted(pd, reverseDistCoeffs)
    a = reverseDistCoeffs;
    r = norm(pd);
    ud = pd(1);
    vd = pd(2);
    G = (a(5)*r^2 + a(6)*ud + a(7)*vd + a(8))*r^2 + 1;
    u = ud + ud*(a(1)*r^2 + a(2)*r^4) + 2*a(3)*ud*vd + a(4)*(r^2 + 2*ud^2);
    v = vd + vd*(a(1)*r^2 + a(2)*r^4) + 2*a(4)*ud*vd + a(3)*(r^2 + 2*vd^2);
    pd = [u v]'/G;
end