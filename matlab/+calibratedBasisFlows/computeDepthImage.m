function [v,p] = computeDepthImage(ppcares, K, width)
%COMPUTEDEPTHMAP Summary of this function goes here
%   Detailed explanation goes here

avgx = mean(ppcares.Ex,2);
avgflow = ppcares.W * avgx;

uc = reshape(avgflow(1:2:end), width, size(avgflow,1)/2/width)';
vc = reshape(avgflow(2:2:end), width, size(avgflow,1)/2/width)';

xctr = K(1,3);
yctr = K(2,3);
f = (K(1,1)+K(2,2)) / 2;
q = size(ppcares.W,2);

I = size(ppcares.W,1)/2;
%A = spalloc(numel(ppcares.W), I, numel(ppcares.W));
%B = spalloc(numel(ppcares.W), 3*size(ppcares.W,2), 3*numel(ppcares.W));
%J = spalloc(numel(ppcares.W), I+6*q, numel(ppcares.W));

    function [e,J] = Tresid(tq)
        
        Acoeff = zeros(I*2*q, 3);
        Bcoeff = zeros(I*2*3*q, 3);
        Jcoeff = zeros(I*2*3*q, 3);
        
        for k=1:size(ppcares.W,2)
            for i=0:size(uc,1)-1
                for j=0:size(uc,2)-1
                    x = j - xctr;
                    y = i - yctr;
                    
                    %A = [f  0 x
                    %     0 -f y];
                    a = [x -f  0
                        y 0 -f];
                    
                    %B = [x*y/f      -(f+x^2/f)   y;
                    %    f+y^2/f     -(x*y)/f     -x];
                    b = [  y          x*y/f      -(f+x^2/f);
                        -x        f+y^2/f     -(x*y)/f];
                    
                    pixind1 = i*size(uc,2) + j;
                    pixind = (k-1)*numel(uc) + pixind1;
                    tqindt = (k-1)*3;
                    
                    [Ai,Aj,As] = find(a*tq(I+q*3+tqindt+1 : I+q*3+tqindt+3));
                    [Ji,Jj,Js] = find(a);
                    [Bi,Bj,Bs] = find(b);
                    Ai = Ai + 2*pixind;
                    Aj = Aj + pixind1;
                    Ji = Ji + 2*pixind;
                    Jj = Jj + I + q*3 + tqindt;
                    Bi = Bi + 2*pixind;
                    Bj = Bj + I + tqindt;
                    
                    %assert(all(all(Acoeff(2*pixind+1:2*pixind+numel(As), :)==0)));
                    %assert(all(all(Bcoeff(6*pixind+1:6*pixind+numel(Bs), :)==0)));
                    %assert(all(all(Jcoeff(6*pixind+1:6*pixind+numel(Js), :)==0)));
                    Acoeff(2*pixind+1:2*pixind+numel(As), :) = [Ai,Aj,As];
                    Bcoeff(6*pixind+1:6*pixind+numel(Bs), :) = [Bi,Bj,Bs];
                    Jcoeff(6*pixind+1:6*pixind+numel(Js), :) = [Ji,Jj,Js];
                    
                    %A(2*pixind+1:2*pixind+2, pixind1+1) = a*tq(I+size(ppcares.W,2)*3+tqindt+1 : I+size(ppcares.W,2)*3+tqindt+3);
                    %J(2*pixind+1:2*pixind+2, I+size(ppcares.W,2)*3+tqindt+1 : I+size(ppcares.W,2)*3+tqindt+3) = a;
                    %B(2*pixind+1:2*pixind+2, tqindt+1:tqindt+3) = b;
                end
            end
        end
        
        Acoeff(Acoeff(:,3)==0, :) = [];
        Bcoeff(Bcoeff(:,3)==0, :) = [];
        Jcoeff(Jcoeff(:,3)==0, :) = [];
        
        % Rotation first
        %C = [ A B ];
        Ccoeff = [Acoeff; Bcoeff];
        C = sparse(Ccoeff(:,1), Ccoeff(:,2), Ccoeff(:,3), I*2*q, 3*q+I);
        J = sparse(Jcoeff(:,1), Jcoeff(:,2), Jcoeff(:,3), I*2*q, 6*q+I);
        v = reshape(ppcares.W, numel(ppcares.W), 1);
        %q = tq(1:I+size(ppcares.W,2)*3);
        
        %e = v - C * q;
        J(:,1:I+q*3) = -C;
        
        % Add small prior on everything
        %e = [ e; 0.000001 * tq ];
        %J = [ J; speye(size(tq,1))*0.000001 ];
        
        Ab = [ J v ];
        R = qr(Ab,0);
        
        J = R(1:I+q*6, 1:I+q*6);
        e = J * tq + R(1:I+q*6, end);
        %fprintf('Evaluated error = %d\n', dot(e,e));
    end

options = optimset('Display', 'iter', 'Jacobian','on', 'MaxIter',500);
tq0 = zeros(6*size(ppcares.W,2) + size(ppcares.W,1)/2, 1);
tqhat = fsolve(@Tresid, tq0, options);

v = reshape(tqhat(size(ppcares.W,1)/2+1:end), 6, size(ppcares.W,2));
p = reshape(tqhat(1:size(ppcares.W,1)/2), size(uc'))';

end

