function mapvid(rposes, rposesgt, outformat)

N = length(rposes);

%figure;

for i=2:N
    clf;
    plot([ rposes(1:i, 1) rposesgt(1:i, 1) ], [ rposes(1:i, 2) rposesgt(1:i, 2) ], 'LineWidth', 2);
    xlabel('meters');
    ylabel('meters');
    legend('Visual odometry', 'Laser scan matching');
    set(gcf, 'PaperPositionMode', 'auto');
    
    drawnow;
    print('-dtiff', sprintf(outformat, i-1), '-r0');

end


end
