function draw_motion(motion, out_format, out_script)

arrow_pts = [
    [ 0 0 ]
    [ -10 10 ]
    [ 10 10 ] ]';

outf = fopen(out_script, 'w');

length = size(motion, 2);
scale = (50 - 2) / max(motion(1,:));

for i=1:length
    pts = arrow_pts;
    pts(2,:) = pts(2,:) - motion(1,i)*scale;
    ang = motion(2,i) * 15.0;
    pts = [ cos(ang) -sin(ang); sin(ang) cos(ang) ] * pts;
    pts(1,:) = pts(1,:) + 50;
    pts(2,:) = pts(2,:) + 50;
    fprintf(outf, 'convert -size 100x100 xc:white ');
    fprintf(outf, '-strokewidth 2 ');
    fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', 50,50,255);
    fprintf(outf, '-draw "line %f,%f %f,%f" ', 50,50, pts(1,1),pts(2,1));
    fprintf(outf, '-draw "line %f,%f %f,%f" ', pts(1,1),pts(2,1), pts(1,2),pts(2,2));
    fprintf(outf, '-draw "line %f,%f %f,%f" ', pts(1,1),pts(2,1), pts(1,3),pts(2,3));
    fprintf(outf, out_format, i);
    fprintf(outf, '\n');
    if mod(i, 100) == 0
        fprintf(1, '%d\n', i);
    end
end

end
