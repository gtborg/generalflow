function join_images(format1, sub1, skip1, format2, sub2, skip2, outformat)

i1 = skip1+1;
i2 = skip2+1;
iout = 1;

while(1)
    img1 = imread(sprintf(format1, i1));
    img2 = imread(sprintf(format2, i2));
    
    if ~isempty(sub1)
        img1 = img1(sub1(1):sub1(2), sub1(3):sub1(4), :);
    end
    if ~isempty(sub2)
        img2 = img2(sub2(1):sub2(2), sub2(3):sub2(4), :);
    end
    
    width = size(img1, 2) + size(img2, 2);
    height = max([ size(img1, 1) size(img2, 1) ]);
    
    x1 = 1;
    x2 = size(img1, 2) + 1;
    y1 = round((height - size(img1, 1)) / 2) + 1;
    y2 = round((height - size(img2, 1)) / 2) + 1;
    
    imgout = ones(height, width, 3, 'uint8') * uint8(255);
    imgout(y1:y1+size(img1,1)-1, x1:x2-1, :) = img1;
    imgout(y2:y2+size(img2,1)-1, x2:width, :) = img2;
    
    imwrite(imgout, sprintf(outformat, iout));
    
    i1 = i1 + 1;
    i2 = i2 + 1;
    iout = iout + 1;
    
    if mod(iout, 30) == 0
        fprintf(1, '%d\n', iout);
    end
end
