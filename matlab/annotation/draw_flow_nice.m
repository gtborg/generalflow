function draw_flow_nice(ppcares, flow_vec, flow_mask, width, mode)

height = size(flow_vec, 1) / width / 2;

circle = zeros(9,9,2);
circle(:,:,1) = repmat(-4:4, 9, 1) / 4;
circle(:,:,2) = circle(:,:,1)';
circle = circle(:,:,1) .^ 2 + circle(:,:,2) .^ 2;
circle(circle <= 1) = 1;
circle(circle > 1) = 0;
circle = logical(circle);

gridspx = 0;
gridspy = 0;
figure;

set(gcf, 'PaperPositionMode', 'auto');
img = ones(480, 640, 3);
imshow(img);
hold on;

if(gridspx == 0 || gridspy == 0)
    gridspx = floor(size(img, 2) / width);
    gridspy = floor(size(img, 1) / height);
end

est = gmestimate(ppcares, flow_vec, flow_mask);

mask = flow_mask;
mask = min(mask(1:width*height), mask(width*height+1:end));
mask = reshape(mask, width, height)';

if mode == 1
    for x = 1:width
        for y = 1:height
            u = x * gridspx - (gridspx/2) - 4;
            v = y * gridspy - (gridspx/2) - 4;
            if mode == 1
                if(fmask(y,x) == 1 && mask(y,x) ~= 1.0)
                    cr = img(v:v+8, u:u+8, 1);
                    cg = img(v:v+8, u:u+8, 2);
                    cr(circle) = uint8(255 * (1.0-mask(y,x)));
                    cg(circle) = uint8(255 * mask(y,x));
                    img(v:v+8, u:u+8, 1) = cr;
                    img(v:v+8, u:u+8, 2) = cg;
                    img(v:v+8, u:u+8, 3) = uint8(zeros(9,9));
                end
            end
        end
    end
elseif mode == 2
    xs = reshape(repmat((1:width)', 1, height), width*height, 1);
    ys = reshape(repmat(1:height, width, 1), width*height, 1);
    us = xs .* gridspx - (gridspx/2) - 4;
    vs = ys .* gridspy - (gridspx/2) - 4;
    lines = [ us, us+est.t(1:width*height), vs, vs+est.t(width*height+1:end) ];
    weights = min(est.Ez(1:width*height), est.Ez(width*height+1:end));
    mask = ~logical(min(flow_mask(1:width*height), flow_mask(width*height+1:end)));
    colors = [ 1-weights weights zeros(width*height, 1) ];
    colors(mask, 1) = .5;
    colors(mask, 2) = .5;
    colors(mask, 3) = .5;

    colors(:,1) = .5;
    colors(:,2) = .5;
    colors(:,3) = 1.0;

    % quiver(lines(:,1), lines(:,2), lines(:,3), lines(:,4));
    set(gca, 'ColorOrder', colors);
    plot(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
elseif mode == 3
    xs = reshape(repmat((1:width)', 1, height), width*height, 1);
    ys = reshape(repmat(1:height, width, 1), width*height, 1);
    us = xs .* gridspx - (gridspx/2) - 4;
    vs = ys .* gridspy - (gridspx/2) - 4;
    mask = ~logical(min(flow_mask(1:width*height), flow_mask(width*height+1:end)));
    lines = [ us, us+flow_vec(1:width*height)*2, vs, vs+flow_vec(width*height+1:end)*2 ];
    lines(mask, :) = [];
%     weights = min(est.Ez(1:width*height), est.Ez(width*height+1:end));
%     weights(mask, :) = [];
%     colors = [ 1-weights weights zeros(size(lines, 1), 1) ];
    colors = ones(width*height, 3);
    colors(:,1) = 0.0;
    colors(:,2) = 0.0;
    colors(:,3) = 1.0;
    if ~isempty(lines)
        set(gca, 'ColorOrder', colors);
        %plot(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
        line(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
    end

end

