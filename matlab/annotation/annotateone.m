function annotateone(infile, outfile, outscript, flow, width)

height = size(flow, 1) / width / 2;

imw = width*20;
imh = height*20;

xs = reshape(repmat((1:width)', 1, height), width*height, 1);
ys = reshape(repmat(1:height, width, 1), width*height, 1);

gridspx = floor(imw / width);
gridspy = floor(imh / height);
us = xs .* gridspx - (gridspx/2) - 4;
vs = ys .* gridspy - (gridspx/2) - 4;

% Open output file
outf = fopen(outscript, 'w');

for i = 1:size(flow, 2)
    lines = [ us, us+flow(1:width*height, i)*5, vs, vs+flow(width*height+1:end, i)*5 ];
    if ~isempty(infile)
        fprintf(outf, ['convert ' infile ' ']);
    else
        fprintf(outf, 'convert -size %dx%d xc:white ', imw, imh);
    end
    fprintf(outf, '-strokewidth 2 ');
    fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', 0, 200, 60);
    for j = 1:size(lines, 1)
        fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
    end
    fprintf(outf, [outfile '\n']);
end


end
