function annotate(inFormat, outFormat, outScript, ppca_result, estall, flow, width, mode)

if ~isempty(flow)
    height = size(flow, 1) / width / 2;

    xs = reshape(repmat((1:width)', 1, height), width*height, 1);
    ys = reshape(repmat(1:height, width, 1), width*height, 1);

    % Read first image to get size
    imf = sprintf(inFormat, 1);
    dims = size(imread(imf));
    gridspx = floor(dims(2) / width);
    gridspy = floor(dims(1) / height);
    us = xs .* gridspx - (gridspx/2) - 4;
    vs = ys .* gridspy - (gridspx/2) - 4;
elseif isstruct(estall)
    height = size(estall.t, 1) / width / 2;

    xs = reshape(repmat((1:width)', 1, height), width*height, 1);
    ys = reshape(repmat(1:height, width, 1), width*height, 1);
    
    dims = [ height*20 width*20 ];
    gridspx = floor(dims(2) / width);
    gridspy = floor(dims(1) / height);
    us = xs .* gridspx - (gridspx/2) - 4;
    vs = ys .* gridspy - (gridspx/2) - 4;
end

% Open output file
outf = fopen(outScript, 'w');

if strcmp(mode, 'observed')
    for i = 1:size(estall.t, 2)
        t = estall.t(:,i);
        Ez = estall.Ez(:,i);
        
        mask = ~logical(min(flow.flow_mask(1:width*height, i), flow.flow_mask(width*height+1:end, i)));
        lines = [ us, us+flow.flow_vec(1:width*height, i)*2, vs, vs+flow.flow_vec(width*height+1:end, i)*2 ];
        lines(mask, :) = [];
        weights = min(Ez(1:width*height), Ez(width*height+1:end));
        weights(mask, :) = [];
        colors = round([ 1-weights weights zeros(size(lines, 1), 1) ] * 255);

        fprintf(outf, ['convert ' inFormat ' '], i);
        fprintf(outf, '-strokewidth 2 ');
        for j = 1:size(lines, 1)
            fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', colors(j,1), colors(j,2), colors(j,3));
            fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
        end
        fprintf(outf, [outFormat '\n'], i);
        
        if mod(i, 100) == 0
            fprintf(1, '%d\n', i);
        end
    end
elseif strcmp(mode, 'observed_nolab')
    for i = 1:size(flow, 2)
    %for i = 2350:2460
        mask = ~isfinite(flow(1:width*height, i)) & isfinite(flow(width*height+1:end, i));
        lines = [ us, us+flow(1:width*height, i)*2, vs, vs+flow(width*height+1:end, i)*2 ];
        lines(mask, :) = [];
        fprintf(outf, ['convert ' inFormat ' '], i);
        fprintf(outf, '-strokewidth 2 ');
        fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', 0, 255, 255);
        for j = 1:size(lines, 1)
            fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
        end
        fprintf(outf, [outFormat '\n'], i);
        
        if mod(i, 100) == 0
            fprintf(1, '%d\n', i);
        end
    end
elseif strcmp(mode, 'dense')
    imw = width*20;
    imh = height*20;
    
    for i = 1:size(estall.t, 2)
    %for i = 2350:2460
        t = estall.t(:,i);
        Ez = estall.Ez(:,i);
        mask = ~isfinite(flow(1:width*height, i)) & isfinite(flow(width*height+1:end, i));
        lines = [ us, us+t(1:width*height)*4, vs, vs+t(width*height+1:end)*4 ];
        weights = min(Ez(1:width*height), Ez(width*height+1:end));
        lines(mask, :) = [];
        weights(mask, :) = [];
        colors = round([ 1-weights weights zeros(size(lines, 1), 1) ] * 255);
        colors(mask, 1) = 50;
        colors(mask, 2) = 205;
        %colors = repmat([ 0, 200, 0 ], size(t, 1), 1);
        if ~isempty(inFormat)
            fprintf(outf, ['convert ' inFormat ' '], i);
        else
            fprintf(outf, 'convert -size %dx%d xc:white ', imw, imh);
        end
        fprintf(outf, '-strokewidth 2 ');
        for j = 1:size(lines, 1)
            fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', colors(j,1), colors(j,2), colors(j,3));
            fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
        end
        fprintf(outf, [outFormat '\n'], i);
        if mod(i, 100) == 0
            fprintf(1, '%d\n', i);
        end
    end
elseif strcmp(mode, 'dense_nolab')
    imw = width*20;
    imh = height*20;
    
    for i = 1:size(estall.t, 2)
    %for i = 2350:2460
        t = estall.t(:,i);
        Ez = estall.Ez(:,i);
        mask = ~logical(min(flow.flow_mask(1:width*height, i), flow.flow_mask(width*height+1:end, i)));
        lines = [ us, us+t(1:width*height)*4, vs, vs+t(width*height+1:end)*4 ];
        colors = repmat([ 200, 0, 200 ], size(t, 1), 1);
        if ~isempty(inFormat)
            fprintf(outf, ['convert ' inFormat ' '], i);
        else
            fprintf(outf, 'convert -size %dx%d xc:white ', imw, imh);
        end
        fprintf(outf, '-strokewidth 6 ');
        for j = 1:size(lines, 1)
            fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', colors(j,1), colors(j,2), colors(j,3));
            fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
        end
        fprintf(outf, [outFormat '\n'], i);
        if mod(i, 100) == 0
            fprintf(1, '%d\n', i);
        end
    end
elseif strcmp(mode, 'bases')
    height = size(ppca_result.W, 1) / width / 2;

    xs = reshape(repmat((1:width)', 1, height), width*height, 1);
    ys = reshape(repmat(1:height, width, 1), width*height, 1);
    
    imw = width*20;
    imh = height*20;
    
    gridspx = floor(imw / width);
    gridspy = floor(imh / height);
    us = xs .* gridspx - (gridspx/2) - 4;
    vs = ys .* gridspy - (gridspx/2) - 4;

    for i = 1:size(ppca_result.W, 2)
        lines = [ us, us+ppca_result.W(1:width*height, i)*5, vs, vs+ppca_result.W(width*height+1:end, i)*5 ];
        fprintf(outf, 'convert -size %dx%d xc:white ', imw, imh);
        fprintf(outf, '-strokewidth 2 ');
        fprintf(outf, '-stroke "rgb(%d, %d, %d)" ', 255, 0, 200);
        for j = 1:size(lines, 1)
            fprintf(outf, '-draw "line %f,%f %f,%f" ', lines(j,1), lines(j,3), lines(j,2), lines(j,4));
        end
        fprintf(outf, [outFormat '\n'], i);
    end

end

fclose(outf);

end
