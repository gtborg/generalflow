function replay_reconstructed(ppca_result, dir)

figure('Color', 'white');

rt = W * inv(WTW) * (WTW + sig2v*eye(q)) * ppca_result.Ex + repmat(ppca_result.mu, 1,N);

for i = 1:1:size(rt, 2)
    clf;
    draw_flow(rt(:,i), 0, 18, ones(size(rt,1),1));
    drawnow;
    saveas(gcf, [ dir sprintf('/%d.tif', i) ]);
end
    