function [alllines allcolors] = lable_images(format, skip, outformat, ppca_result, estall, flow, width, mode)

height = size(ppca_result.W, 1) / width / 2;

circle = zeros(9,9,2);
circle(:,:,1) = repmat(-4:4, 9, 1) / 4;
circle(:,:,2) = circle(:,:,1)';
circle = circle(:,:,1) .^ 2 + circle(:,:,2) .^ 2;
circle(circle <= 1) = 1;
circle(circle > 1) = 0;
circle = logical(circle);

xs = reshape(repmat((1:width)', 1, height), width*height, 1);
ys = reshape(repmat(1:height, width, 1), width*height, 1);

%estall = gmestimate(ppca_result, flow.flow_vec, flow.flow_mask);

alllines = {};
allcolors = {};

gridspx = 0;
gridspy = 0;
i = 1;
while(1)
    img = imread(sprintf(format, i+skip));
    %img = zeros(480, 640);
    
    if i > size(estall.t, 2)
        break;
    end
    
    t = estall.t(:,i);
    Ez = estall.Ez(:,i);
    
    %delete(gcf);
    clf;
    %imshow(img);
    set(gcf, 'PaperPositionMode', 'auto');
    hold on;

    if(gridspx == 0 || gridspy == 0)
        gridspx = floor(size(img, 2) / width);
        gridspy = floor(size(img, 1) / height);
        us = xs .* gridspx - (gridspx/2) - 4;
        vs = ys .* gridspy - (gridspx/2) - 4;
    end
    
    
%     mask = est.Ez;
%     mask = min(mask(1:width*height), mask(width*height+1:end));
%     mask = reshape(mask, width, height)';
%     fmask = flow.flow_mask(:,i);
%     fmask = min(fmask(1:width*height), fmask(width*height+1:end));
%     fmask = reshape(fmask, width, height)';
    
    if mode == 1
        for x = 1:width
            for y = 1:height
                u = x * gridspx - (gridspx/2) - 4;
                v = y * gridspy - (gridspx/2) - 4;
                if mode == 1
                    if(fmask(y,x) == 1 && mask(y,x) ~= 1.0)
                        cr = img(v:v+8, u:u+8, 1);
                        cg = img(v:v+8, u:u+8, 2);
                        cr(circle) = uint8(255 * (1.0-mask(y,x)));
                        cg(circle) = uint8(255 * mask(y,x));
                        img(v:v+8, u:u+8, 1) = cr;
                        img(v:v+8, u:u+8, 2) = cg;
                        img(v:v+8, u:u+8, 3) = uint8(zeros(9,9));
                    end
                end
            end
        end
    elseif mode == 2
%         xs = reshape(repmat((1:width)', 1, height), width*height, 1);
%         ys = reshape(repmat(1:height, width, 1), width*height, 1);
%         us = xs .* gridspx - (gridspx/2) - 4;
%         vs = ys .* gridspy - (gridspx/2) - 4;
        lines = [ us, us+t(1:width*height)*2, vs, vs+t(width*height+1:end)*2 ];
%         weights = min(est.Ez(1:width*height), est.Ez(width*height+1:end));
%         mask = ~logical(min(flow.flow_mask(1:width*height, i), flow.flow_mask(width*height+1:end, i)));
%         colors = [ 1-weights weights zeros(width*height, 1) ];
%         colors(mask, 1) = .5;
%         colors(mask, 2) = .5;
%         colors(mask, 3) = .5;
        
        colors = ones(width*height, 3);
        colors(:,1) = 1.0;
        colors(:,2) = 0;
        colors(:,3) = 1.0;
        
        alllines = [ alllines; lines ];
        allcolors = [ allcolors; colors ];

        % quiver(lines(:,1), lines(:,2), lines(:,3), lines(:,4));
        set(gca, 'ColorOrder', colors);
        plot(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
    elseif mode == 3
        mask = ~logical(min(flow.flow_mask(1:width*height, i), flow.flow_mask(width*height+1:end, i)));
        lines = [ us, us+flow.flow_vec(1:width*height, i)*2, vs, vs+flow.flow_vec(width*height+1:end, i)*2 ];
        lines(mask, :) = [];
        weights = min(Ez(1:width*height), Ez(width*height+1:end));
        weights(mask, :) = [];
        colors = [ 1-weights weights zeros(size(lines, 1), 1) ];
%         colors = ones(width*height, 3);
%         colors(:,1) = 1.0;
%         colors(:,2) = 0.0;
%         colors(:,3) = 1.0;

        alllines = [ alllines; lines ];
        allcolors = [ allcolors; colors ];

        if ~isempty(lines)
            set(gca, 'ColorOrder', colors);
            %plot(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
            line(lines(:,1:2)', lines(:,3:4)', 'LineWidth', 2);
        end

    end
    
    drawnow;
    %print('-dtiff', sprintf(outformat, i), '-r0');
    
    
    %saveas(gcf, sprintf(outformat, i));
    %imwrite(img, sprintf(outformat, i));
    fprintf(1, '%d\n', i);
    
    i = i + 1;
end
