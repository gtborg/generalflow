function draw_flow(flow, paired_xy, width, mask)
%DRAW_FLOW Summary of this function goes here
%   Detailed explanation goes here

if paired_xy
    flowx = flow(1 : 2 : (size(flow, 1) - 1));
    flowy = flow(2 : 2 : (size(flow, 1)));
else
    flowx = flow(1 : (size(flow, 1) / 2));
    flowy = flow((size(flow, 1) / 2 + 1) : end);
end

%figure('color', 'white');
hold on;

% x = repmat((1:width)', size(flowx, 1) / width, 1);
% y = reshape(reshape(x, width, size(flowx, 1) / width)', size(flowx, 1), 1);
u = reshape(flowx, width, size(flowx, 1) / width)';
v = reshape(flowy, width, size(flowx, 1) / width)';

if ~exist('mask','var') || isempty(mask)
    mask = isfinite(flow);
end
mask = min(mask(1:length(mask)/2), mask(length(mask)/2+1:end));
mask = reshape(mask, width, size(flowx, 1) / width)';
for x = 1:width
    for y = 1:size(flowx, 1) / width
        if(mask(y,x) ~= 1.0)
            plot(x, y, 'o', 'MarkerEdgeColor', [ 1.0-mask(y,x) mask(y,x) 0 ], 'MarkerFaceColor', [ 1.0-mask(y,x) mask(y,x) 0 ]);
        end
    end
end

quiver(u*.25, v*.25, 0.0);
set(gca, 'ydir', 'reverse');

% x = 0;
% y = 0;
% for v = [ flowx; flowy ]
%     
%     %Rescale if too large
%     if (norm(v) > 2.0)
%         v = v * (2.0 / norm(v));
%     end
%     
%     if norm(v) > 0.0
%         plot(x, y, '.b');
%         plot([ x x+v(1) ], [ y y+v(2) ], '-b');
%     end
%     x = x + 1;
%     if x >= width
%         x = 0;
%         y = y + 1;
%     end
% end
%axis([0 width-1 0 y]);
