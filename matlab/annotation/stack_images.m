function stack_images(format1, skip1, format2, skip2, outformat)

i1 = skip1 + 1;
i2 = skip2 + 1;
iout = 1;

while(1)
    img1 = imread(sprintf(format1, i1));
    img2 = imread(sprintf(format2, i2));
    
    width = max([ size(img1, 2) size(img2, 2) ]);
    height = size(img1, 1) + size(img2, 1);
    
    x1 = round((width - size(img1, 2)) / 2) + 1;
    x2 = round((width - size(img2, 2)) / 2) + 1;
    y1 = 1;
    y2 = size(img1, 1) + 1;
    
    imgout = ones(height, width, 3, 'uint8') * uint8(255);
    imgout(y1:(y1+size(img1, 1)-1), x1:(x1+size(img1,2)-1), :) = img1;
    imgout(y2:(y2+size(img2, 1)-1), x2:(x2+size(img2,2)-1), :) = img2;
    
    imwrite(imgout, sprintf(outformat, iout));
    
    i1 = i1 + 1;
    i2 = i2 + 1;
    iout = iout + 1;
    
    if mod(iout, 30) == 0
        fprintf(1, '%d\n', iout);
    end
end
