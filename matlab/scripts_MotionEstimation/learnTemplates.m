function learnTemplates(imgDirLists, q, pyrLevel, outputStem)

% Learn a template for each list of image directories
K = numel(imgDirLists);

for k = 1:K
    
    % Get image size
    [ format, firstFrame, ~ ] = dataio.detectImageSequenceFormat(imgDirLists{k}{1});
    img = imread(sprintf(format,firstFrame));
    imscale = 1 / 2^pyrLevel;
    imWidth = round(imscale*size(img,2));
    imHeight = round(imscale*size(img,1));
    
    % Learn template
    [ vals, ~, ~, ~ ] = basisAlignment.jointOptimize( ...
        imgDirLists{k}, q, pyrLevel, 1.0);
    
    % Save subspace to file
    [ subspace, latent ] = basisAlignment.subspaceFromValues(vals{end}, 1.0, imWidth, imHeight);
    subspace.write(sprintf('%s_template_%d', outputStem, k));
    
    for imgSet = 1:numel(latent)
        dlmwrite(sprintf('%s_template_%d.latent%d', outputStem, k, imgSet), ...
            latent{imgSet}', ' ');
    end
    
    % TODO: Write convergence images
    
end

end