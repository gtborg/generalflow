function showMotionImportance(frame, gmppca, M)
%SHOWMOTIONIMPORTANCE Summary of this function goes here
%   Detailed explanation goes here

ppcares.W = gmppca.subspace(0).basis;
ppcares.Ex = zeros(size(ppcares.W,2), 1);
ppcares.basissize(4) = gmppca.subspace(0).width;
ppcares.basissize(5) = gmppca.subspace(0).height;
ppcares = basisAlignment.transformBasis(ppcares, M);

figure;
subplot(2, size(ppcares.W,2)+1, 1);
imshow(frame);
subplot(2, size(ppcares.W,2)+1, size(ppcares.W,2)+1+1);
imshow(inspector.colorTest(1));

Wnorm = ppcares.W ./ max(max(ppcares.W));

for i = 1:size(ppcares.W, 2)
    subplot(2, size(ppcares.W,2)+1, i+1);
    Wimg = reshape(Wnorm(:,i), 2, ppcares.basissize(5), ppcares.basissize(4));
    Wimg = permute(Wimg, [ 3 2 1 ]);
    colored = inspector.computeColor(Wimg(:,:,1), Wimg(:,:,2));
    imshow(colored);

    subplot(2, size(ppcares.W,2)+1, size(ppcares.W,2)+1+i+1);
    imshow(inspector.flowImportance(frame, ppcares.W(:,i)));
end

end

