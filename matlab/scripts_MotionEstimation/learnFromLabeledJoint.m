function [ vals errors factors ] = learnFromLabeledJoint(dirs)
%LEARNFROMLABELEDJOINT Summary of this function goes here
%   Detailed explanation goes here

imgDirs = {};
ys = {};
for i = 1:numel(dirs)
    imgDirs{i} = [ dirs{i}, '/imgs' ];
    initialMotion = dlmread([ dirs{i} '/initialMotion.txt' ]);
    q = size(initialMotion, 2) - 1;
    
    [ ~, ~, lastFrame ] = dataio.detectImageSequenceFormat(imgDirs{i});
    
    ys{i} = Inf(q, lastFrame);
    for row = 1:size(initialMotion, 1)
        frame = initialMotion(row, 1);
        y = initialMotion(row, 2:end)';
        ys{i}(:, frame) = y;
    end
end

[ vals errors factors ] = basisAlignment.jointOptimize(imgDirs, q, ys);

end

