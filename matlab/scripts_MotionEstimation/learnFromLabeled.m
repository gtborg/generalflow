function [ppcares, Minit, Mfinal] = learnFromLabeled(specs, outputName)
%LEARNFROMLABELED Summary of this function goes here
%   Detailed explanation goes here

% Find the installation bin folder
wrapperPath = which('basisflow_wrapper');
parts = textscan(wrapperPath, '%s', 'Delimiter', [filesep filesep]); %This double filesep is a hack that results in '\\' in order to add the necessary escape
parts = parts{1};
prefixPath = fullfile(parts{1:end-3});
binPath = fullfile(prefixPath, 'bin');
clear wrapperPath parts prefixPath

flowUpdated = 0;
flowCombined = [];
initialMotionCombined = [];
initialMotionUpdated = 0;

absoluteConstraintsCombined = [];
relativeConstraintsCombined = [];
constraintsUpdated = 0;

firstFrameThisSpec = 1;
segmentLengths = [];

for i = 1:numel(specs)
    
    videofile = specs(i).video;
    framerate = specs(i).framerate;
    initialMotionFile = specs(i).initialMotion;
    absoluteConstraintFile = specs(i).absoluteConstraints;
    relativeConstraintFile = specs(i).relativeConstraints;
    
    firstFrameThisSpec = size(flowCombined, 2) + 1;
    
    % Check if video file
    isfile = dataio.isVideoFile(videofile);
    
    % Set output directory
    if videofile(end) == '/' || videofile(end) == '\'
        videofile = videofile(1:end-1);
    end
    [ path name ext ] = fileparts(videofile);
    if isfile
        outputDir = path;
    else
        outputDir = fullfile(videofile, '..');
    end
    
    % If an image sequence, get format
    if ~isfile
        finalVideoFile = dataio.detectImageSequenceFormat(videofile);
    else
        finalVideoFile = videofile;
    end
    
    
    % Compute optical flow if not done already
    flowFileName = fullfile(outputDir, [ name '_flow' ]);
    if ~exist(flowFileName, 'file')
        system([ fullfile(binPath, 'recordFlow') ' ' flowFileName ' ' finalVideoFile ], '-echo');
        flowUpdated = 1;
    end
    
    
    % Accumulate optical flow
    [ flow, fdims ] = dataio.rawflowfile(flowFileName);
    flowCombined = [ flowCombined flow ];
    segmentLengths = [ segmentLengths size(flow, 2) ];
    
    
    % Accumulate initial motion
    initialMFileName = [ outputName '_M_initial.txt' ];
    initialMotionIn = dlmread(initialMotionFile);
    if size(initialMotionIn, 2) ~= 7 && size(initialMotionIn, 2) ~= 4
        error('Each row of initial motion file should be:  framenum wx wy wz vx vy vz OR framenum wz vx vy');
    end
    initialMotionIn(:,1) = initialMotionIn(:,1) + firstFrameThisSpec - 1;
    initialMotionIn(:,2:end) = initialMotionIn(:,2:end) / framerate; % Normalize for framerate
    initialMotionCombined = [ initialMotionCombined; initialMotionIn ];
    if dataio.isOutOfDate(initialMFileName, initialMotionFile)
        initialMotionUpdated = 1;
    end
    
    
    % Accumulate constraints
    finalMFileName = [ outputName '_M_final.txt' ];
    absoluteConstraints = dlmread(absoluteConstraintFile);
    relativeConstraints = dlmread(relativeConstraintFile);
    if size(absoluteConstraints, 2) ~= 7 && size(absoluteConstraints, 2) ~= 4
        error('Each row of absolute constraints should be:  framenum roll pitch yaw x y z OR framenum yaw x y');
    end
    if size(relativeConstraints, 2) ~= 8 && size(relativeConstraints, 2) ~= 5
        error('Each row of relative constraints should be:  framenum1 framenum2 droll dpitch dyaw dx dy dz OR framenum1 framenum2 dyaw dx dy');
    end
    absoluteConstraints(:,1) = absoluteConstraints(:,1) + firstFrameThisSpec - 1;
    relativeConstraints(:,1:2) = relativeConstraints(:,1:2) + firstFrameThisSpec - 1;
    absoluteConstraintsCombined = [ absoluteConstraintsCombined; absoluteConstraints ];
    relativeConstraintsCombined = [ relativeConstraintsCombined; relativeConstraints ];
    % If not first spec, tie to first to prevent disconnected graph
    if i ~= 1
        relativeConstraintsCombined = [ relativeConstraintsCombined; 1 firstFrameThisSpec zeros(1,size(relativeConstraints,2)-2) ];
    end
    if dataio.isOutOfDate(finalMFileName, absoluteConstraintFile) ...
            || dataio.isOutOfDate(finalMFileName, relativeConstraintFile)
        constraintsUpdated = 1;
    end
end
    
    
% Run unsupervised learning
ppcaresFileName = [ outputName '_ppcares' ];
if flowUpdated || ~exist([ppcaresFileName '.W'], 'file')
    
    % Get dimensionality from initial motion
    if size(initialMotionCombined, 2) == 7
        yDim = 6;
    elseif size(initialMotionCombined, 2) == 4
        yDim = 2;
    end
    
    % Run gmppca
    opts = gmppca2_opts;
    opts.compute_mean = 0;
    opts.robust = 1;
    ppcares = gmppca2(flowCombined, yDim, opts);

    % Write result
    dataio.write_ppcares(ppcares, ppcaresFileName);
    ppcaresRecomputed = 1;
else
    ppcares = dataio.read_ppcares(ppcaresFileName);
    ppcaresRecomputed = 0;
end


% If initial motion or y was changed, relearn initial motion matrix
if ppcaresRecomputed || initialMotionUpdated
    frameNums = initialMotionCombined(:,1);
    y = ppcares.Ex(:, frameNums);
    initialMotion = initialMotionCombined(:,2:end)';
    
    [ Minit, ~ ] = learn_motion(y, initialMotion);
    dlmwrite(initialMFileName, Minit, ' ');
    initialMrecomputed = 1;
else
    Minit = dlmread(initialMFileName);
    initialMrecomputed = 0;
end


% If initial motion matrix or constraints changed, re-optimize
if initialMrecomputed || constraintsUpdated
    % Break y into segments
    ySegments = {};
    pos = 1;
    for segLength = segmentLengths
        ySegments = [ ySegments ppcares.Ex(:,pos:(pos+segLength-1)) ];
        pos = pos + segLength;
    end
    Mfinal = basisAlignment.optimizeBasis(Minit, ySegments, absoluteConstraintsCombined, relativeConstraintsCombined);
    dlmwrite(finalMFileName, Mfinal, ' ');
else
    Mfinal = dlmread(finalMFileName);
end
            
end

