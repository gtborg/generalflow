path = 'C:/Users/richard/data/GeneralFlow.3';

prevFrame = double(imread(sprintf([path '/2012-07-20_hall1/imgs/%08d.jpg'], 685))) / 255;
curFrame = double(imread(sprintf([path '/2012-07-20_hall1/imgs/%08d.jpg'], 686))) / 255;
M = dlmread([path '/2012-07-20_combined1/planar2_M_initial.txt']);

%prevFrame = imresize(prevFrame, 64/512);
%curFrame = imresize(curFrame, 64/512);

prevFrameCV = basisflow.Utils.MatOfMatrix(prevFrame);
curFrameCV = basisflow.Utils.MatOfMatrix(curFrame);

ppcares_path = 'C:/Users/richard/data/GeneralFlow.3/2012-07-20_combined1/planar2_ppcares';
ppcares = dataio.read_ppcares(ppcares_path);
blurSigma = 2;

subspace = basisflow.Subspace(ppcares_path, size(prevFrame,2), size(prevFrame,1));
params = basisflow.GMPPCALKParams(0, 0, 2.0, 1, 0);
gmppca = basisflow.GMPPCALucasKanade(subspace, params, size(prevFrame,2));

showMotionImportance(curFrame, gmppca, M);

% gmppca.setNextFrame(prevFrameCV);
% gmppca.setNextFrame(curFrameCV);
% 
% filteredPrevFrameG = basisflow.Utils.MatrixOfMat(gmppca.prevFrame(0));
% filteredPrevFrameR = basisflow.Utils.MatrixOfMat(gmppca.resampleCurFrame([1;0]));
% 
% figure; imshow(prevFrame); title 'prevFrame'
% figure; imshow(filteredPrevFrameG); title 'From GMPPCA'
% figure; imshow(filteredPrevFrameR); title 'Filtered'

