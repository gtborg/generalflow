function model = evaluateModelSelection(templateStem, evalImgDir)
%EVALUATEMODELSELECTION Summary of this function goes here
%   Detailed explanation goes here

% Find templates
subspaces = basisflow.SubspaceVector;
k = 0;
while exist(sprintf('%s_template_%d.W', templateStem, k+1))
    k = k + 1;
    subspaces.push_back(basisflow.Subspace( ...
        sprintf('%s_template_%d', templateStem, k)));
end

% Create frame cache and model selection objects
[ format, firstFrame, lastFrame ] = dataio.detectImageSequenceFormat(evalImgDir);
framecache = basisflow.FrameCache(format);
modelsel = basisflow.ModelSelection(subspaces, framecache);

    function l = logSumLog(logA, logC)
        l = logA + log(1 + exp(logC - logA));
    end

% Evaluate
likelihoods = zeros(k, lastFrame);
for frame = firstFrame+1 : lastFrame
    likelihoods(:,frame) = modelsel.evaluateLogLikelihood(frame);
    
    % Apply transition model
%     pStay = 0.999;
%     T = [
%        pStay         0.5*(1-pStay)  0.1*(1-pStay)
%        0.9*(1-pStay) pStay          0.0*(1-pStay)
%        0.1*(1-pStay) 0.5*(1-pStay)  pStay ];
% %     T = 1/k * ones(k,k);
%     
%     if frame >= firstFrame+2
%         transitionLogProb = zeros(k,1);
%         for i = 1:k
%             for j = 1:k
%                 prod = log(T(i,j)) + likelihoods(j,frame-1);
%                 if j == 1
%                     transitionLogProb(i) = prod;
%                 else
%                     transitionLogProb(i) = logSumLog(transitionLogProb(i), prod);
%                 end
%             end
%         end
%         likelihoods(:,frame) = likelihoods(:,frame) + transitionLogProb;
%     end
    
    if mod(frame, 100) == 0
        fprintf('Frame %d\n', frame);
    end
end

%model = sum(repmat((1:k)', 1, lastFrame) .* likelihoods, 1) ./ sum(likelihoods, 1);
[m,i] = max(likelihoods, [], 1);
model = i;
model(all(likelihoods==0,1)) = inf;

end

