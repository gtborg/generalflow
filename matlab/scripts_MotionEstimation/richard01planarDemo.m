M = eye(3);
M(3,1) = 0.003;
y = repmat([1;0;0], 1,100);
y = [ y repmat([0.8;0;pi/2/10], 1,10) ];
y = [ repmat(y, 1,4) y(:,1) ];

%absConst = [ 100 99 0 0 ];
absConst = [ 50 49 0 0 ];
relConst = [ 1 size(y,2) 0 0 0 ];

basisAlignment.optimizeBasis(M, y, absConst, relConst);
