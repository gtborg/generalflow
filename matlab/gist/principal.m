function [ princ, mean ] = principal( X )
%PRINCIPAL Summary of this function goes here
%   Detailed explanation goes here
mean = ones(size(X,2),1);

[COEFF,SCORE] = princomp(X);

princ = COEFF(:,1:80);

for i = 1: size(X,2)

total = sum(X(:,i));
mean(i) = total./size(X,2);
end

end

