function createGistDataset(trainingSets, testingSets, pcsToKeep, outprefix)
%CREATEGISTDATASET Summary of this function goes here
%   Detailed explanation goes here

%% Read data
assert(numel(trainingSets) == numel(testingSets));

training = [];
testing = [];

numTrainingInstances = 0;
numTestingInstances = 0;
for i=1:numel(trainingSets)
    
    % This block reads the training set
    trainSet = trainingSets{i};
    fprintf('Reading %s\n', trainSet);
    j = 0;
    while(1)
        trainingInstanceFile = sprintf('%s/prefix-Gist%06d.txt', trainSet, j);
        if exist(trainingInstanceFile, 'file')
            trainingInstance = dlmread(trainingInstanceFile)';
        else
            break;
        end
        
        % trainingInstance is now a single row
        training(numTrainingInstances+1, 1:714) = trainingInstance;
        training(numTrainingInstances+1, 715) = i; % i is the class label
        
        numTrainingInstances = numTrainingInstances + 1;
        j = j + 1;
    end
    
    % This block reads the testing set
    testSet = testingSets{i};
    fprintf('Reading %s\n', testSet);
    j = 0;
    while(1)
        testingInstanceFile = sprintf('%s/prefix-Gist%06d.txt', testSet, j);
        if exist(testingInstanceFile, 'file')
            testingInstance = dlmread(testingInstanceFile)';
        else
            break;
        end
        
        % testingInstance is now a single row
        testing(numTestingInstances+1, 1:714) = testingInstance;
        testing(numTestingInstances+1, 715) = i; % i is the class label
        
        numTestingInstances = numTestingInstances + 1;
        j = j + 1;
    end
end

assert(size(training,1) == numTrainingInstances);
assert(size(testing,1) == numTestingInstances);

%% Select Columns
% Now we have training and testing data, select the columns we want
columns = [];
for cblock = 0:33
    firstc = cblock*21 + 6;
    lastc = cblock*21 + 21;
    columns = [ columns firstc:lastc ];
end
nDataCols = numel(columns);
columns = [ columns 715 ];

trainingSelected = training(:,columns);
testingSelected = testing(:,columns);

%% Do PCA
fprintf('Doing PCA...\n');
dataMean = mean(trainingSelected(:,1:nDataCols), 1);
coeffs = princomp(trainingSelected(:,1:nDataCols), 'econ');
assert(all(size(coeffs) == [ nDataCols nDataCols ]));
coeffsSelected = coeffs(:,1:pcsToKeep);
trainingCompressed = [ (trainingSelected(:,1:nDataCols)-repmat(dataMean,numTrainingInstances,1))*coeffsSelected   trainingSelected(:,end) ];
testingCompressed = [ (testingSelected(:,1:nDataCols)-repmat(dataMean,numTestingInstances,1))*coeffsSelected       testingSelected(:,end) ];

%% Write file
    function writeARFF(table, name)
        fid = fopen([name '.arff'], 'w');
        fprintf(fid, '@RELATION %s\n', name);
        for col=1:size(table,2)-1
            fprintf(fid, '@ATTRIBUTE %d NUMERIC\n', col);
        end
        fprintf(fid, '@ATTRIBUTE class {');
        classes = unique(table(:,end));
        for class=1:size(classes,1)-1
            fprintf(fid, '%d,', classes(class));
        end
        fprintf(fid, '%d}\n', classes(end));
        fprintf(fid, '@DATA\n');
        fclose(fid);
        dlmwrite([name '.arff'], table, '-append', 'delimiter', ',');
    end

fprintf('Writing ARFF files...\n');
writeARFF(trainingCompressed, [ outprefix '_train' ]);
writeARFF(testingCompressed, [ outprefix '_test' ]);

end

