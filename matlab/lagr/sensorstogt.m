function gt = sensorstogt(sensor_file)

sf = fopen(sensor_file, 'r');

gt_local = [];
gt_global = [];
pose_local = [];
pose_global = [];

while ~feof(sf)
    line = fgetl(sf);
    
    if strcmp(line(1:3), 'RAW')
        framedata = sscanf(line(5:end), '%f');
        status = sscanf(fgetl(sf), '%f');
        localpose = sscanf(fgetl(sf), '%f');
        globalpose = sscanf(fgetl(sf), '%f');
        localrate = sscanf(fgetl(sf), '%f');
        globalrate = sscanf(fgetl(sf), '%f');
        localcov = sscanf(fgetl(sf), '%f');
        globalcov = sscanf(fgetl(sf), '%f');
        
        gt_local = vertcat(gt_local, [ sqrt(sum(localrate(1:2).*localrate(1:2))) localrate(5:6)' ]);
        gt_global = vertcat(gt_global, [ sqrt(sum(localrate(1:2).*localrate(1:2))) globalrate(5:6)' ]);
        pose_local = vertcat(pose_local, [ localpose(1:2)' localpose(5:6)' ]);
        pose_global = vertcat(pose_global, [ globalpose(1:2)' globalpose(5:6)' ]);
    end
end

N = size(gt_local, 1);

dp_local = [ zeros(N,1) ones(N,1)*1/15 gt_local(:,1) gt_local(:,3) ];
% dp_global = [ zeros(N,1) ones(N,1)*1/15 sqrt(gt_global(:,1).^2 + gt_global(:,2).^2) gt_global(:,4) ];
dp_global = diffpose([ (1:N)'*1/15 pose_global(:,1) pose_global(:,2)]);
gt_global = [ dp_global(:,3) gt_global(:,2) dp_global(:,4) ];

gt = struct('gt_local', gt_local, 'gt_global', gt_global, 'dp_local', dp_local, 'dp_global', dp_global, 'pose_local', pose_local, 'pose_global', pose_global);