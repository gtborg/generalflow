[ motest1 valid ] = resample([ (1:length(motion))' (motion(1,:))' ], 3, (1:length(motion))');
[ motest3 valid ] = resample([ (1:length(motion))' (motion(3,:))' ], 3, (1:length(motion))');

motest = [ motest1 motest3 ];
clear motest1 motest3;

[ motgt1 valid ] = resample([ (1:length(gt.gt_local))' gt.gt_local(:,1) ], 3, (1:length(gt.gt_local))');
[ motgt3 valid ] = resample([ (1:length(gt.gt_local))' gt.gt_local(:,3) ], 3, (1:length(gt.gt_local))');

motgt = [ motgt1 motgt3 ];
clear motgt1 motgt3;

figure('Color','White'); plot(motgt(6000:6999, 1), 'r'); hold on; plot(motgt(6000:6999, 2), 'm')
[ smex1 valid ] = resample([ (1:length(6000:6999))' (estall4.Ex(1, 6000:6999)') ], 3, (1:length(6000:6999))');
[ smex2 valid ] = resample([ (1:length(6000:6999))' (estall4.Ex(2, 6000:6999)') ], 3, (1:length(6000:6999))');
[ smex3 valid ] = resample([ (1:length(6000:6999))' (estall4.Ex(3, 6000:6999)') ], 3, (1:length(6000:6999))');
[ smex4 valid ] = resample([ (1:length(6000:6999))' (estall4.Ex(4, 6000:6999)') ], 3, (1:length(6000:6999))');
figure('Color','White'); plot([smex1 smex2 smex3 smex4]);
