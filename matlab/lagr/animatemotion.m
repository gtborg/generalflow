function animatemotion(outformat, range, start, stop, motest, motgt)

% start = range+1;
% stop = length(motest) + range - 1;

figure('Color', 'white');
for i=start:stop
    clf;
    plot([i i], [-.5 2.0], 'Color', [ .7 .7 .7 ], 'LineWidth', 2);
    hold on;
    for j=1:size(motest,2)
        plot(((i-range):(i+range))', motgt((i-range):(i+range), j), 'g-', 'LineWidth', 1.5);
        plot(((i-range):(i+range))', motest((i-range):(i+range), j), 'b-', 'LineWidth', 1);
    end
    set(gca, 'XLim', [ i-range, i+range ]);
    set(gca, 'YLim', [ -.5 2.0 ]);
%     drawnow;
    
    saveas(gcf, sprintf(outformat, i));
end

end
