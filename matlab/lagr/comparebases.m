suboff = 928;
subend = 2857;
flow_sub = flow;
flow_sub.flow_vec = flow_sub.flow_vec(:, suboff:subend);
flow_sub.flow_mask = flow_sub.flow_mask(:, suboff:subend);
ppcares4_928 = gmppca(flow_sub.flow_vec, 4, flow_sub.flow_mask);
[ Mx_928 Mc_928 ] = learn_motion(ppcares4_928.Ex, gt.gt_global(suboff:subend, :)');
fw_928 = ppcares4_928.W*(pinv(Mx_928) * ([ 1 0 0 ]' - Mc_928));
annotateone('', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/fw_928.png', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/fw_928.sh', ...
    fw_928, 64);
% yaw_928 = ppcares4_928.W*(pinv(Mx_928) * ([ 0 0 .1 ]' - Mc_928) + [ 0 0 -.1 0 ]');
est_928 = gmestimate(ppcares4_928, ppcares4_6000.W*(pinv(Mx_6000) * ([ 0 0 .1 ]' - Mc_6000)), ones(size(ppcares4_6000.W, 1), 1));
yaw_928 = ppcares4_928.W*est_928.Ex;
%annotateone('/Users/richard/data/lagrvo_090329_tsrbloop/tile/tile1322.jpg', ...
annotateone('', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/yaw_928.png', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/yaw_928.sh', ...
    yaw_928, 64);

suboff = 2878;
subend = 3907;
flow_sub = flow;
flow_sub.flow_vec = flow_sub.flow_vec(:, suboff:subend);
flow_sub.flow_mask = flow_sub.flow_mask(:, suboff:subend);
ppcares4_sw = gmppca(flow_sub.flow_vec, 4, flow_sub.flow_mask);
suboff = 928;
subend = 2857;
flow_sub = flow;
flow_sub.flow_vec = flow_sub.flow_vec(:, suboff:subend);
flow_sub.flow_mask = flow_sub.flow_mask(:, suboff:subend);
estall4_sw = gmestimate(ppcares4_sw, flow_sub.flow_vec, flow_sub.flow_mask);
% gt_global_tmp = gt.gt_global;
% gt_global_tmp(abs(gt_global_tmp) > 5) = 0;
[ Mx_sw Mc_sw ] = learn_motion(estall4_sw.Ex, gt.gt_global(suboff:subend, :)');
fw_sw = ppcares4_sw.W*(pinv(Mx_sw) * ([ 1 0 0 ]' - Mc_sw));
annotateone('', '/Users/richard/data/lagrvo_090329_tsrbloop/fw_sw.png', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/fw_sw.sh', ...
    fw_sw, 64);

suboff = 6985;
% subend = 7592;
subend = 7846;
flow_sub = flow;
flow_sub.flow_vec = flow_sub.flow_vec(:, suboff:subend);
flow_sub.flow_mask = flow_sub.flow_mask(:, suboff:subend);
ppcares4_6000 = gmppca(flow_sub.flow_vec, 4, flow_sub.flow_mask);
[ Mx_6000 Mc_6000 ] = learn_motion(ppcares4_6000.Ex, gt.gt_global(suboff:subend, :)');
fw_6000 = ppcares4_6000.W*(pinv(Mx_6000) * ([ 1 0 0 ]' - Mc_6000));
annotateone('', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/fw_6000.png', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/fw_6000.sh', ...
    fw_6000, 64);
yaw_6000 = ppcares4_6000.W*(pinv(Mx_6000) * ([ 0 0 .1 ]' - Mc_6000));
% annotateone('/Users/richard/data/lagrvo_090329_tsrbloop/tile/tile7389.jpg', ...
annotateone('', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/yaw_6000.png', ...
    '/Users/richard/data/lagrvo_090329_tsrbloop/yaw_6000.sh', ...
    yaw_6000, 64);

