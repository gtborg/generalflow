% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares1.mat ppcares1;
% sig1 = ppcares1.sig2v;
% clear ppcares1
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares2.mat ppcares2;
% sig2 = ppcares2.sig2v;
% clear ppcares2
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares3.mat ppcares3;
% sig3 = ppcares3.sig2v;
% clear ppcares3
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4.mat ppcares4;
% sig4 = ppcares4.sig2v;
% clear ppcares4
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares5.mat ppcares5;
% sig5 = ppcares5.sig2v;
% clear ppcares5
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares6.mat ppcares6;
% sig6 = ppcares6.sig2v;
% clear ppcares6
% sigs_tile = [ sig1 sig2 sig3 sig4 sig5 sig6 ]';

% load /Users/richard/data/mirror1/ppcares_multiq sigs;
% sigs_mirror1 = sigs';
% load /Users/richard/data/mirror2/ppcares_multiq sigs;
% sigs_mirror2 = sigs';
% 
figure('color', 'white');
plot([ sigs_tile sigs_mirror2 sigs_mirror1 ]);
set(gca, 'FontSize', 16);
xlabel('Number of latent variables');
ylabel('Inlier variance, pixels^{2}');
legend('Mobile robot', 'Catadioptric, pan-tilt-roll', 'Catadioptric, pan-tilt', 'Location', 'NorthEast');
set(gca, 'Position', [ .1 .14 .89 .84 ])
printeps(gcf, 'ppca-sigs');

% suboff = 80;
% subend = 2500;
suboff = 928;
subend = 2857;
% suboff = 2878;
% subend = 3907;

load '/Users/richard/data/lagrvo_090329_tsrbloop/tile_flow.mat' flow
flow_sub = flow;
flow_sub.flow_vec = flow_sub.flow_vec(:, suboff:subend);
flow_sub.flow_mask = flow_sub.flow_mask(:, suboff:subend);
ppcares4 = gmppca(flow_sub.flow_vec, 4, flow_sub.flow_mask);
% save '/Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4_sub.mat' ppcares4

% flow = load_flow('/Users/richard/data/lagrvo_090329_tsrbloop/tile_flow.txt');
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4_sub.mat ppcares4
estall4 = gmestimate(ppcares4, flow.flow_vec, flow.flow_mask);
% % save '/Users/richard/data/lagrvo_090329_tsrbloop/tile_estall4_sub.mat' estall4
% clear flow

% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4.mat ppcares4;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_gt.mat gt;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_estall4.mat estall4;

% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4_sub.mat ppcares4;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_gt.mat gt;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_estall4_sub.mat estall4;

% [ Mx Mc ] = learn_motion(ppcares4.Ex, gt.gt_local(2:2:9228, :)');
[ Mx Mc ] = learn_motion(estall4.Ex(:, suboff:subend), gt.gt_global(suboff:subend, :)');

rot_offset = 0.05;

% Fix some GPS jumps
gt.dp_global(3661, 3:4) = [ 1.47 0 ];
gt.dp_global(3662, 4) = [ 0 ];

gt.dp_global(3017, 3:4) = [ 1.5 0 ];
gt.dp_global(3018, 4) = 0;

globalposes = intpose([ 0 1 0 4.7; gt.dp_global(1:end,:) ]);
global_endyaw = mean(globalposes((subend-10):subend, 3));
localposes = intpose([ 0 1 0 rot_offset+global_endyaw; gt.dp_local((subend+1):end,:) ]);

motion = Mx * estall4.Ex + repmat(Mc, 1, size(estall4.Ex, 2));
motion = motion';
motion = motion((subend+1):end, :);
motion_poses = intpose([ 0 1 0 rot_offset+global_endyaw; zeros(length(motion),1) ones(length(motion),1)*1/15 motion(:,1) motion(:,3)]);

motion_poses = motion_poses + repmat(globalposes(subend,:), size(motion_poses,1), 1);
localposes = localposes + repmat(globalposes(subend,:), size(localposes,1), 1);

set(0, 'DefaultAxesFontName', 'Arial');
figure(1);
set(gcf, 'color', 'none');
set(gca, 'color', 'none');
set(gcf, 'InvertHardcopy', 'off');
hold on;
plot(motion_poses(1:end,1), -motion_poses(1:end,2), 'b', 'LineWidth', 1.5);
plot(localposes(1:end,1), -localposes(1:end,2), 'color', [ .75 0 0 ], 'LineWidth', 1.5);
% plot(globalposes(subend:end,1), -globalposes(subend:end,2), 'color', [ 0 .6 0 ], 'LineWidth', 1);
% plot(globalposes(suboff:subend,1), -globalposes(suboff:subend,2), 'color', [ 0 0 0 ], 'LineWidth', 1);
axis equal;
% set(gca, 'Position', [ .08 .12 .92 .84 ])
set(gca, 'FontSize', 12);
% plot(0, 0, 'ko', 'LineWidth', 1.5, 'MarkerSize', 10);
% text(9, 0, 'Start', 'FontName', 'Helvetica', 'FontSize', 14);
% plot(-12, 34.2, 'ko', 'LineWidth', 1.5, 'MarkerSize', 10);
% text(-20, 39.6, 'True', 'HorizontalAlignment', 'Right', 'FontName', 'Helvetica', 'FontSize', 14);
% text(-20, 28.8, 'end', 'HorizontalAlignment', 'Right', 'FontName', 'Helvetica', 'FontSize', 14);
plot(motion_poses(end,1), -motion_poses(end,2), 'bo', 'LineWidth', 1.5, 'MarkerSize', 7);
% text(motion_poses(end,1)+6, -motion_poses(end,2), 'Vis. odo. end', 'color', 'Blue', 'FontName', 'Helvetica', 'FontSize', 14);
plot(localposes(end,1), -localposes(end,2), 'o', 'color', [ .75 0 0 ], 'LineWidth', 1.5, 'MarkerSize', 7);
% text(localposes(end,1)-6, -localposes(end,2), 'Encoders/IMU end', 'color', [ .75 0 0 ], 'HorizontalAlign', 'Right', 'FontName', 'Helvetica', 'FontSize', 14);
set(gca, 'XLim', [ -75 300 ]);
set(gca, 'YLim', [ -180 60 ]);
xlabel('meters'); ylabel('meters');
% text(-294, -25, 'Trajectories:', 'FontSize', 15, 'FontName', 'Helvetica');
% legend('Optical flow (our method)', 'Encoders/IMU', 'Location', 'NorthWest');
% legend('boxoff');
% print '/Users/richard/papers/richard/cvpr2009-imgs/lagrvo_090225_traj.eps' -depsc2;
% print '/Users/richard/Research/Unsupervised Flow/lagrvo_090225_trajmap.eps' -depsc2;
printeps(1, 'lagrvo_090225_trajmap')

% figure('color','white');
% plot(gt.gt_local(2:9229,1), motion(:,1), '.');
% 
% figure('color','white');
% plot(gt.gt_local(2:9229,3), motion(:,3), '.');

