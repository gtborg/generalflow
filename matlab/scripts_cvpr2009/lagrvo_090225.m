% load /Users/richard/data/lagrvo_090225/ppcares_multiq.mat ppcares3;
load /Users/richard/data/lagrvo_090225/gt.mat gt;

[ Mx Mc ] = learn_motion(ppcares3.Ex, gt.gt_local(2:end,:)');
motion = Mx * ppcares3.Ex + repmat(Mc, 1, 9648);
motion = motion';
motion_poses = intpose([ zeros(length(motion),1) ones(length(motion),1)*1/15 motion(:,1) motion(:,3)]);

localposes = intpose(gt.dp_local(200:end,:));
globalposes = intpose(gt.dp_global(200:end,:));

figure('color','white');
hold on;
plot(motion_poses(:,1), motion_poses(:,2), 'b');
plot(localposes(:,1), localposes(:,2), 'm');
plot(globalposes(:,1), globalposes(:,2), 'g');
axis equal;

% figure('color','white');
% plot(gt.gt_local(2:end,1), motion(:,1), '.');
% 
% figure('color','white');
% plot(gt.gt_local(2:end,3), motion(:,3), '.');

