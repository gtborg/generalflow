Ex = zeros(size(gt,1), size(flow.flow_vec, 2));
for i=1:size(flow.flow_vec, 2)
    est = gmestimate(ppcares, flow.flow_vec(:,i), flow.flow_mask(:,i));
    Ex(:,i) = est.Ex;
end

N = size(Ex,2);
q = size(Ex,1);
Exsmooth = zeros(q,N);
for j=1:q
    [ x valid ] = resample([ (1:N)' Ex(j,:)' ], 3, (1:N)');
    Exsmooth(j,:) = x';
end

[ Mx Mc ] = learn_motion(Exsmooth(:,1:size(gt,2)), gt);
motion = Mx * Exsmooth + repmat(Mc, 1, size(Ex, 2));

start = 1;
fin = 10000;

rposes = intpose([zeros(fin-start+1,1), ones(fin-start+1,1)*1/30, motion(:,start:fin)' ]);
rposesgt = intpose([zeros(fin-start+1,1), ones(fin-start+1,1)*1/30, gt(:,start:fin)' ]);
figure;plot([ rposes(:,1) rposesgt(:,1) ], [ rposes(:,2) rposesgt(:,2) ]);

figure;plot(start:fin, [ motion(1,start:fin); gt(1,start:fin) ]')
xlabel('frame');
ylabel('v');

figure;plot(start:fin, [ motion(2,start:fin); gt(2,start:fin) ]')
xlabel('frame');
ylabel('w');

