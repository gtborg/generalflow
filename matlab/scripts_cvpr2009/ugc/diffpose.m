function [ dp ] = diffpose(poses)

deltas = poses(2:end,:) - poses(1:end-1,:);
mags = sqrt(deltas(:,2) .^ 2 + deltas(:,3) .^ 2);
angs = atan2(deltas(:,3), deltas(:,2));
for i=2:size(angs,1)
    if angs(i) == 0
        angs(i) = angs(i-1);
    end
end
w = [ angs(1); angs(2:end) - angs(1:end-1) ] ./deltas(:,1);

v = mags ./ deltas(:,1);
% for i=2:size(angs,1)
%     if w(i) == 0
%         w(i) = w(i-1);
%     end
%     if v(i) == 0
%         v(i) = v(i-1);
%     end
% end

dp = [ v(1) w(1); v w ];
dp = [ poses(:,1) [ deltas(1,1); deltas(:,1) ] dp ];

end
