#!/usr/bin/python

import os

fout = open('out.txt', 'w')

for filename in os.listdir('fast'):
    if filename.endswith('UTMConditionedMessage.xml'):
        f = open('fast/'+filename, 'r')
        contents = f.read();
        lines = contents.split('\n');
        for l in lines:
            i = l.find('<msgTimestamp>');
            if i != -1:
                ts = l[i+len('<msgTimestamp>'):].partition('<')[0]
            i = l.find('<northings>');
            if i != -1:
                northings = l[i+len('<northings>'):].partition('<')[0]
            i = l.find('<eastings>');
            if i != -1:
                eastings = l[i+len('<eastings>'):].partition('<')[0]
            i = l.find('<heading>');
            if i != -1:
                heading = l[i+len('<heading>'):].partition('<')[0]
        f.close();

        fout.write('%s,%s,%s,%s\n'%(ts, northings, eastings, heading));
        print ts

fout.close();

