imgtimes = dlmread('/Users/richard/data/2007-10-10-campus/timesc.txt');
imgtimes = imgtimes(:,1);
poses = dlmread('/Users/richard/data/2007-10-10-campus/out.txt');
poses(:,1) = poses(:,1) * 1e-3 - 1192050000;

pd = diffpose(poses);

sigma = 0.25;
[ v vvalid ] = resample( [ pd(:,1) pd(:,3) ], sigma, imgtimes);
[ w wvalid ] = resample( [ pd(:,1) pd(:,4) ], sigma, imgtimes);

gt = [ v'; w' ];
