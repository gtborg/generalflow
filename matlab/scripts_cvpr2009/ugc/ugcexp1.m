N = min(size(flow.flow_vec, 2), size(gt, 2));

train_range = 1:N;
test_range = 1:N;
Ntrain = length(train_range);
Ntest = length(test_range);

gt_train = gt(:, train_range);
gt_test = gt(:, test_range);

gt_train(2, abs(gt_train(2,:)) > 1) = 0;
gt_test(2, abs(gt_test(2,:)) > 1) = 0;

fprintf(1, 'Learning motion in training sequence\n');
%Smooth latent variables in training sequence
q = size(ppcares.Ex,1);
Exsmooth = zeros(q,length(train_range));
sigma = 3;
for j=1:q
    [ x valid ] = resample([ train_range' ppcares.Ex(j,train_range)' ], sigma, train_range');
    Exsmooth(j,:) = x';
end

%Smooth training motion
for j=1:q
    [ x valid ] = resample([ train_range' gt_train(j,:)' ], sigma, train_range');
    gt_train(j,:) = x';
end


% Learn motion for training flow
[ Mx Mc ] = learn_motion(Exsmooth, gt_train);


fprintf(1, 'Estimating latent variables for test flow\n');
% Estimate latent variables for test flow
Ex = zeros(q, Ntest);
for i=1:Ntest
    est = gmestimate(ppcares, flow.flow_vec(:,test_range(i)), flow.flow_mask(:,test_range(i)));
    Ex(:,i) = est.Ex;
end

%Smooth test sequence latent variables
Exsmooth = zeros(size(Ex));
sigma = 3;
for j=1:q
    [ x valid ] = resample([ test_range' Ex(j,:)' ], sigma, test_range');
    Exsmooth(j,:) = x';
end

fprintf(1, 'Estimating trajectories\n');
% Estimate motion for test sequence
motion = Mx * Exsmooth + repmat(Mc, 1, size(Exsmooth, 2));

% Get test trajectory
rposes = intpose([zeros(Ntest,1), ones(Ntest,1)*1/30, motion(:,test_range)' ]);
rposesgt = intpose([zeros(Ntest,1), ones(Ntest,1)*1/30, gt(:,test_range)' ]);

% Plot trajectory
figure;plot([ rposes(:,1) rposesgt(:,1) ], [ rposes(:,2) rposesgt(:,2) ]);

% Plot estimates
figure;plot([motion(1,test_range); gt_test(1,:)]');
xlabel('Frame');
ylabel('Velocity, m/s');
figure;plot([motion(2,test_range); gt_test(2,:)]');
xlabel('Frame');
ylabel('Yaw rate, rad/s');

% Plot errors
% errs = sqrt((motion(:,test_range) - gt(:,test_range)) .^ 2);
% figure;plot(errs(1,:), '.');
% figure;plot(errs(2,:), '.');

% Draw basis flows
draw_flow_nice(ppcares.Wortho(:,2)*20, ones(45*13*2,1), 32, 3)
draw_flow_nice(ppcares.Wortho(:,2)*15, ones(45*13*2,1), 32, 3)

