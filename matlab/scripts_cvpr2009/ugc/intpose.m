function poses = intpose(dp)

N = size(dp, 1);

x=0;
y=0;
theta=0;
poses=[];

for i = 1:N
    theta = theta + dp(i,4)*dp(i,2);
    x = x + dp(i,3)*cos(theta)*dp(i,2);
    y = y + dp(i,3)*sin(theta)*dp(i,2);
    poses = [ poses; x y theta ];
end
