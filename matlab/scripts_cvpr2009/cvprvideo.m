% % load /Users/richard/data/lagrvo_090329_tsrbloop/tile_ppcares4_sub.mat ppcares4;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_gt.mat gt;
% load /Users/richard/data/lagrvo_090329_tsrbloop/tile_estall4_sub.mat estall4;
% % load '/Users/richard/data/lagrvo_090329_tsrbloop/tile_flow.mat' flow
% 
% suboff = 80;
% subend = 2500;
% 
% [ Mx Mc ] = learn_motion(estall4.Ex(:, suboff:subend), gt.gt_global(suboff:subend, :)');
% 
% motion = Mx * estall4.Ex + repmat(Mc, 1, length(estall4.Ex));
% 
length = size(estall4.Ex, 2);

xsm = zeros(size(estall4.Ex));
for j = 1:size(estall4.Ex, 1)
    [ y valid ] = resample([ (1:length)' estall4.Ex(j, :)' ], 3, (1:length)');
    xsm(j, :) = y';
end

figx = figure;
set(gcf, 'Position', [0 0 300 300]);
% set(gcf, 'color', 'none');
% set(gcf, 'InvertHardcopy', 'off');
set(gcf, 'PaperPositionMode', 'auto');
plot(0, 'ob', 'LineWidth', 2, 'MarkerSize', 7);
cla
hold on
axis equal
set(gca, 'XLim', [-25, 25]);
set(gca, 'YLim', [-25, 25]);
set(gca, 'XTick', []);
set(gca, 'YTick', []);
set(gca, 'color', 'none');
xlabel 'x_{1}'
ylabel 'x_{2}'

% plot(estall4.Ex(1,1:10:end)', estall4.Ex(2,1:10:end), '.b', 'MarkerSize', 6);

for i = 1:length
%     t_obs = flow.flow_vec(:, i);
%     t_est = estall4.t(:, i);
    x_est = xsm(:, i);
%     m_est = motion(i, :)';
    
    set(0, 'CurrentFigure', figx);
    cla
    plot(x_est(1), x_est(2), 'ob', 'LineWidth', 2, 'MarkerSize', 7);
    drawnow;
    print('-r150', '-dpng', sprintf('/Users/richard/data/lagrvo_090329_tsrbloop/vid_x/%05d.png', i));

end
