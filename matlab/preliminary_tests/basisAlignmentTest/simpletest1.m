import gtsam.*;

if ~exist('simpletest1_imgs', 'dir')
    mkdir('simpletest1_imgs');
end

testImageX = @(x,y) repmat(uint8( ...
    32*(-sqrt((mod(x,4)-2)^2+(mod(y,4)-2)^2)+8)), ...
    [1,1,3]);

S = [ 0.0001 1 0.0001 0.0001 ];
flowSmoothSigma = 0.1;

% Create images
imgs = uint8([]);
yTruth = [];
dx = 0;
dy = 0;
for t = 1:100
    curY = rand(2,1) - 0.5;
    yTruth(:,t) = curY;
    dx = dx + curY(1);
    dy = dy + curY(2);
    for i = 1:16
        for j = 1:16
            imgs(i,j,:,t) = testImageX(j-dx, i-dy);
            %         t = 1;
            %         for dx = 20:-1:-20
            %             imgs(i,j,:,t) = testImageX(j-dx, i);
            %             t = t + 1;
            %         end
            %         for dy = 20:-1:-20
            %             imgs(i,j,:,t) = testImageY(j, i-dy);
            %             t = t + 1;
            %         end
        end
    end
end

% Velocities
yinit = ones(2,t);
yTrain = Inf(size(yTruth));
yTrain(:,2:11) = yTruth(:,2:11);
% yinit = [
%     0 0 0 0 0
%     0 0 0 0 0 ];
% y = [
%     inf 0 0 1 1
%     inf 1 1 0 0 ];
% yinit = [ 0 0 0 0 0 ];
% y = [ 0 1 inf ];

N = size(yTrain,2);
q = size(yTrain,1);

% Save images
for t = 1:size(imgs,4)
    imwrite(imgs(:,:,:,t), sprintf('simpletest1_imgs/img%03d.png', t));
end

if 0
    
% Create factors
framecache = basisflow.FrameCache('simpletest1_imgs/img%03d.png');
fg = gtsam.NonlinearFactorGraph;
imageFactors = {};
latentPriorFactors = gtsam.NonlinearFactorGraph;
for t = 2:N
    imageFactors{t} = basisflow.FullImageFactor.CreateAllPixels( ...
        symbol('V',0), symbol('y',t), ...
        symbol('S',0), symbol('S',1), symbol('S',2), symbol('S',3), ...
        1.0, t, size(imgs,2), size(imgs,1), framecache);
    fg.push_back(imageFactors{t});
    if all(isfinite(yTrain(:,t)))
        p = PriorFactorLieVector( ...
            symbol('y',t), LieVector(yTrain(:,t)), noiseModel.Isotropic.Sigma(q, 0.001));
        latentPriorFactors.add(p);
        fg.add(p);
    else
        p = PriorFactorLieVector( ...
            symbol('y',t), LieVector(zeros(size(yTrain,1),1)), noiseModel.Isotropic.Sigma(q, 1));
        latentPriorFactors.add(p);
        fg.add(p);
    end
end
for j = 0:3
    fg.add(PriorFactorLieScalar( ...
        symbol('S',j), LieScalar(S(j+1)), noiseModel.Isotropic.Sigma(1, 1.0)));
end

% Add smoothness field
smoothnessFactors = gtsam.NonlinearFactorGraph;
for i = 0:(size(imgs,1)-1)
    for j = 0:(size(imgs,2)-1)
        for k = 1:q
            pix = j*size(imgs,1) + i;
            if j >= 1
                for xy = 1:2
                    precsM = zeros(2,q);
                    precsM(xy,k) = 1/(flowSmoothSigma^2);
                    precs = reshape(precsM', 2*q, 1);
                    p = gtsam.BetweenFactorLieMatrix( ...
                        symbol('V',pix), symbol('V',(j-1)*size(imgs,1)+i), ...
                        LieMatrix(zeros(2,q)), noiseModel.Diagonal.Precisions(precs));
                    fg.add(p);
                    smoothnessFactors.add(p);
                end
            end
            if i >= 1
                for xy = 1:2
                    precsM = zeros(2,q);
                    precsM(xy,k) = 1/(flowSmoothSigma^2);
                    precs = reshape(precsM', 2*q, 1);
                    p = gtsam.BetweenFactorLieMatrix( ...
                        symbol('V',pix), symbol('V',j*size(imgs,1)+(i-1)), ...
                        LieMatrix(zeros(2,q)), noiseModel.Diagonal.Precisions(precs));
                    fg.add(p);
                    smoothnessFactors.add(p);
                end
            end
        end
    end
end

% Add basis priors
basisPriors = gtsam.NonlinearFactorGraph;
for j = 0:(numel(imgs(:,:,1,1))-1)
    p = gtsam.PriorFactorLieMatrix( ...
        symbol('V',j), LieMatrix(zeros(2,q)), noiseModel.Isotropic.Sigma(2*q, 1000.0));
    fg.add(p);
    basisPriors.add(p);
end

% Create initializations
values = gtsam.Values;
for j = 0:(numel(imgs(:,:,1,1))-1)
    values.insert(symbol('V',j), LieMatrix(rand(2,q)));
end
for t = 2:size(imgs,4)
    values.insert(symbol('y',t), LieVector(yinit(:,t)));
end
for j = 0:3
    values.insert(symbol('S',j), LieScalar(S(j+1)));
end

% ordering = fg.orderingCOLAMD(values);
% gfg = fg.linearize(values, ordering);
% 
% solver = gtsam.GaussianSequentialSolver(gfg, true);
% delta = solver.optimize();

%params = gtsam.DoglegParams;
params = gtsam.GaussNewtonParams;
params.setVerbosity('ERROR');
%params.setVerbosityDL('VERBOSE');
%opt = gtsam.DoglegOptimizer(fg, values, params);
opt = gtsam.GaussNewtonOptimizer(fg, values, params);
errors = [];
for iter=1:50
    
    % Get linear system
    %ordering = fg.orderingCOLAMD(values);
    %gfg = fg.linearize(values, ordering);
    
    opt.iterate();
    errors(iter) = opt.error;
    %opt.values().at(symbol('V',8*16+8)).print('V: ');

    if mod(iter,5) == 0
        %for j = 2:size(yTrain,2)
        %    opt.values().at(symbol('y',j)).print(sprintf('y%d: ',j));
        %end

        ppcares.W = zeros(2*16*16, q);
        for pix=0:(16*16-1)
            ppcares.W(1+2*pix : 1+2*pix+1, :) = opt.values().at(gtsam.symbol('V', pix)).matrix();
        end
        ppcares.basissize(4:5) = [ 16 16 ];
        inspector.drawBasis(ppcares);
    end
    
%     latentPriorFactors.error(opt.values)
%     
%     newValues = basisAlignment.rotateBasis(opt.values, yTrain);
%     opt = gtsam.GaussNewtonOptimizer(fg, newValues, params);
%     
%     latentPriorFactors.error(opt.values)
% 
%     if mod(iter,5) == 0
%         ppcares.W = zeros(2*16*16, q);
%         for pix=0:(16*16-1)
%             ppcares.W(1+2*pix : 1+2*pix+1, :) = opt.values().at(gtsam.symbol('V', pix)).matrix();
%         end
%         ppcares.basissize(4:5) = [ 16 16 ];
%         inspector.drawBasis(ppcares);
%     end

end

end

