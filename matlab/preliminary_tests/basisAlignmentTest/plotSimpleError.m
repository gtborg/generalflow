
y = [ -5 : 0.1 : 5 ];
W = [ -5 : 0.1 : 5 ];
gradIt = 1.0;
gradIx = 1.0;

meshx = [];
meshy = [];
meshe = [];
i = 1;
for yi = 1:size(y,2)
    for Wi = 1:size(W,2)
        meshx(yi,Wi) = W(Wi);
        meshy(yi,Wi) = y(yi);
        meshe(yi,Wi) = ...
            (gradIt - gradIx * W(Wi) * y(yi)) .^ 2 + ...
            ((1-y(yi)) ./ 0.1) .^ 2;
    end
end

mesh(meshx, meshy, meshe);
xlabel('W');
ylabel('y');
