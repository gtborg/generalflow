function generateImages

for i=1:16
    for j=1:16
        im1(i,j)=sqrt((i-8)^2+(j-8)^2)*(1/25);
        im2(i,j)=sqrt((i-8)^2+(j-8-1)^2)*(1/25);
        im3(i,j)=sqrt((i-8-1)^2+(j-8-1)^2)*(1/25);
    end
end

figure;
subplot(2,2,1); imshow(im1);
subplot(2,2,2); imshow(im2);
subplot(2,2,3); imshow(im3);

imwrite(im1, 'im1.png');
imwrite(im2, 'im2.png');
imwrite(im3, 'im3.png');

end