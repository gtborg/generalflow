
W1 = [ -5 : 0.1 : 5 ];
W2 = [ -5 : 0.1 : 5 ];
gradIt = [ 1.0; 0.0 ];
gradIx = [
    1.0 0.0
    0.0 0.0 ];
y = [ 1.0; 1.0 ];

meshx = [];
meshy = [];
meshe = [];
i = 1;
mine = inf;
for W1i = 1:size(W1,2)
    for W2i = 1:size(W2,2)
        meshx(W1i,W2i) = W1(W1i);
        meshy(W1i,W2i) = W2(W2i);
        e = 0;
        for i = 1:size(gradIt,1)
            e = e + (gradIt(i,:) - gradIx(i,:) * [ W1(W1i); W2(W2i) ] * y(i)) .^ 2;
        end
        meshe(W1i,W2i) = e;
        if e < mine
            mine = e;
            minW1 = W1(W1i);
            minW2 = W2(W2i);
        end
    end
end

mesh(meshx, meshy, meshe);
xlabel('W2');
ylabel('W1');

[ minW1 minW2 ]