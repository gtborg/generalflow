function u = oneRowFlow(theta, v)
%ONEPIXELTEST Summary of this function goes here
%   Detailed explanation goes here

x = theta + (-1:0.1:1);
x(abs(x) < 0.5) = 0;

direction = reshape([ x; repmat(-1,1,length(x)) ], 1, length(x)*2);
u = direction .* v;

end

