%%
x = [];
y = [];
z = [];
u1 = [];
u2 = [];
u3 = [];

for theta = -1 : 0.1 : 1
    for v = 1 %0 : 0.1 : 1
        %for omega = 0 : 0.1 : 1
            u = oneRowFlow(theta, v);
            x = [ x -1:0.1:1 ];
            y = [ y repmat(theta, 1, length(u)/2) ];
            z = [ z zeros(1, length(u)/2) ];
            u1 = [ u1 u(1:2:end) ];
            u2 = [ u2 u(2:2:end) ];
            u3 = [ u3 zeros(1, length(u)/2) ];
        %end
    end
end

figure; quiver3(x,y,z,u1,u2,u3);
xlabel('x');
ylabel('\theta');
zlabel('');
