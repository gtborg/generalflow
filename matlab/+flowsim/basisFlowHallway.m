function [Fu,Fv] = basisFlowHallway(velocity, width, height, K, env)
%BASISFLOWUNDISTORTED Summary of this function goes here
%   Detailed explanation goes here

    Fu = zeros(height, width);
    Fv = zeros(height, width);
    
    f = 0.5*(K(1,1)+K(2,2));
    C = [K(1,3) K(2,3)]';
    
    vrot = velocity(1:3);
    vtrans = velocity(4:6);
    
    for u=1:width
        for v=1:height
            pd_pix = [u v]';
            
            pn_img = (pd_pix - C);
                        
            x = pn_img(1);
            y = pn_img(2);
            
            %A = [f  0 x
            %     0 -f y];
            A = [x -f  0
                 y 0 -f];
            
            %B = [x*y/f      -(f+x^2/f)   y;
            %    f+y^2/f     -(x*y)/f     -x];
            B = [  y          x*y/f      -(f+x^2/f);
                   -x        f+y^2/f     -(x*y)/f];
               
            % Compute inverse depth for hallway
            % Compute X intersection with ground plane
            robotZ = -1.0;
            wallY = 2.0;
            Xgnd = abs(robotZ * (f/y));
            % Compute Y at ground plane intersection
            Ygnd = Xgnd * (x/f);
            
            % Determine whether we're looking at the ground, sky, or wall
            place = '';
            if env == 'h'
                if y >= 0 && abs(Ygnd) < wallY
                    place = 'g';
                else
                    place = 'w';
                end
            elseif env == 'l'
                if y >= 0 && Ygnd > -wallY
                    place = 'g';
                elseif y < 0 && x > 0
                    place = 's';
                else
                    place = 'w';
                end
            elseif env == 'r'
                if y >= 0 && Ygnd < wallY
                    place = 'g';
                elseif y < 0 && x < 0
                    place = 's';
                else
                    place = 'w';
                end
            elseif env == 'p'
                if y >= 0
                    place = 'g';
                elseif y < 0
                    place = 's';
                end
            end
            
            % Compute the depth accordingly
            if place == 'g'
                % We're looking at the ground plane
                z = Xgnd;
            elseif place == 's'
                % We're looking at the sky
                z = 1e50;
            elseif place == 'w'
                % We're looking at the wall
                Xwall = wallY * (f/abs(x));
                z = Xwall;
            else
                z = 1e50;
            end
            
            p = 1 / z;
            
            flow = p*A*vtrans + B*vrot;
            Fu(v,u) = flow(1);
            Fv(v,u) = flow(2);

        end
    end
   % figure;[X,Y]=meshgrid(1:1:100);quiver(X,Y,Fu,Fv)
    
end