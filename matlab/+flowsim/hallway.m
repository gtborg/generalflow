function hallway()
%HALLWAY Summary of this function goes here
%   Detailed explanation goes here

% Set up camera
%camproj('perspective');
%campos([ -5 -1 -2 ]);
%camtarget([ -4 -1 -2 ]);
%camup([0 0 -1])
%set(gca, 'Visible', 'off');
%camva(40);
%set(gcf, 'RendererMode', 'manual');
%set(gcf, 'Renderer', 'zbuffer');

tx = 0;
ty = 0;
tz = -1.0;

vx = 1.0;
fps = 30;

frame = 1;
for t=0:(1/fps):1

    clf;
    set(gcf, 'Color', 'white');
    %set(gcf, 'Position', [1 1 640 640]);
    set(gca, 'Position', [0 0 1 1]);
    set(gcf, 'PaperPosition', [0 0 6.4 6.4]);
    set(gca, 'CameraPosition', [tx ty tz]);
    set(gca, 'CameraTarget', [tx+1 ty tz]);
    set(gca, 'CameraUpVector', [0 0 -1]);
    set(gca, 'Projection', 'perspective');
    set(gca, 'CameraViewAngle', 90);
    set(gca, 'Visible', 'off');
    set(gcf, 'Renderer', 'zbuffer');
    xlim([-1 1])
    ylim([-1 1])
    zlim([-1 1])
    set(gca, 'XForm', eye(4));
    
    % Draw floor
    for x=0:50
        for y=-2:1
            if mod(x+y, 2) == 0
                color = [0 0 0];
            else
                color = [1 1 1];
            end
            patch([x x+1 x+1 x], [y y y+1 y+1], [0 0 0 0], color, 'EdgeColor', 'none', 'Clipping', 'off');
        end
    end
    
    % Draw marker
    %patch([2 3 3 2], [0 0 1 1], [0 0 0 0], [1 0 0 0], 'EdgeColor', 'none');
    
    % Draw walls
    for x=0:50
        for z=-4:0
            if mod(x+z, 2) == 0
                color = [0 0 0];
            else
                color = [1 1 1];
            end
            patch([x x+1 x+1 x], [-2 -2 -2 -2], [z z z+1 z+1], color, 'EdgeColor', 'none', 'Clipping', 'off');
            patch([x x+1 x+1 x], [2 2 2 2], [z z z+1 z+1], color, 'EdgeColor', 'none', 'Clipping', 'off');
        end
    end
    
    drawnow;
    
    print('-dpng', '-r400', sprintf('%08d', frame));
    frame = frame + 1;
        
    tx = tx + vx/fps;

end

end

