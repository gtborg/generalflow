function subspace = basisFlowInfinity(K, width, height, imgWidth, imgHeight)
%BASISFLOWGROUNDPLANE Generate basis flows for a given camera pose and
%calibration, for a ground plane.

import gtsam.*
import basisflow.*

basis = zeros(width*height*2, 6);

for k = 1:6
    v = zeros(6,1);
    v(k) = 1.0;
    basis(:,k) = reshape( ...
        flowGeometry.flowFieldInfinity(K, v, width, height, imgWidth, imgHeight), ...
        2*width*height, 1);
end

% Put basis into a subspace structure
subspace = Subspace(basis, 1.0, 10.0, 0.9, width, height);

end

