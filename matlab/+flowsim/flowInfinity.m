function flow = flowInfinity(v, K, width, height, imgWidth, imgHeight)
%FLOWINFINITY Summary of this function goes here
%   Detailed explanation goes here

import gtsam.*
import generalflow.*

xstep = imgWidth / width;
ystep = imgHeight / height;

flow = zeros(2*height, width);

x = 0;
for j = 0:(width-1)
    y = 0;
    for i = 0:(height-1)
        
        pixel = Point2(x,y);
        
        u = geometry.opticalFlowInfinity(K, pixel, v);
        flow(2*i+1 : 2*i+2, j+1) = u;
        
        y = y + ystep;
    end
    
    x = x + xstep;
end

end

