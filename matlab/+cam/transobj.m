function O = transobj(T, O)
%TRANSOBJ Transform a cell array of coordinates by the transformation T

for i = 1:numel(O)
    O{i} = T * O{i};
end

end

