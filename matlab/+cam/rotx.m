function A = rotx(angle)
%UNTITLED1 Summary of this function goes here
%  Detailed explanation goes here
A = [ 1 0 0 0
    0 cos(angle) -sin(angle) 0
    0 sin(angle) cos(angle) 0
    0 0 0 1 ];