function A = roty(angle)
%UNTITLED1 Summary of this function goes here
%  Detailed explanation goes here
A = [ cos(angle) 0 sin(angle) 0
    0 1 0 0
    -sin(angle) 0 cos(angle) 0
    0 0 0 1 ];
