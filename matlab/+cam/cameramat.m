function C = cameramat( eulers, Pcam )

eulers = pi*eulers/180;

C = roty(-eulers(3)) * rotx(-eulers(2)) * roty(-eulers(1)) * trans(-Pcam);
%C = inv(trans(Pcam) * roty(eulers(1)) * rotx(eulers(2)) * roty(eulers(3)));
%C = C(1:end-1,:);