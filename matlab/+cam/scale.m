function A = scale(S)
%SCALE Return a scaling matrix

A = eye(4);
A(1:3,1:3) = diag(S);