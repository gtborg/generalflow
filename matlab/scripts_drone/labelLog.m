function [ labels, costmap, positions ] = labelLog(directory, flight, droneID, firstFrame, lastFrame, measurements, superpixelLabels)
%LABELLOG Label the log in the given directory with the given flight number.

calibrations.(['d',num2str(1231)]).K = [ ...
    1122.35825, 0, 649.26443
    0, 1146.38402, 222.25459
    0, 0, 1 ];
calibrations.(['d',num2str(1231)]).kc = [ ...
    0.01725   0.06573   -0.04275   0.00121  0.00000 ];

calibrations.(['d',num2str(6906)]).K = [ ...
    1120.31934, 0, 549.23994
    0, 1117.68691, 195.86712
    0, 0, 1];
calibrations.(['d',num2str(6906)]).kc = [ ...
    -0.01472   0.15893   -0.02829   -0.00724  0.00000 ];

calibrations.(['d',num2str(0070)]).K = [ ...
    1080.53830, 0, 685.19535
    0, 1092.05268, 181.54099
    0, 0, 1];
calibrations.(['d',num2str(0070)]).kc = [ ...
    -0.00270   0.15578   -0.03779   0.01922  0.00000 ];

calibrations.(['d',num2str(0061)]).K = [ ...
    1123.47872, 0, 655.74421
    0, 1107.22609, 324.27659
    0, 0, 1];
calibrations.(['d',num2str(0061)]).kc = [ ...
    -0.51179   0.26092   0.00498   0.00216  0.00000 ];

% Get calibration
droneIDstr = ['d',num2str(droneID)];
if isfield(calibrations, droneIDstr)
    K = calibrations.(droneIDstr).K;
    kc = calibrations.(droneIDstr).kc;
    calibrationD = gtsam.Cal3DS2(K(1,1), K(2,2), K(1,2), K(1,3), K(2,3), ...
        kc(1), kc(2), kc(3), kc(4));
else
    error(['Do not have calibration for drone ',num2str(droneID)]);
end

vidDir = sprintf('%s/video_%05d', directory, flight);
stateFile = sprintf('%s/states_%05d.txt', directory, flight);
labelDir = sprintf('%s/labeled_%05d', directory, flight);

% Detect image sequence format
[ imgFormat, firstFramed, lastFramed ] = dataio.detectImageSequenceFormat(vidDir);
if ~exist('firstFrame', 'var') || isempty(firstFrame)
    firstFrame = firstFramed;
end
if ~exist('lastFrame', 'var') || isempty(lastFrame)
    lastFrame = lastFramed;
end
img = imread(sprintf(imgFormat, firstFrame));
imgWidthO = size(img,2);
imgHeightO = size(img,1);
clear img;
gridWidth = round(imgWidthO / 80);
gridHeight = round(imgHeightO / 80);

% Extract flow
prescale = 0.25;
if ~exist('measurements', 'var') || isempty(measurements) ...
        || ~exist('superpixelLabels', 'var') || isempty(superpixelLabels)
    [ measurements, superpixelLabels ] = learningSparseFlow.extractFlow( ...
        {imgFormat}, firstFrame, lastFrame, gridWidth, gridHeight, 1e-4, prescale, calibrationD);
    measurements = measurements{1};
    superpixelLabels = superpixelLabels{1};
    assignin('base', 'measurements', measurements);
    assignin('base', 'superpixelLabels', superpixelLabels);
end

% Load states
states = dlmread(stateFile);

[ labels, costmap, positions ] = labeling.labelFlowSequence( ...
    imgFormat, firstFrame, lastFrame, prescale, calibrationD, ...
    measurements, superpixelLabels, states(:,2:4), 1, labelDir);

end

