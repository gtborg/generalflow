function visualizeFlowLabels(logDir)

log = dlmread([logDir '/1__logFlowClassifier.txt' ]);
resultFrames = dlmread([logDir '/labeledFlow_frames.txt' ]);
resultFeatures = dlmread([logDir '/labeledFlow_features.txt' ]);
timeoffset = log(1,1);
i=2;
framesLine = 2;
featuresLine = 1;
count=0;

mkdir(sprintf('%s/out2', logDir));

while (i < size(log,1))
    sensor = log(i,:);
    sensor(1,1) = sensor(1,1) - timeoffset
    
    curFrame = sensor(9);
    prevFrame = sensor(10);
    numbFeat = sensor(11);
    
    %% Read features
    if resultFrames(framesLine, 8) ~= curFrame
        error('Frame numbers do not match');
    end
    nFeaturesUsed = resultFrames(framesLine, 9);
    labelLines = resultFeatures(featuresLine:(featuresLine+nFeaturesUsed-1), :);
    framesLine = framesLine + 1;
    featuresLine = featuresLine + nFeaturesUsed;
    
    %% image new
    filename = sprintf('%s/img_%010d.pgm', logDir, curFrame);
    imageNew = imread(filename);
    
    %% image old
    filename = sprintf('%s/img_%010d.pgm', logDir, prevFrame);
    imageOld = imread(filename);
    
    %% Plotting style 1
    imgTop = [imageOld imageNew];
    imgBot = imresize(imageNew, 2);
    img = [imgTop; imgBot;];
    m=figure(1);
    imshow(img);
    hold on;
    %% plot features
    for j=1:numbFeat
        feat = log(i+j,:);
        plot (feat(1), feat(2), 'b+', 'MarkerSize',12);
        plot (feat(3)+size(imageOld,2), feat(4), 'b+', 'MarkerSize',12);
        plot([feat(1); feat(3)+size(imageOld,2);], [feat(2); feat(4)], 'g');
        plot([feat(1)*2; feat(3)*2;], [feat(2)*2+size(imageOld,1); feat(4)*2+size(imageOld,1);], ...
            'Color', [ labelLines(j, 8), labelLines(j, 6), labelLines(j, 7) ]);
    end 
    for j=1:size(labelLines,1)
        if labelLines(j, 1) ~= curFrame
            error('Frame numbers do not match');
        end
        plot(labelLines(j, 4)*2, labelLines(j, 5)*2+size(imageOld,1), '.', 'MarkerSize', 15, ...
            'Color', [ labelLines(j, 8), labelLines(j, 6), labelLines(j, 7) ]);
    end
    hold off;
    drawnow;
  
    img = hardcopy(gcf, '-Dopengl');
    imwrite(img, sprintf('%s/out2/%010d.png', logDir, curFrame));
    
    count = count+1;
    i = i + numbFeat + 1;
end