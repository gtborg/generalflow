function extractLogs(directory)
%EXTRACTLOGS Extract log sessions from AR Drone logs (Jason's configuration
%with GPS and USB logger)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frameRate = 29.75;
videoStartDelay = 0.75;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Finding logs in %s...\n', directory);

% List directory
if ~exist(directory, 'dir')
    error('extractLogs:CannotList', 'Cannot list directory');
end
files = dir(directory);

% Find video and log files
videos = [];
logs = [];
for file = { files.name }
    % Get file parts
    [ ~, name, ~ ] = fileparts(file{:});

    % Find videos and logs
    if length(name) > 6 && strcmp(name(1:6), 'video_')
        videos = [ videos str2double(name(7:11)) ];
    elseif length(name) > 1 && strcmp(name(1), 'P')
        logs = [ logs str2double(name(2:3)) ];
    end
end

    function tokens = tokenize(str, delim)
        delims = strfind(str, delim);
        starts = [ 1 (delims+1) ];
        ends = [ (delims-1) length(str) ];
        tokens = cellfun(@(s,e) str(s:e), num2cell(starts), num2cell(ends), 'UniformOutput', false);
    end

% Read log files
videoindex = 0;
states = {};
for logindex = 1:length(logs)
    fprintf('Reading state log %s/P%02d.txt\n', directory, logs(logindex));
    fd = fopen(sprintf('%s/P%02d.txt', directory, logs(logindex)));
    line = '';
    launched = false;
    while ischar(line)
        line = fgetl(fd);
        
        % Split at timestamp
        [ timestamp, data ] = strtok(line, ':');
        data = data(3:end); % Skip : and space
        t = str2double(timestamp) / 1000.0; % In seconds
        
        % Split data
        toks = tokenize(data, ',');
        
        % Is this a launch / land?
        if ~launched && strcmp(toks{1}, 'LNCH')
            launched = true;
            videoindex = videoindex + 1;
            states{videoindex} = [];
            fprintf('  Flight starting at %f\n', t);
        end
        if launched && strcmp(toks{1}, 'LAND')
            launched = false;
        
            % Plot attitude
            figure;
            plot((states{videoindex}(:,1)-states{videoindex}(1,1)-videoStartDelay)*frameRate, ...
                [states{videoindex}(:,2), states{videoindex}(:,3), states{videoindex}(:,4)]);
            legend('Roll', 'Pitch', 'Yaw');
            title(sprintf('Flight number %d\n', videoindex));
        end
        
        % Is this an unlabeled, i.e. state estimate, line
        if launched && length(toks{1} > 0) && (toks{1}(1) >= '0' && toks{1}(1) <= '9')
            state = str2double(toks);
            states{videoindex} = [ states{videoindex};
                t, ...
                state(2:4) ./ 1000 ./ 180 .* pi, ... % Millideg to radians
                state(6:8) ./ 1000 ... % mm to m
                ];
        end
    end
    fclose(fd);
end

% Split videos
videostates = {};
for videoindex = 1:numel(videos)
    vidDir = sprintf('%s/video_%05d', directory, videos(videoindex));
    fprintf('Splitting %s...\n', vidDir);
    if exist(vidDir, 'file')
        error('extractLogs:NoOverwrite', [vidDir,' already exists, refusing to overwrite for safety']);
    end
    mkdir(vidDir);
    nframes = dataio.framesFromVideo({[vidDir,'.mp4']}, [vidDir,'/%08d.jpg']);
    
    fprintf('Aligning %s...\n', vidDir);
    videostates{videoindex} = [];
    worstTimeDiscrepancy = 0;
    for frame = 1:nframes
        zeroTime = (frame-1) / frameRate + videoStartDelay;
        offsetTime = zeroTime + states{videoindex}(1,1);
        [ timeDiscrepancy, nearestStateIndex ] = min(abs(states{videoindex}(:,1) - offsetTime));
        videostates{videoindex}(frame, :) = states{videoindex}(nearestStateIndex, :);
        if timeDiscrepancy > worstTimeDiscrepancy
            worstTimeDiscrepancy = timeDiscrepancy;
        end
    end
    fprintf('  video_%05d worst time discrepancy: %f s\n', videos(videoindex), worstTimeDiscrepancy);
    if worstTimeDiscrepancy > (7/10)
        error('extractLogs:CannotAlign', 'Stopping because of a large time discrepancy in video/state alignment, suggesting mismatched data');
    end
    stateFile = sprintf('%s/states_%05d.txt', directory, videos(videoindex));
    if exist(stateFile, 'file')
        error('extractLogs:NoOverwrite', [stateFile,' already exists, refusing to overwrite for safety']);
    end
    dlmwrite(stateFile, videostates{videoindex}, 'delimiter', '\t', 'precision', 17);
end

end

