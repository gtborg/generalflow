function viewVelocityEstimation(logFile)
%VIEWVELOCITYESTIMATION Summary of this function goes here
%   Detailed explanation goes here

% Read velocity
vel = dlmread(logFile);

%vc = 2; % For Drone log
vc = 5; % For front camera labeling
%vf = 4; % For Drone log
vf = 8; % For front camera labeling

% Integrate to get position
pos = [0 0];
for i=2:size(vel,1)
    pos = [ pos; pos(i-1,:) + vel(i,vc:(vc+1)) ];
end

% Make plot
    function output_txt = dataTipText(~,event_obj)
        idx = event_obj.DataIndex;
        output_txt = sprintf('Position: (%g, %g)\nFrame: %d\nTime: %.20g + %g', ...
            pos(idx, 1), pos(idx, 2), vel(idx+1, vf), vel(1,1), vel(idx+1, 1)-vel(1,1));
    end
figure; plot(pos(:,2), pos(:,1)); xlabel('y'); ylabel('x');
axis equal;
dcm_obj = datacursormode(gcf);
set(dcm_obj, 'UpdateFcn', @dataTipText);

% Make time plots
if vc == 2
    figure;
    subplot(2,1, 1);
    plot(vel(:,1), [ vel(:,2) vel(:,6) ]);
    ylabel('v_x');
    legend('Estimated v', 'Gyro');
    subplot(2,1, 2);
    plot(vel(:,1), [ vel(:,3) vel(:,5) ]);
    ylabel('v_y');
end

end

