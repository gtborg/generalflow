function [ labelDir, left_calibration, states, firstFrame, lastFrame, imageFileFromFrameNum ] = ...
    prepareLog(directory, forceFirstFrame, forceLastFrame)

% Paths
intrinsics_file = [ directory, '/calibration/opencv/intrinsics.yml' ];
ahrs_file = [ directory, '/AHRSData.txt' ];
left_camera_dir = [ directory, '/camera_front_12062806' ];
labelDir = [ directory, '/camera_front_12062806_labelled.temp' ];

% Load calibration
fprintf('Reading calibration...\n');
calibrations = dataio.readOpenCVcalibration(intrinsics_file);
left_calibration = calibrations{1};

assignin('base', 'K', left_calibration);

% Read AHRS frames
fprintf('Reading AHRS data...\n');
ahrsFileData = dlmread(ahrs_file);

% Read image filenames (in format %08d_%d, i.e. framenum_timestamp)
fprintf('Listing images...\n');
image_extensions = { '.png', '.jpg', '.jpeg', '.bmp', '.pgm', '.ppm' };
if ~exist(left_camera_dir, 'dir')
    error('detectImageSequenceFormat:CannotList', 'Cannot list directory');
end
files = dir(left_camera_dir);
imageFiles = cell(numel(files), 2);
states = zeros(numel(files), 4);
firstFrame = inf;
lastFrame = 0;
fprintf('Matching AHRS data with images...\n');
for file = { files.name }
    % Get file parts
    [ ~, name, extension ] = fileparts(file{:});
    
    % See if it's an image file
    if ~isempty(find(cellfun(@(imgext) strcmpi(imgext, extension), image_extensions), 1, 'first'))
        % Parse frame number and timestamp from filename
        fields = textscan(name, '%08d_%d64');
        frame = double(fields{1});
        time = double(fields{2});
        if frame >= 1
            % Find nearest AHRS frame
            [ td, i ] = min(abs(ahrsFileData(:,1) - time));
            if td < 500
                if frame > lastFrame; lastFrame = frame; end
                if frame < firstFrame; firstFrame = frame; end
                imageFiles{frame, 1} = fields{2};
                imageFiles{frame, 2} = [ name, extension ];
                states(frame, :) = ahrsFileData(i, 1:4);
            end
        end
    end
end
imageFiles = imageFiles(1:lastFrame, :);
states = states(1:lastFrame, :);

% Save states
assignin('base', 'states', states);

% Apply corrections to state
states(:,3) = states(:,3) + (2/180*pi); % Add 2 degrees pitch
states(:,2) = zeros(size(states,1), 1); % Set roll to 0

if exist('forceFirstFrame', 'var') && ~isempty(forceFirstFrame)
    firstFrame = forceFirstFrame;
end
if exist('forceLastFrame', 'var') && ~isempty(forceLastFrame)
    lastFrame = forceLastFrame;
end

% Local function to generate image file names
imageFileFromFrameNum = @(framenum) [ left_camera_dir, '/', imageFiles{framenum,2} ];

end
