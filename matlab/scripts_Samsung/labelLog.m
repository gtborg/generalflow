function [ labels, positions, velocities ] = labelLog(directory, forceFirstFrame, forceLastFrame, measurements, superpixelLabels)
%LABELLOG Summary of this function goes here
%   Detailed explanation goes here

if ~exist('forceFirstFrame', 'var')
    forceFirstFrame = [];
end
if ~exist('forceLastFrame', 'var')
    forceLastFrame = [];
end

[ labelDir, left_calibration, states, firstFrame, lastFrame, imageFileFromFrameNum ] = ...
    prepareLog(directory, forceFirstFrame, forceLastFrame);

prescale = 1/32;

if ischar(measurements) && strcmp(measurements, 'dense')
    
    measurements = [];
    superpixelLabels = [];
    
else

    % Extract optical flow
    img = imread(imageFileFromFrameNum(firstFrame));
    imgWidthO = size(img,2);
    imgHeightO = size(img,1);
    clear img;
    gridWidth = round(imgWidthO / 80);
    gridHeight = round(imgHeightO / 80);
    if ~exist('measurements', 'var') || isempty(measurements) ...
            || ~exist('superpixelLabels', 'var') || isempty(superpixelLabels)
        [ measurements, superpixelLabels ] = learningSparseFlow.extractFlow( ...
            {imageFileFromFrameNum}, firstFrame, lastFrame, gridWidth, gridHeight, ...
            1e-4, prescale, left_calibration);
        measurements = measurements{1};
        superpixelLabels = superpixelLabels{1};
        assignin('base', 'measurements', measurements);
        assignin('base', 'superpixelLabels', superpixelLabels);
    end
    
end

% Label
[ labels, positions, velocities ] = labeling.labelFlowSequence( ...
    imageFileFromFrameNum, firstFrame, lastFrame, prescale, left_calibration, ...
    measurements, superpixelLabels, states(:,2:4), 1.5, labelDir);

end

