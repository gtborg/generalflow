% Test with simulated data

% Set up camera
ry = gtsam.Rot3.Ry(pi/2);
rz = gtsam.Rot3.Rz(pi/2);
pose = gtsam.Pose3(ry.compose(rz), gtsam.Point3(0,0,-1));
K = gtsam.Cal3_S2(20.0, 20.0, 0.0, 10.0, 10.0);

% Compute ground plane subspace
subspace1 = flowsim.basisFlowGroundPlane(pose, K, 20, 20, 20, 20);
inspector.drawBasis(dataio.ppcaresFromSubspace(subspace1));

% Compute infinity structure subspace
subspace2 = flowsim.basisFlowInfinity(K, 20, 20, 20, 20);
inspector.drawBasis(dataio.ppcaresFromSubspace(subspace2));

% Add misalignment
rx = gtsam.Rot3.Rx(0/180 * pi);
poseMisaligned = gtsam.Pose3(ry.compose(rz).compose(rx), gtsam.Point3(0,0,-1));

% Simulate a flow field
v = [ 0; 0; 0.01; 0.1; 0; 0 ];
%flow = flowsim.flowGroundPlane(v, poseMisaligned, K, 20, 20, 20, 20);
[ Fu,Fv ] = flowsim.basisFlowHallway(v, 20, 20, K.matrix, 'h');
flow(1:2:2*size(Fu,1), :) = Fu;
flow(2:2:end, :) = Fv;

figure; inspector.drawFlow(flow); title('Simulated flow');

% Set up Values
basisValues = gtsam.Values;
basisValues.insert(dataio.valuesFromSubspace(subspace1, generalflow.vKey('V',1,0)));
basisValues.insert(dataio.valuesFromSubspace(subspace2, generalflow.vKey('V',2,0)));
sigmaValues = gtsam.Values;
sigmaValues.insert(gtsam.symbol('S',1), gtsam.LieVector([0.05]));
sigmaValues.insert(gtsam.symbol('S',2), gtsam.LieVector([0.05]));
sigmaValues.insert(gtsam.symbol('S',3), gtsam.LieVector([1.0]));

% Do inference
measurements = basisflow.SparseFlowField.FromPairedField(flow, 20, 20);
[ y, indicators ] = labeling.labelFlowField(basisValues, sigmaValues, measurements, v);

disp(y);

% Visualize
indC = zeros(20, 20, 3);
indC(:,:,1) = indicators(:,:,3); % Red for outliers
indC(:,:,2) = indicators(:,:,1); % Green for ground plane
indC(:,:,3) = indicators(:,:,2); % Blue for infinity
figure; image(indC);

