% Test flow field labeling

import gtsam.*
import basisflow.*

basis1 = [
    1
    0
    1
    0 ];
basis2 = [
    0
    1
    0
    1 ];

flow = [
    1 1
    0 0 ];

measurements = SparseFlowField.FromPairedField(flow, 2, 1);

basisValues = Values;
basisValues.insert(vKey('V',1,0), LieMatrix(basis1(1:2, :)));
basisValues.insert(vKey('V',1,1), LieMatrix(basis1(3:4, :)));
basisValues.insert(vKey('V',2,0), LieMatrix(basis2(1:2, :)));
basisValues.insert(vKey('V',2,1), LieMatrix(basis2(3:4, :)));

sigmaValues = Values;
sigmaValues.insert(symbol('S',1), LieVector([0.1]));
sigmaValues.insert(symbol('S',2), LieVector([0.1]));
sigmaValues.insert(symbol('S',3), LieVector([10.0]));

[ v indicators ] = labeling.labelFlowField(basisValues, sigmaValues, measurements);
