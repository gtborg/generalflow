function [ yout, indicators ] = labelFlowFieldDense( ...
    attitude, altitude, camCalibration, inlierSigma, outlierSigma, ...
    inlierPixSigma, outlierPixSigma, groundClassPrior, distantClassPrior, ...
    images, y)
%LABELFLOWFIELD Summary of this function goes here
%   Detailed explanation goes here

import gtsam.*
import generalflow.*
import generalflow.denselabeling.*

q = 6;
graph = NonlinearFactorGraph;

% Initialize
if ~exist('y','var') || isempty(y)
    y = zeros(q,1);
    latentSigma = 100.0;
else
    latentSigma = 0.1;
end

% Create latent variable factors
imageFactor = Velocity2ClassFactor( ...
    symbol('y',0), attitude, altitude, camCalibration, inlierSigma, outlierSigma, ...
    inlierPixSigma, outlierPixSigma, groundClassPrior, distantClassPrior, ...
    images);
graph.add(imageFactor);
graph.add(PriorFactorLieVector(symbol('y',0), LieVector(y), ...
    noiseModel.Isotropic.Sigma(q, latentSigma)));
latentValues = Values;
latentValues.insert(symbol('y',0), LieVector(y));

% Optimize
params = GaussNewtonParams;
params.setVerbosity('ERROR');
opt = GaussNewtonOptimizer(graph, latentValues, params);
latentValues = opt.optimize;
% latentValues.disp;
% for i = 1:5
%     opt.iterate;
%     latentValues = opt.values;
%     %fprintf('Error: %g\n', opt.error);
%     %indicatorValues.disp;
% end
%latentValues.disp;

yout = latentValues.at(symbol('y',0)).vector;

indicators = imageFactor.indicators;
indicators = indicators';
indicators = reshape(indicators, size(images.prevFrame, 1), size(images.prevFrame, 2), 3);

end

