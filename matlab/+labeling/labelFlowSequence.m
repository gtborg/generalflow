function [ labels, positions, velocities ] = labelFlowSequence( ...
    imgFormat, firstFrame, lastFrame, prescale, calibrationD, ...
    measurements, superpixelLabels, attitudesRPY, cameraHeight, labelDir)
%LABELFLOWSEQUENCE Label superpixels in a sequence of optical flow.
% imgFormat: Either a printf-style string or a function that produces image
%   filenames from frame numbers.
% firstFrame: The first frame number (this script will start at
%   firstFrame+1, assuming that the first optical flow is available between
%   firstFrame and firstFrame+1).
% lastFrame: The last frame number.
% prescale: The same prescale parameter passed into
%   learningSparseFlow.extractFlow, this is the factor by which images were
%   scaled before computing optical flow.
% calibrationD: A gtsam.Cal3DS2 calibration for the camera.  This should
%   correspond to the *original* image size, not the prescaled image size.
%   The calibration's focal lengths and image center will be scaled by
%   prescale in this function.
% measurements: Sparse flow measurements from
%   learningSparseFlow.extractFlow.
% superpixelLabels: Superpixel labels from learningSparseFlow.extractFlow.
% attitudesRPY: A lastFrame x 3 matrix of roll-pitch-yaw camera attitudes,
%   attitudesRPY(frame,:) should exist for frame = firstFrame+1:lastFrame,
%   i.e. attitudesRPY may have many ignored rows before firstFrame.
% cameraHeight: The camera height above the ground plane.
% labelDir: The output directory for visualization images.

% Parameters
inlierSigma = 7;
outlierSigma = 15;
inlierPixSigma = 0.01;
outlierPixSigma = 0.01;
groundClassPrior = 0.4;
distantClassPrior = 0.4;

% Get scaled calibration
k = calibrationD.k;
calibrationDscaled = gtsam.Cal3DS2( ...
    calibrationD.fx * prescale, calibrationD.fy * prescale, ...
    calibrationD.skew, calibrationD.px * prescale, calibrationD.py * prescale, ...
    k(1), k(2), k(3), k(4));

% Get non-distortion calibration
calibrationScaled = gtsam.Cal3_S2(calibrationDscaled.fx, calibrationDscaled.fy, ...
    calibrationDscaled.skew, calibrationDscaled.px, calibrationDscaled.py);

% Create output directory
if exist('labelDir', 'var') && ~isempty(labelDir)
%     if exist(labelDir, 'file')
%         error([labelDir,' already exists, exiting to not overwrite it']);
%     end
    mkdir(labelDir);
end

figure;
subplot(2,3,1); cla; inspector.colorTest; title('Color code');

%costmap = basisflow.CostMap(200, 200, 1);
integratedPlanarPose = gtsam.Pose2;
positions = zeros(2, size(attitudesRPY,1));
velocities = zeros(6, lastFrame);

% Do dense labeling if no flow measurements are provided
labelDense = isempty(measurements);

% Create cost map color map
%costmapColors = [
%    repmat(1.0, 64, 1), (0:1/63:1)', (0:1/63:1)'
%    (1:-1/63:0)', repmat(1.0, 64, 1), (1:-1/63:0)' ];

% Loop over frames
labels = {}; %zeros(imgHeight, imgWidth, 3, size(states,1));
indicators = [];
y = zeros(6,1);
for i = firstFrame:lastFrame
    
    tic;
    
    % Set up camera
    rx = attitudesRPY(i,1);
    ry = attitudesRPY(i,2);
    rz = gtsam.Rot2.Logmap(integratedPlanarPose.rotation);
    pose = gtsam.Pose3(gtsam.Rot3.RzRyRx(rx, ry, rz), ...
        gtsam.Point3(integratedPlanarPose.translation.x, integratedPlanarPose.translation.y, ...
        -cameraHeight));
    
    % Draw image
    if ischar(imgFormat)
        imgFile = sprintf(imgFormat, i);
    else
        imgFile = imgFormat(i);
    end
    img = imread(imgFile);
    img = imresize(img, prescale);
    img = uint8(rect(double(img), eye(3,3), ...
        [ calibrationDscaled.fx; calibrationDscaled.fy ], ...
        [ calibrationDscaled.px; calibrationDscaled.py ], ...
        [ calibrationDscaled.k; 0 ]));
    img = double(img) / 255.0;
    subplot(2,3,4); cla; imshow(img); title('Video frame');
    freezeColors;

    if labelDense
        
        if i == firstFrame
            images = generalflow.flow.ImagePairGrad(zeros(size(img)), img);
        else
            
            images = generalflow.flow.ImagePairGrad(images, img);
            
            tic;
            [ y, indicators ] = labeling.labelFlowFieldDense( ...
                pose.rotation, -pose.translation.z, calibrationScaled, ...
                inlierSigma * prescale, outlierSigma * prescale, ...
                inlierPixSigma, outlierPixSigma, ...
                groundClassPrior, distantClassPrior, images);
            toc; fprintf('(inference)\n');
            
        end
 
    else
        
        if i > firstFrame
            
            imgWidth = measurements{i}.imgWidth;
            imgHeight = measurements{i}.imgHeight;
            
            % Compute ground plane subspace
            %subspace1 = flowsim.basisFlowGroundPlane(pose, calibration, ...
            %    imgWidth, imgHeight, imgWidth, imgHeight);
            %subplot(3,3,2); cla; inspector.drawBasis(dataio.ppcaresFromSubspace(subspace1)); title('Ground plane (green)');
            
            % Compute infinity structure subspace
            %subspace2 = flowsim.basisFlowInfinity(calibration, ...
            %    imgWidth, imgHeight, imgWidth, imgHeight);
            %subplot(3,3,3); cla; inspector.drawBasis(dataio.ppcaresFromSubspace(subspace2)); title('Infinity (blue)');
            
            % Get points from measurements
            points = measurements{i}.getPixels;
            
            % Set up Values
            basisValues = gtsam.Values;
            %basisValues.insert(dataio.valuesFromSubspace(subspace1, generalflow.vKey('V',1,0)));
            %basisValues.insert(dataio.valuesFromSubspace(subspace2, generalflow.vKey('V',2,0)));
            basisValues.insert(generalflow.geometry.sparseBasisGroundPlane(pose, calibrationScaled, 1, ...
                imgWidth, imgHeight, generalflow.vKey('V',1,0), points));
            basisValues.insert(generalflow.geometry.sparseBasisInfinity(calibrationScaled, 1, ...
                imgWidth, imgHeight, generalflow.vKey('V',2,0), points));
            sigmaValues = gtsam.Values;
            sigmaValues.insert(gtsam.symbol('S',1), gtsam.LieVector([inlierSigma * prescale]));
            sigmaValues.insert(gtsam.symbol('S',2), gtsam.LieVector([inlierSigma * prescale]));
            sigmaValues.insert(gtsam.symbol('S',3), gtsam.LieVector([outlierSigma * prescale]));
            
            toc;
            fprintf('(setup)\n');
            
            % Draw basis flows
            if isempty(superpixelLabels{i})
                ppcares = dataio.ppcaresFromSubspace(basisflow.Subspace( ...
                    basisValues, generalflow.vKey('V',1,0), 0, imgWidth, imgHeight));
                subplot(2,3,2); cla; inspector.drawBasis(ppcares); title('Ground plane (green)');
                ppcares = dataio.ppcaresFromSubspace(basisflow.Subspace( ...
                    basisValues, generalflow.vKey('V',2,0), 0, imgWidth, imgHeight));
                subplot(2,3,3); cla; inspector.drawBasis(ppcares); title('Infinity (blue)');
            else
                subplot(2,3,2); cla; inspector.drawSuperpixelBasis( ...
                    basisValues, generalflow.vKey('V',1,0), superpixelLabels{i}, measurements{i}.size, ...
                    imgWidth, imgHeight); title('Ground plane (green)');
                subplot(2,3,3); cla; inspector.drawSuperpixelBasis( ...
                    basisValues, generalflow.vKey('V',2,0), superpixelLabels{i}, measurements{i}.size, ...
                    imgWidth, imgHeight); title('Infinity (blue)');
            end
            
            % Draw flow
            if isempty(superpixelLabels{i})
                subplot(2,3,5); cla; inspector.drawFlow(basisflow.sparseFlow.flowOnGrid( ...
                    measurements{i}, imgWidth, imgHeight)); title('Optical flow');
            else
                subplot(2,3,5); cla; ...
                    inspector.drawSuperpixelFlow(measurements{i}, superpixelLabels{i}, imgWidth, imgHeight, 0.1); ...
                    title('Optical flow');
            end
            
            % Do inference
            %y = states(i, 2:end)';
            %disp(y');
            tic;
            [ y, indicators ] = labeling.labelFlowField(basisValues, sigmaValues, ...
                groundClassPrior, distantClassPrior, measurements{i}, superpixelLabels{i});
            toc; fprintf('(inference)\n');
            %labels{i} = indicators;
            
        end
        
    end
    
    disp(y');
    
    % Save velocity
    velocities(:,i) = y;
    
    % Integrate pose
    vFlat = [y(4); y(5); y(3)];
    integratedPlanarPose = integratedPlanarPose.compose(gtsam.Pose2.Expmap(vFlat));
    disp(integratedPlanarPose);
    positions(:,i) = integratedPlanarPose.translation.vector;
    
    % Update cost map
    %costmap.update(pose, calibrationScaled, indicators(:,:,1), indicators(:,:,3), ...
    %    imgWidth, imgHeight, imgWidth, imgHeight);
    
    % Visualize
    if ~isempty(indicators)
        indC = zeros(size(indicators, 1), size(indicators, 2), 3);
        indC(:,:,1) = indicators(:,:,3); % Red for outliers
        indC(:,:,2) = indicators(:,:,1); % Green for ground plane
        indC(:,:,3) = indicators(:,:,2); % Blue for infinity
        subplot(2,3,6); cla; image(indC); axis off; axis image; title('Pixel labels');
    end
    
    %cmapFree = costmap.mapFree;
    %cmapOcc = costmap.mapOccupied;
    %cmapImg = cmapFree - cmapOcc;
    %cmapImg = cmapImg ./ max(max(abs(cmapImg))) * 63 + 63;
    %subplot(3,3,7); cla; image(cmapImg); colormap(costmapColors); axis off; axis image; title('Cost map');
    %freezeColors;
    
    img = hardcopy(gcf, '-Dopengl');
    imwrite(img, sprintf('%s/%08d.png', labelDir, i));
    
    drawnow;
end

end

