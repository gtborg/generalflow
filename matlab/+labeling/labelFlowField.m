function [ yout, indicators ] = labelFlowField(basisValues, sigmaValues, ...
    groundClassPrior, distantClassPrior, measurements, superpixelLabels, y)
%LABELFLOWFIELD Summary of this function goes here
%   Detailed explanation goes here

import gtsam.*
import generalflow.*
import generalflow.flowlabeling.*

q = size(basisValues.at(vKey('V',1,0)).matrix, 2);
clear firstPix;

graph = NonlinearFactorGraph;

indicatorValues = Values;
% for i = 0:(measurements.size-1)
%     indicatorValues.insert(symbol('l',i), LieVector(zeros(3,1)));
% end
generalflow.flow.writeIndicatorValuesMATLAB(indicatorValues, zeros(3,measurements.size));

% Initialize
if ~exist('y','var') || isempty(y)
    y = zeros(q,1);
    latentSigma = 100.0;
else
    latentSigma = 0.1;
end

% Create latent variable factors
graph.push_back(OnlyLatent2FlowFactor.CreateWholeFrame( ...
    measurements, vKey('V',1,0), vKey('V',2,0), symbol('y',0), ...
    symbol('S',1), symbol('l',0), ...
    basisValues, sigmaValues, indicatorValues, groundClassPrior, distantClassPrior));
graph.add(PriorFactorLieVector(symbol('y',0), LieVector(y), ...
    noiseModel.Isotropic.Sigma(q, latentSigma)));
latentValues = Values;
latentValues.insert(symbol('y',0), LieVector(y));

% Optimize
params = GaussNewtonParams;
params.setVerbosity('ERROR');
opt = GaussNewtonOptimizer(graph, latentValues, params);
latentValues = opt.optimize;
% latentValues.disp;
for i = 1:5
    opt.iterate;
    latentValues = opt.values;
    %fprintf('Error: %g\n', opt.error);
    %indicatorValues.disp;
end
%latentValues.disp;

yout = latentValues.at(symbol('y',0)).vector;

indicators = zeros(measurements.imgHeight, measurements.imgWidth, 3);
indVals = generalflow.flow.readIndicatorValuesMATLAB(indicatorValues, measurements.size);
indVals = indVals';
if isempty(superpixelLabels)
    pix = 0;
    indicators = reshape(indVals, measurements.imgHeight, measurements.imgWidth, 3);
%     for j = 0:measurements.imgWidth-1
%         for i = 0:measurements.imgHeight-1
%             indicators(i+1,j+1,:) = ...
%                 permute(indicatorValues.at(symbol('l',pix)).vector, [2,3,1]);
%             pix = pix + 1;
%         end
%     end
else
    for k = 1:3
%         compValues = zeros(measurements.size,1);
%         for l = 0:(measurements.size-1)
%             val = indicatorValues.at(symbol('l',l)).vector;
%             compValues(l+1) = val(k);
%         end
        indicators(:,:,k) = generalflow.flow.fillSuperpixels(superpixelLabels, indVals(:,k));
    end
end

end

