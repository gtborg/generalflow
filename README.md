README - generalflow
====================

What is generalflow
-------------------

generalflow is a collection of libraries and utilities for working with general optical flow subspaces.  Capabilities include learning subspaces from data, and using them to estimate egomotion and semantically label superpixels.

Installation
------------

generalflow works on Linux and Mac, but note that ROS tends to work more straightforwardly on Linux.  Everything has also been compiled on Windows, but with some difficulty due to ROS on Windows.

[generalflow](https://bitbucket.org/gtborg/generalflow) is actually spread across 2 packages - generalflow (this package) contains base libraries that implement the mathematical formulations.  [generalflow_ros](https://bitbucket.org/gtborg/generalflow_ros) contains ROS datastructures and frontend utility programs for running the code.

A few dependencies are required:  Intel TBB, [GTSAM](http://borg.cc.gatech.edu/borg/download), and ROS (tested with the "desktop" Ubuntu package, i.e. ros-indigo-desktop).  It is best to install Intel TBB before GTSAM because GTSAM's CMake configuration will detect TBB and enable multithreaded solving (TBB is optional for GTSAM but required for generalflow).  Also note that GTSAM requires Boost.

Finally, compile and install generalflow and generalflow_ros, in that order:

```
#!bash
$ cd ~/src  # Or any directory you want
$ git clone https://bitbucket.org/gtborg/generalflow.git
$ cd generalflow
$ mkdir build && cd build
$ cmake ..
$ make && sudo make install

$ . /opt/ros/indigo/setup.bash  # Or hydro, etc, your ROS version
$ mkdir -P ~/catkin_ws/src      # Skip if reusing an existing catkin workspace
$ cd ~/catkin_ws/src            # Or cd to any existing workspace
$ catkin_init_ws                # Only if this is a new catkin workspace
$ git clone https://bitbucket.org/gtborg/generalflow_ros
$ cd ..                         # Back to workspace directory
$ catkin_make
```

Usage
-----

There are 3 steps to follow - set parameters, learn optical flow templates, and run inference.  Before doing any of them though, in the terminal(s) you'll work in, you need to source the setup file for your catkin workspace:

$ . ~/catkin_ws/devel/setup.bash

**Set parameters:**

At this time, a few parameters in the source code must be modified:

Open generalflow_ros/generalflow_ros/src/learnTemplates.cpp.  Then edit the parameters on lines 49-52:

 - imagePreScale:  Set this to a global scale factor for the images.  This simply reduces the image resolution to speed up learning and labeling at the cost of reducing detail.  For example, if the original camera images are 1280x1024, setting imagePreScale to 0.5 will scale images down to 640x512 for all operations.  One good technique is to run with a small scale (e.g. 0.125 or 0.0625) to adjust parameters and experiment, and then a high resolution scale (e.g. 0.5 or 1.0) for learning the final subspaces to be used for labeling.
 - roi:  (x, y, w, h) Specify a region of interest to crop the image before working with it.  This is useful for ignoring "garbage" parts of the frame.  To use the full image, you should specify the full **unscaled** image resolution, e.g. (0, 0, 1280, 1024).
 - aboveHorizon:  The approximate image line (counting from line zero at the top of the image) where the horizon is, in the **unscaled** image.  This need not be very accurate, and if the horizon appears curved due to lens distortion, just set this to a rough average value.  If the horizon is very non-horizontal, you may need to instead edit the code that generates the above/below-horizon prior below in this file to generate a different shape.
 - nthFrame:  A parameter to control skipping frames to speed up learning.  Setting this to 1 will use every frame, 5 will use every 5th frame, etc.

 Now, open generalflow_ros/generalflow_ros/src/runInference.cpp.  Edit the parameters on lines 56-60:

 - imagePreScale, roi, aboveHorizon:  Set these to exactly the same values as in learnTemplates.cpp.
 - nthFrame:  Same meaning as in learnTemplates.cpp, but you may set this to something different for the inference step.
 - horizonHalfMargin:  A "fuzz amount" to account for non-exact horizon location and horizon curvature/inclination, again measured in the **unscaled** image.  This parameter adds a padding amount of horizontalHalfMargin above and below the horizon location specified in aboveHorizon.  In this padding region, a prior suggesting above/below horizon is not used.  This is useful to account for horizon curvature, a non-level horizon, and pitching of the vehicle during motion that cause the horizon location to move between frames.

 **Learn optical flow templates:**

 The learnTemplates program learns the optical flow templates, given training data.  The training data consists of videos, stored in ROS bag files.  The following videos must be used:

  - 1 video in which the camera undergoes pure rotation only.  This rotation motion can be arbitrary but should contain diverse rotation directions in at least 3 directions.  A good way to collect this data is to simply hold the camera in your hand and rotate it around arbitrarily while ensuring to cover at least 3 different rotation directions (e.g. yaw, pitch, roll).
  - At least 1 video in which the camera is mounted on the vehicle and is being driven in its typical environment.

Before running the learnTemplates program, a roscore node must be running.  Each time you want to learn a new set of templates, start a bag recording.

$ rosbag record

Then in a new terminal run the learnTemplates program with the above videos as command line arguments:

$ devel/lib/generalflow_ros/learnTemplates rotation_video.bag vehicle_video1.bag vehicle_video2.bag ...

After the learnTemplates program finishes, change to the terminal running 'rosbag record', and press Ctrl-C to stop recording.  Optionally, you can then rename the .bag file to something meaningful.

The recorded bag file contains the learned optical flow templates as well as a lot of visualization data, which makes the file size huge.  At this time, reducing the visualization info requires modifying the source code.

**Run inference:**

Finally, you can run inference on one or more new videos using the learned templates.  Again, start a rosbag record instance to capture the results of inference:

$ rosbag record

Then in a new terminal run:

$ devel/lib/generalflow_ros/runInference learned_templates.bag robot_video1.bag robot_video2.bag

When runInference finishes, Ctrl-C the rosbag record program to stop recording.

Visualizing Results
-------------------

A graphical Python program (using PySide QT bindings, which should get installed with ROS) is included to visualize the results of learning and inference:

$ python src/generalflow_ros/generalflow_ros/Viewer/viewer.py

Click the open button and open a .bag file output from either learnTemplates or runInference.  Depending on your disk speed, the log may take a few minutes to load before the program displays images and becomes responsive.