/**
 * @file    OnlyBasisFlowFactor.h
 * @brief   Optical flow factor on only the basis
 * @author  Richard Roberts
 * @created Nov 30, 2012
 */

#pragma once

#include <basisflow/dllexport.h>
#include <basisflow/SparseFlowField.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace basisflow {

  class generalflow_basisflow_EXPORT OnlyBasisFlowFactor : public gtsam::NonlinearFactor {
  public:
    typedef gtsam::NonlinearFactor Base;
    typedef OnlyBasisFlowFactor This;
    typedef boost::shared_ptr<This> shared_ptr;

  private:
    gtsam::Key latentKey_;
    gtsam::Key sigmaKey_;
    boost::shared_ptr<const gtsam::Values> latentValues_;
    boost::shared_ptr<const gtsam::Values> sigmaValues_;
    double inlierPrior_;
    SparseFlowField::Measurement measurement_;
    
  public:
    /** Constructor
     * @param basisKey Key for basis (n x q LieMatrix)
     * @param latentKey Key for latent variable (q LieVector)
     * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
     * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
     * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix) 
     * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
     * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
     */
    OnlyBasisFlowFactor(gtsam::Key basisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
      boost::shared_ptr<const gtsam::Values> latentValues, boost::shared_ptr<const gtsam::Values> sigmaValues,
      double inlierPrior, const SparseFlowField::Measurement& measurement)
      : Base(basisKey), latentKey_(latentKey), sigmaKey_(sigmaKey), latentValues_(latentValues), sigmaValues_(sigmaValues),
      inlierPrior_(inlierPrior), measurement_(measurement) {}

    /** Create all factors for a whole frame of optical flow measurements */
    static gtsam::NonlinearFactorGraph CreateWholeFrame(
      const SparseFlowField& measurements, gtsam::Key firstBasisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
      boost::shared_ptr<gtsam::Values> latentValues, boost::shared_ptr<gtsam::Values> sigmaValues, double inlierPrior);

    /** Virtual destructor */
    virtual ~OnlyBasisFlowFactor() {}

    gtsam::Key basisKey() const { return Base::keys_[0]; }
    gtsam::Key latentKey() const { return latentKey_; }
    gtsam::Key sigmaKey() const { return sigmaKey_; }

    /** Print */
    virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

    /** Compare two factors */
    virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

    /** Error vector dimension */
    virtual size_t dim() const { return 2; }

    /** Compute the error of the factor for the given variable assignments */
    virtual double error(const gtsam::Values& c) const;

    /** Linearize the factor to a JacobianFactor */
    virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x, const gtsam::Ordering& ordering) const;
  };

}
