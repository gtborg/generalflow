/**
 * @file    OnlySigmaImageFactor.h
 * @brief   Image factor on only the basis
 * @author  Richard Roberts
 * @created Aug 4, 2012
 */

#pragma once

#include <basisflow/dllexport.h>
#include <basisflow/FrameCache.h>

#include <boost/tuple/tuple.hpp>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace basisflow {

  class generalflow_basisflow_EXPORT OnlySigmaImageFactor : public gtsam::NonlinearFactor {
	public:
		typedef gtsam::NonlinearFactor Base;
		typedef OnlySigmaImageFactor This;
		typedef boost::shared_ptr<This> shared_ptr;

	private:
		gtsam::Key basisKey_;
		gtsam::Key latentKey_;
		boost::shared_ptr<const gtsam::Values> basisValues_;
		boost::shared_ptr<const gtsam::Values> latentValues_;

		double inlierPrior_;
		std::vector<int> frameNums_;
		boost::shared_ptr<FrameCache> frameCache_;
		
	public:
		/** Constructor
		 * @param basisKey Key for basis (n x q LieMatrix)
		 * @param latentKey Key for latent variable (q LieVector)
		 * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
		 * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
		 * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix) 
		 * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
		 * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
		 */
		OnlySigmaImageFactor(gtsam::Key basisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
 			boost::shared_ptr<const gtsam::Values> basisValues, boost::shared_ptr<const gtsam::Values> latentValues,
			double inlierPrior, const std::vector<int>& frameNums, boost::shared_ptr<FrameCache> frameCache)
			: Base(sigmaKey), basisKey_(basisKey), latentKey_(latentKey), basisValues_(basisValues), latentValues_(latentValues),
			inlierPrior_(inlierPrior), frameNums_(frameNums), frameCache_(frameCache) {}

		/** Virtual destructor */
		virtual ~OnlySigmaImageFactor() {}

		gtsam::Key basisKey() const { return basisKey_; }
		gtsam::Key latentKey() const { return latentKey_; }
		gtsam::Key sigmaKey() const { return Base::keys_[0]; }

		/** Print */
		virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

		/** Compare two factors */
		virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

		/** Error vector dimension */
		virtual size_t dim() const { return 1; }

		/** Compute the error of the factor for the given variable assignments */
		virtual double error(const gtsam::Values& c) const;

		/** Linearize the factor to a JacobianFactor */
		virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x, const gtsam::Ordering& ordering) const;

	private:
		double errorOneFrame(int frameNum, const Eigen::Vector4d& sigmas) const;
		boost::tuple<double,Eigen::Vector4d,Eigen::Vector4d>
			errorAndDerivsOneFrame(int frameNum, const Eigen::Vector4d& sigmas) const;
	};

}
