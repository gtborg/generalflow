/*
 * GMPPCALucasKanade.cpp
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */

#include <gtsam/base/timing.h>
#include <gtsam/base/debug.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/inference/GenericSequentialSolver-inl.h>
#include <gtsam/linear/GaussianMultifrontalSolver.h>
#include <gtsam/linear/HessianFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/DoglegOptimizerImpl.h>
#include <gtsam/nonlinear/NonlinearOptimizer.h>
#include <gtsam/nonlinear/Symbol.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <limits>
#include <functional>
#include <cmath>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/math/special_functions/fpclassify.hpp>

#include <opencv2/highgui/highgui.hpp>

#include "GMPPCALucasKanade.h"
#include "Utils.h"

bool snapshot = false;

//int xg,yg,new1=1; // used for mousecall back funtion for debugging

using namespace gtsam;
using namespace gtsam::symbol_shorthand;
using namespace std;
namespace bn = boost::numeric::ublas;
using boost::math::lround;

//void onMouse( int , int , int , int, void* ); // mousecallback funtion

namespace basisflow {

  string imageFileDebugStem;

  inline double sq(double x) { return x*x; }

  /* ************************************************************************* */
  std::vector<Subspace> GMPPCALucasKanade::subspacePyramid(const Subspace& subspace, size_t imWidth, int nPyrLevels) {

    size_t imHeight = subspace.dimSpace() / 2 / imWidth;
    std::vector<Subspace> pyramid(nPyrLevels);
    pyramid[0] = subspace;
    cout << "Level 0, " << pyramid[0].basis().rows() << "x" << pyramid[0].basis().cols() << " for " << imWidth << "x" << imHeight << endl;

    // Use OpenCV to downsample the subspace
    size_t curWidth = imWidth;
    size_t curHeight = imHeight;
    for(int level = 1; level < nPyrLevels; ++level) {
      // Downsampled basis that we'll construct
      Matrix basis(pyramid[level-1].dimSpace()/4, subspace.dimLatent());
      curWidth /= 2;
      curHeight /= 2;
      cout << "Level " << level << ", " << basis.rows() << "x" << basis.cols() << " for " << curWidth << "x" << curHeight << endl;
      for(size_t k = 0; k < subspace.dimLatent(); ++k) {
        // Get one basis vector from the last basis
        Matrix currentM = sub(pyramid[level-1].basis(), 0,pyramid[level-1].dimSpace(), k,k+1);
        // Interpret as OpenCV "image"
        cv::Mat current(curHeight*2, curWidth*2, CV_64FC2, &currentM(0,0));
        // Downsample by 1/2
        Matrix downsampledM(curHeight*curWidth*2, 1);
        cv::Mat downsampled(curHeight, curWidth, CV_64FC2, &downsampledM(0,0));
        assert((double*)(downsampled.datalimit) == &downsampledM(downsampledM.rows()-1, 0) + 1);
        cv::pyrDown(current, downsampled);
        // Copy into the downsampled basis
        //ublas::noalias(ublas::subrange(basis, 0,basis.rows(), k,k+1)) = downsampledM;
        basis.block(0, k, basis.rows(), 1).noalias() = downsampledM;
      }
      // Create downsampled subspace, dividing flow by 2 and sigmas by 2 (sigma^2 divided by 4).
      pyramid[level] = Subspace(
          basis * 0.5, pyramid[level-1].inlierSigma2()/4.0, pyramid[level-1].outlierSigma2()/4.0, subspace.inlierPrior(), curWidth, curHeight);
//      gtsam::print(basis, "basis: ");
    }

    return pyramid;
  }

  /* ************************************************************************* */
  GMPPCALucasKanade::GMPPCALucasKanade(const Subspace& subspace, const Params& params, size_t imWidth) :
      frameNum_(0), subspaces_(subspacePyramid(subspace,imWidth,params.nPyrLevels)), params_(params),
        imgOps1_(new ImgOps(params.blurSigma, 2*lround(3.0*params.blurSigma)+1)),
      imgOps_(new ImgOps(params.blurSigma, 2*lround(3.0*params.blurSigma)+1)),
      latent(params.nPyrLevels), outliers(params.nPyrLevels) {}

  /* ************************************************************************* */
  void GMPPCALucasKanade::setNextFrame(const cv::Mat& frame) {
    // Convert to grayscale and resize
    cv::Mat curFrame;
    if(frame.channels() > 1) {
      if(params_.makeColorAvailable) {
        curScaled_.create(frame.rows>>params_.imageScalePower, frame.cols>>params_.imageScalePower, CV_8UC3);
        cv::resize(frame, curScaled_, curScaled_.size(), 0.0, 0.0, cv::INTER_AREA);
        cv::Mat cur8u;
        cv::cvtColor(curScaled_, cur8u, CV_BGR2GRAY);
        cur8u.convertTo(curFrame, CV_64F, 1.0/255.0);
        cv::resize(curScaled_, curScaled_, cv::Size(), 1.0 / (1 << params_.firstPyrLevel), 1.0 / (1 << params_.firstPyrLevel), cv::INTER_AREA);
      } else {
        cv::Mat grayframe;
        cv::cvtColor(frame, grayframe, CV_BGR2GRAY);
        cv::Mat cur8u(frame.rows>>params_.imageScalePower, frame.cols>>params_.imageScalePower, CV_8UC1);
        cv::resize(grayframe, cur8u, cur8u.size(), 0.0, 0.0, cv::INTER_AREA);
        cur8u.convertTo(curFrame, CV_64F, 1.0/255.0);
      }
    } else {
      if(frame.depth() == CV_64F)
        curFrame = frame;
      else if(frame.depth() == CV_32F)
        frame.convertTo(curFrame, CV_64F);
      else if(frame.depth() == CV_8U)
        frame.convertTo(curFrame, CV_64F, 1.0/255.0);
      else
        throw std::invalid_argument("Unknown input image type in GMPPCALucasKanade::setNextFrame");
    }

    curFrame_.swap(prevFrame_);
    ImagePyramid curPyramid(curFrame, params_.nPyrLevels);
    curFrame_.swap(curPyramid);
  }

  /* ************************************************************************* */
  void GMPPCALucasKanade::nextFrame(const cv::Mat& frame, Vector& out_latent, Vector& out_inliers) {

    ++ frameNum_;

    gttic(setNextFrame);
    setNextFrame(frame);
    gttoc(setNextFrame);

    gttic(computeFlow);
    if(!prevFrame_.empty())
      computeFlow(out_latent, out_inliers);
    gttoc(computeFlow);

  }

  /* ************************************************************************* */
  pair<double, Point2> GMPPCALucasKanade::errorAndImgGrad(
      const cv::Mat& prevFrameGradX, const cv::Mat& prevFrameGradY,
      const cv::Mat& prevFrame, const cv::Mat& curFrame,
      pair<long int,long int> loc, const Point2& x) const {
    long int pimx = loc.first;
    long int pimy = loc.second;
    double previmgval;
    int krad = (imgOps_->size() - 1) / 2;
    if(pimx >= krad && pimy >= krad && pimx < prevFrame.cols-krad && pimy < prevFrame.rows-krad)
      previmgval = prevFrame.at<double>(pimy, pimx);
    else
      previmgval = std::numeric_limits<double>::infinity();

    if(ISDEBUG("zero error")) {
      cout << "prevFrame(" << pimx << "," << pimy << ") = " << previmgval << endl;
      cout << "curFrame(" << pimx << "," << pimy << ") = " << curFrame.at<double>(pimy,pimx) << endl;
    }

    pair<double,Point2> ret;

    ret.first = imgOps_->resample(curFrame, Point2(pimx,pimy) + x);

    ret.second = Point2(prevFrameGradX.at<double>(pimy, pimx), prevFrameGradY.at<double>(pimy, pimx));
    if(ISDEBUG("zero error")) {
      cout << "estimated cur(" << pimx << "," << pimy << ") = " << ret.first << endl;
    }
    ret.first -= previmgval;

    return ret;
  }

  /* ************************************************************************* */
  pair<Matrix,Matrix> GMPPCALucasKanade::gradientMatrices(
      const cv::Mat& prevFrameGradX, const cv::Mat& prevFrameGradY,
      const cv::Mat& prevFrame, const cv::Mat& curFrame, const Subspace& basis, const Vector& y, const vector<pair<size_t, size_t> >& imgXYs) const {
    gttic(GMPPCALucasKanade_gradientMatrices);

    double pixSigmaIn = 2.0 / 255.0;
    double pixSigmaOut = 5.0 / 255.0;
    double sigmaIn2 = basis.inlierSigma2();
    double sigmaOut2 = basis.outlierSigma2();

    //cv::imshow("X grad", Utils::colorize(prevFrameGradX));
    //cv::imshow("Y grad", Utils::colorize(prevFrameGradY));
    //cv::waitKey(5);

    gttic(allocate);
    Matrix AbIn(imgXYs.size(), basis.basis().cols()+1);
    Matrix AbOut(imgXYs.size(), basis.basis().cols()+1);

//    lastErrs.resize(imgXYs.size());
    gttoc(allocate);

    cv::Mat warped, errs;
    if(!imageFileDebugStem.empty()) {
      warped.create(curFrame.rows, curFrame.cols, CV_64FC1);
      errs.create(curFrame.rows, curFrame.cols, CV_64FC1);
    }

    gttic(compute_A_b);
    Vector v = basis.basis() * y;
    Matrix J;
    for(size_t i = 0; i < imgXYs.size(); ++i) {

      size_t trueloc = imgXYs[i].second * curFrame.cols + imgXYs[i].first;

      //      gttic(transform);
      // Transform with basis to get image shift
      assert(y.size() == basis.basis().cols());
      Point2 u(v(2*trueloc), v(2*trueloc+1));

      long int pimx = imgXYs[i].first;
      long int pimy = imgXYs[i].second;
      long int cimx = imgXYs[i].first + lround(u.x());
      long int cimy = imgXYs[i].second + lround(u.y());
      int krad = (imgOps_->size() - 1) / 2;
      bool inBounds = pimx >= krad && pimy >= krad && pimx < prevFrame.cols-krad && pimy < prevFrame.rows-krad &&
          cimx >= krad && cimy >= krad && cimx < prevFrame.cols-krad && cimy < prevFrame.rows-krad;

      double err;
      double gradX;
      double gradY;
      double invsigmaIn, invsigmaOut;
      //      gttoc(transform);
      if(inBounds) {
        // Compute pixel intensity error and image gradient
        //        gttic(errorAndImgGrad);
        pair<double, Point2> err_grad(errorAndImgGrad(
            prevFrameGradX, prevFrameGradY, prevFrame, curFrame, make_pair(pimx,pimy), u));
        assert(std::isfinite(err_grad.first));
        err = err_grad.first;
        gradX = err_grad.second.x();
        gradY = err_grad.second.y();

        if(!imageFileDebugStem.empty()) {
          warped.at<double>(pimy,pimx) = prevFrame.at<double>(pimy, pimx) + err;
          errs.at<double>(pimy,pimx) = err;
        }

        // Compute sigma of the image intensity measurement, which is dependent on
        // the image gradient (see derivation)
        if(gradX > 1e-10 || gradX < -1e-10 || gradY > 1e-10 || gradY < -1e-10) {
          invsigmaIn = 1.0 / sqrt((gradX*gradX + gradY*gradY) * sigmaIn2 + pixSigmaIn*pixSigmaIn);
          invsigmaOut = 1.0 / sqrt((gradX*gradX + gradY*gradY) * sigmaOut2 + pixSigmaOut*pixSigmaOut);
        } else {
          invsigmaIn = 1.0 / pixSigmaIn;
          invsigmaOut = 1.0 / pixSigmaOut;
        }
        //        gttoc(errorAndImgGrad);
      } else {
        err = 0.0;
        gradX = 0.0;
        gradY = 0.0;
        invsigmaIn = 0.0;
        invsigmaOut = 0.0;
      }

      J = Eigen::Matrix<double,1,2>(gradX,gradY) *
          basis.basis().block(2*trueloc, 0, 2, basis.basis().cols());
      AbIn.block(i,0, 1, basis.basis().cols()).noalias() = J * invsigmaIn;
      AbOut.block(i,0, 1, basis.basis().cols()).noalias() = J * invsigmaOut;

      AbIn(i, basis.basis().cols()) = -err * invsigmaIn;
      AbOut(i, basis.basis().cols()) = -err * invsigmaOut;
    }
    gttoc(compute_A_b);

    if(!imageFileDebugStem.empty()) {
      cv::imwrite(imageFileDebugStem + "_warped.png", warped*255.0);
      cv::imwrite(imageFileDebugStem + "_errs.png", errs*127.0 + 127.0);
      cv::imwrite(imageFileDebugStem + "_prev.png", prevFrame*255.0);
    }

    //cv::imshow("curFrame warped", Utils::gray2rgb(warped));
    //cv::imshow("curFrame", Utils::gray2rgb(curFrame));
    //cv::imshow("prevFrame", Utils::gray2rgb(prevFrame));
    //cv::imshow("errs", Utils::gray2rgb(errs));

    gttoc(GMPPCALucasKanade_gradientMatrices);
    return make_pair(AbIn, AbOut);
  }

  /* ************************************************************************* */
  class NonlinearError {
    const cv::Mat& prevFrameGradX_;
    const cv::Mat& prevFrameGradY_;
    const cv::Mat& prevFrame_;
    const cv::Mat& curFrame_;
    const Subspace& basis_;
    const vector<pair<size_t, size_t> >& imgXYs_;
    const Vector& inliers_;
    const GMPPCALucasKanade& lk_;

  public:
    NonlinearError(const cv::Mat& prevFrameGradX, const cv::Mat& prevFrameGradY,
          const cv::Mat& prevFrame, const cv::Mat& curFrame, const Subspace& basis,
          const vector<pair<size_t, size_t> >& imgXYs, const Vector& inliers, const GMPPCALucasKanade& lk) :
            prevFrameGradX_(prevFrameGradX), prevFrameGradY_(prevFrameGradY),
            prevFrame_(prevFrame), curFrame_(curFrame), basis_(basis), imgXYs_(imgXYs), inliers_(inliers), lk_(lk) {}

    double error(const Values& x) const {
      Matrix AbIn, AbOut;
      boost::tie(AbIn, AbOut) = lk_.gradientMatrices(prevFrameGradX_, prevFrameGradY_,
          prevFrame_, curFrame_, basis_, x.at<LieVector>(Y(0)), imgXYs_);
      return 0.5 * (AbIn.col(AbIn.cols()-1).array() * inliers_.array().unaryExpr(ptr_fun<double,double>(sqrt))).matrix().squaredNorm();
    }
  };

  /* ************************************************************************* */
  void GMPPCALucasKanade::updateLatent(Values& values, const FactorGraph<JacobianFactor>& gfg, const Vector& outliers, double& Delta_io, const NonlinearError& errorfn) const {

    gttic(reweight);
    // Reweight according to mixture model (see derivation)
    assert(boost::dynamic_pointer_cast<noiseModel::Unit>(gfg[0]->get_model()));
    assert(gfg[0]->get_model()->dim() == outliers.size());
    gfg[0]->get_model() = noiseModel::Diagonal::Precisions(outliers);
    gttoc(reweight);

    // Solve
    gttic(dogleg_iteration);
    Ordering ordering; ordering += Y(0);
//    gttic(construct solver);
    GaussianSequentialSolver solver(gfg);
//    gttoc(construct solver);
//    gttic(solver optimize);

    // ///////////////// Uncomment to use Gauss-Newton
    VectorValues::shared_ptr delta(solver.optimize());
    values = values.retract(*delta, ordering);
    // ///////////////// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//    gttoc(solver optimize);

    // ///////////////// Uncomment to use Dogleg
    //BayesTree<GaussianConditional>::shared_ptr gbt = GaussianMultifrontalSolver(gfg).eliminate();
    //DoglegOptimizerImpl::IterationResult result = DoglegOptimizerImpl::Iterate(Delta_io, DoglegOptimizerImpl::ONE_STEP_PER_ITERATION, *gbt, errorfn, values, ordering, errorfn.error(values));
    //Delta_io = result.Delta;
    //values = values.retract(result.dx_d, ordering);
    // ///////////////// ^^^^^^^^^^^^^^^^^^^^^^^

    gttoc(dogleg_iteration);
  }

  /* ************************************************************************* */
  void GMPPCALucasKanade::updateOutliers(size_t level, const Values& values,
      const FactorGraph<JacobianFactor>& inlierGraph,
      const FactorGraph<JacobianFactor>& outlierGraph, Vector& outliers) const {

    gttic(evaluate);
    VectorValues zerovv(VectorValues::Zero(1,subspaces_[0].dimLatent()));
    Vector inerror = inlierGraph[0]->error_vector(zerovv);
    Vector outerror = outlierGraph[0]->error_vector(zerovv);
    gttoc(evaluate);

    gttic(prob);
    for(size_t i=0; i<inlierGraph[0]->rows(); ++i) {
      double inSqrtPrec = inlierGraph[0]->getA(inlierGraph[0]->begin()).row(i).norm();
      double outSqrtPrec = outlierGraph[0]->getA(outlierGraph[0]->begin()).row(i).norm();


      double priorAdjust;
//      size_t imgY = i / curFrame_(level).cols;
//      if(imgY > size_t(curFrame_(level).rows / 2))
//        priorAdjust = 0.1;
//      else
//        priorAdjust = 0.0;
      priorAdjust = 0.0;

      double pIn = inSqrtPrec * exp(-0.5 * inerror[i] * inerror[i]) * (subspaces_[level].inlierPrior() - priorAdjust);
      double pOut = outSqrtPrec * exp(-0.5 * outerror[i] * outerror[i]) * (1.0 - subspaces_[level].inlierPrior() + priorAdjust);

      if(pIn + pOut > 1e-10)
        outliers[i] = pIn / (pIn + pOut);
      else
        outliers[i] = subspaces_[level].inlierPrior() - priorAdjust;
    }
    gttoc(prob);

//    cv::Mat inliersMat = Utils::matOfVector(outliers, curFrame_(level).cols);
//    inliersMat = Utils::scale(inliersMat, 1<<level, cv::INTER_LINEAR);
//    cv::imshow((boost::format("outliers %d") % level).str(), inliersMat);
  }

  /* ************************************************************************* */
  void GMPPCALucasKanade::computeFlowAtLevel(size_t level, double absoluteTolerance, boost::optional<Vector> prior, Vector& latent, Vector& inliers) const {
    gttic(setup);

    size_t nHorz = curFrame_(level).cols;
    size_t nVert = curFrame_(level).rows;
    if(nVert*nHorz*2 != subspaces_[level].dimSpace()) {
      cerr << "Subspace dimensions do not match image dimensions.  Probably created with different\n"
          "scale settings or a different camera." << endl;
      throw invalid_argument("Subspace dimensions do not match image dimensions");
    }

    bool writeDiag = false;
    if(boost::filesystem::exists("diag"))
      writeDiag = true;

//    // Add image factor between latent variables and images
//    typedef ImageShiftFactor<Values, LatentKey> ImageFactor;
//
//    boost::shared_ptr<ImageFactor> imageFactor(
//        level == params_.firstPyrLevel ?
//            new ImageFactor(prevFrame_(level), curFrame_(level), imgXYs, subspaces_[level].basis(), sqrt(subspaces_[level].inlierSigma2()), 1.0/255.0, imgOps_, LatentKey(0)) :
//            new ImageFactor(prevFrame_(level), curFrame_(level), imgXYs, subspaces_[level].basis(), sqrt(subspaces_[level].inlierSigma2()), 1.0/255.0, imgOps_, LatentKey(0)));

    // Add prior on latent variables
    typedef PriorFactor<LieVector> PriorFactor;
    PriorFactor::shared_ptr priorFactor;
    if(prior)
      priorFactor.reset(new PriorFactor(Y(0), *prior, noiseModel::Isotropic::Sigma(latent.rows(), 0.01)));
    else
      priorFactor.reset(new PriorFactor(Y(0), zero(subspaces_[level].dimLatent()), noiseModel::Isotropic::Sigma(subspaces_[level].dimLatent(), 100.0)));

    int converged = 0;
    size_t iteration = 0;
    Matrix grad_W, imgCovIn, imgCovOut;
    double lastError = 1e50;
    Values solnValues; solnValues.insert(Y(0), LieVector(latent));
    Ordering::shared_ptr ordering(new Ordering()); (*ordering) += Y(0);

    // Compute image derivatives
    const cv::Mat& kernel(imgOps_->gaussianKernel(0.0));
    cv::Mat prevFiltered;
    cv::sepFilter2D(prevFrame_(level), prevFiltered, prevFrame_(level).depth(), kernel, kernel, cv::Point(-1,-1), 0.0, cv::BORDER_CONSTANT);
    cv::Mat prevFrameGradX, prevFrameGradY;
    boost::tie(prevFrameGradX, prevFrameGradY) = ImgOps::ComputeDerivatives(prevFrame_(level));

    //vector<pair<size_t, size_t> > gradXYs;
    imgXYs_.resize(std::max(imgXYs_.size(), level+1));
    imgXYs_[level].clear();
    if(imgXYs_[level].empty()) {
      imgXYs_[level].reserve(nVert*nHorz);

      //for(size_t j=0; j<nVert; j+=(1<<params_.flowSamplePower))
      //  for(size_t i=0; i<nHorz; i+=(1<<params_.flowSamplePower))
      //    imgXYs_[level].push_back(make_pair(i,j));

      //create a vector of the bext gradient pixels in full image
      //gradXYs.reserve(nVert*nHorz);
      for(size_t i=0; i<nVert; i+=(1<<params_.flowSamplePower))
        for(size_t j=0; j<nHorz; j+=(1<<params_.flowSamplePower)){
          double grad = sq(prevFrameGradX.at<double>(i,j)) + sq(prevFrameGradY.at<double>(i,j));
          size_t randk = rand() % 100;
          if(grad>0.0025 && randk < 3)
            imgXYs_[level].push_back(make_pair(j,i));
        }


      //  for(size_t k=1;k<300;k++){
      //    size_t randk = rand()% gradXYs.size();
      //    imgXYs.push_back(make_pair(gradXYs[randk].first,gradXYs[randk].second));
      //  }

    }

    if(inliers.size() != imgXYs_[level].size())
      inliers = Vector::Ones(imgXYs_[level].size());

    if(inliers.size() != imgXYs_[level].size())
      inliers = Vector::Ones(imgXYs_[level].size());

//    char buffer [50];
//    cv::namedWindow("image", CV_WINDOW_AUTOSIZE);
//    cvSetMouseCallback("image",onMouse,0);
//    char c=0;
//    cv::Mat interrim = prevFrameGradX;
//    double minVal, maxVal;
//    cv::Point minloc, maxloc;
//    printf("%d is depth\n", prevFrameGradX.depth());
//
//    minMaxLoc(prevFrameGradX, &minVal, &maxVal,&minloc,&maxloc);
//    cout<<"minloc :"<<minloc<<"\t maxloc : "<<maxloc<<"\n";
//    cout<<"minValue :"<<minVal<<"\t maxvalue : "<<maxVal<<"\n";
//    cv::Mat draw;
//    interrim.convertTo(interrim, CV_8U, 255.0/(maxVal-minVal),-minVal);
//
//    while(c!='c'){
//
//    sprintf(buffer,"%lf", (double) prevFrameGradX.at<double>(yg,xg));
//    putText(interrim,buffer,cvPoint (xg,yg),cv::FONT_HERSHEY_TRIPLEX,0.4,255);
//    imshow("image", interrim);
//    c = cv::waitKey(10) & 0xff;
//  }


    gttoc(setup);

    gttic(iterate);
    double Delta = 0.01;
    while(converged < 1) {

      //int c = cv::waitKey(0);
      //if(c == 'o')
      //  break;

      imageFileDebugStem = ((boost::format("diag/%04d_%02d_%02d")%frameNum_%level%iteration).str()).c_str();

      gttic(compute_A);
      Matrix AbIn, AbOut;
      boost::tie(AbIn, AbOut) = gradientMatrices(prevFrameGradX, prevFrameGradY,
        prevFiltered, curFrame_(level), subspaces_[level], solnValues.at<LieVector>(Y(0)), imgXYs_[level]);
      gttoc(compute_A);

      imageFileDebugStem.clear();

      if(writeDiag) {
        // Output linear problem
        ofstream f(((boost::format("diag/%04d_%02d_%02d_AbIn.txt")%frameNum_%level%iteration).str()).c_str());
        f << AbIn << endl;
        f.close();
        f.open(((boost::format("diag/%04d_%02d_%02d_AbOut.txt")%frameNum_%level%iteration).str()).c_str());
        f << AbOut << endl;
        f.close();
      }

//      if(level == 4)
//        gtsam::print((Matrix(AbIn.rows(), 8) << AbIn, AbOut).finished(), "AbIn AbOut: ");

      gttic(linear_graphs);
      FactorGraph<JacobianFactor>::shared_ptr jfg(new FactorGraph<JacobianFactor>());
      jfg->push_back(JacobianFactor::shared_ptr(new JacobianFactor(
          ordering->at(Y(0)), AbIn.leftCols(latent.rows()),
          AbIn.col(latent.rows()), noiseModel::Unit::Create(AbIn.rows()))));
      if(priorFactor)
        jfg->push_back(boost::dynamic_pointer_cast<JacobianFactor>(priorFactor->linearize(solnValues, *ordering)));
      GaussianFactorGraph::shared_ptr gfg(new GaussianFactorGraph(*jfg));

//      GaussianFactor::shared_ptr fromImgFactor = imageFactor->linearize(solnValues, *ordering);
//      assert(assert_equal(*fromImgFactor, *(*gfg)[0], 1e-3));

      FactorGraph<JacobianFactor>::shared_ptr gfgo(new FactorGraph<JacobianFactor>());
      gfgo->push_back(JacobianFactor::shared_ptr(new JacobianFactor(
          ordering->at(Y(0)), AbOut.leftCols(latent.rows()),
          AbOut.col(latent.rows()), noiseModel::Unit::Create(AbOut.rows()))));
      if(priorFactor)
        gfgo->push_back(boost::dynamic_pointer_cast<JacobianFactor>(priorFactor->linearize(solnValues, *ordering)));
      gttoc(linear_graphs);

      gttic(updateOutliers);
      updateOutliers(level, solnValues, *jfg, *gfgo, inliers);
      gttoc(updateOutliers);

      if(params_.initialization != Params::FIXED) {
        gttic(updateLatent);
        updateLatent(solnValues, *jfg, inliers, Delta,
          NonlinearError(prevFrameGradX, prevFrameGradY, prevFiltered, curFrame_(level), subspaces_[level], imgXYs_[level], inliers, *this));
        gttoc(updateLatent);
      }

      if(writeDiag) {
        ofstream f(((boost::format("diag/%04d_%02d_%02d_Latent.txt")%frameNum_%level%iteration).str()).c_str());
        f << solnValues.at<LieVector>(Y(0)) << endl;
        f.close();
      }

      ++ iteration;

      // Compute error
      gttic(check_error);
      VectorValues zero(VectorValues::Zero(vector<size_t>(1, subspaces_[level].dimLatent())));
      double curError = gfg->error(zero);
      cout << level << ": error: " << curError << "  [ " << solnValues.at<LieVector>(Y(0)).transpose() << " ]" << endl;
      gttoc(check_error);

      if(checkConvergence(0.0, absoluteTolerance, 0.0, lastError, curError, NonlinearOptimizerParams::SILENT))
        ++ converged;

      lastError = curError;

      if(iteration >= 5 || params_.initialization == Params::FIXED)
        ++ converged;
    }

    latent = solnValues.at<LieVector>(Y(0));

    // Plot errors
//    static int counter = 0;
//    ofstream plotfile((boost::format("errors%d_%d.txt")%counter%level).str().c_str());
//    for(double dx=-0.01; dx<=0.01; dx += 0.00025) {
//      VectorValues dxv(1, 1);
//      dxv[0] = Vector_(1, dx);
//      Values d(solnValues.retract(dxv, *ordering));
//      double err = NonlinearError(prevFrameGradX, prevFrameGradY, prevFiltered, curFrame_(level), subspaces_[level], imgXYs, inliers, *this).error(d);
//      plotfile << dx << "\t" << err << "\n";
//    }
//    if(level == params_.firstPyrLevel)
//      ++ counter;

    gttoc(iterate);

//    errors[level] = imageFactor->lastErrs;

//    out_errors = imageFactor->lastErrs;
  }

  /* ************************************************************************* */
  void GMPPCALucasKanade::computeFlow(Vector& out_latent, Vector& out_inliers) const {

    boost::optional<Vector> prior;
    if(params_.initialization == Params::PRIOR)
      prior = out_latent;
    else
      prior = boost::none;

    //cout << "Latent " << out_latent << endl;

    // Initialize outliers and latent variables
    if(out_latent.size() != subspaces_[0].dimLatent())
      out_latent = zero(subspaces_[0].dimLatent());
    //out_inliers = ones(curFrame_.back().rows * curFrame_.back().cols);

    // Compute all levels except for the highest
    for(size_t level = params_.nPyrLevels - 1; level > params_.firstPyrLevel; --level) {

      if(!out_latent.unaryExpr(ptr_fun<double,int>(isfinite)).all())
        out_latent = zero(subspaces_[0].dimLatent());

      computeFlowAtLevel(level, 1e-4, prior, out_latent, out_inliers);

      latent[level] = out_latent;
      outliers[level] = out_inliers;
      
      out_inliers = Vector();

      // Initialize inliers for the next level up
    //  {
    //    size_t rows = curFrame_(level).rows;
    //    size_t cols = curFrame_(level).cols;
    //    size_t rowsUp = curFrame_(level-1).rows;
    //    size_t colsUp = curFrame_(level-1).cols;
    //    Vector curInliers(rowsUp * colsUp);
    //    for(size_t i=0; i<rows; ++i)
    //      for(size_t j=0; j<cols; ++j) {
    //        curInliers((2*i)*colsUp + (2*j)) = out_inliers(i*cols + j);
    //        curInliers((2*i)*colsUp + (2*j+1)) = out_inliers(i*cols + j);
    //        curInliers((2*i+1)*colsUp + (2*j)) = out_inliers(i*cols + j);
    //        curInliers((2*i+1)*colsUp + (2*j+1)) = out_inliers(i*cols + j);
    //      }
    //      out_inliers.swap(curInliers);
    //  }
    }

    // Compute for highest level with a tighter tolerance
    computeFlowAtLevel(params_.firstPyrLevel, 1e-4, prior, out_latent, out_inliers);
    latent[params_.firstPyrLevel] = out_latent;
    outliers[params_.firstPyrLevel] = out_inliers;

    //// Compute final outliers

    //// Compute image derivatives
    //const cv::Mat& kernel(imgOps_->gaussianKernel(0.0));
    //cv::Mat prevFiltered;
    //cv::sepFilter2D(prevFrame_(params_.firstPyrLevel), prevFiltered, prevFrame_(params_.firstPyrLevel).depth(), kernel, kernel, cv::Point(-1,-1), 0.0, cv::BORDER_CONSTANT);
    //cv::Mat prevFrameGradX, prevFrameGradY;
    //imgOps_->computeDerivatives(prevFrame_(params_.firstPyrLevel), prevFrameGradX, prevFrameGradY);

    //vector<pair<size_t, size_t> > imgXYs;
    //imgXYs.reserve(curFrame_(params_.firstPyrLevel).rows*curFrame_(params_.firstPyrLevel).cols);

    ////create a vector of the bext gradient pixels in full image
    //for(size_t j=0; j<curFrame_(params_.firstPyrLevel).rows; j+=(1<<params_.flowSamplePower))
    //  for(size_t i=0; i<curFrame_(params_.firstPyrLevel).cols; i+=(1<<params_.flowSamplePower))
    //    imgXYs.push_back(make_pair(i,j));

    //gttic(compute A);
    //Matrix AbIn, AbOut;
    //boost::tie(AbIn, AbOut) = gradientMatrices(prevFrameGradX, prevFrameGradY,
    //    prevFiltered, curFrame_(params_.firstPyrLevel), subspaces_[params_.firstPyrLevel], out_latent, imgXYs);
    //gttoc(compute A);

    ////if(level == 4)
    ////  gtsam::print((Matrix(AbIn.rows(), 8) << AbIn, AbOut).finished(), "AbIn AbOut: ");

    //gttic(linear graphs);
    //FactorGraph<JacobianFactor>::shared_ptr jfg(new FactorGraph<JacobianFactor>());
    //jfg->push_back(JacobianFactor::shared_ptr(new JacobianFactor(
    //    0, AbIn.leftCols(out_latent.rows()),
    //    AbIn.col(out_latent.rows()), sharedUnit(AbIn.rows()))));
    //GaussianFactorGraph::shared_ptr gfg(new GaussianFactorGraph(*jfg));

    //FactorGraph<JacobianFactor>::shared_ptr gfgo(new FactorGraph<JacobianFactor>());
    //gfgo->push_back(JacobianFactor::shared_ptr(new JacobianFactor(
    //    0, AbOut.leftCols(out_latent.rows()),
    //    AbOut.col(out_latent.rows()), sharedUnit(AbOut.rows()))));
    //gttoc(linear graphs);

    //Values solnValues; solnValues.insert(Y(0), LieVector(out_latent));
    //out_inliers = Vector(imgXYs.size());
    //updateOutliers(params_.firstPyrLevel, solnValues, *jfg, *gfgo, out_inliers);
    //outliers[params_.firstPyrLevel] = out_inliers;
  }

  /* ************************************************************************* */
  vector<Point2> GMPPCALucasKanade::predict(const Vector& latent, size_t level) const {
    Vector flow(subspaces_[level].basis() * latent);
    size_t nBlocks = subspaces_[level].dimSpace() >> 1;
    vector<Point2> ret(nBlocks);
    for(size_t i=0; i<nBlocks; ++i)
      ret[i] = Point2(flow(2*i), flow(2*i+1));
    return ret;
  }

  /* ************************************************************************* */
  double GMPPCALucasKanade::marginalLog(const Vector& inliers, const Vector& latent, int id) const {

    const cv::Mat& kernel(imgOps_->gaussianKernel(0.0));
    cv::Mat prevFrame;
    cv::sepFilter2D(prevFrame_(params_.firstPyrLevel), prevFrame, prevFrame_(params_.firstPyrLevel).depth(), kernel, kernel, cv::Point(-1,-1), 0.0, cv::BORDER_CONSTANT);
    cv::Mat prevFrameGradX, prevFrameGradY;
    boost::tie(prevFrameGradX, prevFrameGradY) = ImgOps::ComputeDerivatives(prevFrame);
    const cv::Mat& curFrame(curFrame_(params_.firstPyrLevel));
    const Subspace& basis(subspaces_[params_.firstPyrLevel]);

    double pixSigmaIn = 2.0 / 255.0;
    double pixSigmaOut = 5.0 / 255.0;
    double sigmaIn2 = basis.inlierSigma2();
    double sigmaOut2 = basis.outlierSigma2();

    size_t nHorz = curFrame.cols;
    size_t nVert = curFrame.rows;
    vector<pair<size_t, size_t> > imgXYs;
    imgXYs.reserve(nVert*nHorz);
    for(size_t j=0; j<nVert; j+=(1<<params_.flowSamplePower))
      for(size_t i=0; i<nHorz; i+=(1<<params_.flowSamplePower))
        imgXYs.push_back(make_pair(i,j));

    Vector v = basis.basis() * latent;

    // Matrices for marginalizing out y
    Matrix A(basis.dimLatent(), basis.dimLatent());
    Matrix B(imgXYs.size(), basis.dimLatent());
    Vector D(imgXYs.size());

    // Initialize A to identity because it is I + \sum ...
    A = Matrix::Identity(A.rows(), A.cols());

    //Matrix A(imgXYs.size(), basis.basis().cols());
    Vector bIn(imgXYs.size());
    Vector bOut(imgXYs.size());
    Vector errs(imgXYs.size());
    Matrix A_J;
    double log_cI_sq = 0.0;
    for(size_t loc = 0; loc < imgXYs.size(); ++loc) {

      int trueloc = imgXYs[loc].second * curFrame.cols + imgXYs[loc].first;

      // Transform with basis to get image shift
      assert(latent.size() == basis.basis().cols());
      Point2 x(v(2*trueloc), v(2*trueloc+1));

      long int pimx = imgXYs[loc].first;
      long int pimy = imgXYs[loc].second;
      long int cimx = imgXYs[loc].first + lround(x.x());
      long int cimy = imgXYs[loc].second + lround(x.y());
      int krad = (imgOps_->size() - 1) / 2;
      bool inBounds = pimx >= krad && pimy >= krad && pimx < prevFrame.cols-krad && pimy < prevFrame.rows-krad &&
          cimx >= krad && cimy >= krad && cimx < prevFrame.cols-krad && cimy < prevFrame.rows-krad;

      double err;
      double gradX;
      double gradY;
      double invsigmaIn, invsigmaOut;
      if(inBounds) {
        // Compute pixel intensity error and image gradient
        pair<double, Point2> err_grad(errorAndImgGrad(
            prevFrameGradX, prevFrameGradY, prevFrame, curFrame, make_pair(pimx,pimy), x));
        assert(std::isfinite(err_grad.first));
        err = err_grad.first;
        gradX = err_grad.second.x();
        gradY = err_grad.second.y();

        // Compute sigma of the image intensity measurement, which is dependent on
        // the image gradient (see derivation)
        if(gradX > 1e-10 || gradX < -1e-10 || gradY > 1e-10 || gradY < -1e-10) {
          invsigmaIn = 1.0 / sqrt((gradX*gradX + gradY*gradY) * sigmaIn2 + pixSigmaIn*pixSigmaIn);
          invsigmaOut = 1.0 / sqrt((gradX*gradX + gradY*gradY) * sigmaOut2 + pixSigmaOut*pixSigmaOut);
        } else {
          invsigmaIn = 1.0 / pixSigmaIn;
          invsigmaOut = 1.0 / pixSigmaOut;
        }

        log_cI_sq += log(sq(invsigmaIn) * inliers(loc) + sq(invsigmaOut) * (1.0-inliers(loc)));
      } else {
        err = 0.0;
        gradX = 0.0;
        gradY = 0.0;
        invsigmaIn = 1.0 / pixSigmaIn;
        invsigmaOut = 1.0 / pixSigmaOut;
      }

      // Update matrices for marginalizing out y
      const double invsigma2 = inliers(loc)*sq(invsigmaIn) + (1.0 - inliers(loc))*sq(invsigmaOut);
      const Eigen::Matrix<double,1,2> grad(gradX,gradY);
      const Eigen::Block<const Matrix> Vi = basis.basis().block(2*trueloc,0, 2,basis.dimLatent());
      A += invsigma2 * Vi.transpose() * grad.transpose() * grad * Vi;
      B.row(loc) = invsigma2 * grad * Vi;
      D(loc) = invsigma2;

      // Compute A = I*W (see derivation)
      //A_J = Eigen::Matrix<double,1,2>(gradX,gradY) *
      //    basis.basis().block(2*loc, 0, 2, basis.basis().cols());
      //A.block(loc,0, 1, basis.basis().cols()).noalias() = A_J * invsigmaIn * sqrt(outliers(loc));
      bIn(loc) = invsigmaIn * err * sqrt(inliers(loc));
      bOut(loc) = invsigmaOut * err * sqrt(1.0-inliers(loc));
      errs(loc) = err;
    }

    //cv::imshow((boost::format("b %d")%id).str(), Utils::colorize(Utils::matOfVector(bIn, curFrame.cols)));
    //cv::imshow((boost::format("errs %d")%id).str(), Utils::colorize(Utils::matOfVector(255.0/5.0*errs, curFrame.cols)));

    //double detATA = (A.transpose() * A).determinant();
    //double logdetATAinvsqrt = -0.5 * log(detATA);
    //double log_c = log(2.0*M_PI) * -0.5*A.rows() - log(2.0*M_PI) * -0.5*basis.basis().cols();
    double L_I_v_impulse = -0.5 * (bIn.squaredNorm() + bOut.squaredNorm());
    double log_inliers = 0.0;
    for(size_t i=0; i<inliers.rows(); ++i) {
      log_inliers +=
//          log(basis.inlierPrior()) * inliers(i) +
//          log(1.0-basis.inlierPrior()) * (1.0-inliers(i));
          log(0.95) * inliers(i) +
          log(0.05) * (1.0-inliers(i));
    }
    cout << "L_I_v_impulse: " << L_I_v_impulse << ",  log_cI: " << 0.5*log_cI_sq << ",  log_inliers: " << log_inliers << endl;

    // Marginalizing out y
    cout << "det(A): " << A.determinant() << endl;
    cout << "log(det(D)): " << D.unaryExpr(ptr_fun<double,double>(log)).sum() << endl;
    cout << "A - B.transpose() * D.asDiagonal() * B:\n" << Matrix(A - B.transpose() * D.asDiagonal() * B) << endl;
    Matrix detM1 = A - B.transpose() * D.asDiagonal() * B;
    double logdetM1 = detM1.householderQr().logAbsDeterminant();
    double logdetA = A.householderQr().logAbsDeterminant();
    cout << "log(det(A - B.transpose() * D.asDiagonal() * B)): " << logdetM1 << endl;
    cout << "log(A.determinant()): " << logdetA << endl;
    double logdetL = logdetM1 + -logdetA + D.unaryExpr(ptr_fun<double,double>(log)).sum();
    double logC = log(2*M_PI) * (-0.5*imgXYs.size()) + 0.5*logdetL;
    double LI1 = errs.transpose() * D.asDiagonal() * errs;
    double LI2 = -errs.transpose() * B * A.inverse() * B.transpose() * errs;
    double LI3 = -errs.transpose() * B * A.inverse() * latent;
    double fI = latent.transpose() * (Matrix::Identity(A.rows(), A.cols()) - A.inverse()) * latent;
    double logpI = logC + (-0.5 * (LI1 + LI2 + LI3 + fI));

    cout << "logdetL: " << logdetL << ",  logC: " << logC <<
        "\nLI1: " << LI1 << ",  LI2: " << LI2 << ",  LI3: " << LI3 <<
        "\nfI: " << fI << ",  logpI: " << logpI << endl;

#if 0
    // Output histogram data
    static map<int,ValueWithDefault<int,0> > frame;
    ofstream histfile((boost::format("hist_%d_%d")%id%frame[id]).str().c_str());
    ++ (*frame[id]);
    for(size_t i=0; i<bIn.rows(); ++i) {
      double contrib = 0.5*log_cI_sq / double(bIn.rows());
      histfile << i << "\t" << contrib + -0.5 * sq(bIn(i)) << "\t" << inliers(i) << "\n";
    }
    histfile.close();
#endif

    //return log_c + 0.5*log_cI_sq + L_I_v_impulse + log_inliers;
    //return 0.5*log_cI_sq + logdetATAinvsqrt + log_inliers;
    return logpI + log_inliers;
  }

//  double GMPPCALucasKanade::updateBasis(FullValues& values, vector<Vector>& outliers, const vector<cv::Mat>& images, const vector<Vector>& motion, SharedDiagonal motionNoise) {
//
//    // Get image locations
//    size_t nHorz = images[0].cols >> params_.flowSamplePower;
//    size_t nVert = images[0].rows >> params_.flowSamplePower;
//    double hstep = double(1 << params_.flowSamplePower);
//    double vstep = double(1 << params_.flowSamplePower);
//    double hoffset = hstep / 2.0;
//    double voffset = vstep / 2.0;
//    assert(nVert*nHorz*2 == subspace_.dimSpace());
//    vector<pair<size_t, size_t> > imgXYs;
//    imgXYs.reserve(nVert*nHorz);
//    size_t factorI = 0;
//    for(size_t j=0; j<nVert; ++j)
//      for(size_t i=0; i<nHorz; ++i) {
//        long int pimx = lround(hoffset + double(i) * hstep);
//        long int pimy = lround(voffset + double(j) * vstep);
//        assert(pimx >= 0.0 && pimy >= 0.0);
//        imgXYs.push_back(make_pair(pimx,pimy));
//        ++ factorI;
//      }
//
//    ///////////
//    // E-step, compute sufficient statistics by solving least-squares problem
//    assert(images.size() == motion.size());
//
//    vector<vector<Matrix> > u_xi(images.size());
//    vector<Matrix> xi_xi(images.size());
//    double error = 0;
//
//    for(size_t t=0; t<motion.size(); ++t) {
//
//      FullGraph graph;
//
//      // Compute outlier expectations
//      typedef TangentMapFactor<FullValues, LatentKey, FlowKey> BasisFactor;
//      LieVector zeroLatent(zero(this->subspace_.dimLatent()));
//      SharedDiagonal inlierSigma(sharedSigma(2, subspace_.inlierSigma2()));
//      SharedDiagonal outlierSigma(sharedSigma(2, subspace_.outlierSigma2()));
//      for(size_t i=0; i<imgXYs.size(); ++i) {
//        BasisFactor inlierFactor(zeroLatent, Point2(),
//            ublas::project(subspace_.basis(), ublas::range(2*i,2*i+2), ublas::range(0,subspace_.dimLatent())),
//            inlierSigma, LatentKey(t), FlowKey(t*imgXYs.size() + i));
//        BasisFactor outlierFactor(zeroLatent, Point2(),
//            ublas::project(subspace_.basis(), ublas::range(2*i,2*i+2), ublas::range(0,subspace_.dimLatent())),
//            outlierSigma, LatentKey(t), FlowKey(t*imgXYs.size() + i));
//
//        double inError = inlierFactor.error(values);
//        double outError = outlierFactor.error(values);
//
//        double pIn = 1.0/subspace_.inlierSigma2() * exp(-0.5 * inError) * subspace_.inlierPrior();
//        double pOut = 1.0/subspace_.outlierSigma2() * exp(-0.5 * outError) * (1.0-subspace_.inlierPrior());
//
//        if(pIn + pOut > 1e-20)
//          outliers[t][i] = pIn / (pIn + pOut);
//        else
//          outliers[t][i] = 0.0;
//
//        //      cout << "outliers[" << t << "][" << i << "] = " << outliers[t][i] << "\n";
//      }
//
//      // Add motion priors
//      typedef PriorFactor<FullValues, LatentKey> Prior;
//      graph.push_back(boost::shared_ptr<Prior>(new Prior(LatentKey(t), LieVector(motion[t]), motionNoise)));
//
//      // Add inlier and outlier basis factors
//      typedef PriorFactor<FullValues, FlowKey> FlowPrior;
//      for(size_t i=0; i<imgXYs.size(); ++i) {
//        SharedDiagonal inlierNoise(sharedPrecision(2, outliers[t][i] * (1.0/subspace_.inlierSigma2())));
//        SharedDiagonal outlierNoise(sharedPrecision(2, (1.0-outliers[t][i] * (1.0/subspace_.outlierSigma2()))));
//        graph.push_back(boost::shared_ptr<BasisFactor>(
//            new BasisFactor(zeroLatent, Point2(),
//                            ublas::project(subspace_.basis(), ublas::range(2*i,2*i+2), ublas::range(0,subspace_.dimLatent())),
//                            inlierNoise, LatentKey(t), FlowKey(t*imgXYs.size() + i))));
//        graph.push_back(boost::shared_ptr<FlowPrior>(new FlowPrior(FlowKey(t*imgXYs.size() + i), Point2(), outlierNoise)));
//      }
//
//      // Add image factors
//      typedef OpticalFlowFactor<FullValues, FlowKey> FlowFactor;
//      SharedDiagonal imageNoise(sharedSigma(1, 0.01));
//      if(t >= 1)
//        for(size_t i=0; i<imgXYs.size(); ++i)
//          graph.push_back(boost::shared_ptr<FlowFactor>(
//              new FlowFactor(images[t-1], images[t], imgXYs[i], imageNoise, imgOps_, FlowKey(t*imgXYs.size() + i))));
//
//      // Linearize
//      Ordering ordering;
//      for(size_t i=0; i<imgXYs.size(); ++i)
//        ordering += FlowKey(t*imgXYs.size() + i);
//      ordering += LatentKey(t);
//      gttic(Linearize);
//      FactorGraph<JacobianFactor>::shared_ptr gfg(graph.linearize(values, ordering));
//      //gttoc(1);
//
//      // Save error
//      error += gaussianError(*gfg, values.zero(ordering));
//
//      // Solve and update
//      GaussianSequentialSolver solver(*gfg);
//      boost::shared_ptr<VectorValues> delta(optimize_(*solver.eliminate()));
//      //  delta->print("Delta: ");
//      values = values.retract(*delta, ordering);
//
//      const size_t blocksize = 70;
//      u_xi[t].resize(imgXYs.size());
//      for(size_t i=0; i<imgXYs.size(); i+=blocksize) {
//
//        const size_t iNextBlock = std::min(imgXYs.size(), i+blocksize);
//
//        cout << "t " << t << ",  " << "i " << i << "-" << iNextBlock << endl;
//
//        // Compute joint marginal
//        gttic(Marginal);
//        vector<Index> keys(1 + iNextBlock - i);
//        keys[0] = ordering[LatentKey(t)];
//        for(size_t ii=0; ii<iNextBlock-i; ++ii)  keys[1+ii] = ordering[FlowKey(t*imgXYs.size() + i+ii)];
//        FactorGraph<JacobianFactor>::shared_ptr joint(solver.jointFactorGraph(keys)->dynamicCastFactors<FactorGraph<HessianFactor> >()->convertCastFactors<FactorGraph<JacobianFactor> >());
//        //gttoc(2);
//
//        // Count rows
//        gttic(Allocate A);
//        size_t rows = 0;
//        BOOST_FOREACH(const JacobianFactor::shared_ptr& factor, *joint) { rows += factor->size1(); }
//        Matrix A(zeros(rows, subspace_.dimLatent() + 2*(iNextBlock-i)));
//        //gttoc(3);
//        // Assemble A matrix
//        gttic(Assemble A);
//        size_t row = 0;
//        BOOST_FOREACH(const JacobianFactor::shared_ptr factor, *joint) {
//          for(JacobianFactor::const_iterator index = factor->begin(); index != factor->end(); ++index) {
//            if(*index == keys[0])
//              ublas::noalias(ublas::subrange(A, row,row+factor->size1(), 0,subspace_.dimLatent())) = factor->getA(index);
//            else if(*index >= keys[1] && *index <= keys.back())
//              ublas::noalias(ublas::subrange(A, row,row+factor->size1(), (*index-keys[1])*2+subspace_.dimLatent(),(*index-keys[1])*2+subspace_.dimLatent()+2)) =
//                  factor->getA(index);
//            else
//              throw runtime_error("Unexpected index in joint marginal factor graph");
//          }
//          row += factor->size1();
//        }
//        //gttoc(4);
//
//        // Compute covariance
//        gttic(Covariance);
//        Matrix covariance(inverse(ublas::prod(ublas::trans(A), A)));
//        //gttoc(5);
//
//        // Extract moments (sufficient statistics)
//        for(size_t ii=0; ii<iNextBlock-i; ++ii) {
//          u_xi[t][i+ii] =
//              ublas::subrange(covariance, subspace_.dimLatent()+ii*2,subspace_.dimLatent()+ii*2+2, 0,subspace_.dimLatent()) +
//              ublas::outer_prod(values[FlowKey(t*imgXYs.size() + i+ii)].vector(), values[LatentKey(t)].vector());
//        }
//        xi_xi[t] =
//            ublas::subrange(covariance, 0,subspace_.dimLatent(), 0,subspace_.dimLatent()) +
//            ublas::outer_prod(values[LatentKey(t)].vector(), values[LatentKey(t)].vector());
//      }
//
//    }
//
//    ///////////
//    // M-step, update basis
//    gttic(Calc basis);
//    for(size_t i=0; i<imgXYs.size(); ++i) {
//      Matrix numerator(zeros(2, subspace_.dimLatent()));
//      Matrix denominator(zeros(subspace_.dimLatent(), subspace_.dimLatent()));
//
//      for(size_t t=0; t<images.size(); ++t) {
//        double lambda_ti = outliers[t][i];
//
//        // Sum matrices
//        numerator += lambda_ti * u_xi[t][i];
//        denominator += lambda_ti * xi_xi[t];
//      }
//
//      ublas::subrange(subspace_.basis(), 2*i,2*i+2, 0,subspace_.dimLatent()) = ublas::prod(numerator, inverse(denominator));
//    }
//    //gttoc(6);
//
////    print(subspace_.basis(), "basis: ");
//
//    return error;
//  }
//
//  void GMPPCALucasKanade::learnBasis(const vector<cv::Mat>& images, const std::vector<Vector>& motion, gtsam::SharedDiagonal motionNoise) {
//
//    // Get image locations
//    size_t nHorz = images[0].cols >> params_.flowSamplePower;
//    size_t nVert = images[0].rows >> params_.flowSamplePower;
//    double hstep = double(1 << params_.flowSamplePower);
//    double vstep = double(1 << params_.flowSamplePower);
//    double hoffset = hstep / 2.0;
//    double voffset = vstep / 2.0;
//    assert(nVert*nHorz*2 == subspace_.dimSpace());
//    vector<pair<size_t, size_t> > imgXYs;
//    imgXYs.reserve(nVert*nHorz);
//    size_t factorI = 0;
//    for(size_t j=0; j<nVert; ++j)
//      for(size_t i=0; i<nHorz; ++i) {
//        long int pimx = lround(hoffset + double(i) * hstep);
//        long int pimy = lround(voffset + double(j) * vstep);
//        assert(pimx >= 0.0 && pimy >= 0.0);
//        imgXYs.push_back(make_pair(pimx,pimy));
//        ++ factorI;
//      }
//
//    FullValues values;
//    vector<Vector> outliers;
//    for(size_t t=0; t<images.size(); ++t) {
//      values.insert(LatentKey(t), LieVector(zero(this->subspace_.dimLatent())));
//      for(size_t i=0; i<imgXYs.size(); ++i)
//        values.insert(FlowKey(t*imgXYs.size() + i), Point2());
//      outliers.push_back(ones(imgXYs.size()));
//    }
//
//    int converged = 0;
//    double lastError = 1e10;
//    int iteration = 1;
//    while(converged < 1) {
//
//      gttic(UpdateBasis);
////      FullValues lastValues(values);
//      double curError = updateBasis(values, outliers, images, motion, motionNoise);
//      //gttoc(2);
//
//      cout << "error = " << curError << " (previously " << lastError << ")" << endl;
//
////      double diff = 0.0;
////      for(size_t t=0; t<images.size(); ++t)
////        diff += ublas::norm_2(values[LatentKey(t)] - lastValues[LatentKey(t)]);
////      cout << "diff = " << diff << endl;
//
//      if(curError <= lastError && check_convergence(1e-5, 1e-5, 0.0, lastError, curError, NonlinearOptimizationParameters::SILENT))
//        ++ converged;
//
//      lastError = curError;
//
//      if(iteration >= 20)
//        ++ converged;
//
//      ++ iteration;
//
//      tictoc_print();
//    }
//
//  }

}
