/**
 * @file    OnlyBasisImageFactor.cpp
 * @brief   Image factor on only the basis
 * @author  Richard Roberts
 * @created Aug 4, 2012
 */

#include "OnlyBasisImageFactor.h"

#include "flowOps.h"
#include "Utils.h"
#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/nonlinear/Symbol.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace basisflow {

	inline double sq(double x) { return x*x; }

	/* **************************************************************************************** */
	void OnlyBasisImageFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
		Base::print(s, keyFormatter);
	}

	/* **************************************************************************************** */
	bool OnlyBasisImageFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
		const OnlyBasisImageFactor *t = dynamic_cast<const This*>(&f);
		if(t && Base::equals(f))
			return latentKey_ == t->latentKey_ && sigmaKey_ == t->sigmaKey_ && frameNums_ == t->frameNums_;
		else
			return false;
	}

	/* **************************************************************************************** */
	double OnlyBasisImageFactor::error(const Values& x) const {

		const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
		double error = 0.0;
		for(size_t i = 0; i < frameNums_.size(); ++i) {
			// References to values for convenience
			const int frame = frameNums_[i];
			const LieVector& latent = latentValues_->at<LieVector>(latentKey_ + frame);

			// Load images from disk (we do this each time this function is called to avoid loading
			// the whole video into memory, i.e. to do out-of-core optimization).
			const FrameCache::LoadedImages& images =
				frameCache_->loadImages(frame, sigmas(0), sigmas(1), sigmas(2), sigmas(3));
      const int imWidth = frameCache_->imWidth();
      const int imHeight = frameCache_->imHeight();

			// Loop over all pixels doing low-rank updates on the Hessian
			size_t pix = 0;
			for(int j = 0; j < imWidth; ++j) {
				for(int i = 0; i < imHeight; ++i) {
					const LieMatrix& basis = x.at<LieMatrix>(basisKey() + pix);

					// Compute sqrt error and derivatives
					double inlier;
					Vector e = flowOps::fullImageError(images, j, i, imWidth, imHeight,
						inlierPrior_, basis, latent, sigmas(0), sigmas(1), sigmas(2), sigmas(3), inlier,
						boost::none, boost::none, boost::none, boost::none, boost::none, boost::none);

					error += e.squaredNorm();
				}
			}
		}
		return error;
	}

	/* **************************************************************************************** */
	boost::shared_ptr<GaussianFactor> OnlyBasisImageFactor::linearize(const Values& x, const Ordering& ordering) const {

		const int q = x.at<LieMatrix>(basisKey()).cols();
		const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
    const int imWidth = frameCache_->imWidth();
    const int imHeight = frameCache_->imHeight();

		// Create Hessian
		size_t nPix = imWidth * imHeight;
		vector<pair<Index, Matrix> > terms(nPix);
		for(size_t j = 0; j < nPix; ++j)
			terms[j] = make_pair(ordering[basisKey()+j], Matrix(0, 2*q));
		HessianFactor::shared_ptr hessian = boost::make_shared<HessianFactor>(
			JacobianFactor(terms, Vector(), noiseModel::Unit::Create(0)));

		for(size_t i = 0; i < frameNums_.size(); ++i) {
			// References to values for convenience
			const int frame = frameNums_[i];
			const LieVector& latent = latentValues_->at<LieVector>(latentKey_ + frame);

			// Load images from disk (we do this each time this function is called to avoid loading
			// the whole video into memory, i.e. to do out-of-core optimization).
			const FrameCache::LoadedImages& images =
				frameCache_->loadImages(frame, sigmas(0), sigmas(1), sigmas(2), sigmas(3));

			// Loop over all pixels doing low-rank updates on the Hessian
			size_t pix = 0;
			for(int j = 0; j < imWidth; ++j) {
				for(int i = 0; i < imHeight; ++i) {
					const LieMatrix& basis = x.at<LieMatrix>(basisKey() + pix);

					// Compute sqrt error and derivatives
					Matrix ddV;
					double inlier;
					Vector e = flowOps::fullImageError(images, j, i, imWidth, imHeight,
						inlierPrior_, basis, latent, sigmas(0), sigmas(1), sigmas(2), sigmas(3), inlier,
						ddV, boost::none, boost::none, boost::none, boost::none, boost::none);

					// Build temporary JacobianFactor to do low-rank update easily
					JacobianFactor pixJacobian(pix, ddV, e, noiseModel::Unit::Create(1));

					// Apply low-rank update
					Scatter scatter;
					scatter.insert(make_pair(pix, SlotEntry(pix, 2*q)));
					hessian->updateATA(pixJacobian, scatter);

					++pix;
				}
			}
		}

		return hessian;
	}
}
