// From http://subversion.assembla.com/svn/jhuworkshop2010/trunk/common/cpp/StreamSageImage/ColorFlow.cpp

#include "ColorFlow.h"

namespace StreamSage 
{
  ColorFlow::ColorFlow()	
  {
    makecolorwheel();
  };

  void ColorFlow::computeColor(float fx, float fy, unsigned char *pix)
  {

#define M_PI 3.141592653589793238462643f
    float rad = sqrt(fx * fx + fy * fy);
    float a = atan2(-fy, -fx) / M_PI;
    float fk = (a + 1.0f) / 2.0f * (ncols-1);
    int k0 = (int)fk;
    int k1 = (k0 + 1) % ncols;
    float f = fk - k0;
    //f = 0; // uncomment to see original color wheel
    for (int b = 0; b < 3; b++) {
      float col0 = colorwheel[k0][b] / 255.0f;
      float col1 = colorwheel[k1][b] / 255.0f;
      float col = (1 - f) * col0 + f * col1;
      if (rad <= 1)
        col = 1 - rad * (1 - col); // increase saturation with radius
      else
        col *= .75; // out of range
      pix[2 - b] = (int)(255.0 * col);
    }
  }

  void ColorFlow::setcols(int r, int g, int b, int k)
  {
    colorwheel[k][0] = r;
    colorwheel[k][1] = g;
    colorwheel[k][2] = b;
  }

  void ColorFlow::makecolorwheel()
  {
    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow 
    //  than between yellow and green)
    int RY = 15;
    int YG = 6;
    int GC = 4;
    int CB = 11;
    int BM = 13;
    int MR = 6;
    ncols = RY + YG + GC + CB + BM + MR;
    //printf("ncols = %d\n", ncols);

    assert(ncols < MAXCOLS);

    int i;
    int k = 0;

    for (i = 0; i < RY; i++) 
      setcols(255,	   255*i/RY,	 0,	       k++);

    for (i = 0; i < YG; i++) 
      setcols(255-255*i/YG, 255,		 0,	       k++);

    for (i = 0; i < GC; i++) 
      setcols(0,		   255,		 255*i/GC,     k++);

    for (i = 0; i < CB; i++) 
      setcols(0,		   255-255*i/CB, 255,	       k++);

    for (i = 0; i < BM; i++) 
      setcols(255*i/BM,	   0,		 255,	       k++);

    for (i = 0; i < MR; i++) 
      setcols(255,	   0,		 255-255*i/MR, k++);
  }

  void ColorFlow::apply(const cv::Mat &ofResult, cv::Mat &ofDisplayImg, float maxFlow)
  {

    unsigned char rgb_pix[3];

    if ((ofDisplayImg.size().width != ofResult.size().width) ||
      (ofDisplayImg.size().height != ofResult.size().height))
    {
      // resize display image
      ofDisplayImg.create(ofResult.size(), CV_8UC3);
    }

    // convert optical flow result to hsv color space for visualization
    for (int rowi = 0; rowi < ofResult.rows; rowi++)
    {

      const cv::Vec2f* ofResultRowPtr = ofResult.ptr<cv::Vec2f>(rowi);
      cv::Vec3b* ofDisplayRowPtr = ofDisplayImg.ptr<cv::Vec3b>(rowi);

      for (int coli = 0; coli < ofResult.cols; coli++)
      {
        float ofX = ofResultRowPtr[coli][0]/maxFlow;
        float ofY = ofResultRowPtr[coli][1]/maxFlow;

        computeColor(ofX,ofY,&(rgb_pix[0]));

        // need to convert from rgb to bgr
        ofDisplayRowPtr[coli].val[0] = rgb_pix[2];
        ofDisplayRowPtr[coli].val[1] = rgb_pix[1];
        ofDisplayRowPtr[coli].val[2] = rgb_pix[0];
      }
    }


  }

}
