/**
* @file    ScalarInequality.h
* @brief   An inequality constraint on a single element of a LieVector
* @author  Richard Roberts
* @created Sept 12, 2012
*/

#pragma once

#include <gtsam/base/LieVector.h>
#include <gtsam/slam/BoundingConstraint.h>

namespace basisflow {

  class ScalarInequality : public gtsam::BoundingConstraint1<gtsam::LieVector> {
  private:
    int element_;
  public:
    typedef gtsam::BoundingConstraint1<gtsam::LieVector> Base;
    typedef ScalarInequality This;

    ScalarInequality(gtsam::Key key, int element, double threshold, bool isGreaterThan, double mu) :
      Base(key, threshold, isGreaterThan, mu), element_(element) {}
    ScalarInequality(gtsam::Key key, int element, double threshold, bool isGreaterThan) :
      Base(key, threshold, isGreaterThan), element_(element) {}

    virtual double value(const gtsam::LieVector& x, boost::optional<gtsam::Matrix&> H = boost::none) const {
      double v = x(element_);
      if(H) {
        *H = gtsam::Matrix::Zero(1, x.size());
        (*H)(0,element_) = 1.0;
      }
      return v;
    }
  };

}
