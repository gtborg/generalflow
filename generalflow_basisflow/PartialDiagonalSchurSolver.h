/**
* @file    PartialDiagonalSchurSolver.h
* @brief   Solves a special linear system
* @author  Richard Roberts
* @created Sept 12, 2012
*/

#pragma once

#include <basisflow/dllexport.h>
#include <gtsam/base/Matrix.h>

namespace basisflow {

  /** Solves special linear systems of the following form:
  \f[
  p\left(x\right)=\intop_{y}\prod_{i}p\left(x_{i}\cond y\right)p\left(y\right)
  \f]
  \f[
  \left[\begin{array}{cccc}
  \Lambda_{11} &  &  & \Lambda_{1y}\\
  & \Lambda_{22} &  & \Lambda_{2y}\\
  &  & \ddots\\
  \Lambda_{1y}^{T} & \Lambda_{1y}^{T} &  & \Lambda_{yy}
  \end{array}\right]\left[\begin{array}{c}
  x_{1}\\
  x_{2}\\
  \vdots\\
  x_{y}
  \end{array}\right]=\left[\begin{array}{c}
  \eta_{1}\\
  \eta_{2}\\
  \vdots\\
  \eta_{y}
  \end{array}\right]
  \f]
  */
  class generalflow_basisflow_EXPORT PartialDiagonalSchurSolver {
  private:
    typedef Eigen::DiagonalMatrix<double, Eigen::Dynamic> LambdaIIType;
    LambdaIIType LambdaII_;
    gtsam::Matrix LambdaIy_;
    gtsam::Matrix Lambdayy_;
    gtsam::Vector eta_I_;
    gtsam::Vector eta_y_;
    double c_;

    mutable gtsam::Vector lastRightVector_;
    mutable gtsam::Vector lastEtaBar_;

  public:
    PartialDiagonalSchurSolver(int dimI, int dimy) :
      LambdaII_(dimI), LambdaIy_(dimI, dimy), Lambdayy_(dimy,dimy), eta_I_(dimI), eta_y_(dimy), c_(0.)
    {
      LambdaII_.setZero();
      LambdaIy_.setZero();
      Lambdayy_.setZero();
      eta_I_.setZero();
      eta_y_.setZero();
    }

    void updateI(int i, double L_IiIi, double eta_Ii, double c);
    void updatey(const gtsam::Matrix& L_yy, const gtsam::Vector& eta_y, double c);
    void updateIy(int i, double L_IiIi, const gtsam::Vector& L_yIi, const gtsam::Matrix& L_yy,
      double eta_Ii, const gtsam::Vector& eta_y, double c);

    double logLikelihood(const gtsam::Vector& I) const;
    const gtsam::Vector& lastRightVector() const { return lastRightVector_; }
    const gtsam::Vector& lastEtaBar() const { return lastEtaBar_; }
  };

}
