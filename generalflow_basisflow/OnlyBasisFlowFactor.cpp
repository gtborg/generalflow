/**
 * @file    OnlyBasisFlowFactor.cpp
 * @brief   Optical flow factor on only the basis
 * @author  Richard Roberts
 * @created Nov 30, 2012
 */

#include "OnlyBasisFlowFactor.h"
#include "sparseFlow.h"
#include "flowOps.h"

#include <functional>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace basisflow {

  inline double sq(double x) { return x*x; }

  /* **************************************************************************************** */
  NonlinearFactorGraph OnlyBasisFlowFactor::CreateWholeFrame(
    const SparseFlowField& measurements, gtsam::Key firstBasisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
    boost::shared_ptr<Values> latentValues, boost::shared_ptr<Values> sigmaValues, double inlierPrior)
  {
    throw runtime_error("Need to fix basis indexing");
    // Create a factor for each measurement
    NonlinearFactorGraph result;
    for(size_t i = 0; i < measurements.size(); ++i) {
      const Key basisKey = firstBasisKey + i;
      result.push_back(boost::make_shared<OnlyBasisFlowFactor>(basisKey, latentKey, sigmaKey,
        latentValues, sigmaValues, inlierPrior, measurements[i]));
    }
    return result;
  }

  /* **************************************************************************************** */
  void OnlyBasisFlowFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
    Base::print(s, keyFormatter);
  }

  /* **************************************************************************************** */
  bool OnlyBasisFlowFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
    const OnlyBasisFlowFactor *t = dynamic_cast<const This*>(&f);
    if(t && Base::equals(f))
      return latentKey_ == t->latentKey_ && sigmaKey_ == t->sigmaKey_ && measurement_.equals(t->measurement_, tol);
    else
      return false;
  }

  /* **************************************************************************************** */
  double OnlyBasisFlowFactor::error(const Values& x) const
  {
    // Retrieve values that we use
    const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
    const LieVector& latent = latentValues_->at<LieVector>(latentKey());
    const LieMatrix& basis = x.at<LieMatrix>(basisKey());

    // Error = 1/2 * e^2
    double inlier;
    return 0.5 * sparseFlow::fullFlowError(measurement_, inlierPrior_, basis, latent, sigmas(0), sigmas(1), inlier).squaredNorm();
  }

  /* **************************************************************************************** */
//#pragma optimize("", off)
  boost::shared_ptr<GaussianFactor> OnlyBasisFlowFactor::linearize(const Values& x, const Ordering& ordering) const
  {
    // Retrieve values that we use
    const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
    const LieVector& latent = latentValues_->at<LieVector>(latentKey());
    const LieMatrix& basis = x.at<LieMatrix>(basisKey());
    const Index j = ordering[basisKey()];
    static const noiseModel::Diagonal::shared_ptr unit = noiseModel::Unit::Create(2);

    // b = -e
    // dedV = ddV
    double inlier;
    Matrix ddV;
    const Vector b = -sparseFlow::fullFlowError(measurement_, inlierPrior_, basis, latent, sigmas(0), sigmas(1), inlier, ddV, boost::none);

    return boost::make_shared<JacobianFactor>(j, ddV, b, unit);
  }
//#pragma optimize("", on)

}
