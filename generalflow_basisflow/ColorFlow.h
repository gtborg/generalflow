// From http://subversion.assembla.com/svn/jhuworkshop2010/trunk/common/cpp/StreamSageImage/ColorFlow.h

#ifndef ColorFlow_h
#define ColorFlow_h

#include "assert.h"

#include <opencv2/core/core.hpp>

namespace StreamSage 
{
  /// This code has been adapted from the color_flow code found
  /// at the middlebury optical flow evaluation web site
  class ColorFlow
  {

  public:

    ColorFlow();

    ~ColorFlow(){};


    void computeColor(float fx, float fy, unsigned char *rgb_pix);

    void apply(const cv::Mat &ofResult, cv::Mat &ofDisplayImg, float maxFlow);


  private:

    int ncols;
    enum {MAXCOLS = 60};
    int colorwheel[MAXCOLS][3];

    void setcols(int r, int g, int b, int k);

    void makecolorwheel();

  };

}

#endif