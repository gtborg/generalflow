/*
 * lkinference.cpp
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */

#define DRAW

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <cstdlib>
#include <math.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/bind.hpp>
#include <gtsam/base/timing.h>

#include "../GMPPCALucasKanade.h"
#include "../Utils.h"

using namespace std;
using namespace basisflow;
using namespace gtsam;
using boost::math::round;
using boost::math::lround;

int main(int argc, char* argv[]) {

  if(argc < 2) {
    cout << "Please specify base name for ppcares files\n";
    cout << "Usage:  " << argv[0] << "  BASEFILENAME  [INPUT_VIDEO]" << endl;
    return 1;
  }

  cout << "argv[1] = " << argv[1] << endl;

  // See if a calibration file exists
  cv::Mat distCoeffs;
  cv::Mat camMatrix;
  if(boost::filesystem::exists(string(argv[1]) + ".yml")) {
    cout << "Found camera calibration file" << endl;
    cv::FileStorage calibration(string(argv[1]) + ".yml", cv::FileStorage::READ);
    cv::read(calibration["distortion_coefficients"], distCoeffs);
    cv::read(calibration["camera_matrix"], camMatrix);
  } else
    camMatrix = cv::Mat::eye(3,3, CV_64F);

  boost::shared_ptr<cv::VideoCapture> cap;
  if(argc >= 3) {
    cout << "Opening video file " << argv[2] << "..." << endl;
    if(!(boost::filesystem::exists(argv[2]) && boost::filesystem::is_regular_file(argv[2]))) {
      cout << argv[2] << " does not exist" << endl;
      exit(1);
    }
    cap.reset(new cv::VideoCapture(argv[2]));
    if(argc >= 4)
      cap->set(CV_CAP_PROP_POS_MSEC, atof(argv[3]));
  } else {
    cout << "Opening camera..." << endl;
    cap.reset(new cv::VideoCapture(0));
  }

  ofstream outfile("lkinference_out.txt");

  cv::Mat distorted;
  *cap >> distorted;
  cv::Mat frame;
  cv::undistort(distorted, frame, camMatrix, distCoeffs);
  cout << "Frame: " << frame.cols << " x " << frame.rows << endl;

  Subspace subspace(argv[1], frame.cols, frame.rows);

  // Figure out how many pyramid levels to use
  int nLevels = 0;
  while((frame.cols >> nLevels) << nLevels == frame.cols && (frame.rows >> nLevels) << nLevels == frame.rows)
    ++ nLevels;

  GMPPCALucasKanade::Params params(0, 0, 2.0, nLevels, 1, GMPPCALucasKanade::Params::INITIALIZATION);
#ifdef DRAW
  params.makeColorAvailable = true;
#else
  params.makeColorAvailable = false;
#endif
  GMPPCALucasKanade lk(subspace, params, frame.cols >> params.imageScalePower);

  enum { ERROR_IMAGE, SALIENT } drawType = SALIENT;

  Vector latent/* = Vector_(1, 0.01666666666667)*/, inliers;
  lk.nextFrame(frame, latent, inliers);

  boost::timer::cpu_timer timer;
  ofstream latvariables("latent.txt");

  char c = 0;
  size_t timeFrameCounter = 0;
  size_t frameNumber = 0;
  while(c != 'q') {

    gttic(get_frame);
    cout << "Frame " << cap->get(CV_CAP_PROP_POS_MSEC) << endl;
    *cap >> distorted;
    if(distorted.empty())
      break;
    cv::undistort(distorted, frame, camMatrix, distCoeffs);
    gttoc(get_frame);

    gttic(nextFrame);
    if(!latent.unaryExpr(ptr_fun(isfinite<double>)).all())
      latent = Vector();
    lk.nextFrame(frame, latent, inliers);
    gttoc(nextFrame);

    ++ timeFrameCounter;
    ++ frameNumber;

#ifdef DRAW
    gttic(draw);

    // Draw image/flow and error pyramids using OpenCV's image subranges
    size_t upPower = 1;
    double upScale = 1 << upPower;
    cv::Mat tiled(lk.curFrame_(params.firstPyrLevel).rows*upScale*1.5, lk.curFrame_(params.firstPyrLevel).cols*upScale*2.0, CV_8UC3);
    tiled.setTo(cv::Scalar(0,0,0));
    size_t xPosF = 0;
    size_t xPosE = lk.curFrame_(params.firstPyrLevel).cols*upScale;
    for(size_t level=params.firstPyrLevel; level<params.nPyrLevels; ++level) {
      vector<Point2> tpred(lk.predict(lk.latent[level], level));

      // Grab the subimage of the tiled destination image
      cv::Mat color(lk.curFrame_(level).rows*upScale, lk.curFrame_(level).cols*upScale, CV_8UC3);

      // Scale up the frame and convert it to color
      cv::Mat gray(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC1);
      lk.curFrame_(level).convertTo(gray, CV_8U, 255.0);
      cv::Mat colorSm(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC3);
      cv::cvtColor(gray, colorSm, CV_GRAY2BGR);

      cv::Mat imgup;
      cv::resize(colorSm, color, color.size(), 0.0, 0.0, cv::INTER_NEAREST);


      // Draw flow vectors on frame
      int nHorz = gray.cols >> params.flowSamplePower;
      int nVert = gray.rows >> params.flowSamplePower;
      double scale = (double)((1 << params.flowSamplePower) << upPower);
      double offset = scale / 2.0;
      for(size_t i=0; i<lk.imgXYs_[level].size(); ++i) {
        size_t imX = lk.imgXYs_[level][i].first, imY = lk.imgXYs_[level][i].second;
        double x0 = upScale * (double)imX + offset;
        double y0 = upScale * (double)imY + offset;
        double x = x0 + 8*(1<<params.imageScalePower) * tpred[imY*gray.cols + imX].x();
        double y = y0 + 8*(1<<params.imageScalePower) * tpred[imY*gray.cols + imX].y();
        double red;
        double green;
        red = (1.0-lk.outliers[level][i]) * 255.0;
        green = lk.outliers[level][i] * 255.0;
        cv::line(color, cv::Point((int)round(8.0*x0), (int)round(8.0*y0)),
          cv::Point((int)round(8.0*x), (int)round(8.0*y)), cv::Scalar(0,green,red), 1, CV_AA, 3);
      }

      gtsam::print(lk.latent[level], boost::str(boost::format("Latent[%d]: ") % level));

      if(level==params.firstPyrLevel){
        latvariables << lk.latent[level].transpose() << endl;
      }

      if(level == params.firstPyrLevel) {
        cv::Mat sub = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosF,xPosF+tiled.cols/2));
        color.copyTo(sub);
      } else {
        cv::Mat sub = tiled(
            cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
            cv::Range(xPosF, xPosF+lk.curFrame_(level).cols*upScale));
        color.copyTo(sub);
        xPosF += lk.curFrame_(level).cols*upScale;
      }

      // Grab subimage for drawing the error image
      if(level == params.firstPyrLevel)
        color = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosE,xPosE+tiled.cols/2));
      else {
        color = tiled(
            cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
            cv::Range(xPosE, xPosE+lk.curFrame_(level).cols*upScale));
        xPosE += lk.curFrame_(level).cols*upScale;
      }

      if(drawType == ERROR_IMAGE) {
//        // Convert the vector of errors to an error image
//        cv::Mat errImg = Utils::matOfVector(lk.errors[level], lk.curFrame_(level).cols);  assert(errImg.rows == lk.curFrame_(level).rows);
//
//        // Colorize the error image and copy it into the tiled image
//        cv::Mat colorErrImg(Utils::scale(Utils::colorize(3*errImg), upScale, cv::INTER_NEAREST));
//        colorErrImg.convertTo(color, CV_8UC3, 255.0);
      } else if(drawType == SALIENT && level == params.firstPyrLevel) {
        cv::Mat mask(lk.curScaled_.rows, lk.curScaled_.cols, CV_8U);
        mask.setTo(0);
        for(size_t j=0; j<lk.outliers[params.firstPyrLevel].rows(); ++j) {
          size_t imgX, imgY;
          boost::tie(imgX,imgY) = lk.imgXYs_[params.firstPyrLevel][j];
          mask.at<uint8_t>(imgY,imgX) = lk.outliers[params.firstPyrLevel](j) >= 0.5 ? 0 : 255;
        }
        cv::Mat masked;
        lk.curScaled_.copyTo(masked, mask);
        cv::resize(masked, color, color.size());
      }
    }
    cv::Mat displayTiled;
    double displayScaleFactor = 1024.0 / tiled.cols;
    cv::resize(tiled, displayTiled, cv::Size(), displayScaleFactor, displayScaleFactor);
    imshow("Image", displayTiled);
    c = cv::waitKey(10) & 0xff;

    gttoc(draw);

    cv::imwrite((boost::format("frames/%08d.png")%frameNumber).str(), tiled);
#endif

    outfile << latent.transpose() << endl;

    if(timer.elapsed().wall != 0.0) {
      cout << "Rate: " << double(timeFrameCounter) / timer.elapsed().wall << " Hz" << endl;
    }
    if(timeFrameCounter == 30) {
      timeFrameCounter = 0;
      timer.stop(); timer.start(); // Resets the timer
    }

    tictoc_finishedIteration();
    tictoc_print();

  }

  return 0;
}
