/*
 * lkinference.cpp
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */

#define DRAW

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <math.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/bind.hpp>
#include <gtsam/base/timing.h>

#include "../GMPPCALucasKanade.h"
#include "../Utils.h"
#include "../flycap/FlyCap.h"

using namespace std;
using namespace generalflow;
using namespace gtsam;

int main(int argc, char* argv[]) {

  if(argc < 2) {
    cout << "Please specify base name for ppcares files\n";
    cout << "Usage:  " << argv[0] << "  BASEFILENAME  [INPUT_VIDEO]" << endl;
    return 1;
  }

  cout << "argv[1] = " << argv[1] << endl;
  Subspace subspace(argv[1], 128, 3);

  // See if a calibration file exists
  cv::Mat distCoeffs;
  cv::Mat camMatrix;
  if(boost::filesystem::exists(string(argv[1]) + ".yml")) {
    cout << "Found camera calibration file" << endl;
    cv::FileStorage calibration(string(argv[1]) + ".yml", cv::FileStorage::READ);
    cv::read(calibration["distortion_coefficients"], distCoeffs);
    cv::read(calibration["camera_matrix"], camMatrix);
  } else
    camMatrix = cv::Mat::eye(3,3, CV_64F);

  boost::shared_ptr<cv::VideoCapture> cap;
  boost::shared_ptr<FlyCap> flycap;
  bool usingFlyCap;
  if(argc >= 3) {
    if(string(argv[2]) == "FlyCap"){
      usingFlyCap = true;
      flycap.reset(new FlyCap());
    } else {
      usingFlyCap = false;
      cout << "Opening video file " << argv[2] << "..." << endl;
      cap.reset(new cv::VideoCapture(argv[2]));
      if(argc >= 4)
        cap->set(CV_CAP_PROP_POS_MSEC, atof(argv[3]));
    }
  } else {
    cout << "Opening camera..." << endl;
    cap.reset(new cv::VideoCapture(0));
  }

  ofstream outfile("lkinference_out.txt");

  cv::Mat distorted;
  if(usingFlyCap){
    distorted = flycap->grabImage();
  } else {
    *cap >> distorted;
  }
  cv::Mat frame;
  cv::undistort(distorted, frame, camMatrix, distCoeffs);
  cout << "Frame: " << frame.cols << " x " << frame.rows << endl;

  GMPPCALucasKanade::Params params(0, 0, 2.0, 9, 4, GMPPCALucasKanade::Params::INITIALIZATION);
#ifdef DRAW
  params.makeColorAvailable = true;
#else
  params.makeColorAvailable = false;
#endif
  GMPPCALucasKanade lk(subspace, params, frame.cols >> params.imageScalePower);

  enum { ERROR_IMAGE, SALIENT } drawType = SALIENT;

  Vector latent/* = Vector_(1, 0.01666666666667)*/, inliers;
  cv::imwrite("frame_0.png", frame);
  lk.nextFrame(frame, latent, inliers);

  boost::timer timer;
  ofstream latvariables("latent.txt");

  char c = 0;
  size_t framen = 0;
  while(c != 'q') {

    gttic(get frame);
    if(!usingFlyCap)
      cout << "Frame " << cap->get(CV_CAP_PROP_POS_MSEC) << endl;
    if(usingFlyCap){
      distorted = flycap->grabImage();
    } else {
      *cap >> distorted;
    }
    cv::undistort(distorted, frame, camMatrix, distCoeffs);
    gttoc(get frame);

    gttic(nextFrame);
    //if(!latent.unaryExpr(ptr_fun<double,int>(isfinite)).all())
      latent = Vector();
    cv::imwrite((boost::format("frame_%d.png")%framen).str(), frame);
    lk.nextFrame(frame, latent, inliers);
    gttoc(nextFrame);

    ++ framen;

#ifdef DRAW
    gttic(draw);

    // Draw image/flow and error pyramids using OpenCV's image subranges
    size_t upPower = 1;
    double upScale = 1 << upPower;
    cv::Mat tiled(lk.curFrame_(params.firstPyrLevel).rows*upScale*1.5, lk.curFrame_(params.firstPyrLevel).cols*upScale*2.0, CV_8UC3);
    tiled.setTo(cv::Scalar(0,0,0));
    size_t xPosF = 0;
    size_t xPosE = lk.curFrame_(params.firstPyrLevel).cols*upScale;
    for(size_t level=params.firstPyrLevel; level<params.nPyrLevels; ++level) {
      vector<Point2> tpred(lk.predict(lk.latent[level], level));

      // Grab the subimage of the tiled destination image
      cv::Mat color(lk.curFrame_(level).rows*upScale, lk.curFrame_(level).cols*upScale, CV_8UC3);

      // Scale up the frame and convert it to color
      cv::Mat gray(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC1);
      lk.curFrame_(level).convertTo(gray, CV_8U, 255.0);
      cv::Mat colorSm(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC3);
      cv::cvtColor(gray, colorSm, CV_GRAY2BGR);

      cv::Mat imgup;
      cv::resize(colorSm, color, color.size(), 0.0, 0.0, cv::INTER_NEAREST);


      // Draw flow vectors on frame
      int nHorz = gray.cols >> params.flowSamplePower;
      int nVert = gray.rows >> params.flowSamplePower;
      double scale = (double)((1 << params.flowSamplePower) << upPower);
      double offset = scale / 2.0;
      for(int i=0; i<nVert; i+=1) {
        for(int j=0; j<nHorz; j+=1) {
          double x0 = scale * (double)j + offset;
          double y0 = scale * (double)i + offset;
          double x = x0 + (1<<params.imageScalePower) * tpred[i*nHorz + j].x();
          double y = y0 + (1<<params.imageScalePower) * tpred[i*nHorz + j].y();
          double red;
          double green;
          if(level == params.firstPyrLevel) {
            red = (1.0-lk.outliers[level](i*nHorz + j)) * 255.0;
            green = lk.outliers[level](i*nHorz + j) * 255.0;
          } else {
            red = 255.0;
            green = 255.0;
          }
          cv::line(color, cv::Point((int)round(8.0*x0), (int)round(8.0*y0)),
              cv::Point((int)round(8.0*x), (int)round(8.0*y)), cv::Scalar(0,green,red), 1, CV_AA, 3);
        }
      }

      gtsam::print(lk.latent[level], boost::str(boost::format("Latent[%d]: ") % level));

      if(level==params.firstPyrLevel){
        latvariables << lk.latent[level]<<"\n";
      }

      if(level == params.firstPyrLevel) {
        cv::Mat sub = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosF,xPosF+tiled.cols/2));
        color.copyTo(sub);
      } else {
        cv::Mat sub = tiled(
            cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
            cv::Range(xPosF, xPosF+lk.curFrame_(level).cols*upScale));
        color.copyTo(sub);
        xPosF += lk.curFrame_(level).cols*upScale;
      }

      // Grab subimage for drawing the error image
      if(level == params.firstPyrLevel)
        color = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosE,xPosE+tiled.cols/2));
      else {
        color = tiled(
            cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
            cv::Range(xPosE, xPosE+lk.curFrame_(level).cols*upScale));
        xPosE += lk.curFrame_(level).cols*upScale;
      }

      if(drawType == ERROR_IMAGE) {
//        // Convert the vector of errors to an error image
//        cv::Mat errImg = Utils::matOfVector(lk.errors[level], lk.curFrame_(level).cols);  assert(errImg.rows == lk.curFrame_(level).rows);
//
//        // Colorize the error image and copy it into the tiled image
//        cv::Mat colorErrImg(Utils::scale(Utils::colorize(3*errImg), upScale, cv::INTER_NEAREST));
//        colorErrImg.convertTo(color, CV_8UC3, 255.0);
      } else if(drawType == SALIENT && level == params.firstPyrLevel) {
        Vector vmask(lk.outliers[params.firstPyrLevel].rows());
        for(size_t j=0; j<lk.outliers[params.firstPyrLevel].rows(); ++j) {
          vmask(j) = lk.outliers[params.firstPyrLevel](j) >= 0.5 ? 0.0 : 1.0;
        }
        cv::Mat mask = Utils::matOfVector(vmask, lk.curScaled_.cols);
        mask.convertTo(mask, CV_8U, 255.0);
        cv::Mat masked;
        lk.curScaled_.copyTo(masked, mask);
        cv::resize(masked, color, color.size());
      }
    }
    imshow("Image", tiled);
    c = cv::waitKey(10) & 0xff;

    gttoc(draw);
#endif

    for(size_t i=0; i<latent.size(); ++i)
      outfile << latent(i) << "\t";
    outfile << endl;

    cv::imwrite((boost::format("frames/%08d.png")%framen).str(), tiled);

    if(timer.elapsed() != 0.0) {
      cout << "Rate: " << double(framen) / timer.elapsed() << " Hz" << endl;
    }
    if(framen % 30 == 0) {
      timer.restart();
    }

    tictoc_finishedIteration();
    tictoc_print();

  }

  return 0;
}
