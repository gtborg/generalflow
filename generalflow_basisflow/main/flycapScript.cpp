/*
 * flycapScript.cpp
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <math.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/bind.hpp>
#include <gtsam/base/timing.h>

#include "../GMPPCALucasKanade.h"
#include "../Utils.h"
#include "../flycap/FlyCap.h"

using namespace std;
using namespace generalflow;
using namespace gtsam;

int main(int argc, char* argv[]) {

  if(argc < 2) {
    cout << "Please specify base name for ppcares files\n";
    cout << "Usage:  " << argv[0] << "  BASEFILENAME  [INPUT_VIDEO]" << endl;
    return 1;
  }

  cout << "argv[1] = " << argv[1] << endl;
  Subspace subspace(argv[1], 128, 3);

  // See if a calibration file exists
  cv::Mat distCoeffs;
  cv::Mat camMatrix;
  if(boost::filesystem::exists(string(argv[1]) + ".yml")) {
    cout << "Found camera calibration file" << endl;
    cv::FileStorage calibration(string(argv[1]) + ".yml", cv::FileStorage::READ);
    cv::read(calibration["distortion_coefficients"], distCoeffs);
    cv::read(calibration["camera_matrix"], camMatrix);
  } else
    camMatrix = cv::Mat::eye(3,3, CV_64F);

  boost::shared_ptr<cv::VideoCapture> cap;
  boost::shared_ptr<FlyCap> flycap;
  if(argc >= 3) {
      flycap.reset(new FlyCap());
    if(argc >= 4)
      cap->set(CV_CAP_PROP_POS_MSEC, atof(argv[3]));
  } else {
    cout << "Opening camera..." << endl;
    cap.reset(new cv::VideoCapture(0));
  }

  ofstream outfile("lkinference_out.txt");

  cv::Mat distorted;
  distorted = flycap->grabImage();
  cv::Mat frame;
  cv::undistort(distorted, frame, camMatrix, distCoeffs);
  cout << "Frame: " << frame.cols << " x " << frame.rows << endl;

  GMPPCALucasKanade::Params params(0, 0, 2.0, 9, 4, GMPPCALucasKanade::Params::INITIALIZATION);

  params.makeColorAvailable = false;

  GMPPCALucasKanade lk(subspace, params, frame.cols >> params.imageScalePower);

  enum { ERROR_IMAGE, SALIENT } drawType = SALIENT;

  Vector latent/* = Vector_(1, 0.01666666666667)*/, inliers;
  cv::imwrite("frame_0.png", frame);
  lk.nextFrame(frame, latent, inliers);

  boost::timer timer;
  ofstream latvariables("latent.txt");

  char c = 0;
  size_t framen = 0;
  while(c != 'q') {

    gttic(get frame);
    distorted = flycap->grabImage();
    cv::undistort(distorted, frame, camMatrix, distCoeffs);
    gttoc(get frame);

    gttic(nextFrame);
    //if(!latent.unaryExpr(ptr_fun<double,int>(isfinite)).all())
      latent = Vector();
    cv::imwrite((boost::format("frame_%d.png")%framen).str(), frame);
    lk.nextFrame(frame, latent, inliers);
    gttoc(nextFrame);

    ++ framen;


    for(size_t i=0; i<latent.size(); ++i)
      outfile << latent(i) << "\t";
    outfile << endl;

    cv::imwrite((boost::format("frames/%08d.png")%framen).str(), tiled);

    if(timer.elapsed() != 0.0) {
      cout << "Rate: " << double(framen) / timer.elapsed() << " Hz" << endl;
    }
    if(framen % 30 == 0) {
      timer.restart();
    }

    tictoc_finishedIteration();
    tictoc_print();

  }

  return 0;
}
