/*
 * lkinference.cpp
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */

#define DRAW

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/bind.hpp>
#include <gtsam/base/timing.h>

#include "../GMPPCALucasKanade.h"
#include "../Utils.h"

using namespace std;
using namespace basisflow;
using namespace gtsam;
using boost::math::round;

int main(int argc, char* argv[]) {

  if(argc < 2) {
    cout << "Please specify base name for ppcares files\n";
    cout << "Usage:  " << argv[0] << "  BASEFILENAME  [INPUT_VIDEO]" << endl;
    return 1;
  }

  cout << "argv[1] = " << argv[1] << endl;

  // Read multi-subspace file
  ifstream multifile((string(argv[1]) + "_multi.txt").c_str());
  if(!multifile.good()) {
    cout << "Problem opening " << argv[1] << "_multi.txt" << endl;
    return 1;
  }

  vector<Subspace> subspaces;
  while(!multifile.eof()) {
    char buf[4096];
    multifile.getline(buf, 4096);
    if(strlen(buf) > 0)
      subspaces.push_back(Subspace(string(argv[1]) + "_" + buf, 80, 3));
    else
      break;
  }

  // See if a calibration file exists
  cv::Mat distCoeffs;
  cv::Mat camMatrix;
  if(boost::filesystem::exists(string(argv[1]) + "_camera.yml")) {
    cout << "Found camera calibration file" << endl;
    cv::FileStorage calibration(string(argv[1]) + "_camera.yml", cv::FileStorage::READ);
    cv::read(calibration["distortion_coefficients"], distCoeffs);
    cv::read(calibration["camera_matrix"], camMatrix);
  } else
    camMatrix = cv::Mat::eye(3,3, CV_64F);

  boost::shared_ptr<cv::VideoCapture> cap;
  if(argc >= 3) {
    cout << "Opening video file " << argv[2] << "..." << endl;
    cap.reset(new cv::VideoCapture(argv[2]));
    if(argc >= 4)
      cap->set(CV_CAP_PROP_POS_MSEC, atof(argv[3]));
  } else {
    cout << "Opening camera..." << endl;
    cap.reset(new cv::VideoCapture(0));
  }

  ofstream outfile("lkinference_out.txt");
  ofstream outinliers("inliers_out.txt");

  cv::Mat distorted;
  *cap >> distorted;
  cv::Mat frame;
  cv::undistort(distorted, frame, camMatrix, distCoeffs);
  cout << "Frame: " << frame.cols << " x " << frame.rows << endl;

  GMPPCALucasKanade::Params params(0, 1, 1.0, 5, 3, GMPPCALucasKanade::Params::INITIALIZATION);
#ifdef DRAW
  params.makeColorAvailable = true;
#else
  params.makeColorAvailable = false;
#endif
  vector<GMPPCALucasKanade> lks;
  BOOST_FOREACH(const Subspace& subspace, subspaces) {
    lks.push_back(GMPPCALucasKanade(subspace, params, frame.cols >> params.imageScalePower));
  }

  enum { ERROR_IMAGE, SALIENT } drawType = SALIENT;

  vector<Vector> latent(subspaces.size()), inliers(subspaces.size());
  for(size_t k=0; k<subspaces.size(); ++k)
    lks[k].nextFrame(frame, latent[k], inliers[k]);

  boost::timer::cpu_timer timer;

  char c = 0;
  size_t framen = 0;
  double lastpos = cap->get(CV_CAP_PROP_POS_AVI_RATIO);
  while(c != 'q') {

    double curpos = cap->get(CV_CAP_PROP_POS_AVI_RATIO);
    if(curpos < lastpos)
      break;
    lastpos = curpos;

    gttic_(get_frame);
    cout << "Frame " << cap->get(CV_CAP_PROP_POS_MSEC) << endl;
    *cap >> distorted;
    cv::undistort(distorted, frame, camMatrix, distCoeffs);
    gttoc_(get_frame);

    gttic_(nextFrame);
    Vector L_k(subspaces.size());
    double L_sum = 0.0;
    for(size_t k=0; k<subspaces.size(); ++k) {
      latent[k] = Vector(); // Reset latent variables to prevent bad solutions from carrying on
      lks[k].nextFrame(frame, latent[k], inliers[k]);
      gtsam::print(latent[k], (boost::format("x[%d] = ")%k).str());
      L_k(k) = lks[k].marginalLog(inliers[k], latent[k], k);
      if(k == 0)
        L_sum = L_k(k);
      else
        L_sum += log(1 + exp(L_k(k) - L_sum));
    }
    gttoc_(nextFrame);

    outinliers << framen;
    for(size_t k=0; k<subspaces.size(); ++k) {
      cout << "p(k=" << k << ") = " << exp(L_k(k) - L_sum) << ", L(k) = " << L_k(k) << ", |z| = " << inliers[k].sum() << endl;
      outinliers << "\t" << L_k(k) << "\t" << exp(L_k(k) - L_sum) << "\t" << inliers[k].sum();
    }
    outinliers << endl;
    outinliers.flush();

    ++ framen;

#ifdef DRAW
    gttic_(draw);

    for(size_t k=0; k<subspaces.size(); ++k) {
      GMPPCALucasKanade& lk(lks[k]);

      // Draw image/flow and error pyramids using OpenCV's image subranges
      size_t upPower = 0;
      double upScale = 1 << upPower;
      cv::Mat tiled(lk.curFrame_(params.firstPyrLevel).rows*upScale*1.5, lk.curFrame_(params.firstPyrLevel).cols*upScale*2.0, CV_8UC3);
      tiled.setTo(cv::Scalar(0,0,0));
      size_t xPosF = 0;
      size_t xPosE = lk.curFrame_(params.firstPyrLevel).cols*upScale;
      for(size_t level=params.firstPyrLevel; level<params.nPyrLevels; ++level) {
        vector<Point2> tpred(lk.predict(lk.latent[level], level));

        // Grab the subimage of the tiled destination image
        cv::Mat color(lk.curFrame_(level).rows*upScale, lk.curFrame_(level).cols*upScale, CV_8UC3);

        // Scale up the frame and convert it to color
        cv::Mat gray(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC1);
        lk.curFrame_(level).convertTo(gray, CV_8U, 255.0);
        cv::Mat colorSm(lk.curFrame_(level).rows, lk.curFrame_(level).cols, CV_8UC3);
        cv::cvtColor(gray, colorSm, CV_GRAY2BGR);

        cv::Mat imgup;
        cv::resize(colorSm, color, color.size(), 0.0, 0.0, cv::INTER_NEAREST);

        // Draw flow vectors on frame
        int nHorz = gray.cols >> params.flowSamplePower;
        int nVert = gray.rows >> params.flowSamplePower;
        double scale = (double)((1 << params.flowSamplePower) << upPower);
        double offset = scale / 2.0;
        for(int i=0; i<nVert; i+=1) {
          for(int j=0; j<nHorz; j+=1) {
            double x0 = scale * (double)j + offset;
            double y0 = scale * (double)i + offset;
            double x = x0 + (1<<params.imageScalePower) * tpred[(i<<params.flowSamplePower)*nHorz + (i<<params.flowSamplePower)].x();
            double y = y0 + (1<<params.imageScalePower) * tpred[(i<<params.flowSamplePower)*nHorz + (i<<params.flowSamplePower)].y();
            double red = level == params.firstPyrLevel ? (1.0-lk.outliers[level](i*nHorz + j)) * 255.0 : 255.0;
            double green = level == params.firstPyrLevel ? lk.outliers[level](i*nHorz + j) * 255.0 : 255.0;
            cv::line(color, cv::Point((int)round(8.0*x0), (int)round(8.0*y0)),
                cv::Point((int)round(8.0*x), (int)round(8.0*y)), cv::Scalar(0,green,red), 1, CV_AA, 3);
          }
        }

//        gtsam::print(lk.latent[level], boost::str(boost::format("Latent[%d]: ") % level));

        if(level == params.firstPyrLevel) {
          cv::Mat sub = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosF,xPosF+tiled.cols/2));
          color.copyTo(sub);
        } else {
          cv::Mat sub = tiled(
              cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
              cv::Range(xPosF, xPosF+lk.curFrame_(level).cols*upScale));
          color.copyTo(sub);
          xPosF += lk.curFrame_(level).cols*upScale;
        }

        // Grab subimage for drawing the error image
        if(level == params.firstPyrLevel)
          color = tiled(cv::Range(0,lk.curFrame_(params.firstPyrLevel).rows*upScale), cv::Range(xPosE,xPosE+tiled.cols/2));
        else {
          color = tiled(
              cv::Range(lk.curFrame_(params.firstPyrLevel).rows*upScale, lk.curFrame_(params.firstPyrLevel).rows*upScale + lk.curFrame_(level).rows*upScale),
              cv::Range(xPosE, xPosE+lk.curFrame_(level).cols*upScale));
          xPosE += lk.curFrame_(level).cols*upScale;
        }

        if(drawType == ERROR_IMAGE) {
          //        // Convert the vector of errors to an error image
          //        cv::Mat errImg = Utils::matOfVector(lk.errors[level], lk.curFrame_(level).cols);  assert(errImg.rows == lk.curFrame_(level).rows);
          //
          //        // Colorize the error image and copy it into the tiled image
          //        cv::Mat colorErrImg(Utils::scale(Utils::colorize(3*errImg), upScale, cv::INTER_NEAREST));
          //        colorErrImg.convertTo(color, CV_8UC3, 255.0);
        } else if(drawType == SALIENT && level == params.firstPyrLevel) {
//          Vector vmask(lk.outliers[params.firstPyrLevel].rows());
//          for(size_t j=0; j<lk.outliers[params.firstPyrLevel].rows(); ++j) {
//            vmask(j) = lk.outliers[params.firstPyrLevel](j) >= 0.5 ? 0.0 : 1.0;
//          }
//          cv::Mat mask = Utils::matOfVector(vmask, lk.curScaled_.cols);
//          mask.convertTo(mask, CV_8U, 255.0);
//          cv::Mat masked;
//          lk.curScaled_.copyTo(masked, mask);
//          cv::resize(masked, color, color.size());
        }
      }
      imshow((boost::format("Subspace %d")%k).str(), tiled);
    }

    c = cv::waitKey(10) & 0xff;
    gttoc_(draw);
#endif

//    cv::imwrite((boost::format("frames/%08d.png")%framen).str(), tiled);

    if(timer.elapsed().wall != 0.0) {
      cout << "Rate: " << double(framen) / timer.elapsed().wall << " Hz" << endl;
    }
    if(framen % 30 == 0) {
      timer.stop(); timer.start(); // Resets the timer
    }

    tictoc_finishedIteration_();
    tictoc_print_();

  }

  return 0;
}
