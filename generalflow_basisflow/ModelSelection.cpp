/**
 * @file    ModelSelection.cpp
 * @brief   Select among several optical flow templates
 * @author  Richard Roberts
 * @created Sept 14, 2012
 */

#include "ModelSelection.h"
#include "PartialDiagonalSchurSolver.h"

#include <basisflow/flowOps.h>

using namespace gtsam;
using namespace std;

namespace basisflow {

  static inline double sq(double x) { return x*x; }

  /* **************************************************************************************** */
  Vector ModelSelection::evaluateLogLikelihood(int frameNum) const {

    const int pimWidth = subspaces_.front().width();
    const int pimHeight = subspaces_.front().height();
    const int q = subspaces_.front().dimLatent();

    Vector result = Vector::Zero(subspaces_.size());

    lastRightVectors_.resize(pimWidth*pimHeight, subspaces_.size());
    lastEtaBars_.resize(pimWidth*pimHeight, subspaces_.size());

    for(size_t k=0; k<subspaces_.size(); ++k) {

      Eigen::Vector4d sigmas(subspaces_[k].inlierSigma2(), subspaces_[k].outlierSigma2(),
        subspaces_[k].inlierPixSigma2(), subspaces_[k].outlierPixSigma2());
      sigmas = sigmas.array().sqrt();

      const FrameCacheLoadedImages& images = frameCache_->loadImages(
        frameNum, sigmas(0), sigmas(1), sigmas(2), sigmas(3));
      const Matrix Dtimg = images.curFrame() - images.prevFrame();

      PartialDiagonalSchurSolver solver(pimWidth*pimHeight, subspaces_.front().dimLatent());

      // Update solver for each pixel
      for(int j=0; j<pimWidth; ++j) {
        for(int i=0; i<pimHeight; ++i) {
          const int pix = pimHeight*j + i;
          Matrix::ConstRowsBlockXpr basis = subspaces_[k].basis().middleRows(2*pix, 2);
            const Eigen::RowVector2d grad = flowOps::grad(images.prevGradX(), images.prevGradY(), j, i);
          // Note we assume only inliers here
          const double J = flowOps::J(1.0, images.weightsV()(i,j), images.weightsF()(i,j));

          // Calculate information updates
          const double Lii = sq(J);
          const Vector LIy = sq(J)*grad*basis;
          const Matrix Lyy = sq(J)*basis.transpose()*grad.transpose()*grad*basis;
          const double etai = 0.0;
          const Vector etay = Vector::Zero(basis.cols());

          // Update solver information
          solver.updateIy(pix, Lii, LIy, Lyy, etai, etay, 0.0);

          // Update solver with y prior
          solver.updatey(Matrix::Identity(q,q), Vector::Zero(q), 0.0);
        }
      }

      // Calculate negative log likelihood
      Eigen::Map<const Vector> DtAsVector(&Dtimg(0,0), pimWidth*pimHeight);
      result(k) = solver.logLikelihood(DtAsVector);

      lastRightVectors_.col(k) = solver.lastRightVector();
      lastEtaBars_.col(k) = solver.lastEtaBar();
    }

    return result;
  }

}
