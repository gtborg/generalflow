/**
 * @file    CostMap.h
 * @brief   A planar cost map
 * @author  Richard Roberts
 * @created January 28, 2013
 */

#include "CostMap.h"
#include "flowGeometry.h"

using namespace gtsam;

namespace basisflow {

  /* **************************************************************************************** */
  void CostMap::update(const gtsam::Pose3& pose, const gtsam::Cal3_S2& K, const gtsam::Matrix& imageLabelsFree, const gtsam::Matrix& imageLabelsOccupied,
    int gridWidth, int gridHeight, int imgWidth, int imgHeight)
  {
    double xScale = double(imgWidth) / double(gridWidth);
    double yScale = double(imgHeight) / double(gridHeight);
    for(int j = 0; j < imageLabelsFree.cols(); ++j)
      for(int i = 0; i < imageLabelsFree.rows(); ++i) {
        // Get ground plane projection
        const Point3 planarPoint = flowGeometry::cameraGroundPlaneIntersection(pose, K, Point2(xScale*double(j), yScale*double(i)));
        if(pose.transform_to(planarPoint).x() > 0.0) {
          std::pair<int,int> mapCoords = mapFromWorld(planarPoint.x(), planarPoint.y());
          if(mapCoords.first >= 0 && mapCoords.first < mapFree_.cols() &&
            mapCoords.second >= 0 && mapCoords.second < mapFree_.rows())
          {
            mapFree_(mapCoords.second, mapCoords.first) += imageLabelsFree(i,j);
            mapOccupied_(mapCoords.second, mapCoords.first) += imageLabelsOccupied(i,j);
          }
        }
      }
  }

}
