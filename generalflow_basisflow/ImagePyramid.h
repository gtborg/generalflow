/**
 * @file    ImagePyramid.h
 * @brief   
 * @author  Richard Roberts
 * @created Feb 20, 2011
 */

#pragma once

#include <basisflow/dllexport.h>
#include <vector>
#include <opencv2/core/core.hpp>

namespace basisflow {

  class generalflow_basisflow_EXPORT ImagePyramid {

    std::vector<cv::Mat> pyramid_;

  public:
    ImagePyramid() {}
    ImagePyramid(const cv::Mat& image, size_t nLevels);

    const cv::Mat& operator()(size_t level) const { return pyramid_[level]; }

    void swap(ImagePyramid& other) { this->pyramid_.swap(other.pyramid_); }

    bool empty() const { return pyramid_.empty(); }

    const cv::Mat& back() const { return pyramid_.back(); }
  };

}

