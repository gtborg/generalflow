/*
 * testNumerics.cpp
 *
 *  Created on: Jun 27, 2010
 *      Author: Richard Roberts
 */

#include <CppUnitLite/TestHarness.h>

#include <gtsam/base/Vector.h>
#include <gtsam/base/Matrix.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>

using namespace gtsam;
using namespace std;

/* ************************************************************************* */
TEST(Numerics, kronecker_row_vectors)
{
  Vector y = Vector(3) << 1.0, 2.0, 3.0));
  Vector H = Vector(2) << 4.0, 5.0));

  Matrix A = y * H.transpose();
  // Reinterpret as 1x6 matrix
  A = Eigen::Map<Matrix>(&A(0,0), 1, 6);

  Matrix expected(Matrix_(1, 6,
      4.0, 8.0, 12.0, 5.0, 10.0, 15.0));

  CHECK(assert_equal(expected, A));
}

TEST(Numerics, kronecker_rearrange)
{
  Vector y = Vector(3) << 1.0, 2.0, 3.0));
  Vector H = Vector(2) << 4.0, 5.0));
  Matrix A = y * H.transpose();
  // Reinterpret as 1x6 matrix
  A = Eigen::Map<Matrix>(&A(0,0), 1, 6);

  Matrix W(Matrix_(2,3,
      6.0, 7.0, 8.0,
      9.0, 10.0, 11.0));
  Matrix Wvec = W;
  Wvec.transposeInPlace();
  Wvec = Eigen::Map<Matrix>(&Wvec(0,0), 6, 1);

  Matrix actual = A * Wvec;

  Matrix Hm(Matrix_(1,2, &(H.data()[0])));
  Matrix ym(Matrix_(3,1, &(y.data()[0])));
  Matrix expected1 = Hm * W * ym;
  Matrix expected2 = Matrix_(1,1,
      1.0*(4.0*6.0+5.0*9.0) + 2.0*(4.0*7.0+5.0*10.0) + 3.0*(4.0*8.0+5.0*11.0));

  CHECK(assert_equal(expected1, actual));
  CHECK(assert_equal(expected2, actual));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
