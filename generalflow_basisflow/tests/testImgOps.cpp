/**
 * @file    testImageShiftFactor.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Feb 11, 2011
 */

#include <CppUnitLite/TestHarness.h>

#include <cv.h>
#include <highgui.h>
#include <gtsam/base/TestableAssertions.h>

#include "../ImgOps.h"

using namespace std;
using namespace gtsam;
using namespace basisflow;

/* ************************************************************************* */
TEST(ImgOps, gaussianKernel1) {

  // sigma = 0.5, kernel width of 5
  ImgOps imgops(0.5,5);

  const Vector expected = (Vector(5) << 0.00026387, 0.10645, 0.78657, 0.10645, 0.00026387);

  const cv::Mat& kernel(imgops.gaussianKernel(0.0));
  Vector actual(5);
  Eigen::Map<const Vector> eigenKernel(&kernel.at<double>(0,0), kernel.rows, kernel.cols);
  actual = eigenKernel;

  EXPECT(assert_equal(actual, expected, 1e-5));

  // Check normalized
  double sum = 0.0;
  for(size_t i=0; i<5; ++i)
    sum += kernel.at<double>(i);
  DOUBLES_EQUAL(1.0, sum, 1e-10);
}

/* ************************************************************************* */
TEST(ImgOps, gaussianKernel2) {
  ImgOps imgops(0.5,5);
  const cv::Mat& kernel(imgops.gaussianKernel(-0.4));

  // Check normalized
  double sum = 0.0;
  for(size_t i=0; i<5; ++i)
    sum += kernel.at<double>(i);
  DOUBLES_EQUAL(1.0, sum, 1e-10);
}

/* ************************************************************************* */
TEST(ImgOps, gaussianKernel3) {
  ImgOps imgops(0.5,5);
  const cv::Mat& kernel(imgops.gaussianKernel(0.3));

  // Check normalized
  double sum = 0.0;
  for(size_t i=0; i<5; ++i)
    sum += kernel.at<double>(i);
  DOUBLES_EQUAL(1.0, sum, 1e-10);
}

/* ************************************************************************* */
TEST(ImgOps, linearize) {

  ImgOps imgops(0.5,5);

  cv::Mat img;
  img.create(10,10, CV_64F);

  for(int i=0; i<10; ++i)
    for(int j=0; j<10; ++j) {
      img.at<double>(i,j) = j / 10.0;
    }

  cv::Mat gradImgX(10,10,CV_64F), gradImgY(10,10,CV_64F);
  for(int i=0; i<10; ++i)
    for(int j=0; j<10; ++j) {
      gradImgX.at<double>(i,j) = 0.1;
      gradImgY.at<double>(i,j) = 0.0;
    }

  std::pair<double, Point2> val_grad = *imgops.linear(img, gradImgX, gradImgY, Point2(5.0, 5.0));
  DOUBLES_EQUAL(0.5, val_grad.first, 1e-5);
  EXPECT(assert_equal(Point2(0.1,0.0), val_grad.second, 1e-5));

  val_grad = *imgops.linear(img, gradImgX, gradImgY, Point2(5.5, 5.0));
  DOUBLES_EQUAL(0.55, val_grad.first, 1e-5);
  EXPECT(assert_equal(Point2(0.1,0.0), val_grad.second, 1e-5));
}

/* ************************************************************************* */
TEST(ImgOps, resample) {
  ImgOps imgops(0.5,5);

  cv::Mat img;
  img.create(10,10, CV_64F);

  img.at<double>(3,3) = 1.0;
  img.at<double>(3,4) = 2.0;
  img.at<double>(4,3) = 3.0;
  img.at<double>(4,4) = 4.0;

  DOUBLES_EQUAL(1.0, imgops.resampleBilinear(img, Point2(3,3)), 1e-5);
  DOUBLES_EQUAL(2.0, imgops.resampleBilinear(img, Point2(4,3)), 1e-5);
  DOUBLES_EQUAL(3.0, imgops.resampleBilinear(img, Point2(3,4)), 1e-5);
  DOUBLES_EQUAL(4.0, imgops.resampleBilinear(img, Point2(4,4)), 1e-5);

  DOUBLES_EQUAL(1.2, imgops.resampleBilinear(img, Point2(3.2,3)), 1e-5);
  DOUBLES_EQUAL(1.4, imgops.resampleBilinear(img, Point2(3,3.2)), 1e-5);
  DOUBLES_EQUAL(2.5, imgops.resampleBilinear(img, Point2(3.5,3.5)), 1e-5);
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
