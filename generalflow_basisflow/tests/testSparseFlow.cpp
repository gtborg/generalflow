/**
 * @file    testImageShiftFactor.cpp
 * @brief
 * @author  Richard Roberts
 * @created Feb 11, 2011
 */

#include <CppUnitLite/TestHarness.h>

#include <gtsam/base/numericalDerivative.h>
#include <gtsam/linear/GaussianSequentialSolver.h>
#include <gtsam/nonlinear/Symbol.h>
#include "../OnlyBasisFlowFactor.h"
#include "../sparseFlow.h"

using namespace std;
using namespace gtsam;
using namespace basisflow;

/* ************************************************************************* */
TEST(sparseFlow, solving1)
{
  using namespace gtsam::symbol_shorthand;

  Matrix basis(2,2); basis <<
    0.9, 0.0,
    0.0, 0.9;
  Vector latent1(2); latent1 << 1.0, 0.0;
  SparseFlowField::Measurement m1(0,0, 1.0, 0.0);
  Vector latent2(2); latent2 << 0.0, 1.0;
  SparseFlowField::Measurement m2(0,0, 0.0, 1.0);

  // Parameters
  const double inlierPrior = 0.9;
  const double flowSigmaV = 0.5;
  const double flowSigmaF = 2.0;

  // At the solution, so expect that e = 0
  Vector expected_e(2); expected_e << 0, 0;

  // Compute actual error
  Matrix ddVActual, ddyActual;
  double inlier;
  Vector e = sparseFlow::fullFlowError(m1, inlierPrior, basis, latent1, flowSigmaV, flowSigmaF,
    inlier, ddVActual, ddyActual);

  EXPECT(assert_equal(expected_e, e));

  // Try solving

  // Expected basis
  Matrix basisExpected(2,2); basisExpected <<
    1.0, 0.0,
    0.0, 1.0;

  // Create factors
  boost::shared_ptr<Values> basisValues(new Values);
  basisValues->insert(V(0), LieMatrix(basis));
  boost::shared_ptr<Values> latentValues(new Values);
  latentValues->insert(Y(0), LieVector(latent1));
  latentValues->insert(Y(1), LieVector(latent2));
  boost::shared_ptr<Values> sigmaValues(new Values);
  sigmaValues->insert(S(0), LieVector(2, flowSigmaV, flowSigmaF));
  OnlyBasisFlowFactor basisFactor1(V(0), Y(0), S(0), latentValues, sigmaValues, inlierPrior, m1);
  OnlyBasisFlowFactor basisFactor2(V(0), Y(1), S(0), latentValues, sigmaValues, inlierPrior, m2);

  Ordering basisOrder;
  basisOrder.push_back(V(0));

  GaussianFactorGraph gfg;
  gfg.push_back(basisFactor1.linearize(*basisValues, basisOrder));
  gfg.push_back(basisFactor2.linearize(*basisValues, basisOrder));
  VectorValues soln = *GaussianSequentialSolver(gfg).optimize();

  Values newBasisValues = basisValues->retract(soln, basisOrder);
  EXPECT(assert_equal(basisExpected, Matrix(newBasisValues.at<LieMatrix>(V(0)))));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
