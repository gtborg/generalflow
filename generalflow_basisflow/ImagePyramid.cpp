/**
 * @file    ImagePyramid.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Feb 20, 2011
 */

#include "ImagePyramid.h"

#include <stdexcept>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

namespace basisflow {

  ImagePyramid::ImagePyramid(const cv::Mat& image, size_t nLevels) :
        pyramid_(nLevels) {

    if(!(nLevels > 0))
      throw invalid_argument("nLevels must be at least 1");

    // 0th level is the original image
    image.copyTo(pyramid_[0]);

    // Use OpenCV to downsample images
    for(size_t level = 1; level < pyramid_.size(); ++level) {
      // Downsample by 1/2
      cv::pyrDown(pyramid_[level-1], pyramid_[level]);
    }
  }

}
