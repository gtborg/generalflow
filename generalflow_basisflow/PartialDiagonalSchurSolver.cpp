/**
* @file    PartialDiagonalSchurSolver.cpp
* @brief   Solves a special linear system
* @author  Richard Roberts
* @created Sept 12, 2012
*/

#include "PartialDiagonalSchurSolver.h"

using namespace gtsam;

namespace basisflow {

  void PartialDiagonalSchurSolver::updateI(int i, double L_IiIi, double eta_Ii, double c) {
    this->LambdaII_.diagonal()(i) += L_IiIi;
    this->eta_I_(i) += eta_Ii;
    this->c_ += c;
  }

  void PartialDiagonalSchurSolver::updatey(const gtsam::Matrix& L_yy, const gtsam::Vector& eta_y, double c) {
    this->Lambdayy_ += L_yy;
    this->eta_y_ += eta_y;
    this->c_ += c;
  }

  void PartialDiagonalSchurSolver::updateIy(int i, double L_IiIi, const gtsam::Vector& L_yIi, const gtsam::Matrix& L_yy,
    double eta_Ii, const gtsam::Vector& eta_y, double c)
  {
    this->updateI(i, L_IiIi, eta_Ii, 0.0);
    this->updatey(L_yy, eta_y, 0.0);
    this->LambdaIy_.row(i) += L_yIi.transpose();
    this->c_ += c;
  }

  double PartialDiagonalSchurSolver::logLikelihood(const gtsam::Vector& I) const {
    // The mean of I in the partitioned system is
    // (L_II - L_Iy*L_yy^-1*L_yI)^-1 * eta_bar
    // which using the matrix inversion lemma is
    // L_II^-1 - L_II^-1*L_Iy*(L_yy^-1 + L_Iy^T*L_II^-1*L_Iy)^-1*L_Iy^T*L_II^-1
    // where eta_bar = eta_I - L_Iy*L_yy^-1*eta_y
    
    lastEtaBar_ = this->eta_I_ - this->LambdaIy_ * this->Lambdayy_.inverse() * this->eta_y_;
    const LambdaIIType LambdaII_inv = this->LambdaII_.inverse();
    //const Vector mean =
    //  LambdaII_inv * eta_bar - 
    //  LambdaII_inv * this->LambdaIy_ * (Lambdayy_ + this->LambdaIy_.transpose()*LambdaII_inv*this->LambdaIy_).inverse() * this->LambdaIy_.transpose() * LambdaII_inv * eta_bar;

    // Compute measurement centered around prediction
    //const Vector centered_I = I - mean;

    // Compute quadratic and linear contributions to error
    lastRightVector_ =
      LambdaII_ * I -
      this->LambdaIy_ * Lambdayy_.inverse() * this->LambdaIy_.transpose() * I;
    const Eigen::Matrix<double,1,1> quadraticTerm = I.transpose() * lastRightVector_;
    const Eigen::Matrix<double,1,1> linearTerm = I.transpose() * lastEtaBar_;
    
    // Compute normalizing term
    const double log_determinant_LambdaII =
      log((this->Lambdayy_ + this->LambdaIy_.transpose() * LambdaII_inv * this->LambdaIy_).determinant())
      + -log(Lambdayy_.determinant())
      + this->LambdaII_.diagonal().array().log().sum();
    const double log_normalizer = 0.5 * (-I.size() * log(2.*M_PI) + log_determinant_LambdaII);

    // TODO: Check sign here
    double result = log_normalizer - 0.5*quadraticTerm(0,0) - linearTerm(0,0);

    return result;
  }

}