/**
 * @file    OnlySigmaImageFactor.cpp
 * @brief   Image factor on only the basis
 * @author  Richard Roberts
 * @created Aug 4, 2012
 */

#include "OnlySigmaImageFactor.h"

#include "flowOps.h"
#include "Utils.h"
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/nonlinear/Symbol.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace basisflow {

	inline double sq(double x) { return x*x; }

	/* **************************************************************************************** */
	void OnlySigmaImageFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
		Base::print(s, keyFormatter);
	}

	/* **************************************************************************************** */
	bool OnlySigmaImageFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
		const OnlySigmaImageFactor *t = dynamic_cast<const This*>(&f);
		if(t && Base::equals(f))
			return basisKey_ == t->basisKey_ && latentKey_ == t->latentKey_ && frameNums_ == t->frameNums_;
		else
			return false;
	}

	/* **************************************************************************************** */
	double OnlySigmaImageFactor::error(const Values& x) const {

		const Vector sigmas = x.at<LieVector>(keys_[0]);

		double error = 0.0;
		for(size_t i = 0; i < frameNums_.size(); ++i)
			error += errorOneFrame(frameNums_[i], sigmas);
		return error;
	}

	/* **************************************************************************************** */
	boost::shared_ptr<GaussianFactor> OnlySigmaImageFactor::linearize(const Values& x, const Ordering& ordering) const {

		const Vector sigmas = x.at<LieVector>(sigmaKey());

		// Create Hessian initialized to zeros
		HessianFactor::shared_ptr hessian = boost::make_shared<HessianFactor>(
			JacobianFactor(ordering[sigmaKey()], Matrix(0,4), Vector(), noiseModel::Unit::Create(0)));

		// Helper function to compute numerical derivatives
		struct _ErrorComputer {
			static double Error(const OnlySigmaImageFactor& factor, int frameNum, int sigmaIndex, const Eigen::Vector4d& otherSigmas, const Vector& sigma) {
				Eigen::Vector4d sigmas = otherSigmas;
				sigmas(sigmaIndex) = sigma(0);
				return factor.errorOneFrame(frameNum, sigmas);
			}
		};

		// Compute and update Hessian for each frame
		for(size_t i = 0; i < frameNums_.size(); ++i) {
			// References to values for convenience
			const int frame = frameNums_[i];

			// Compute error and first and second derivatives (ignoring cross-information terms)
			Eigen::Vector4d f;
			Eigen::Vector4d df;
			Eigen::Vector4d dfdf;
			if(false) {
				for(int sigmaIndex = 0; sigmaIndex < 4; ++sigmaIndex) {
					f(sigmaIndex) = errorOneFrame(frame, sigmas);
					df(sigmaIndex) = gtsam::numericalGradient(boost::function<double(const LieVector&)>(
						boost::bind(&_ErrorComputer::Error, *this, frame, sigmaIndex, sigmas, _1)),
						LieVector(1, sigmas(sigmaIndex)))
						(0);
					dfdf(sigmaIndex) = gtsam::numericalHessian(boost::function<double(const LieVector&)>(
						boost::bind(&_ErrorComputer::Error, *this, frame, sigmaIndex, sigmas, _1)),
						LieVector(1, sigmas(sigmaIndex)))
						(0,0);
				}
			} else {
				double error;
				boost::tie(error,df,dfdf) = errorAndDerivsOneFrame(frame, sigmas);
				f.setConstant(error);
			}

			// Update Hessian (note we ignore cross-information terms)
			hessian->info(hessian->begin(), hessian->begin()) += dfdf.asDiagonal();
			hessian->linearTerm(hessian->begin()) += -df;
			hessian->constantTerm() += f.sum();
		}

		return hessian;
	}

	/* **************************************************************************************** */
	double OnlySigmaImageFactor::errorOneFrame(int frameNum, const Eigen::Vector4d& sigmas) const {
		const LieVector& latent = latentValues_->at<LieVector>(latentKey_ + frameNum);

		// Load images from disk (we do this each time this function is called to avoid loading
		// the whole video into memory, i.e. to do out-of-core optimization).
		const FrameCache::LoadedImages& images =
			frameCache_->loadImages(frameNum, sigmas(0), sigmas(1), sigmas(2), sigmas(3));
    const int imWidth = frameCache_->imWidth();
    const int imHeight = frameCache_->imHeight();

		double error = 0.0;

		// Loop over all pixels accumulating the error
		size_t pix = 0;
		for(int j = 0; j < imWidth; ++j) {
			for(int i = 0; i < imHeight; ++i) {
				const LieMatrix& basis = basisValues_->at<LieMatrix>(basisKey() + pix);

				const Eigen::RowVector2d grad = flowOps::grad(images.prevGradX(), images.prevGradY(), j, i);
				const double Dt = images.curFrame()(i,j) - images.prevFrame()(i,j);
				const double imgError = flowOps::imgError(Dt, grad, basis, latent);
				const double imgErrorSq = imgError*imgError;
				const double weightV = images.weightsV()(i,j);
				const double weightF = images.weightsF()(i,j);
				const double Elambda = flowOps::Elambda(inlierPrior_, imgErrorSq, weightV, weightF);
				const double J = flowOps::J(Elambda, weightV, weightF);
				const double Jsq = J*J;

				error += 0.5 * log(2*M_PI*Jsq);
				error += 0.5 * Jsq * imgErrorSq;
			}
		}

		return error;
	}

	/* **************************************************************************************** */
	boost::tuple<double,Eigen::Vector4d,Eigen::Vector4d>
		OnlySigmaImageFactor::errorAndDerivsOneFrame(int frameNum, const Eigen::Vector4d& sigmas) const {
		const LieVector& latent = latentValues_->at<LieVector>(latentKey_ + frameNum);

		// Load images from disk (we do this each time this function is called to avoid loading
		// the whole video into memory, i.e. to do out-of-core optimization).
		const FrameCache::LoadedImages& images =
			frameCache_->loadImages(frameNum, sigmas(0), sigmas(1), sigmas(2), sigmas(3));
    const int imWidth = frameCache_->imWidth();
    const int imHeight = frameCache_->imHeight();

		double error = 0.0;
		Eigen::Vector4d df = Eigen::Vector4d::Zero();
		Eigen::Vector4d dfdf = Eigen::Vector4d::Zero();

		// Loop over all pixels accumulating the error
		size_t pix = 0;
		for(int j = 0; j < imWidth; ++j) {
			for(int i = 0; i < imHeight; ++i) {
				const LieMatrix& basis = basisValues_->at<LieMatrix>(basisKey() + pix);

				const Eigen::RowVector2d grad = flowOps::grad(images.prevGradX(), images.prevGradY(), j, i);
				const double Dt = images.curFrame()(i,j) - images.prevFrame()(i,j);
				const double imgError = flowOps::imgError(Dt, grad, basis, latent);
				const double imgErrorSq = imgError*imgError;
				const double weightV = images.weightsV()(i,j);
				const double weightF = images.weightsF()(i,j);
				const double Elambda = flowOps::Elambda(inlierPrior_, imgErrorSq, weightV, weightF);
				const double J = flowOps::J(Elambda, weightV, weightF);
				const double Jsq = J*J;

				error += 0.5 * log(2*M_PI*Jsq);
				error += 0.5 * Jsq * imgErrorSq;

				df.array() += 1. / J;
				df(0) += -0.5 * Elambda * imgErrorSq * sq(weightV) * grad.squaredNorm();
				df(1) += -0.5 * (1.-Elambda) * imgErrorSq * sq(weightF) * grad.squaredNorm();
				df(2) += -0.5 * Elambda * imgErrorSq * sq(weightV);
				df(3) += -0.5 * (1.-Elambda) * imgErrorSq * sq(weightF);

				dfdf(0) += Elambda * (0.5 * pow(J,-3.) * sq(weightV) + imgErrorSq * pow(weightV,3.) * grad.squaredNorm()) * grad.squaredNorm();
				dfdf(1) += (1.-Elambda) * (0.5 * pow(J,-3.) * sq(weightF) + imgErrorSq * pow(weightF,3.) * grad.squaredNorm()) * grad.squaredNorm();
				dfdf(2) += Elambda * (0.5 * pow(J,-3.) * sq(weightV) + imgErrorSq * pow(weightV,3.));
				dfdf(3) += (1.-Elambda) * (0.5 * pow(J,-3.) * sq(weightF) + imgErrorSq * pow(weightF,3.));
			}
		}

		return boost::make_tuple(error, df, dfdf);
	}
}
