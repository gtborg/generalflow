/*
 * GMPPCALucasKanade.h
 *
 *  Created on: Jul 3, 2010
 *      Author: Richard Roberts
 */
#pragma once

#include <basisflow/dllexport.h>
#include <basisflow/Subspace.h>
#include <basisflow/ImgOps.h>
#include <basisflow/ImagePyramid.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Point2.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/linear/GaussianSequentialSolver.h>
#include <gtsam/linear/JacobianFactor.h>

#include <opencv2/core/core.hpp>
#include <vector>

namespace basisflow {

class NonlinearError;

class generalflow_basisflow_EXPORT GMPPCALucasKanade {
public:
  struct generalflow_basisflow_EXPORT Params {
    unsigned int imageScalePower;
    unsigned int flowSamplePower;
    bool makeColorAvailable;
    double blurSigma;
    size_t nPyrLevels;
    size_t firstPyrLevel;

    enum Initialization { INITIALIZATION, PRIOR, FIXED };
    Initialization initialization;

    Params() {}
    Params(unsigned int _imageScalePower, unsigned int _flowSamplePower, double _blurSigma = 0.5, size_t _nPyrLevels = 1, size_t _firstPyrLevel = 0, Initialization _initialization = INITIALIZATION) :
      imageScalePower(_imageScalePower), flowSamplePower(_flowSamplePower), blurSigma(_blurSigma), nPyrLevels(_nPyrLevels), firstPyrLevel(_firstPyrLevel), initialization(_initialization) {}

  private:
    /** Serialization function */
    friend class boost::serialization::access;
    template<class ARCHIVE>
    void serialize(ARCHIVE & ar, const unsigned int version)
    {
      ar & BOOST_SERIALIZATION_NVP(imageScalePower);
      ar & BOOST_SERIALIZATION_NVP(flowSamplePower);
      ar & BOOST_SERIALIZATION_NVP(blurSigma);
      ar & BOOST_SERIALIZATION_NVP(nPyrLevels);
      ar & BOOST_SERIALIZATION_NVP(firstPyrLevel);
      ar & BOOST_SERIALIZATION_NVP(makeColorAvailable);
      //ar & BOOST_SERIALIZATION_NVP(computeLatent);
    }
  };

  cv::Mat gradimX;
  cv::Mat gradimY;
  cv::Mat diff;

public:
  int frameNum_;
  std::vector<Subspace> subspaces_;
  Params params_;
  ImagePyramid prevFrame_;
  ImagePyramid curFrame_;
  cv::Mat curScaled_;
  boost::shared_ptr<ImgOps> imgOps1_;
  boost::shared_ptr<ImgOps> imgOps_;
  mutable std::vector<gtsam::Vector> latent;
  mutable std::vector<gtsam::Vector> outliers;
//  mutable std::vector<gtsam::Vector> errors;
  mutable std::vector<std::vector<std::pair<size_t,size_t> > > imgXYs_;

  static std::vector<Subspace> subspacePyramid(const Subspace& subspace, size_t imWidth, int nPyrLevels);

public:
  GMPPCALucasKanade() : frameNum_(0) {}

  GMPPCALucasKanade(const Subspace& subspace, const Params& params, size_t imWidth);

  const Subspace& subspace() const { return subspaces_[0]; }

	const Subspace& subspace(size_t level) const { return subspaces_[level]; }

	cv::Mat prevFrame(size_t level) const { return prevFrame_(level); }

	cv::Mat curFrame(size_t level) const { return curFrame_(level); }

  void nextFrame(const cv::Mat& frame, gtsam::Vector& out_latent, gtsam::Vector& out_inliers);
  void setNextFrame(const cv::Mat& frame);
  const cv::Mat& scaled() const { return curScaled_; }

  std::vector<gtsam::Point2> predict(const gtsam::Vector& latent, size_t level = 0) const;

  const Params& params() const { return params_;}

  double marginalLog(const gtsam::Vector& inliers, const gtsam::Vector& latent, int id) const;

//  void learnBasis(const std::vector<cv::Mat>& images, const std::vector<gtsam::Vector>& motion, gtsam::SharedDiagonal motionNoise);

public:
  std::pair<double, gtsam::Point2> errorAndImgGrad(
      const cv::Mat& prevFrameGradX, const cv::Mat& prevFrameGradY,
      const cv::Mat& prevFrame, const cv::Mat& curFrame,
      std::pair<long int,long int> loc, const gtsam::Point2& x) const;
  std::pair<gtsam::Matrix,gtsam::Matrix> gradientMatrices(
      const cv::Mat& prevFrameGradX, const cv::Mat& prevFrameGradY,
      const cv::Mat& prevFrame, const cv::Mat& curFrame,
      const Subspace& basis, const gtsam::Vector& y, const std::vector<std::pair<size_t, size_t> >& imgXYs) const;
  void updateLatent(gtsam::Values& values, const gtsam::FactorGraph<gtsam::JacobianFactor>& graph, const gtsam::Vector& outliers, double& Delta_io, const NonlinearError& errorfn) const;
  void updateOutliers(size_t level, const gtsam::Values& values, const gtsam::FactorGraph<gtsam::JacobianFactor>& inlierGraph,
      const gtsam::FactorGraph<gtsam::JacobianFactor>& outlierGraph, gtsam::Vector& outliers) const;
  void computeFlow(gtsam::Vector& out_latent, gtsam::Vector& out_inliers) const;
  void computeFlowAtLevel(size_t level, double relativeTolerance, boost::optional<gtsam::Vector> prior, gtsam::Vector& latent, gtsam::Vector& inliers) const;

//  double updateBasis(FullValues& values, std::vector<gtsam::Vector>& outliers, const std::vector<cv::Mat>& images, const std::vector<gtsam::Vector>& motion, gtsam::SharedDiagonal motionNoise);
};

typedef GMPPCALucasKanade::Params GMPPCALKParams;

}

