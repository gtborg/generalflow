/**
 * @file    ModelSelection.h
 * @brief   Select among several optical flow templates
 * @author  Richard Roberts
 * @created Sept 14, 2012
 */

#pragma once

#include <vector>

#include <basisflow/dllexport.h>
#include <basisflow/Subspace.h>
#include <basisflow/FrameCache.h>

namespace basisflow {

  typedef std::vector<Subspace> SubspaceVector;

  class generalflow_basisflow_EXPORT ModelSelection {

    SubspaceVector subspaces_;
    boost::shared_ptr<FrameCache> frameCache_;

    mutable gtsam::Matrix lastRightVectors_;
    mutable gtsam::Matrix lastEtaBars_;

  public:
    ModelSelection(const SubspaceVector& subspaces, boost::shared_ptr<FrameCache> frameCache) :
      subspaces_(subspaces), frameCache_(frameCache) {}

    gtsam::Vector evaluateLogLikelihood(int frameNum) const;

    const gtsam::Matrix& lastRightVectors() const { return lastRightVectors_; }
    const gtsam::Matrix& lastEtaBars() const { return lastEtaBars_; }

  };

}
