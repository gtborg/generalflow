/**
 * @file    CostMap.h
 * @brief   A planar cost map
 * @author  Richard Roberts
 * @created January 28, 2013
 */
#pragma once

#include <gtsam/base/Matrix.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <utility>
#include <basisflow/dllexport.h>

namespace basisflow {

  class generalflow_basisflow_EXPORT CostMap {

    gtsam::Matrix mapFree_;
    gtsam::Matrix mapOccupied_;
    double x0_, y0_, xstep_, ystep_;

  public:

    CostMap(double width, double height, double cellWidth) :
      mapFree_(gtsam::Matrix::Zero(std::ceil(height/cellWidth), std::ceil(width/cellWidth))),
      mapOccupied_(gtsam::Matrix::Zero(std::ceil(height/cellWidth), std::ceil(width/cellWidth))),
      x0_(-width/2.0), y0_(-height/2.0), xstep_(cellWidth), ystep_(cellWidth) {}

    std::pair<int,int> mapFromWorld(double x, double y) const {
      return std::make_pair(
        (int)std::floor((x - x0_) / xstep_ + 0.5),
        (int)std::floor((y - y0_) / ystep_ + 0.5));
    }

    std::pair<double,double> worldFromMap(int x, int y) const {
      return std::make_pair(
        double(x) * xstep_ + x0_,
        double(y) * ystep_ + y0_);
    }

    void update(const gtsam::Pose3& pose, const gtsam::Cal3_S2& K, const gtsam::Matrix& imageLabelsFree, const gtsam::Matrix& imageLabelsOccupied,
      int gridWidth, int gridHeight, int imgWidth, int imgHeight);

    const gtsam::Matrix& mapFree() const { return mapFree_; }
    const gtsam::Matrix& mapOccupied() const { return mapOccupied_; }

  };

}
