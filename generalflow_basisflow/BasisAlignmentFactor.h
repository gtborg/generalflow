/**
 * @file    BasisAlignmentFactor.h
 * @brief   Factor for aligning the optical flow subspace basis
 * @author  Richard Roberts
 * @created May 30, 2012
 */

#pragma once

#include <boost/bind.hpp>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/nonlinear/NonlinearFactor.h>

namespace basisflow {

  template<class POSE>
  class BasisAlignmentFactor : public gtsam::NoiseModelFactor3<POSE, POSE, gtsam::LieMatrix> {

  private:
    Vector y_;

  public:

    typedef BasisAlignmentFactor<POSE> This;
    typedef gtsam::NoiseModelFactor3<POSE, POSE, gtsam::LieMatrix> Base;

		BasisAlignmentFactor(gtsam::Key j1, gtsam::Key j2, gtsam::Key jM, const Vector& measuredY, const gtsam::noiseModel::Base::shared_ptr& relativePoseModel) :
			Base(relativePoseModel, j1, j2, jM), y_(measuredY) {} 

		virtual size_t dim() const { return POSE::Dim(); }

    /** Compute the error vector */
		virtual Vector evaluateError(const POSE& x1, const POSE& x2, const gtsam::LieMatrix& M,
			boost::optional<Matrix&> H1 = boost::none,
			boost::optional<Matrix&> H2 = boost::none,
			boost::optional<Matrix&> H3 = boost::none) const {

				// Compute numerical derivatives
				if(H1)
					*H1 = gtsam::numericalDerivative31(boost::function<Vector(const POSE&, const POSE&, const gtsam::LieMatrix&)>(
					boost::bind(&This::evaluateError, this, _1, _2, _3, boost::none, boost::none, boost::none)),
					x1, x2, M);
				if(H2)
					*H2 = gtsam::numericalDerivative32(boost::function<Vector(const POSE&, const POSE&, const gtsam::LieMatrix&)>(
					boost::bind(&This::evaluateError, this, _1, _2, _3, boost::none, boost::none, boost::none)),
					x1, x2, M);
				if(H3)
					*H3 = gtsam::numericalDerivative33(boost::function<Vector(const POSE&, const POSE&, const gtsam::LieMatrix&)>(
					boost::bind(&This::evaluateError, this, _1, _2, _3, boost::none, boost::none, boost::none)),
					x1, x2, M);

				Vector h = POSE::Expmap(M*y_).localCoordinates(x1.between(x2));

				return h;
		}

  };

}
