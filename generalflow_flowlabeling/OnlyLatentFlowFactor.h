/**
* @file    OnlyLatentFlowFactor.h
* @brief   Optical flow factor on only the latent variables
* @author  Richard Roberts
* @created Nov 30, 2012
*/

#pragma once

#include <generalflow_flowlabeling/dllexport.h>
#include <generalflow_core/SparseFlowField.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace generalflow {
  namespace flowlabeling {

    class generalflow_flowlabeling_EXPORT OnlyLatentFlowFactor : public gtsam::NonlinearFactor {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef OnlyLatentFlowFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Key basisKey_;
      gtsam::Key sigmaKey_;
      boost::shared_ptr<const gtsam::Values> basisValues_;
      boost::shared_ptr<const gtsam::Values> sigmaValues_;
      double inlierPrior_;
      SparseFlowField::Measurement measurement_;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      OnlyLatentFlowFactor(gtsam::Key basisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
        boost::shared_ptr<const gtsam::Values> basisValues, boost::shared_ptr<const gtsam::Values> sigmaValues,
        double inlierPrior, const SparseFlowField::Measurement& measurement)
        : Base(gtsam::ListOfOne(latentKey)), basisKey_(basisKey), sigmaKey_(sigmaKey), basisValues_(basisValues), sigmaValues_(sigmaValues),
        inlierPrior_(inlierPrior), measurement_(measurement) {}

      /** Create all factors for a whole frame of optical flow measurements */
      static gtsam::NonlinearFactorGraph CreateWholeFrame(
        const SparseFlowField& measurements, gtsam::Key firstBasisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
        boost::shared_ptr<gtsam::Values> basisValues, boost::shared_ptr<gtsam::Values> sigmaValues, double inlierPrior);

      /** Virtual destructor */
      virtual ~OnlyLatentFlowFactor() {}

      gtsam::Key basisKey() const { return basisKey_; }
      gtsam::Key latentKey() const { return Base::keys_[0]; }
      gtsam::Key sigmaKey() const { return sigmaKey_; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 2; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;
    };

  }
}