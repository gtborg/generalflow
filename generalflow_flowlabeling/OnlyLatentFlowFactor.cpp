/**
 * @file    OnlyBasisFlowFactor.cpp
 * @brief   Optical flow factor on only the latent variables
 * @author  Richard Roberts
 * @created Nov 30, 2012
 */

#include <generalflow_flowlabeling/OnlyLatentFlowFactor.h>
#include <generalflow_core/linearFlow.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace generalflow {
  namespace flowlabeling {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    NonlinearFactorGraph OnlyLatentFlowFactor::CreateWholeFrame(
      const SparseFlowField& measurements, gtsam::Key firstBasisKey, gtsam::Key latentKey, gtsam::Key sigmaKey,
      boost::shared_ptr<Values> basisValues, boost::shared_ptr<Values> sigmaValues, double inlierPrior)
    {
      throw runtime_error("Need to fix basis indexing");
      // Create a factor for each measurement
      NonlinearFactorGraph result;
      for (size_t i = 0; i < measurements.size(); ++i) {
        const Key basisKey = firstBasisKey + i;
        result.push_back(boost::make_shared<OnlyLatentFlowFactor>(basisKey, latentKey, sigmaKey,
          basisValues, sigmaValues, inlierPrior, measurements[i]));
      }
      return result;
    }

    /* **************************************************************************************** */
    void OnlyLatentFlowFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool OnlyLatentFlowFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const OnlyLatentFlowFactor *t = dynamic_cast<const This*>(&f);
      if (t && Base::equals(f))
        return basisKey_ == t->basisKey_ && sigmaKey_ == t->sigmaKey_ && measurement_.equals(t->measurement_, tol);
      else
        return false;
    }

    /* **************************************************************************************** */
    double OnlyLatentFlowFactor::error(const Values& x) const
    {
      // Retrieve values that we use
      const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
      const LieVector& latent = x.at<LieVector>(latentKey());
      const LieMatrix& basis = basisValues_->at<LieMatrix>(basisKey());

      // Error = 1/2 * e^2
      double inlier;
      return 0.5 * linearFlow::fullFlowError(measurement_, inlierPrior_, basis, latent, sigmas(0), sigmas(1), inlier).squaredNorm();
    }

    /* **************************************************************************************** */
    boost::shared_ptr<GaussianFactor> OnlyLatentFlowFactor::linearize(const Values& x) const
    {
      // Retrieve values that we use
      const LieVector& sigmas = sigmaValues_->at<LieVector>(sigmaKey());
      const LieVector& latent = x.at<LieVector>(latentKey());
      const LieMatrix& basis = basisValues_->at<LieMatrix>(basisKey());
      static const noiseModel::Diagonal::shared_ptr unit = noiseModel::Unit::Create(2);

      // b = -e
      // dedy = ddy
      double inlier;
      Matrix ddy;
      const Vector b = -linearFlow::fullFlowError(measurement_, inlierPrior_, basis, latent, sigmas(0), sigmas(1), inlier, boost::none, ddy);
      return boost::make_shared<JacobianFactor>(latentKey(), ddy, b, unit);
    }

  }
}
