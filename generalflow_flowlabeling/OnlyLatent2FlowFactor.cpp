/**
 * @file    OnlyBasis2FlowFactor.cpp
 * @brief   Optical flow factor for 2 basis flow models on only the latent variables
 * @author  Richard Roberts
 * @created Jan 16, 2013
 */

#include <generalflow_flowlabeling/OnlyLatent2FlowFactor.h>
#include <generalflow_core/linearFlow.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace generalflow {
  namespace flowlabeling {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    NonlinearFactorGraph OnlyLatent2FlowFactor::CreateWholeFrame(
      const SparseFlowField& measurements,
      gtsam::Key firstBasis1Key, gtsam::Key firstBasis2Key, gtsam::Key latentKey, gtsam::Key sigmaKey, gtsam::Key firstIndicatorKey,
      boost::shared_ptr<const Values> basisValues, boost::shared_ptr<const Values> sigmaValues, boost::shared_ptr<Values> indicatorValues,
      double comp1Prior, double comp2Prior)
    {
      // Create a factor for each measurement
      NonlinearFactorGraph result;
      int measI = 0;
      BOOST_FOREACH(const SparseFlowField::Measurement& m, measurements) {
        const Key basis1Key = firstBasis1Key + measI;
        const Key basis2Key = firstBasis2Key + measI;
        const Key indicatorKey = firstIndicatorKey + measI;
        result.push_back(boost::make_shared<OnlyLatent2FlowFactor>(OnlyLatent2FlowFactor(basis1Key, basis2Key, latentKey, sigmaKey, indicatorKey,
          basisValues, sigmaValues, indicatorValues, comp1Prior, comp2Prior, m))); // Use copy constructor to avoid 10-argument limit in boost::make_shared
        ++ measI;
      }
      return result;
    }

    /* **************************************************************************************** */
    void OnlyLatent2FlowFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool OnlyLatent2FlowFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const This *t = dynamic_cast<const This*>(&f);
      if (t && Base::equals(f))
        return basis1Key_ == t->basis1Key_ && basis2Key_ == t->basis2Key_ && sigmaKey_ == t->sigmaKey_ && measurement_.equals(t->measurement_, tol);
      else
        return false;
    }

    /* **************************************************************************************** */
    double OnlyLatent2FlowFactor::error(const Values& x) const
    {
      // Retrieve values that we use
      const LieVector& sigmas1 = sigmaValues_->at<LieVector>(sigmaKey());
      const LieVector& sigmas2 = sigmaValues_->at<LieVector>(sigmaKey() + 1);
      const LieVector& sigmasO = sigmaValues_->at<LieVector>(sigmaKey() + 2);
      const LieVector& latent = x.at<LieVector>(latentKey());
      const LieMatrix& basis1 = basisValues_->at<LieMatrix>(basis1Key());
      const LieMatrix& basis2 = basisValues_->at<LieMatrix>(basis2Key());

      // Error = 1/2 * e^2
      Eigen::Vector3d indicators;
      const double e = 0.5 * linearFlow::fullFlowError(measurement_, comp1Prior_, comp2Prior_, basis1, basis2, latent,
        sigmas1(0), sigmas2(0), sigmasO(0), indicators).squaredNorm();
      indicatorValues_->update(indicatorKey(), LieVector(indicators));
      return e;
    }

    /* **************************************************************************************** */
    boost::shared_ptr<GaussianFactor> OnlyLatent2FlowFactor::linearize(const Values& x) const
    {
      // Retrieve values that we use
      const LieVector& sigmas1 = sigmaValues_->at<LieVector>(sigmaKey());
      const LieVector& sigmas2 = sigmaValues_->at<LieVector>(sigmaKey() + 1);
      const LieVector& sigmasO = sigmaValues_->at<LieVector>(sigmaKey() + 2);
      const LieVector& latent = x.at<LieVector>(latentKey());
      const LieMatrix& basis1 = basisValues_->at<LieMatrix>(basis1Key());
      const LieMatrix& basis2 = basisValues_->at<LieMatrix>(basis2Key());
      static const noiseModel::Diagonal::shared_ptr unit = noiseModel::Unit::Create(4);

      // b = -e
      // dedy = ddy
      Matrix ddy;
      Eigen::Vector3d indicators;
      const Vector b = -linearFlow::fullFlowError(measurement_, comp1Prior_, comp2Prior_, basis1, basis2, latent,
        sigmas1(0), sigmas2(0), sigmasO(0), indicators, boost::none, boost::none, ddy);
      indicatorValues_->update(indicatorKey(), LieVector(indicators));

      if (!b.allFinite() || !ddy.allFinite()) {
        throw std::runtime_error("Infinite values in OnlyLatent2FlowFactor::linearize");
      }

      return boost::make_shared<JacobianFactor>(latentKey(), ddy, b, unit);
    }

  }
}
