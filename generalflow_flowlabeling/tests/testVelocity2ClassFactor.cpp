/**
 * @file    testLinearFlow.cpp
 * @brief
 * @author  Richard Roberts
 * @created Feb 11, 2011
 */

#include <CppUnitLite/TestHarness.h>

#include <generalflow_flowlabeling/Velocity2ClassFactor.h>
#include <generalflow_core/geometry.h>
#include <generalflow_core/SparseFlowField.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/linear/HessianFactor.h>

#include <boost/bind.hpp>

using namespace generalflow::flowlabeling;
using namespace generalflow;
using namespace gtsam;

/* ************************************************************************* */
namespace {
  double EvaluateError(const Velocity2ClassFactor& factor, const LieVector& v)
  {
    Values x;
    x.insert(factor.key(), v);
    return factor.error(x);
  }
}

/* ************************************************************************* */
TEST(Velocity2ClassFactor, error_and_derivatives)
{
  // Simulate flow field with a given velocity
  Pose3 camera(Rot3(), Point3(0,0,-3));
  Cal3_S2 calibration(600, 600, 0, 300, 300);
  Vector6 v; v << 0.01, 0.02, 0.0, 0.1, 0.0, 0.2;
  SparseFlowField simulatedFlow = SparseFlowField::FromPairedField(
    geometry::flowFieldGroundPlane(camera, calibration, v, 3, 3, 600, 600), 600, 600);

  // Convert SparseFlowField to vector of pair of points
  std::vector<std::pair<Point2, Point2> > flowPoints;

  BOOST_FOREACH(const SparseFlowField::Measurement& m, simulatedFlow) {
    flowPoints.push_back(std::make_pair(
        Point2(m.x(), m.y()),
        Point2(m.x() + m.ux(), m.y() + m.uy())));
  }

  // Create factor
  Velocity2ClassFactor factor(
    0, camera.rotation(), -camera.translation().z(), calibration, 0.25, 5.0, 0.33, 0.33, flowPoints);

  // Create values at known solution
  Values x;
  x.insert(0, LieVector(v));

  // Linearize factor
  GaussianFactor::shared_ptr linearizedFactor = factor.linearize(x);
  const HessianFactor& hessian = *boost::dynamic_pointer_cast<HessianFactor>(linearizedFactor);

  // Check error and derivatives
  Matrix hessianExpected = numericalHessian<LieVector>(boost::bind(EvaluateError, factor, _1), LieVector(v));
  Vector gradientExpected = numericalGradient<LieVector>(boost::bind(EvaluateError, factor, _1), LieVector(v));
  double errorExpected = 0.0;

  Matrix augmentedHessianExpected(7,7);
  augmentedHessianExpected <<
    hessianExpected, -gradientExpected,
    -gradientExpected.transpose(), errorExpected;

  EXPECT(assert_equal(augmentedHessianExpected, hessian.augmentedInformation()));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

