/**
 * @file    OnlyLatent2FlowFactor.h
 * @brief   Optical flow factor for 2 basis flow models on only the latent variables
 * @author  Richard Roberts
 * @created Jan 16, 2013
 */

#pragma once

#include <generalflow_flowlabeling/dllexport.h>
#include <generalflow_core/SparseFlowField.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace generalflow {
  namespace flowlabeling {

    class generalflow_flowlabeling_EXPORT OnlyLatent2FlowFactor : public gtsam::NonlinearFactor {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef OnlyLatent2FlowFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Key basis1Key_;
      gtsam::Key basis2Key_;
      gtsam::Key sigmaKey_;
      gtsam::Key indicatorKey_;
      boost::shared_ptr<const gtsam::Values> basisValues_;
      boost::shared_ptr<const gtsam::Values> sigmaValues_;
      boost::shared_ptr<gtsam::Values> indicatorValues_;
      double comp1Prior_;
      double comp2Prior_;
      SparseFlowField::Measurement measurement_;

    public:
      /** Constructor
      * @param basis1Key Key for basis 1 (n x q LieMatrix)
      * @param basis2Key Key for basis 2 (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      OnlyLatent2FlowFactor(gtsam::Key basis1Key, gtsam::Key basis2Key, gtsam::Key latentKey, gtsam::Key sigmaKey, gtsam::Key indicatorKey,
        boost::shared_ptr<const gtsam::Values> basisValues, boost::shared_ptr<const gtsam::Values> sigmaValues,
        boost::shared_ptr<gtsam::Values> indicatorValues,
        double comp1Prior, double comp2Prior, const SparseFlowField::Measurement& measurement)
        : Base(gtsam::ListOfOne(latentKey)), basis1Key_(basis1Key), basis2Key_(basis2Key), sigmaKey_(sigmaKey), indicatorKey_(indicatorKey),
        basisValues_(basisValues), sigmaValues_(sigmaValues), indicatorValues_(indicatorValues),
        comp1Prior_(comp1Prior), comp2Prior_(comp2Prior), measurement_(measurement) {}

      /** Create all factors for a whole frame of optical flow measurements */
      static gtsam::NonlinearFactorGraph CreateWholeFrame(
        const SparseFlowField& measurements,
        gtsam::Key firstBasis1Key, gtsam::Key firstBasis2Key, gtsam::Key latentKey, gtsam::Key sigmaKey, gtsam::Key indicatorKey,
        boost::shared_ptr<const gtsam::Values> basisValues, boost::shared_ptr<const gtsam::Values> sigmaValues, boost::shared_ptr<gtsam::Values> indicatorValues,
        double comp1Prior, double comp2Prior);

      /** Virtual destructor */
      virtual ~OnlyLatent2FlowFactor() {}

      gtsam::Key basis1Key() const { return basis1Key_; }
      gtsam::Key basis2Key() const { return basis2Key_; }
      gtsam::Key latentKey() const { return Base::keys_[0]; }
      gtsam::Key sigmaKey() const { return sigmaKey_; }
      gtsam::Key indicatorKey() const { return indicatorKey_; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 2; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;
    };

  }
}
