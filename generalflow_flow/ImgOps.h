/**
* @file    ImgOps.h
* @brief
* @author  Richard Roberts
* @created Jan 12, 2011
*/

#pragma once

#include <generalflow_flow/dllexport.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/optional.hpp>
#include <gtsam/base/FastMap.h>
#include <gtsam/geometry/Point2.h>
#include <vector>
#include <utility>
#include <iostream>

namespace generalflow {

  class generalflow_flow_EXPORT ImgOps {

    double sigma_;
    size_t size_;
    const size_t nCached_;
    mutable gtsam::FastMap<int, std::vector<double> > cachedKernels_;
    mutable gtsam::FastMap<int, cv::Mat> cachedGaussianKernels_;


  public:
    ImgOps(double sigma, size_t size, size_t nCached = 35) : sigma_(sigma), size_(size), nCached_(nCached) {
      //      std::cout << "Kernel size " << size << "x" << size << endl;
    }

    double sigma() const { return sigma_; }
    size_t size() const { return size_; }

    const std::vector<double>& derivKernel(double subpixelOffset) const;
    cv::Mat gaussianKernel(double subpixelOffset) const;
    boost::optional<std::pair<double, gtsam::Point2> > linear(const cv::Mat& img, const cv::Mat& gradImgX, const cv::Mat& gradImgY, const gtsam::Point2 x) const;
    double resample(const cv::Mat& image, const gtsam::Point2& location) const;

    double resampleBilinear(const cv::Mat& image, const gtsam::Point2& location) const;

    static std::pair<cv::Mat, cv::Mat> ComputeDerivatives(const cv::Mat& img);
    static std::pair<gtsam::Matrix, gtsam::Matrix> ComputeDerivatives(const gtsam::Matrix& img);

  };

}

