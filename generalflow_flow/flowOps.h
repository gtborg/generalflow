/**
* @file    flow.h
* @brief   Shared operations with optical flow
* @author  Richard Roberts
* @created August 1, 2012
*/
#pragma once

#include <generalflow_flow/dllexport.h>
#include <generalflow_flow/Subspace.h>
#include <generalflow_flow/ImgOps.h>
#include <generalflow_flow/FrameCache.h>
#include <generalflow_denselabeling/types.h>

#include <opencv2/core/core.hpp>
#include <vector>

#include <gtsam/inference/Key.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>

namespace generalflow {
  namespace flow {

    /* **************************************************************************************** */
    class generalflow_flow_EXPORT ImagePairGrad
    {
    public:
      typedef boost::shared_ptr<ImagePairGrad> shared_ptr;

      bool normalize_;
      gtsam::Matrix prevFrame_;
      gtsam::Matrix curFrameUnnormalized_;
      double curFrameUnnormalizedMean_;
      gtsam::Matrix curFrame_;
      Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic> curFrameColor_;
      gtsam::Matrix prevGradX_;
      gtsam::Matrix prevGradY_;

      const gtsam::Matrix& prevFrame() const { return prevFrame_; }
      const gtsam::Matrix& curFrame() const { return curFrame_; }
      const Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>& curFrameColor() const { return curFrameColor_; }
      const gtsam::Matrix& prevGradX() const { return prevGradX_; }
      const gtsam::Matrix& prevGradY() const { return prevGradY_; }

      ImagePairGrad() {}
      ImagePairGrad(const gtsam::Matrix& prevFrame, const gtsam::Matrix& curFrame, bool normalize = true,
                    boost::optional<const Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>&> curFrameColor = boost::none);
      ImagePairGrad(const ImagePairGrad& lastPair, const gtsam::Matrix& newCurFrame,
                    boost::optional<const Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>&> curFrameColor = boost::none);

      bool equals(const ImagePairGrad& other, double tol = 1e-9) const;
      bool empty() const { return prevFrame_.rows() == 0; }
    };

    /* **************************************************************************************** */
    struct generalflow_flow_EXPORT FullImageErrorVisualization {
      int iteration;
      int framenum;
      int K;
      cv::Mat prevFrame;
      cv::Mat curFrame;
      cv::Mat temporalDeriv;
      cv::Mat errs;
      cv::Mat normalizedErrs;
      cv::Mat invSigmas;
      cv::Mat indicatorEvidence;
      cv::Mat indicatorSmoothness;
      cv::Mat indicators;
      cv::Mat expectedFlow;
      FullImageErrorVisualization(int iteration, int framenum, int imgWidth, int imgHeight, int K) :
        iteration(iteration), framenum(framenum), K(K),
        prevFrame(imgHeight, imgWidth, CV_32F),
        curFrame(imgHeight, imgWidth, CV_32F),
        temporalDeriv(imgHeight, imgWidth, CV_32F),
        errs(K*imgHeight, imgWidth, CV_32F),
        normalizedErrs(K*imgHeight, imgWidth, CV_32F),
        invSigmas(K*imgHeight, imgWidth, CV_32F),
        indicatorEvidence(K*imgHeight, imgWidth, CV_32F),
        indicatorSmoothness(2*imgHeight, imgWidth, CV_32F),
        indicators(K*imgHeight, imgWidth, CV_32F),
        expectedFlow(2*imgHeight, imgWidth, CV_32F)
        {}
      void write(const std::string& prefix = "") const;
    };

    generalflow_flow_EXPORT std::pair<gtsam::Matrix, gtsam::Matrix> warpMaps(const Subspace& subspace, const gtsam::Vector& y);

    generalflow_flow_EXPORT gtsam::Matrix warpWithBasis(const gtsam::Matrix& frame, const Subspace& subspace, const gtsam::Vector& y);

    generalflow_flow_EXPORT gtsam::Matrix pixelWeights(const gtsam::Matrix& gradX, const gtsam::Matrix& gradY, double flowSigma, double pixSigma);

#if 0

    generalflow_flow_EXPORT gtsam::Vector fullImageError(
      const ImagePairGrad& images,
      int pixX, int pixY, int imgWidth, int imgHeight,
      double inlierPrior, const gtsam::LieMatrix& basis, const gtsam::LieVector& latent,
      double flowSigmaV, double flowSigmaF, double pixSigmaV, double pixSigmaF,
      double& inlier,
      boost::optional<gtsam::Matrix&> ddV,
      boost::optional<gtsam::Matrix&> ddy,
      boost::optional<gtsam::Matrix&> ddS0,
      boost::optional<gtsam::Matrix&> ddS1,
      boost::optional<gtsam::Matrix&> ddS2,
      boost::optional<gtsam::Matrix&> ddS3);
    generalflow_flow_EXPORT double fullImageErrorNormalized(
      const ImagePairGrad& images,
      int pixX, int pixY, int imgWidth, int imgHeight,
      double inlierPrior, const gtsam::LieMatrix& basis, const gtsam::LieVector& latent,
      double flowSigmaV, double flowSigmaF, double pixSigmaV, double pixSigmaF,
      double& inlier);

    generalflow_flow_EXPORT Eigen::Vector2d fullImageError(
      const ImagePairGrad& images, int pixX, int pixY,
      const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2, const gtsam::LieVector& latent,
      double comp0Prior, double comp1Prior, double comp2Prior,
      double flowSigmaF, double flowSigmaB1, double flowSigmaB2,
      double pixSigmaF, double pixSigmaB1, double pixSigmaB2,
      Eigen::Vector3d& indicators,
      boost::optional<gtsam::Matrix&> ddV1,
      boost::optional<gtsam::Matrix&> ddV2,
      boost::optional<gtsam::Matrix&> ddy,
      const boost::shared_ptr<FullImageErrorVisualization>& visualization =
          boost::shared_ptr<FullImageErrorVisualization>());

    generalflow_flow_EXPORT Eigen::Vector2d fullImageError(
      const ImagePairGrad& images, int pixX, int pixY,
      const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2, const gtsam::LieVector& latent,
      double flowSigmaF, double flowSigmaB1, double flowSigmaB2,
      double pixSigmaF, double pixSigmaB1, double pixSigmaB2,
      const Eigen::Vector3d& indicators,
      boost::optional<gtsam::Matrix&> ddV1,
      boost::optional<gtsam::Matrix&> ddV2,
      boost::optional<gtsam::Matrix&> ddy,
      const boost::shared_ptr<FullImageErrorVisualization>& visualization =
          boost::shared_ptr<FullImageErrorVisualization>());
#endif

    enum SolveIndicatorsMode { PIXELS, SUPERPIXELS };

    generalflow_flow_EXPORT gtsam::Matrix solveIndicators(
        const ImagePairGrad& images,
        const gtsam::Matrix& basisRot,
        const std::vector<gtsam::Matrix>& basisTrans,
        const gtsam::Vector& velocity,
        const std::vector<gtsam::Matrix>& classPriors,
        const gtsam::Vector& flowSigmas,
        const gtsam::Vector& pixSigmas,
        const gtsam::Matrix& sigmaV,
        SolveIndicatorsMode mode,
        const boost::shared_ptr<FullImageErrorVisualization>& visualization =
            boost::shared_ptr<FullImageErrorVisualization>());

//    generalflow_flow_EXPORT Eigen::Vector2d fullImageErrorImpl(
//      const ImagePairGrad& images,
//      int pixX, int pixY,
//      const Eigen::RowVector2d& grad,
//      Eigen::RowVector3d imgErrors,
//      const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2, const gtsam::LieVector& latent,
//      const Eigen::RowVector3d sigmas,
//      const Eigen::Vector3d& indicators,
//      boost::optional<gtsam::Matrix&> ddV1,
//      boost::optional<gtsam::Matrix&> ddV2,
//      boost::optional<gtsam::Matrix&> ddy,
//      const boost::shared_ptr<FullImageErrorVisualization>& visualization);
      
#if 0
    namespace internal
    {
      template<typename VectorIndicators>
      struct fullImageErrorImplT_RowsAtCompileTimeSelector
      {
        static const gtsam::DenseIndex RowsAtCompileTime =
        VectorIndicators::RowsAtCompileTime == Eigen::Dynamic ? Eigen::Dynamic : VectorIndicators::RowsAtCompileTime - 1;
      };
    }
#endif

    template<typename MatrixRot, typename MatrixTrans,
    typename VectorVelocity, typename VectorSigmas, typename VectorIndicators>
    Eigen::Matrix<double, VectorIndicators::RowsAtCompileTime, 1>
    fullImageErrorImpl(
      const ImagePairGrad& images, int pixX, int pixY,
      const MatrixRot& basisRot,
      const std::vector<MatrixTrans>& basisTrans,
      const VectorVelocity& velocity,
      const VectorSigmas& flowSigmas,
      const VectorSigmas& pixSigmas,
      const VectorIndicators& indicators,
      boost::optional<gtsam::Matrix&> ddWRot,
      boost::optional<std::vector<gtsam::Matrix>&> ddWk,
      boost::optional<gtsam::Matrix&> ddy,
      const boost::shared_ptr<FullImageErrorVisualization>& visualization =
          boost::shared_ptr<FullImageErrorVisualization>());

    inline Eigen::RowVector2d grad(const gtsam::Matrix& gradX, const gtsam::Matrix& gradY, int pixX, int pixY) {
      return (Eigen::RowVector2d() << gradX(pixY, pixX), gradY(pixY, pixX)).finished();
    }
    inline double imgError(double Dt, const Eigen::RowVector2d& grad, const gtsam::Matrix& basis, const gtsam::Vector& v) {
      return Dt + (grad * basis * v)(0, 0);
    }
    inline double Elambda(double inlierPrior, double imgErrorSq, double weightV, double weightF) {
      const double pIn = sqrt(weightV) * exp(-0.5 * weightV * imgErrorSq) * inlierPrior;
      const double pOut = sqrt(weightF) * exp(-0.5 * weightF * imgErrorSq) * (1. - inlierPrior);
      return
        pIn + pOut > 1e-10 ?
        pIn / (pIn + pOut) :
        inlierPrior;
    }
    inline double J(double Elambda, double weightV, double weightF) {
      return sqrt(Elambda * weightV + (1. - Elambda) * weightF);
    }
  }

  generalflow_flow_EXPORT gtsam::Key vKey(unsigned char chr, size_t imgSet, size_t frameNum);
  generalflow_flow_EXPORT unsigned char vKeyChr(gtsam::Key key);
  generalflow_flow_EXPORT size_t vKeyFrameNum(gtsam::Key key);
  generalflow_flow_EXPORT size_t vKeyImgSet(gtsam::Key key);

}

#include <generalflow_flow/flowOps-inl.h>
