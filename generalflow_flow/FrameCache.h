/**
* @file    FrameCache.h
* @brief   Caches a loaded frame and relevant computations for doing out-of-core optimization over many frames.
* @author  Richard Roberts
* @created Aug 6, 2012
*/
#pragma once

#include <gtsam/base/Matrix.h>
#include <generalflow_flow/dllexport.h>
#include <generalflow_flow/Subspace.h>

#include <opencv2/core/core.hpp>

namespace generalflow {

  class generalflow_flow_EXPORT FrameCache {

  public:
    class LoadedImages {
    public:
      gtsam::Matrix prevFrame_;
      gtsam::Matrix curFrame_;
      gtsam::Matrix prevGradX_;
      gtsam::Matrix prevGradY_;
      gtsam::Matrix weightsV_;
      gtsam::Matrix weightsF_;
      const gtsam::Matrix& prevFrame() const { return prevFrame_; }
      const gtsam::Matrix& curFrame() const { return curFrame_; }
      const gtsam::Matrix& prevGradX() const { return prevGradX_; }
      const gtsam::Matrix& prevGradY() const { return prevGradY_; }
      const gtsam::Matrix& weightsV() const { return weightsV_; }
      const gtsam::Matrix& weightsF() const { return weightsF_; }
      friend class FrameCache;
    };

  protected:
    std::string filenameFormat_;
    int pixelSkip_;
    int imWidth_;
    int imHeight_;
    int maxCacheBytes_;
    LoadedImages loadedImages_;

  public:
    FrameCache(const std::string& filenameFormat, int pixelSkip, int imWidth, int imHeight, int maxCacheBytes) :
      filenameFormat_(filenameFormat), pixelSkip_(pixelSkip), imWidth_(imWidth), imHeight_(imHeight), maxCacheBytes_(maxCacheBytes) {}
    virtual ~FrameCache() {}
    virtual const LoadedImages& loadImages(int curFrameNum,
      double flowSigmaV, double flowSigmaF, double pixSigmaV, double pixSigmaF);
    int imWidth() const { return imWidth_; }
    int imHeight() const { return imHeight_; }

  protected:
    struct CachedImages {
      cv::Mat prevFrame;
      cv::Mat curFrame;
      gtsam::Matrix prevGradX;
      gtsam::Matrix prevGradY;
      CachedImages() {}
      CachedImages(const cv::Mat& _prevFrame, const cv::Mat& _curFrame, const gtsam::Matrix& _prevGradX, const gtsam::Matrix& _prevGradY) :
        prevFrame(_prevFrame), curFrame(_curFrame), prevGradX(_prevGradX), prevGradY(_prevGradY) {}
    };

    typedef std::map<int, CachedImages> Cache_;
    Cache_ cache_;

    virtual std::pair<cv::Mat, cv::Mat> readImages(int curFrameNum) const;
    virtual std::pair<gtsam::Matrix, gtsam::Matrix> gradientImages(const gtsam::Matrix& prevFrame) const;
  };

  typedef FrameCache::LoadedImages FrameCacheLoadedImages;

}
