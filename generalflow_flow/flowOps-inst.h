/**
* @file    flowOps.cpp
* @brief   Shared operations with optical flow
* @author  Richard Roberts
* @created August 1, 2012
*/

#include <gtsam/base/Vector.h>
#include <gtsam/base/Matrix.h>

#include <vector>
#include <utility>

namespace generalflow {
namespace flow {

static Eigen::RowVectorXd calculatePixelMarginalSigma(
    const gtsam::Matrix& basisRot, const std::vector<gtsam::Matrix>& basesTrans,
    const gtsam::DenseIndex pix,
    const std::pair<gtsam::Vector, gtsam::Vector>& flow_pixSigmasSquared,
    const gtsam::Matrix& sigmaV,
    const Eigen::RowVector2d& grad)
{
  using namespace Eigen;
  using gtsam::DenseIndex;

  const DenseIndex kappa = basesTrans.size() + 1;
  const DenseIndex r = basisRot.cols();

  // See "marginalize velocity.lyx"
  // It = Ixy*W*v + eps(Ixy*W*SigmaV*WT*IxyT) + eps(Ixy*SigmaU*IxyT) + eps(SigmaI)

  // All variances have the contribution from the last two terms
  VectorXd variances =
      (grad.squaredNorm() * flow_pixSigmasSquared.first.array())
      + (flow_pixSigmasSquared.second).array();

  // Add contribution from components that include translation
  for(DenseIndex k = 1; k < kappa; ++k)
  {
    MatrixXd combinedBasis(2, r + basesTrans[k-1].cols());
    combinedBasis << basisRot.middleRows(2*pix, 2), basesTrans[k-1].middleRows(2*pix, 2);
    variances(k) += (grad * combinedBasis) * (sigmaV * combinedBasis.transpose() * grad.transpose());
  }

  // The last component only includes rotation
  variances(kappa) += grad * basisRot.middleRows(2*pix, 2) * sigmaV.topLeftCorner(r, r)
        * basisRot.middleRows(2*pix, 2).transpose() * grad.transpose();

  const RowVectorXd sigmas = variances.cwiseSqrt();

  return sigmas;
}

}
}

