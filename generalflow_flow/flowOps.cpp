/**
* @file    flowOps.cpp
* @brief   Shared operations with optical flow
* @author  Richard Roberts
* @created August 1, 2012
*/

#include <generalflow_core/linearFlow.h>
#include <generalflow_flow/flowOps.h>
#include <generalflow_flow/LoopyBelief-inst.h>
#include <generalflow_flow/Utils.h>

#include <tbb/tbb.h>
#include <fstream>

#include <gtsam/symbolic/SymbolicFactorGraph.h>
#include <gtsam/symbolic/SymbolicBayesTree.h>

#include <generalflow_flow/flowOps-inst.h>

//#include <dai/factorgraph.h>
//#include <dai/factor.h>
//#include <dai/bp.h>
//#include <dai/jtree.h>
//
//#include <grante/FactorType.h>
//#include <grante/FactorGraphModel.h>
//#include <grante/FactorGraph.h>
//#include <grante/Factor.h>
//#include <grante/BeliefPropagation.h>

#include <3rdparty/MRF2.2/MaxProdBP.h>
#include <3rdparty/MRF2.2/TRW-S.h>
#include <3rdparty/SLIC/SLICSuperpixels/SLIC.h>

using namespace std;
using namespace gtsam;

using Eigen::Vector2d;
using Eigen::Vector3d;
using Eigen::RowVector2d;
using Eigen::RowVector3d;
using Eigen::RowVectorXd;
using Eigen::Map;
using Eigen::Stride;
using Eigen::Dynamic;
using Eigen::RowMajor;
using Eigen::ColMajor;

namespace generalflow {
  namespace flow {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    ImagePairGrad::ImagePairGrad(const Matrix& prevFrame, const Matrix& curFrame, bool normalize,
                                 boost::optional<const Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>&> curFrameColor) :
      normalize_(normalize), prevFrame_(prevFrame),
      curFrameColor_(curFrameColor ? *curFrameColor : Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>())
    {
      boost::tie(prevGradX_, prevGradY_) = ImgOps::ComputeDerivatives(prevFrame_);
      if(normalize_)
      {
        curFrameUnnormalized_ = curFrame;
        curFrameUnnormalizedMean_ = curFrameUnnormalized_.array().mean();
        curFrame_ = curFrameUnnormalized_ * (prevFrame_.array().mean() / curFrameUnnormalizedMean_);
      }
      else
      {
        curFrame_ = curFrame;
      }
    }

    /* **************************************************************************************** */
    ImagePairGrad::ImagePairGrad(const ImagePairGrad& lastPair, const Matrix& newCurFrame,
                                 boost::optional<const Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>&> curFrameColor):
    curFrameColor_(curFrameColor ? *curFrameColor : Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic>())
    {
      normalize_ = lastPair.normalize_;
      if(normalize_)
      {
        prevFrame_ = lastPair.curFrameUnnormalized_;
        curFrameUnnormalized_ = newCurFrame;
        curFrameUnnormalizedMean_ = curFrameUnnormalized_.array().mean();
        curFrame_ = curFrameUnnormalized_ * (lastPair.curFrameUnnormalizedMean_ / curFrameUnnormalizedMean_);
      }
      else
      {
        prevFrame_ = lastPair.curFrame_;
        curFrame_ = newCurFrame;
      }
      boost::tie(prevGradX_, prevGradY_) = ImgOps::ComputeDerivatives(prevFrame_);
    }

    /* ************************************************************************* */
    bool ImagePairGrad::equals(const ImagePairGrad& other, double tol) const
    {
      return equal_with_abs_tol(prevFrame_, other.prevFrame_, tol) &&
        equal_with_abs_tol(curFrame_, other.curFrame_, tol) &&
        equal_with_abs_tol(prevGradX_, other.prevGradX_, tol) &&
        equal_with_abs_tol(prevGradY_, other.prevGradY_, tol);
    }

    /* **************************************************************************************** */
    void FullImageErrorVisualization::write(const std::string& prefix) const
    {
      ofstream img1file((boost::format("%simg1_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());
      ofstream img2file((boost::format("%simg2_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());
      ofstream dtfile((boost::format("%sDt_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());
      ofstream errfile((boost::format("%serrors_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());
      ofstream indfile((boost::format("%sindicators_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());
      ofstream flowfile((boost::format("%sflow_%04d_%06d.txt") % prefix % iteration % framenum).str().c_str());

      img1file << prevFrame << endl;
      img2file << curFrame << endl;
      dtfile << temporalDeriv << endl;
      errfile << errs << endl;
      indfile << indicators << endl;
      flowfile << expectedFlow << endl;
    }

    /* ************************************************************************* */
    pair<Matrix, Matrix> warpMaps(const Subspace& subspace, const Vector& y) {

      size_t rows = subspace.height(), cols = subspace.width();

      // TODO: row/col major basis flow
      // Get pixel offsets into a 32-bit 2-channel OpenCV matrix
      Vector u = subspace.basis() * y; // Optical flow (paired x,y)
      Matrix ux(rows, cols);
      Matrix uy(rows, cols);
      // Compute map locations
      for (size_t i = 0; i < rows; ++i)
        for (size_t j = 0; j < cols; ++j) {
          ux(i, j) = double(j) + u(j * 2 * rows + 2 * i);
          uy(i, j) = double(i) + u(j * 2 * rows + 2 * i + 1);
        }

        return make_pair(ux, uy);
    }

    /* **************************************************************************************** */
    Matrix warpWithBasis(const Matrix& frame, const Subspace& subspace, const Vector& y) {

      if (frame.rows() != subspace.height() || frame.cols() != subspace.width())
        throw std::invalid_argument("Subspace dimensions do not match image size");

      pair<Matrix, Matrix> ux_uy = warpMaps(subspace, y);
      cv::Mat u_mat(ux_uy.first.cols(), ux_uy.first.rows(), CV_64FC2); // Transposed

      // TODO: row/col major basis flow
      // Get pixel offsets into a 32-bit 2-channel OpenCV matrix
      // Compute map locations
      for (size_t i = 0; i < frame.rows(); ++i)
        for (size_t j = 0; j < frame.cols(); ++j) {
          u_mat.at<pair<double, double> >(j, i).second = ux_uy.first(i, j);
          u_mat.at<pair<double, double> >(j, i).first = ux_uy.second(i, j); // Transposed
        }
        cv::Mat map;
        u_mat.convertTo(map, CV_32FC2);

        // Resample
        cv::Mat cvFrame(frame.cols(), frame.rows(), CV_64F, const_cast<double*>(&frame(0, 0))); // Transposed
        Matrix result(frame.rows(), frame.cols());
        cv::Mat cvResult(frame.cols(), frame.rows(), CV_64F, const_cast<double*>(&result(0, 0))); // Transposed
        cv::remap(cvFrame, cvResult, map, cv::Mat(), cv::INTER_CUBIC, cv::BORDER_CONSTANT, -1.0);

        return result;
    }

    /* **************************************************************************************** */
    Matrix pixelWeights(const Matrix& gradX, const Matrix& gradY, double flowSigma, double pixSigma) {
      Map<const Vector> Ix(&gradX(0, 0), gradX.size());
      Map<const Vector> Iy(&gradY(0, 0), gradY.size());
      Matrix weights(gradX.rows(), gradX.cols());
      Map<Vector>(&weights(0, 0), weights.size()) = (flowSigma*flowSigma * (Ix.array().square() + Iy.array().square()) + pixSigma*pixSigma).inverse(); // Note: element-wise inverse here
      return weights;
    }

    /* **************************************************************************************** */
    static void _calculateImageEvidence(
        const tbb::blocked_range<size_t>& range,
        const ImagePairGrad& images,
        const gtsam::Matrix& basisRot,
        const std::vector<gtsam::Matrix>& basesTrans,
        const gtsam::Vector& velocity,
        const std::vector<gtsam::Matrix>& classPriors,
        const std::pair<gtsam::Vector, gtsam::Vector>& flow_pixSigmasSquared,
        const Matrix& sigmaV,
        Eigen::Matrix<float, Dynamic, Dynamic>& imageEvidence)
    {
      const DenseIndex hei = images.prevFrame().rows();
      const DenseIndex r = basisRot.cols();
      const DenseIndex q = velocity.size() - r;
      const DenseIndex kappa = basesTrans.size() + 1;

      assert(kappa + 1 == classPriors.size()
        && kappa + 1 == flow_pixSigmasSquared.first.size()
        && kappa + 1 == flow_pixSigmasSquared.second.size());

      DenseIndex pixX = range.begin() / hei;
      DenseIndex pixY = range.begin() % hei;
      for(size_t pix = range.begin(); pix != range.end(); ++pix)
      {
        const RowVector2d grad = flow::grad(images.prevGradX(), images.prevGradY(), pixX, pixY);
        const double Dt = images.curFrame()(pixY, pixX) - images.prevFrame()(pixY, pixX);
        RowVectorXd imgErrors(kappa + 1);
        auto omega = velocity.head(r);
        auto v = velocity.tail(q);
        const double IRot = (grad * basisRot.middleRows(2*pix, 2) * omega)(0, 0); // Pre-compute rotation contribution
        imgErrors(0) = Dt;
        for(DenseIndex k = 1; k < kappa; ++k) {
          imgErrors(k) = Dt + IRot + (grad * basesTrans[k-1].middleRows(2*pix, 2) * v)(0, 0);
          // Infinite values in basis cause infinite values here and indicate undefined flow, so set
          // residual to zero.
          if(!isfinite(imgErrors(k-1)))
            imgErrors(k) = 0.0;
        }
        imgErrors(kappa) = Dt + IRot; // Infinity model

        RowVectorXd priors(kappa + 1);
        for(DenseIndex k = 0; k <= kappa; ++k)
          priors(k) = classPriors[k](pixY, pixX);

        const RowVectorXd sigmas = calculatePixelMarginalSigma(
            basisRot, basesTrans, pix, flow_pixSigmasSquared, sigmaV, grad);

        imageEvidence.col(pix) = linearFlow::indicatorProbabilities<1, Eigen::Dynamic>(
            imgErrors, priors, sigmas.cwiseInverse()).cast<float>();

        ++ pixY;
        if(pixY == hei)
        {
          pixY = 0;
          ++ pixX;
        }
      }
    }

    /* **************************************************************************************** */
    static void _calculateImageEvidenceLogContributions(
        const tbb::blocked_range<size_t>& range,
        const ImagePairGrad& images,
        const gtsam::Matrix& basisRot,
        const std::vector<gtsam::Matrix>& basesTrans,
        const gtsam::Vector& velocity,
        const std::vector<gtsam::Matrix>& classPriors,
        const std::pair<gtsam::Vector, gtsam::Vector>& flow_pixSigmasSquared,
        const Matrix& sigmaV,
        Eigen::Matrix<float, Dynamic, Dynamic>& imageEvidence)
    {
      const DenseIndex hei = images.prevFrame().rows();
      const DenseIndex r = basisRot.cols();
      const DenseIndex q = velocity.size() - r;
      const DenseIndex kappa = basesTrans.size() + 1;

      assert(kappa + 1 == classPriors.size()
        && kappa + 1 == flow_pixSigmasSquared.first.size()
        && kappa + 1 == flow_pixSigmasSquared.second.size());

      DenseIndex pixX = range.begin() / hei;
      DenseIndex pixY = range.begin() % hei;
      for(size_t pix = range.begin(); pix != range.end(); ++pix)
      {
        const RowVector2d grad = flow::grad(images.prevGradX(), images.prevGradY(), pixX, pixY);
        const double Dt = images.curFrame()(pixY, pixX) - images.prevFrame()(pixY, pixX);
        RowVectorXd imgErrors(kappa + 1);
        auto omega = velocity.head(r);
        auto v = velocity.tail(q);
        const double IRot = (grad * basisRot.middleRows(2*pix, 2) * omega)(0, 0); // Pre-compute rotation contribution
        imgErrors(0) = Dt;
        for(DenseIndex k = 1; k < kappa; ++k) {
          imgErrors(k) = Dt + IRot + (grad * basesTrans[k-1].middleRows(2*pix, 2) * v)(0, 0);
          // Infinite values in basis cause infinite values here and indicate undefined flow, so set
          // residual to zero.
          if(!isfinite(imgErrors(k-1)))
            imgErrors(k) = 0.0;
        }
        imgErrors(kappa) = Dt + IRot; // Infinity model

        RowVectorXd priors(kappa + 1);
        for(DenseIndex k = 0; k <= kappa; ++k)
          priors(k) = classPriors[k](pixY, pixX);

        const RowVectorXd sigmas = calculatePixelMarginalSigma(
            basisRot, basesTrans, pix, flow_pixSigmasSquared, sigmaV, grad);
        const Eigen::ArrayXf invSigmas = sigmas.transpose().array().cast<float>().inverse();

//        cout << "normalizer: " << -sigmas.array().log() << endl;
//        cout << "error: " << -0.5f * (imgErrors.transpose().array().cast<float>() * invSigmas).square().transpose() << endl;
//        cout << "prior: " << priors.array().cast<float>().log() << endl;

        // Calculate log of evidence contribution
        imageEvidence.col(pix).array() =
            -sigmas.transpose().array().cast<float>().log() // Normalizer
            + -0.5f * (imgErrors.transpose().array().cast<float>() * invSigmas).square() // Error term
            + priors.transpose().array().cast<float>().log(); // Prior

        ++ pixY;
        if(pixY == hei)
        {
          pixY = 0;
          ++ pixX;
        }
      }
    }

    /* **************************************************************************************** */
    generalflow_flow_EXPORT gtsam::Matrix solveIndicators(
      const ImagePairGrad& images,
      const gtsam::Matrix& basisRot,
      const std::vector<gtsam::Matrix>& basisTrans,
      const gtsam::Vector& velocity,
      const std::vector<gtsam::Matrix>& classPriors,
      const gtsam::Vector& flowSigmas,
      const gtsam::Vector& pixSigmas,
      const Matrix& sigmaV,
      SolveIndicatorsMode mode,
      const boost::shared_ptr<FullImageErrorVisualization>& visualization)
    {
      // Constants
      const DenseIndex wid = images.prevFrame().cols();
      const DenseIndex hei = images.prevFrame().rows();
      const DenseIndex K = basisTrans.size() + 2;

      // Calculate image evidence
      const gtsam::Vector flowSigmasSquared =
          flowSigmas.array().square();
      const gtsam::Vector pixSigmasSquared =
          pixSigmas.array().square();

      if(mode == PIXELS)
      {
        Eigen::Matrix<float, Dynamic, Dynamic> indicatorEvidence(K, wid*hei);
        const std::pair<gtsam::Vector, gtsam::Vector> flow_pixSigmasSquared(flowSigmasSquared, pixSigmasSquared);
        tbb::parallel_for(tbb::blocked_range<size_t>(0, wid*hei), boost::bind(&_calculateImageEvidenceLogContributions,
            _1, boost::cref(images), boost::cref(basisRot), boost::cref(basisTrans),
            boost::cref(velocity),
            boost::cref(classPriors), boost::cref(flow_pixSigmasSquared),
            boost::cref(sigmaV), boost::ref(indicatorEvidence)));

        if(visualization)
        {
          DenseIndex pix = 0;
          for(DenseIndex x = 0; x < wid; ++x)
            for(DenseIndex y = 0; y < hei; ++y)
              Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >
                  ((float*)visualization->indicatorEvidence.data, K*hei, wid).col(x).segment(K*y, K)
                  = indicatorEvidence.col(pix ++);
        }

        Eigen::MatrixXf indicatorEnergy = -indicatorEvidence;

        Eigen::VectorXf V(K * K);
        Eigen::VectorXf vWeights(hei * wid);
        Eigen::VectorXf hWeights(hei * wid);

        for(DenseIndex k = 0; k < K; ++k)
          for(DenseIndex j = 0; j < K; ++j)
            V(k*K + j) = (k == j ? -log(0.90) : -log(0.10 / double(K-1)));

        for(DenseIndex x = 0; x < wid; ++x)
          for(DenseIndex y = 0; y < hei; ++y)
          {
            // Note swapped h and v because we're giving libMRF a transposed image
            hWeights(x*hei + y) = 1.0 - std::max(1.0, 2.0 * images.prevGradY().cwiseAbs()(y,x));
            vWeights(x*hei + y) = 1.0 - std::max(1.0, 2.0 * images.prevGradX().cwiseAbs()(y,x));

            if(visualization)
              Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >
                  ((float*)visualization->indicatorSmoothness.data, 2*hei, wid).col(x).segment(2*y, 2)
                  = Eigen::Vector2f(vWeights(x*hei + y), hWeights(x*hei + y));
          }

        DataCost dataCost(indicatorEnergy.data());
        //      SmoothnessCost smoothnessCost = SmoothnessCost(MRF::SmoothCostGeneralFn(smoothness));
        SmoothnessCost smoothnessCost(V.data()/*, hWeights.data(), vWeights.data()*/);
        EnergyFunction energy(&dataCost, &smoothnessCost);
        MaxProdBP bp(hei, wid, K, &energy); // Swapped width and height since BP expects row-major data but we use col-major
        bp.initialize();
        bp.clearAnswer();
        float t;
        bp.optimize(5, t);

        // Extract indicator probabilities
        Matrix indicators = Matrix::Constant(K, wid*hei, 0.01);
        for(DenseIndex j = 0; j < wid*hei; ++j)
        {
          indicators(bp.getLabel(j), j) = 1.0 - (K-1) * 0.01;
        }

        return indicators;
      }
      else
      {
        Eigen::Matrix<float, Dynamic, Dynamic> indicatorEvidence(K, wid*hei);
        const std::pair<gtsam::Vector, gtsam::Vector> flow_pixSigmasSquared(flowSigmasSquared, pixSigmasSquared);
        tbb::parallel_for(tbb::blocked_range<size_t>(0, wid*hei), boost::bind(&_calculateImageEvidenceLogContributions,
            _1, boost::cref(images), boost::cref(basisRot), boost::cref(basisTrans),
            boost::cref(velocity),
            boost::cref(classPriors), boost::cref(flow_pixSigmasSquared),
            boost::cref(sigmaV), boost::ref(indicatorEvidence)));

        if(visualization)
        {
          DenseIndex pix = 0;
          for(DenseIndex x = 0; x < wid; ++x)
            for(DenseIndex y = 0; y < hei; ++y)
              Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >
                  ((float*)visualization->indicatorEvidence.data, K*hei, wid).col(x).segment(K*y, K)
                  = indicatorEvidence.col(pix ++);
        }

        // Use superpixels
        // Convert to ARBG for SLIC
        if(images.curFrameColor().size() == 0)
          throw runtime_error("Need color image for superpixel mode");
        const size_t npix = images.curFrame().size();
        vector<uint32_t> argb(npix);
        const uint8_t *frameData = images.curFrameColor().data();
        for(size_t i = 0; i < npix; ++i) {
          argb[i] =
            (frameData[0] << 16) |
            (frameData[1] << 8) |
            (frameData[2]);
          frameData += 3;
        }

        // Call SLIC
        int *klabels;
        int numlabels;
        const int superpixelsize = 10 * 10;
        const double compactness = 20.0;
        SLIC slic;
        // Note swapped hei/wid because our images are column-major and SLIC expects row-major
        slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(
          &argb[0], hei, wid, klabels, numlabels, superpixelsize, compactness);

        // Accumulate evidence in each superpixel
        Eigen::MatrixXf superpixelEvidence = Eigen::MatrixXf::Zero(K, numlabels);
        //Eigen::VectorXf superpixelPixelCounts = Eigen::VectorXf::Zero(numlabels);
        for(size_t i = 0; i < npix; ++i)
        {
          superpixelEvidence.col(klabels[i]) += indicatorEvidence.col(i);
        }

        // Exp and normalize
        for(DenseIndex j = 0; j < (DenseIndex)numlabels; ++j)
        {
//          superpixelEvidence.col(j) = superpixelEvidence.col(j).array().exp();
//          for(DenseIndex k = 0; k < K; ++k)
//            if(superpixelEvidence(k, j) < 0.01)
//              superpixelEvidence(k, j) = 0.01; // Bump up near-zero probabilities
          const float logSum = Utils::logSum(superpixelEvidence.col(j));
          superpixelEvidence.col(j) -= Eigen::VectorXf::Constant(K, logSum); //superpixelPixelCounts(j);
          superpixelEvidence.col(j) = superpixelEvidence.col(j).array().exp();
        }

        //cout << superpixelEvidence.transpose() << endl;

        // Fill output
        Matrix indicators(K, wid*hei);
        for(size_t i = 0; i < npix; ++i)
          indicators.col(i) = superpixelEvidence.col(klabels[i]).cast<double>();

        delete[] klabels;

        return indicators;
      }

#if 0
      const vector<unsigned int> cardinalityOne(1, K);
      const vector<unsigned int> cardinalityTwo(2, K);
      const vector<unsigned int> cardinalityAll(wid*hei, K);

      Grante::FactorGraphModel model;
      Grante::FactorType* smoothnessFactor = new Grante::FactorType("smoothness", cardinalityTwo, probabilityTableSmoothness);
      model.AddFactorType(smoothnessFactor);
      Grante::FactorType* evidenceFactor = new Grante::FactorType("evidence", cardinalityOne, vector<double>());
      model.AddFactorType(evidenceFactor);

      Grante::FactorGraph graph(&model, cardinalityAll);
      DenseIndex pix = 0;
      for(DenseIndex j = 0; j < wid; ++j)
      {
        for(DenseIndex i = 0; i < hei; ++i)
        {
          // Create probability table and unary factor
          vector<double> probabilityTable(K);
          Eigen::Map<Vector>(&probabilityTable[0], K, 1) = indicatorEvidence.col(pix);
          const size_t idx = pix;
          const size_t idxR = pix + hei;
          const size_t idxD = pix + 1;

          graph.AddFactor(new Grante::Factor(evidenceFactor, vector<unsigned int>(1, idx), probabilityTable));

          // Create smoothness factors
          if(i != hei - 1)
          {
            // Neighbor below
            vector<unsigned int> vars(2);
            vars[0] = idx;
            vars[1] = idxD;
            graph.AddFactor(new Grante::Factor(smoothnessFactor, vars, vector<double>()));
          }
          if(j != wid - 1)
          {
            // Neighbor to the right
            vector<unsigned int> vars(2);
            vars[0] = idx;
            vars[1] = idxR;
            graph.AddFactor(new Grante::Factor(smoothnessFactor, vars, vector<double>()));
          }

          ++ pix;
        }
      }

      // Do belief propagation
      Grante::BeliefPropagation bp(&graph, Grante::BeliefPropagation::ParallelSync);
      bp.SetParameters(false, 2, 0.0);
      bp.PerformInference();

      // Extract indicator probabilities
      Matrix indicators(K, wid*hei);
      for(size_t j = 0; j < wid*hei; ++j)
      {
        const std::vector<double>& beliefs = bp.beliefs()[j];
        indicators.col(j) = Eigen::Map<const Vector>(&beliefs[0], 3);
      }

      //cout << indicators.transpose() << endl;
#endif

#if 0
      // Get ordering
      cout << "Getting ordering" << endl;
      SymbolicFactorGraph symb;
      symb.reserve(wid*hei*3);
      DenseIndex pix = 0;
      for(DenseIndex j = 0; j < wid; ++j)
      {
        for(DenseIndex i = 0; i < hei; ++i)
        {
          symb += SymbolicFactor(pix);

          // Create smoothness factors
          if(i != hei - 1)
            // Neighbor below
            symb += SymbolicFactor(pix+1, pix);
          if(j != wid - 1)
            symb += SymbolicFactor(pix+hei, pix);

          ++ pix;
        }
      }
      Ordering ordering = Ordering::COLAMD(symb);
      FastMap<Key, size_t> orderMap;
      for(size_t idx = 0; idx < ordering.size(); ++idx)
        orderMap[ordering[idx]] = idx;
#endif

#if 0
      //cout << "Building DAI graph" << endl;
      vector<dai::Factor> factors;
      factors.reserve(wid*hei*3);
      DenseIndex pix = 0;
      for(DenseIndex j = 0; j < wid; ++j)
      {
        for(DenseIndex i = 0; i < hei; ++i)
        {
          // Create probability table and unary factor
          vector<double> probabilityTable(K);
          Eigen::Map<Vector>(&probabilityTable[0], K, 1) = indicatorEvidence.col(pix);

//          const size_t idx = orderMap[pix];
//          const size_t idxR = orderMap[pix+hei];
//          const size_t idxD = orderMap[pix+1];
          const size_t idx = pix;
          const size_t idxR = pix + hei;
          const size_t idxD = pix + 1;

          factors.push_back(dai::Factor(
              dai::VarSet(dai::Var(idx, K)),
              probabilityTable));

          // Create smoothness factors
          if(i != hei - 1)
            // Neighbor below
            factors.push_back(dai::Factor(
                dai::VarSet(dai::Var(idx, K), dai::Var(idxD, K)),
                probabilityTableSmoothness));
          if(j != wid - 1)
            // Neighbor to the right
            factors.push_back(dai::Factor(
                dai::VarSet(dai::Var(idx, K), dai::Var(idxR, K)),
                probabilityTableSmoothness));

          ++ pix;
        }
      }

      //cout << "Constructing DAI graph" << endl;
      dai::FactorGraph graph(factors);

      //cout << "Running inference" << endl;
      dai::PropertySet props;
      props.set("maxiter", size_t(1));
      props.set("tol", 1e-9);
      props.set("verbose", size_t(0));

      props.set("updates", string("SEQRND"));
      props.set("logdomain", false);
      dai::BP bp(graph, props);
      bp.init();
      bp.run();

//      props.set("updates", string("HUGIN"));
//      props.set("inference", string("SUMPROD"));
//      //dai::boundTreewidth(graph, &dai::eliminationCost_MinFill, 1000000);
//      dai::JTree jt(graph, props);
//      jt.init();
//      jt.run();

      // Extract indicator probabilities
      //cout << "Extracting results" << endl;
      Matrix indicators(K, wid*hei);
      //vector<size_t> maxes = jt.findMaximum();
      for(size_t j = 0; j < wid*hei; ++j)
      {
        dai::Factor belief = bp.belief(dai::Var(j, K));
        for(DenseIndex k = 0; k < K; ++k)
          indicators(k, j) = /*(maxes[j] == k ? 1.0 : 0.0);*/ belief[k];
      }

      //cout << indicators.transpose() << endl;
#endif

#if 0
      // Build graph
      DiscreteFactorGraph graph;
      map<Key, DiscreteKey> allKeys;

      ofstream gf("graph.txt");
      gf << "graph {\n";

      DenseIndex pix = 0;
      for(DenseIndex j = 0; j < wid; ++j)
      {
        for(DenseIndex i = 0; i < hei; ++i)
        {
          // Create probability table and unary factor
          vector<double> probabilityTable(K);
          Eigen::Map<Vector>(&probabilityTable[0], K, 1) = indicatorEvidence.col(pix);
          graph += DecisionTreeFactor(DiscreteKey(pix, K), probabilityTable);

          // Create smoothness factors
          if(i != hei - 1)
          {
            // Neighbor below
            graph += DecisionTreeFactor(DiscreteKey(pix + 1, K) & DiscreteKey(pix, K), probabilityTableSmoothness);
            symb += SymbolicFactor(pix+1, pix);
            gf << pix+1 << " -- " << pix << ";\n";
          }
          if(j != wid - 1)
          {
            // Neighbor to the right
            graph += DecisionTreeFactor(DiscreteKey(pix + hei, K) & DiscreteKey(pix, K), probabilityTableSmoothness);
            symb += SymbolicFactor(pix+hei, pix);
            gf << pix+hei << " -- " << pix << ";\n";
          }

          // Add to allKeys map (discrete code in progress)
          allKeys[pix] = DiscreteKey(pix, K);

          ++ pix;
        }
      }

      gf << "}\n";
      gf.close();

      SymbolicBayesTree sbt = *symb.eliminateMultifrontal();
      sbt.saveGraph("bt.txt");

      // Solve graph
      LoopyBelief solver(graph, allKeys);
      for(size_t iter = 1; iter < 10; ++iter)
        (void) solver.iterate(allKeys);
      DiscreteFactorGraph beliefs = *solver.iterate(allKeys);
      //beliefs.print();

      // Solve graph with elimination
      Assignment<Key> result = *graph.eliminateSequential()->optimize();
      result.print();

      // Extract indicator probabilities
      Matrix indicators(K, wid*hei);
      for(size_t j = 0; j < beliefs.size(); ++j)
      {
        if(beliefs[j]->keys().size() != 1 && beliefs[j]->keys().front() != j)
          throw runtime_error("Unexpected content in beliefs");
        DecisionTreeFactor belief = beliefs[j]->toDecisionTreeFactor();
        for(DenseIndex k = 0; k < K; ++k)
          indicators(k, j) = belief.choose(j, k)(Assignment<Key>());
      }
#endif
    }
  }

  /* **************************************************************************************** */
  static const size_t chrBits = sizeof(unsigned char) * 8;
  static const size_t imgSetBits = 28;
  static const size_t frameNumBits = 28;
  static const Key maxChr = 0xff;
  static const Key maxImgSet = 0xfffffff;
  static const Key maxFrameNum = 0xfffffff;
  static const Key chrMask = maxChr << frameNumBits << imgSetBits;
  static const Key imgSetMask = maxImgSet << frameNumBits;
  static const Key frameNumMask = maxFrameNum;

  Key vKey(unsigned char chr, size_t imgSet, size_t frameNum) {
    if (imgSet & ~maxImgSet)
      throw std::invalid_argument("basisKey: image set number too large");
    if (frameNum & ~maxFrameNum)
      throw std::invalid_argument("basisKey: frame number too large");
    return (size_t(chr) << frameNumBits << imgSetBits) | (imgSet << frameNumBits) | frameNum;
  }

  unsigned char vKeyChr(Key key) {
    return (unsigned char) ((key & chrMask) >> frameNumBits >> imgSetBits);
  }

  size_t vKeyImgSet(Key key) {
    return (size_t) ((key & imgSetMask) >> frameNumBits);
  }

  size_t vKeyFrameNum(Key key) {
    return (size_t) (key & frameNumMask);
  }
}
