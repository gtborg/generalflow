/*
 * Utils.cpp
 *
 *  Created on: May 5, 2010
 *      Author: richard
 */

#include <stdexcept>
#include <math.h>
#include <boost/math/special_functions/round.hpp>

#include <generalflow_flow/Utils.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace gtsam;
using namespace std;
using boost::math::round;

namespace generalflow {

/* **************************************************************************************** */
cv::Mat Utils::colorize(const cv::Mat& img) {
  if(img.depth() != CV_64F || img.channels() != 1)
    throw invalid_argument("Utils::colorize needs an image of CV_64F with 1 channel");

  cv::Mat result(img.rows, img.cols, CV_64FC3);
  for(int j=0; j<img.cols; ++j)
    for(int i=0; i<img.rows; ++i)
      if(img.at<double>(i,j) < 0.0)
        result.at<cv::Vec<double,3> >(i,j) = cv::Vec<double,3>(0.0, 0.0, -img.at<double>(i,j));
      else
        result.at<cv::Vec<double,3> >(i,j) = cv::Vec<double,3>(0.0, img.at<double>(i,j), 0.0);
  return result;
}

/* **************************************************************************************** */
void Utils::drawFlow(cv::Mat& img, const vector<Point2>& flow, int flowSamplePower, Vector indicators, double scale) {
  int nHorz = img.cols >> flowSamplePower;
  int nVert = img.rows >> flowSamplePower;
  double imscale = double(1 << flowSamplePower);
  double offset = scale / 2.0;
  for(int i=0; i<nVert; i+=1) {
    for(int j=0; j<nHorz; j+=1) {
      double x0 = imscale * (double)j + offset;
      double y0 = imscale * (double)i + offset;
      double x = x0 + scale * flow[i*nHorz + j].x();
      double y = y0 + scale * flow[i*nHorz + j].y();
      double red = (1.0-indicators(i*nHorz + j)) * 255.0;
      double green = indicators(i*nHorz + j) * 255.0;
      cv::line(img, cv::Point((int)round(4.0*x0), (int)round(4.0*y0)),
          cv::Point((int)round(4.0*x), (int)round(4.0*y)), cv::Scalar(0,green,red), 1.25, CV_AA, 2);
    }
  }
}

/* **************************************************************************************** */
Matrix Utils::matrixOfMat(const cv::Mat& mat) {
  if(mat.channels() != 1) throw invalid_argument("Only supports single-channel matrices");
  if(!mat.isContinuous()) throw runtime_error("Do not support non-continuous-data Mats");
  cv::Mat mat64f;
  if(mat.type() == CV_64F)
    mat64f = mat;
  else if(mat.type() == CV_32F)
    mat.convertTo(mat64f, CV_64F);
  else if(mat.type() == CV_16U)
    mat.convertTo(mat64f, CV_64F, 1./65535.);
  else if(mat.type() == CV_8U)
    mat.convertTo(mat64f, CV_64F, 1./255.);
  else
    throw invalid_argument("Mat must be 64F, 32F, 16U, or 8U");
	Eigen::Map<const Eigen::Matrix<double,-1,-1,Eigen::RowMajor> > eigenMat(&mat64f.at<double>(0,0), mat64f.rows, mat64f.cols);
  return eigenMat; // Copies and transposes automatically
}

/* **************************************************************************************** */
cv::Mat Utils::matOfMatrix(const Matrix& matrix) {
  cv::Mat ret(matrix.rows(), matrix.cols(), CV_64FC1);
  Eigen::Map<Eigen::Matrix<double,-1,-1,Eigen::RowMajor> > eigenRet(&ret.at<double>(0,0), matrix.rows(), matrix.cols());
  eigenRet = matrix;
  return ret;
}

/* **************************************************************************************** */
cv::Mat Utils::mat8uOfMatrix(const Matrix& matrix) {
  cv::Mat fpMat = matOfMatrix(matrix);
  cv::Mat ret(matrix.rows(), matrix.cols(), CV_8UC1);
  fpMat.convertTo(ret, CV_8UC1, 255.);
  return ret;
}

/* **************************************************************************************** */
cv::Mat Utils::matOfVector(const Vector& vector, size_t cols) {
  if(vector.size() % cols != 0)
    throw invalid_argument("Number of columns specified in Utils::matOfVector is not evenly divisible into the vector length.");
  size_t rows = vector.size() / cols;
  cv::Mat ret(rows, cols, CV_64FC1);
  Eigen::Map<Eigen::Matrix<double,-1,-1,Eigen::RowMajor> > eigenRet(&ret.at<double>(0,0), rows*cols, 1);
  eigenRet = vector;
  return ret;
}

/* **************************************************************************************** */
cv::Mat Utils::scale(const cv::Mat& img, double factor, int interpolation) {
  cv::Mat result;
  cv::resize(img, result, cv::Size(), factor, factor, interpolation);
  return result;
}

/* **************************************************************************************** */
cv::Mat Utils::gray2rgb(const cv::Mat& img) {
  if(img.type() == CV_64F) {
    cv::Mat img32;
    img.convertTo(img32, CV_32F);
    cv::Mat color(img.rows, img.cols, CV_32FC3);
    cv::cvtColor(img32, color, CV_GRAY2BGR);
    cv::Mat rgb;
    color.convertTo(rgb, CV_64F);
    return rgb;
  } else {
    cv::Mat color(img.rows, img.cols, CV_MAKETYPE(img.depth(), 3));
    cv::cvtColor(img, color, CV_GRAY2BGR);
    return color;
  }
}

/* **************************************************************************************** */
// Utility function to decode timestamps from image files
int Utils::decodedTimestamps(size_t framei, const boost::filesystem::path& imageDir) {
  using namespace boost::filesystem;
  static std::vector<int> times;
  if (times.empty()) {
    for (directory_iterator it(imageDir); it != directory_iterator(); ++it) {
      string imgname = it->path().string();
      size_t slash = imgname.find_last_of('/');
      if (imgname.size() - slash > 9 && imgname.substr(slash + 1, 4).compare(
          "img_") == 0) {
        imgname = imgname.substr(slash + 5, imgname.size() - slash - 9);
        times.push_back(atol(imgname.c_str()));
      }
    }
    std::sort(times.begin(), times.end());
  }
  return times.at(framei);
}

/* **************************************************************************************** */
cv::Mat Utils::grayAndUndistort(const cv::Mat& img, const cv::Mat& camMatrix, const cv::Mat& distCoeffs) {
  cv::Mat grayD;
  cv::cvtColor(img, grayD, CV_RGB2GRAY);

  // Undistort Image
  cv::Mat grayU;
  cv::undistort(grayD, grayU, camMatrix, distCoeffs);
  cv::Mat gray;
  grayU.convertTo(gray, CV_64F, 1.0 / 255.0);

  return gray;
}

/* ************************************************************************* */
cv::Mat Utils::rectify(const cv::Mat& I, const gtsam::Cal3_S2& K, const gtsam::Rot3& c0Rc) {
  cv::Mat I32 = I;

  // openCV warp (remap) doesn't work correctly with 64bit floating-point images. Change to 32bit!
  if (I.type() == CV_64F) {
    I.convertTo(I32, CV_32FC1);
  }

  Matrix H0_c = K.matrix() * c0Rc.matrix() * K.matrix_inverse();
  cv::Mat I0(I.rows, I.cols, I.type());
  cv::Mat cvH = (cv::Mat_<double>(3,3) << H0_c(0,0), H0_c(0,1), H0_c(0,2),
      H0_c(1,0), H0_c(1,1), H0_c(1,2),
      H0_c(2,0), H0_c(2,1), H0_c(2,2));
  warpPerspective(I32, I0, cvH, I0.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0.0));

  if (I.type() == CV_64F) {
    I0.convertTo(I0, CV_64FC1);
  }
  return I0;
}

/* **************************************************************************************** */
cv::Mat Utils::grabImage(int framei, const boost::filesystem::path& imgdir, const std::string& imgFnFormat) {
  cv::Mat image = cv::imread(imgdir.string() + (boost::format(imgFnFormat) % Utils::decodedTimestamps(framei, imgdir)).str());
  return image;
}

}
