/**
* @file    sparseFlow.cpp
* @brief   Functions for computing and manipulating sparse flow
* @author  Richard Roberts
* @created November 23, 2012
 */

#include <boost/foreach.hpp>
#include <limits>

#include <gtsam/base/timing.h>
#include <gtsam/inference/Symbol.h>

// For some reason these OpenCV headers need to be included after the TBB headers included by gtsam
#include <opencv2/video/video.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <generalflow_core/SparseFlowField.h>
#include <generalflow_flow/sparseFlow.h>
#include <generalflow_flow/Utils.h>
#include <3rdparty/SLIC/SLICSuperpixels/SLIC.h>

using namespace std;
using namespace gtsam;

namespace generalflow {

  namespace {

    cv::Mat lastSuperpixelLabels;

    /* **************************************************************************************** */
    cv::Mat farnebackWrapper(cv::Mat prevFrame, cv::Mat curFrame, int pyrLevels, double pyrScale, int winSize, int numIters, int polyN, int polySigma) {
      cv::Mat flow;

      bool haveGPU = false;
#ifdef GENERALFLOW_HAVE_GSLIC
      // If we compiled the GPU version of SLIC (gSLIC).  Also check if GPU is currently working.
      {
        int devices = cv::gpu::getCudaEnabledDeviceCount();
        if(devices > 0)
          haveGPU = true;
      }
      if(haveGPU) {
        cv::Mat flowuv[2];
        auto_ptr<cv::gpu::GpuMat> gpuPrevFrame(new cv::gpu::GpuMat(prevFrame));
        auto_ptr<cv::gpu::GpuMat> gpuCurFrame(new cv::gpu::GpuMat(curFrame));
        auto_ptr<cv::gpu::GpuMat> gpuFlowU(new cv::gpu::GpuMat);
        auto_ptr<cv::gpu::GpuMat> gpuFlowV(new cv::gpu::GpuMat);
        cv::gpu::FarnebackOpticalFlow optflow;
        optflow.numLevels = pyrLevels;
        optflow.pyrScale = pyrScale;
        optflow.winSize = winSize;
        optflow.numIters = numIters;
        optflow.polyN = polyN;
        optflow.polySigma = polySigma;
        optflow(*gpuPrevFrame, *gpuCurFrame, *gpuFlowU, *gpuFlowV);
        gpuFlowU->download(flowuv[0]);
        gpuFlowV->download(flowuv[1]);
        cv::merge(flowuv, 2, flow);
      }
#endif
      if(!haveGPU) {
        // Fall back on CPU version
        cv::calcOpticalFlowFarneback(prevFrame, curFrame, flow, pyrScale, pyrLevels, winSize, numIters, polyN, polySigma, 0);
      }

      return flow;
    }

    /* **************************************************************************************** */
    SparseFlowField sampleDenseFlow(const cv::Mat& flow, int gridWidth, int gridHeight) {
      vector<SparseFlowField::Measurement> measurements; {
        const float gridCellWidth = float(flow.cols) / float(gridWidth);
        const float gridCellHeight = float(flow.rows) / float(gridHeight);
        for(int j=0; j<gridWidth; ++j) {
          for(int i=0; i<gridHeight; ++i) {
            const pair<float,float> x_y = calcPixel(j, i, gridCellWidth, gridCellHeight);
            const pair<int,int> xi_yi = x_y;
            measurements.push_back(SparseFlowField::Measurement(x_y.first, x_y.second,
              flow.at<cv::Vec2f>(xi_yi.second, xi_yi.first)[0], flow.at<cv::Vec2f>(xi_yi.second, xi_yi.first)[1]));
          }
        }
      }
      return SparseFlowField(flow.cols, flow.rows, measurements);
    }

    /* **************************************************************************************** */
    SparseFlowField superpixelAvgDenseFlow(const cv::Mat& flow, cv::Mat& frame, int gridWidth, int gridHeight) {
      gttic_(superpixelAvgDenseFlow);
      //#ifdef GENERALFLOW_HAVE_GSLIC
      //#else
      // Convert to ARBG for SLIC
      if(frame.depth() != CV_8U || frame.channels() != 3 || !frame.isContinuous())
        throw runtime_error("Need continuous 8-bit 3-channel color image");
      const int npix = frame.rows * frame.cols;
      vector<uint32_t> argb(npix);
      uint8_t *frameData = frame.data;
      for(int i = 0; i < npix; ++i) {
        argb[i] =
          (frameData[2] << 16) |
          (frameData[1] << 8) |
          (frameData[0]);
        frameData += 3;
      }

      // Call SLIC
      gttic_(SLIC);
      int *klabels;
      int numlabels;
      const int superpixelsize = (int)(0.5f +
        (float(frame.cols) / float(gridWidth)) * (float(frame.rows) / float(gridHeight)));
      const double compactness = 20.0;
      SLIC slic;
      slic.DoSuperpixelSegmentation_ForGivenSuperpixelSize(
        &argb[0], frame.cols, frame.rows, klabels, numlabels, superpixelsize, compactness);
      gttoc_(SLIC);
      //#endif

      // Find centroid of each segment
      vector<gtsam::Point2> centroids(numlabels);
      vector<Eigen::Vector2f> flowAvg(numlabels, Eigen::Vector2f::Zero());
      vector<int> counts(numlabels);
      cv::Mat labels(frame.rows, frame.cols, CV_32SC1, klabels);
      for(int j = 0; j < frame.cols; ++j)
        for(int i = 0; i < frame.rows; ++i) {
          const int label = labels.at<int>(i,j);
          if(label >= 0) {
            centroids[label] += gtsam::Point2(j,i);
            flowAvg[label] += Eigen::Vector2f(flow.at<cv::Vec2f>(i,j)[0], flow.at<cv::Vec2f>(i,j)[1]);
          }
          ++ counts[label];
        }
        for(int k = 0; k < numlabels; ++k) {
          centroids[k] *= 1.0 / double(counts[k]);
          flowAvg[k] *= 1.0 / double(counts[k]);
        }

        // Copy to labels mat
        lastSuperpixelLabels = cv::Mat(frame.rows, frame.cols, CV_32SC1);
        labels.copyTo(lastSuperpixelLabels);

        // Sample flow
        SparseFlowField flowfield(frame.cols, frame.rows);
        for(int k = 0; k < numlabels; ++k) {
          const int x = centroids[k].x() + 0.5;
          const int y = centroids[k].y() + 0.5;
          //const float ux = flow.at<cv::Vec2f>(y,x)[0];
          //const float uy = flow.at<cv::Vec2f>(y,x)[1];
          const float ux = flowAvg[k][0];
          const float uy = flowAvg[k][1];
          flowfield.push_back(SparseFlowField::Measurement(x,y, ux,uy));
        }

        // Draw
        //static StreamSage::ColorFlow colorflow;
        //for(int j = 0; j < frame.cols; ++j) {
        //  for(int i = 0; i < frame.rows; ++i) {
        //    const int label = labels.at<int>(i,j);
        //    if(label >= 0) {
        //      const int x = centroids[label].x() + 0.5;
        //      const int y = centroids[label].y() + 0.5;
        //      const float ux = flow.at<cv::Vec2f>(y,x)[0];
        //      const float uy = flow.at<cv::Vec2f>(y,x)[1];
        //      unsigned char color[3];
        //      colorflow.computeColor(ux, uy, color);
        //      frame.at<cv::Vec3b>(i,j) = cv::Vec3b(color[2], color[1], color[0]);
        //    }
        //  }
        //}

        delete[] klabels;

        gttoc_(superpixelAvgDenseFlow);
        return flowfield;
    }

    /* **************************************************************************************** */
    void undistortIfHaveCalibration(cv::Mat& frame1, cv::Mat& frame2, boost::optional<const Cal3DS2&> calibration) {
      gttic_(undistortIfHaveCalibration);
      cv::Mat frame1U, frame2U;
      if(calibration) {
        cv::Mat camMatrix = Utils::matOfMatrix(calibration->K());
        cv::Mat coeffs = Utils::matOfVector(calibration->k(), calibration->k().size());
        cv::undistort(frame1, frame1U, camMatrix, coeffs);
        cv::undistort(frame2, frame2U, camMatrix, coeffs);
        frame1 = frame1U;
        frame2 = frame2U;
      }
    }
  }

  namespace flow {

    /* **************************************************************************************** */
    SparseFlowField computeFlow(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      double minEigThreshold, boost::optional<const gtsam::Cal3DS2&> calibration, const gtsam::Vector& initialization)
    {
      // Undistort images
      undistortIfHaveCalibration(prevFrame, curFrame, calibration);

      // Check image sizes
      if(prevFrame.rows != curFrame.rows || prevFrame.cols != curFrame.cols)
        throw std::invalid_argument("Previous and current frame sizes do not match");

      // Create grid
      cv::Mat prevPts(1, gridWidth*gridHeight, CV_32FC2); {
        const float xstep = float(prevFrame.cols) / float(gridWidth); // Horizontal grid spacing
        const float ystep = float(prevFrame.rows) / float(gridHeight); // Vertical grid spacing
        int p = 0;
        float x = xstep / 2.f; // Current x position (in middle of each grid cell)
        for(size_t j=0; j<gridWidth; ++j) {
          float y = ystep / 2.f; // Current y position (in middle of each grid cell)
          for(size_t i=0; i<gridHeight; ++i) {
            prevPts.at<pair<float,float> >(p) = make_pair(x,y);
            ++ p;
            y += ystep;
          }
          x += xstep;
        }
      }

      // See if we have an initialization
      cv::Mat curPts(1, gridWidth*gridHeight, CV_32FC2); {
        if(initialization.rows() > 0) {
          if(initialization.rows() != gridWidth*gridHeight)
            throw std::invalid_argument("Size of initialization flow field does not match current flow field size");
          for(size_t i=0; i<curPts.cols; ++i)
            curPts.at<pair<float,float> >(i) = make_pair( // Offset the previous point by the prediction of the flow in initialization
            prevPts.at<pair<float,float> >(i).first + float(initialization(2*i)),
            prevPts.at<pair<float,float> >(i).second + float(initialization(2*i+1)));
        } else {
          prevPts.copyTo(curPts);
        }
      }

      // Call OpenCV
      cv::Mat status(1, gridWidth*gridHeight, CV_8UC1);
      cv::Mat errors(1, gridWidth*gridHeight, CV_32FC1);
      const cv::Size winSize(7,7);
      const int maxLevel = 3;
      const cv::TermCriteria termCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 30, 0.01);
      const int flags = cv::OPTFLOW_USE_INITIAL_FLOW ;
      cv::calcOpticalFlowPyrLK(prevFrame, curFrame, prevPts, curPts, status, errors, winSize, maxLevel, termCriteria, flags, minEigThreshold);

      // Convert output
      vector<SparseFlowField::Measurement> measurements; {
        int p = 0;
        for(int j=0; j<gridWidth; ++j) {
          for(int i=0; i<gridHeight; ++i) {
            if(status.at<unsigned char>(p))
              measurements.push_back(SparseFlowField::Measurement(
              curPts.at<pair<float,float> >(p).first, curPts.at<pair<float,float> >(p).second,
              curPts.at<pair<float,float> >(p).first - prevPts.at<pair<float,float> >(p).first,
              curPts.at<pair<float,float> >(p).second - prevPts.at<pair<float,float> >(p).second));
            ++ p;
          }
        }
      }
      return SparseFlowField(prevFrame.cols, prevFrame.rows, measurements);
    }

    /* **************************************************************************************** */
    SparseFlowField computeFlowFarneback(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      int pyrLevels, double pyrScale, int winSize, int numIters, int polyN, int polySigma, boost::optional<const gtsam::Cal3DS2&> calibration)
    {
      // Undistort images
      undistortIfHaveCalibration(prevFrame, curFrame, calibration);

      // Check image sizes
      if(prevFrame.rows != curFrame.rows || prevFrame.cols != curFrame.cols)
        throw std::invalid_argument("Previous and current frame sizes do not match");

      cv::Mat flow = farnebackWrapper(prevFrame, curFrame, pyrLevels, pyrScale, winSize, numIters, polyN, polySigma);
      return sampleDenseFlow(flow, gridWidth, gridHeight);
    }

    /* **************************************************************************************** */
    generalflow_flow_EXPORT SparseFlowField computeFlowTVL1(cv::Mat prevFrameColor, cv::Mat curFrameColor, int gridWidth, int gridHeight,
      bool useSuperpixels, bool normalizeBrightness,
      double tau, double lambda, double theta, int nscales, int warps, double epsilon, int iterations,
      boost::optional<const gtsam::Cal3DS2&> calibration)
    {
      gttic_(computeFlowTVL1);

      // Check image sizes
      if(prevFrameColor.rows != curFrameColor.rows || prevFrameColor.cols != curFrameColor.cols)
        throw std::invalid_argument("Previous and current frame sizes do not match");

      // Undistort images
      undistortIfHaveCalibration(prevFrameColor, curFrameColor, calibration);

      // Convert to grayscale
      cv::Mat prevFrame;
      cv::Mat curFrame;
      if(prevFrameColor.channels() == 3)
        cv::cvtColor(prevFrameColor, prevFrame, CV_BGR2GRAY);
      else if(useSuperpixels)
        throw std::invalid_argument("For superpixel mode, need color image");
      else
        prevFrame = prevFrameColor;
      if(curFrameColor.channels() == 3)
        cv::cvtColor(curFrameColor, curFrame, CV_BGR2GRAY);
      else if(useSuperpixels)
        throw std::invalid_argument("For superpixel mode, need color image");
      else
        curFrame = curFrameColor;

      // Normalize brightness
      double avgBrightnessCur = cv::sum(curFrame)[0];
      double avgBrightnessPrev = cv::sum(prevFrame)[0];
      prevFrame = (avgBrightnessCur / avgBrightnessPrev) * prevFrame;

      gttic_(TVL1);
      cv::Mat flow;
      cv::Ptr<cv::DenseOpticalFlow> calc = cv::createOptFlow_DualTVL1();
      calc->set("tau", tau);
      calc->set("lambda", lambda);
      calc->set("theta", theta);
      calc->set("nscales", nscales);
      calc->set("warps", warps);
      calc->set("epsilon", epsilon);
      calc->set("iterations", iterations);
      calc->set("useInitialFlow", false);
      calc->calc(prevFrame, curFrame, flow);
      gttoc_(TVL1);

      SparseFlowField flowfield;
      if(useSuperpixels)
        flowfield = superpixelAvgDenseFlow(flow, curFrameColor, gridWidth, gridHeight);
      else
        flowfield = sampleDenseFlow(flow, gridWidth, gridHeight);
      gttoc_(computeFlowTVL1);
      tictoc_print_();
      tictoc_reset_();
      return flowfield;
    }

    /* **************************************************************************************** */
    generalflow_flow_EXPORT SparseFlowField computeFlowBrox(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      float alpha, float gamma, float pyrScale, int innerIterations, int outerIterations, int solverIterations,
      boost::optional<const gtsam::Cal3DS2&> calibration)
    {
      // Undistort images
      undistortIfHaveCalibration(prevFrame, curFrame, calibration);

      // Check image sizes
      if(prevFrame.rows != curFrame.rows || prevFrame.cols != curFrame.cols)
        throw std::invalid_argument("Previous and current frame sizes do not match");

      {
        int devices = cv::gpu::getCudaEnabledDeviceCount();
        if(devices <= 0)
          throw std::runtime_error("No CUDA devices present");
      }
      cv::Mat flowuv[2];
      cv::gpu::GpuMat gpuPrevFrame(prevFrame);
      cv::gpu::GpuMat gpuCurFrame(curFrame);
      cv::gpu::GpuMat gpuFlowU;
      cv::gpu::GpuMat gpuFlowV;
      cv::gpu::BroxOpticalFlow calc = cv::gpu::BroxOpticalFlow(
        alpha, gamma, pyrScale, innerIterations, outerIterations, solverIterations);
      calc(gpuPrevFrame, gpuCurFrame, gpuFlowU, gpuFlowV);
      gpuFlowU.download(flowuv[0]);
      gpuFlowV.download(flowuv[1]);
      cv::Mat flow;
      cv::merge(flowuv, 2, flow);

      return sampleDenseFlow(flow, gridWidth, gridHeight);
    }

    /* **************************************************************************************** */
    //generalflow_flow_EXPORT SparseFlowField computeFlowFarnebackSuperpixels(const cv::Mat& _prevFrame, const cv::Mat& _curFrame,
    //  boost::optional<const gtsam::Cal3DS2&> calibration = boost::none)
    //{
    //  // Undistort images
    //  cv::Mat prevFrame, curFrame;
    //  if(calibration) {
    //    cv::Mat camMatrix = Utils::matOfMatrix(calibration->K());
    //    cv::Mat coeffs = Utils::matOfVector(calibration->k(), calibration->k().size());
    //    cv::undistort(_prevFrame, prevFrame, camMatrix, coeffs);
    //    cv::undistort(_curFrame, curFrame, camMatrix, coeffs);
    //  } else {
    //    prevFrame = _prevFrame;
    //    curFrame = _curFrame;
    //  }

    //  // Check image sizes
    //  if(prevFrame.rows != curFrame.rows || prevFrame.cols != curFrame.cols)
    //    throw std::invalid_argument("Previous and current frame sizes do not match");
    //}

    /* **************************************************************************************** */
    cv::Mat getLastSuperpixelLabels() {
      return lastSuperpixelLabels;
    }

    /* **************************************************************************************** */
    Matrix fillSuperpixels(const cv::Mat& superpixelLabels, const Vector& values) {
      if(superpixelLabels.depth() != CV_32S || superpixelLabels.channels() != 1)
        throw invalid_argument("Need single-channel 32S matrix");
      Matrix result(superpixelLabels.rows, superpixelLabels.cols);
      for(int j = 0; j < superpixelLabels.cols; ++j)
        for(int i = 0; i < superpixelLabels.rows; ++i)
          result(i,j) = values(superpixelLabels.at<int32_t>(i,j));
      return result;
    }

    /* **************************************************************************************** */
    void writeIndicatorValuesMATLAB(gtsam::Values& values, const gtsam::Matrix& indicators) {
      for(int i = 0; i < indicators.cols(); ++i)
        values.insert(symbol('l',i), LieVector(indicators.col(i)));
    }

    /* **************************************************************************************** */
    gtsam::Matrix readIndicatorValuesMATLAB(const gtsam::Values& values, int n) {
      const Vector sample = values.at<LieVector>(symbol('l',0));
      Matrix result(sample.rows(), n);
      Values::const_iterator it = values.find(symbol('l',0));
      for(size_t i = 0; i < n; ++i) {
        if(it == values.end())
          throw std::invalid_argument("Number of requested indicators are not present in the Values");
        result.col(i) = dynamic_cast<const LieVector&>(it->value);
        ++ it;
      }
      return result;
    }

    /* **************************************************************************************** */
    void drawFlow(cv::Mat& image, const SparseFlowField& flow, int gridWidth, int gridHeight, double scale) {
      // Check image size
      if(image.cols != flow.imgWidth() || image.rows != flow.imgHeight())
        throw std::invalid_argument("Image size does not match image size from flow field");

      // Get grid properties
      const float gridCellWidth = float(flow.imgWidth()) / float(gridWidth);
      const float gridCellHeight = float(flow.imgHeight()) / float(gridHeight);
      BOOST_FOREACH(const SparseFlowField::Measurement& m, flow) {
        const pair<float,float> x_y = calcPixel(m.x(), m.y(), gridCellWidth, gridCellHeight);
        const float uxScaled = m.ux_ * scale;
        const float uyScaled = m.uy_ * scale;
        const int aaShift = 4;
        cv::line(image, (1<<aaShift)*cv::Point2f(x_y.first, x_y.second), (1<<aaShift)*cv::Point2f(x_y.first+uxScaled,x_y.second+uyScaled), cv::Scalar(255, 128, 128), 2, CV_AA, aaShift);
      }
    }

  }
}

