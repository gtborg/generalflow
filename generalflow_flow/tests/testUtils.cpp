/**
* @file    testFlowOps.cpp
* @brief
* @author  Richard Roberts
* @created Jan 1, 2014
*/

#include <CppUnitLite/TestHarness.h>

#include <gtsam/base/Matrix.h>

#include <generalflow_flow/Utils.h>

using namespace gtsam;
using namespace std;
using namespace generalflow;

/* ************************************************************************* */
TEST(Utils, logSum)
{
  const Vector values = (Vector(4) << 1.0, 500.0, 3.0, 9000.0);
  const Vector logValues = values.array().log();

  const double expectedLogSum = log(values.array().sum());

  const double actualLogSum = Utils::logSum(logValues);

  EXPECT_DOUBLES_EQUAL(expectedLogSum, actualLogSum, 1e-10);
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

