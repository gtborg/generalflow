/**
* @file    testFlowOps.cpp
* @brief
* @author  Richard Roberts
* @created Jan 1, 2014
*/

#include <CppUnitLite/TestHarness.h>

#include <gtsam/base/Matrix.h>

#include <3rdparty/MRF2.2/MaxProdBP.h>
#include <3rdparty/MRF2.2/TRW-S.h>

using namespace gtsam;
using namespace std;

/* ************************************************************************* */
TEST(BP, solve)
{
  Eigen::MatrixXf evidence = (Eigen::MatrixXf(4, 4) <<
                              .9, .1, .1, .1,
                              .1, .9, .1, .1,
                              .1, .1, .9, .1,
                              .1, .1, .1, .9);
  Eigen::MatrixXf smoothness(4,4);
  for(DenseIndex k = 0; k < 4; ++k)
    for(DenseIndex j = 0; j < 4; ++j)
      smoothness(k*4 + j) = -log(1.0/double(4)); // (k == j ? -log(0.5) : -log(0.5 / double(K-1)));

  Eigen::MatrixXf evidenceEnergy = -evidence.array().log();

  DataCost dataCost(evidenceEnergy.data());
  SmoothnessCost smoothnessCost(smoothness.data());
  EnergyFunction energy(&dataCost, &smoothnessCost);
  MaxProdBP bp(2, 2, 4, &energy);
  bp.initialize();
  bp.clearAnswer();
  float t;
  bp.optimize(5, t);

  Eigen::MatrixXf indicators = Eigen::MatrixXf::Constant(4, 4, 0.01);
  for(DenseIndex j = 0; j < 2*2; ++j)
    indicators(bp.getLabel(j), j) = 1.0 - (4-1) * 0.01;

  cout << indicators.transpose() << endl;
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

