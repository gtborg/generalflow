/**
* @file    testFlowOps.cpp
* @brief
* @author  Richard Roberts
* @created Jan 1, 2014
*/

#include <CppUnitLite/TestHarness.h>
 
#include <generalflow_flow/flowOps.h>
#include <generalflow_flow/flowOps-inst.h>

using namespace gtsam;
using namespace std;

/* ************************************************************************* */
TEST(flowOps, ImagePairGrad)
{
  using namespace generalflow::flow;

  Matrix im1 = Matrix::Constant(5, 5, 0.1);
  Matrix im2 = Matrix::Constant(5, 5, 0.2);
  Matrix im3 = Matrix::Constant(5, 5, 0.3);

  ImagePairGrad images(im1, im2);

  // im2 should be normalized to the same level as im1
  EXPECT(assert_equal(im1, images.prevFrame()));
  EXPECT(assert_equal(im1, images.curFrame()));

  images = ImagePairGrad(images, im3);

  // im3 should be normalized to the same level as im2
  EXPECT(assert_equal(im2, images.prevFrame()));
  EXPECT(assert_equal(im2, images.curFrame()));
}

/* ************************************************************************* */
TEST(flowOps, fullImageErrorImpl)
{
  /*
# Calculate expected test values in python:
from numpy import *
basisRot = matrix([[1,2,3],[4,5,6]])
basisTrans1 = matrix([[7],[8]])
basisTrans2 = matrix([[9],[10]])
v = matrix([[.1,.2,.3,.4]]).transpose()
grad = matrix([[1,2]])
flowSigma = (
    diagflat([5.0*5.0, 5.0*5.0]),
    diagflat([1.0*1.0, 1.0*1.0]),
    diagflat([1.0*1.0, 1.0*1.0]),
    diagflat([1.0*1.0, 1.0*1.0]))
pixSigma = (.02*.02, .01*.01, .01*.01, .01*.01)
indicators = array([0.1, 0.3, 0.2, 0.4])

r0 = matrix([[1]])
r1 = matrix([[1]]) + grad * hstack((basisRot, basisTrans1)) * v
r2 = matrix([[1]]) + grad * hstack((basisRot, basisTrans2)) * v
r3 = matrix([[1]]) + grad * basisRot * v[0:3]

invSigmas = array([(1.0 / sqrt(grad * flowSigma[0] * grad.transpose() + pixSigma[0]))[0,0],
                   (1.0 / sqrt(grad * flowSigma[1] * grad.transpose() + pixSigma[1]))[0,0],
                   (1.0 / sqrt(grad * flowSigma[2] * grad.transpose() + pixSigma[2]))[0,0],
                   (1.0 / sqrt(grad * flowSigma[3] * grad.transpose() + pixSigma[3]))[0,0]])

e0 = sqrt(indicators[0]) * r0 * invSigmas[0]
e1 = sqrt(indicators[1]) * r1 * invSigmas[1]
e2 = sqrt(indicators[2]) * r2 * invSigmas[2]
e3 = sqrt(indicators[3]) * r3 * invSigmas[2]

ddvU = vstack((zeros([1, v.size]),
               grad * hstack((basisRot, basisTrans1)),
               grad * hstack((basisRot, basisTrans2)),
               grad * hstack((basisRot, zeros(basisTrans1.shape)))
             ))
ddv = diagflat(sqrt(indicators) * invSigmas) * ddvU

ddWRotU0 = (grad.transpose() * v[0:3].transpose()).reshape([1, basisRot.size], order='C')
ddWRot = diagflat(sqrt(indicators) * invSigmas) * vstack((zeros([1, basisRot.size]),
                                                           ddWRotU0,
                                                           ddWRotU0,
                                                           ddWRotU0))

ddW1U0 = (grad.transpose() * v[3].transpose()).reshape([1, basisTrans1.size], order='C')
ddW1 = diagflat(sqrt(indicators) * invSigmas) * vstack((zeros([1, basisTrans1.size]),
                                                         ddW1U0,
                                                         zeros([1, basisTrans2.size]),
                                                         zeros([1, basisTrans1.size])))

ddW2U0 = (grad.transpose() * v[3].transpose()).reshape([1, basisTrans2.size], order='C')
ddW2 = diagflat(sqrt(indicators) * invSigmas) * vstack((zeros([1, basisTrans2.size]),
                                                         zeros([1, basisTrans2.size]),
                                                         ddW2U0,
                                                         zeros([1, basisTrans1.size])))

  */
  const Matrix basisRot = (Matrix(2, 3) <<
      1, 2, 3,
      4, 5, 6);
  const Matrix basisTrans1 = (Matrix(2, 1) <<
      7,
      8);
  const Matrix basisTrans2 = (Matrix(2, 1) <<
      9,
      10);
  vector<Matrix> basesTrans;
  basesTrans.push_back(basisTrans1);
  basesTrans.push_back(basisTrans2);

  const Vector flowSigmas = (Vector(4) << 5.0, 1.0, 1.0, 1.0);
  const Vector pixSigmas = (Vector(4) << 0.02, 0.01, 0.01, 0.01);

  const Vector velocity = (Vector(4) << 0.1, 0.2, 0.3, 0.4);
  const Vector indicators = (Vector(4) << 0.1, 0.3, 0.2, 0.4);

  generalflow::flow::ImagePairGrad images((Matrix(1, 1) << 0.0), (Matrix(1, 1) << 1.0), false);
  images.prevGradX_ = (Matrix(1, 1) << 1.0);
  images.prevGradY_ = (Matrix(1, 1) << 2.0);

  Matrix ddWRot;
  vector<Matrix> ddWk(2, Matrix());
  Matrix ddy;

  const Vector e = generalflow::flow::fullImageErrorImpl(images, 0, 0,
      basisRot, basesTrans, velocity, flowSigmas, pixSigmas,
      indicators, ddWRot, ddWk, ddy);

  const Vector expectedE = (Vector(4) << 0.02828423,  4.40903745,  4.0799592 ,  2.48899098);
  const Matrix expected_ddy = (Matrix(4,4) <<
                               0.        ,  0.        ,  0.        ,  0.,
                               2.20451872,  2.9393583 ,  3.67419787,  5.63377007,
                               1.799982  ,  2.399976  ,  2.99997   ,  5.799942,
                               2.54555896,  3.39407861,  4.24259826,  0.);
  const Matrix expected_ddWRot = (Matrix(4,6) <<
                                  0.        ,  0.        ,  0.        ,  0.        ,  0.        ,  0.,
                                  0.02449465,  0.0489893 ,  0.07348396,  0.0489893 ,  0.09797861,  0.14696791,
                                  0.0199998 ,  0.0399996 ,  0.0599994 ,  0.0399996 ,  0.0799992 ,  0.1199988,
                                  0.02828399,  0.05656798,  0.08485197,  0.05656798,  0.11313595,  0.16970393);
  const Matrix expected_ddW1 = (Matrix(4,2) <<
                                0.        ,  0.        ,
                                0.09797861,  0.19595722,
                                0.        ,  0.        ,
                                0.        ,  0.);
  const Matrix expected_ddW2 = (Matrix(4,2) <<
                                0.        ,  0.        ,
                                0.        ,  0.        ,
                                0.0799992 ,  0.1599984,
                                0.        ,  0.);

  EXPECT(assert_equal(expectedE, e, 1e-6));
  EXPECT(assert_equal(expected_ddy, ddy, 1e-6));
  EXPECT(assert_equal(expected_ddWRot, ddWRot, 1e-6));
  EXPECT(assert_equal(expected_ddW1, ddWk[0], 1e-6));
  EXPECT(assert_equal(expected_ddW2, ddWk[1], 1e-6));
}

/* ************************************************************************* */
TEST(flowOps, calculatePixelMarginalSigma)
{
  const Matrix basisRot = (Matrix(2, 3) <<
      1, 2, 3,
      4, 5, 6);
  const Matrix basisTrans = (Matrix(2, 1) <<
      7,
      8);
  vector<Matrix> basesTrans;
  basesTrans.push_back(basisTrans);

  const DenseIndex pix = 0;

  const Vector flowSigmas = (Vector(3) << 5.0, 1.0, 1.0);
  const Vector pixSigmas = (Vector(3) << 0.02, 0.01, 0.01);
  const Vector flowSigmasSquared = flowSigmas.array().square();
  const Vector pixSigmasSquared = pixSigmas.array().square();

  const Matrix sigmaV = (Matrix(4,4) <<
      1, 2, 3, 4,
      5, 6, 7, 8,
      9, 10, 11, 12,
      13, 14, 15, 16);

  const Eigen::RowVector2d grad(0.5, 0.6);

  const Eigen::RowVectorXd expected = (Eigen::RowVectorXd(3) <<
      3.90517605, 66.19176762, 31.56913208);

  const Eigen::RowVectorXd actual = generalflow::flow::calculatePixelMarginalSigma(
      basisRot, basesTrans, pix, make_pair(flowSigmasSquared, pixSigmasSquared),
      sigmaV, grad);

  EXPECT(assert_equal(Vector(expected.transpose()), Vector(actual.transpose()), 1e-6));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

