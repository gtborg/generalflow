/**
 * @file    sparseFlow.h
 * @brief   Functions for computing and manipulating sparse flow
 * @author  Richard Roberts
 * @created November 23, 2012
 */
#pragma once

#include <opencv2/core/core.hpp>
#include <boost/optional.hpp>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/nonlinear/Values.h>
#include <generalflow_flow/dllexport.h>
#include <generalflow_core/SparseFlowField.h>

namespace generalflow {
  namespace flow {

    /**
     * @brief Computes sparse optical flow on a regular grid.
     *
     * @param prevFrame   The previous frame.
     * @param curFrame    The current frame.
     * @param gridWidth   Width of the grid.
     * @param gridHeight  Height of the grid.
     *
     * @return  The sparse flow
     */
    generalflow_flow_EXPORT SparseFlowField computeFlow(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      double minEigThreshold = 5e-3, boost::optional<const gtsam::Cal3DS2&> calibration = boost::none,
      const gtsam::Vector& initialization = gtsam::Vector());

    /**
     * @brief Computes dense optical flow using the Farneback method
     *
     * @param prevFrame   The previous frame.
     * @param curFrame    The current frame.
     * @param gridWidth   Width of the grid.
     * @param gridHeight  Height of the grid.
     *
     * @return  The flow
     */
    generalflow_flow_EXPORT SparseFlowField computeFlowFarneback(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      int pyrLevels = 5, double pyrScale = 0.5, int winSize = 13, int numIters = 10, int polyN = 5, int polySigma = 1.1,
      boost::optional<const gtsam::Cal3DS2&> calibration = boost::none);
    
    /**
     * @brief Computes dense optical flow using TVL1
     *
     * @param prevFrame   The previous frame.
     * @param curFrame    The current frame.
     * @param gridWidth   Width of the grid.
     * @param gridHeight  Height of the grid.
     *
     * @return  The flow
     */
    generalflow_flow_EXPORT SparseFlowField computeFlowTVL1(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      bool useSuperpixels = false, bool normalizeBrightness = true,
      double tau = 0.25, double lambda = 0.2, double theta = 0.5, int nscales = 5, int warps = 5, double epsilon = 0.01, int iterations = 10,
      boost::optional<const gtsam::Cal3DS2&> calibration = boost::none);

    /**
     * @brief Computes dense optical flow using the Brox method
     *
     * @param prevFrame   The previous frame.
     * @param curFrame    The current frame.
     * @param gridWidth   Width of the grid.
     * @param gridHeight  Height of the grid.
     *
     * @return  The flow
     */
    generalflow_flow_EXPORT SparseFlowField computeFlowBrox(cv::Mat prevFrame, cv::Mat curFrame, int gridWidth, int gridHeight,
      float alpha = 0.197, float gamma = 50.0, float pyrScale = 0.5, int innerIterations = 10, int outerIterations = 77, int solverIterations = 10,
      boost::optional<const gtsam::Cal3DS2&> calibration = boost::none);

    /** Get the last superpixel labels computed during a call to computeFlow* with useSuperpixels=true */
    generalflow_flow_EXPORT cv::Mat getLastSuperpixelLabels();

    /** Fill in an scalar image using the given superpixel labels and the
     * values for each superpixel class. */
    generalflow_flow_EXPORT gtsam::Matrix fillSuperpixels(const cv::Mat& superpixelLabels, const gtsam::Vector& values);

    /** Write indicator values into a Values from a MATLAB matrix */
    generalflow_flow_EXPORT void writeIndicatorValuesMATLAB(gtsam::Values& values, const gtsam::Matrix& indicators);

    /** Write indicator values into a Values from a MATLAB matrix */
    generalflow_flow_EXPORT gtsam::Matrix readIndicatorValuesMATLAB(const gtsam::Values& values, int n);

    /**
     * @brief Computes dense optical flow using the Farneback method, grouped into superpixels
     *
     * @param prevFrame   The previous frame.
     * @param curFrame    The current frame.
     *
     * @return  The flow
     */
    //generalflow_flow_EXPORT SparseFlowField computeFlowFarnebackSuperpixels(const cv::Mat& prevFrame, const cv::Mat& curFrame,
    //  boost::optional<const gtsam::Cal3DS2&> calibration = boost::none);

    /**
     * Draw the flow field on an image
     */
    generalflow_flow_EXPORT void drawFlow(cv::Mat& image, const SparseFlowField& flow, int gridWidth, int gridHeight, double scale = 1.0);
}

}
