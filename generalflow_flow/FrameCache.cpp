/**
* @file    FrameCache.h
* @brief   Caches a loaded frame and relevant computations for doing out-of-core optimization over many frames.
* @author  Richard Roberts
* @created Aug 6, 2012
*/

#include "FrameCache.h"
#include "Utils.h"
#include "ImgOps.h"
#include "flowOps.h"

#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {

  /* **************************************************************************************** */
  pair<cv::Mat, cv::Mat> FrameCache::readImages(int curFrameNum) const {
    //cout << "Reading " << (boost::format(filenameFormat_) % (curFrameNum-1)).str() << endl;
    pair<cv::Mat, cv::Mat> images = make_pair(
      cv::imread((boost::format(filenameFormat_) % (curFrameNum - 1)).str(), 0),
      cv::imread((boost::format(filenameFormat_) % curFrameNum).str(), 0)); // Load grayscale
    if (images.first.rows == 0 || images.second.rows == 0)
      throw std::invalid_argument("One of the previous or current frame could not be loaded");
    return images;
  }

  /* **************************************************************************************** */
  std::pair<Matrix, Matrix> FrameCache::gradientImages(const Matrix& prevFrame) const {
    return ImgOps::ComputeDerivatives(prevFrame);
  }

  /* **************************************************************************************** */
  const FrameCache::LoadedImages& FrameCache::loadImages(int curFrameNum,
    double flowSigmaV, double flowSigmaF, double pixSigmaV, double pixSigmaF)
  {
    // This will hold the frame in its original format
    cv::Mat origPrevFrame, origCurFrame;

    // See if we have the frame in the cache
    Cache_::const_iterator cacheEntry = cache_.find(curFrameNum);
    if (cacheEntry != cache_.end()) {
      // Get the frame from the cache
      origPrevFrame = cacheEntry->second.prevFrame;
      origCurFrame = cacheEntry->second.curFrame;
      loadedImages_.prevGradX_ = cacheEntry->second.prevGradX;
      loadedImages_.prevGradY_ = cacheEntry->second.prevGradY;
      //cout << "Frame " << curFrameNum << " was cached." << endl;
    }
    else {
      // Load the frame from disk (will probably be in 8-bit greyscale)
      cv::Mat prevFrameFile, curFrameFile;
      boost::tie(prevFrameFile, curFrameFile) = readImages(curFrameNum); // Loads grayscale
      //cout << "Loaded frame " << curFrameNum << "." << endl;

      // Determine how much we should downscale with interpolation and how much we should decimate
      int scaledWidth = imWidth_ * (pixelSkip_ + 1);
      int scaledHeight = imHeight_ * (pixelSkip_ + 1);
      scaledWidth = std::min(scaledWidth, curFrameFile.cols);
      scaledHeight = std::min(scaledHeight, curFrameFile.rows);

      // Downscale with interpolation first (still in 8-bit greyscale)
      cv::Mat prevFrameScaled, curFrameScaled;
      if (scaledWidth == curFrameFile.cols && scaledHeight == curFrameFile.rows) {
        prevFrameScaled = prevFrameFile;
        curFrameScaled = curFrameFile;
      }
      else {
        cv::resize(prevFrameFile, prevFrameScaled, cv::Size(scaledWidth, scaledHeight), cv::INTER_AREA);
        cv::resize(curFrameFile, curFrameScaled, cv::Size(scaledWidth, scaledHeight), cv::INTER_AREA);
      }
      cout << "Downscaled to " << scaledWidth << "x" << scaledHeight << endl;

      // Convert the downscaled image in floating-point before computing gradients
      cv::Mat gradX, gradY; {
        cv::Mat prevFrameScaledF, curFrameScaledF; {
          double scale =
            prevFrameScaled.depth() == CV_8U ? 1. / 255. :
            prevFrameScaled.depth() == CV_16U ? 1. / 65535 :
            prevFrameScaled.depth() == CV_32F ? 1. :
            prevFrameScaled.depth() == CV_64F ? 1. :
            (throw invalid_argument("Unsupported image depth"), 0.);
          prevFrameScaled.convertTo(prevFrameScaledF, CV_64F, scale); // Convert to floating point
          curFrameScaled.convertTo(curFrameScaledF, CV_64F, scale); // Convert to floating point
        }

        // Compute gradients on downscaled floating point image
        boost::tie(gradX, gradY) = ImgOps::ComputeDerivatives(prevFrameScaledF);
      }

      // Downscale the rest of the way with decimation on the 8-bit image
      if (pixelSkip_ == 0) {
        origPrevFrame = prevFrameScaled;
        origCurFrame = curFrameScaled;
      }
      else {
        cv::resize(prevFrameScaled, origPrevFrame, cv::Size(imWidth_, imHeight_), cv::INTER_NEAREST);
        cv::resize(curFrameScaled, origCurFrame, cv::Size(imWidth_, imHeight_), cv::INTER_NEAREST);
        cv::resize(gradX, gradX, cv::Size(imWidth_, imHeight_), cv::INTER_NEAREST);
        cv::resize(gradY, gradY, cv::Size(imWidth_, imHeight_), cv::INTER_NEAREST);
      }
      cout << "Decimated to " << imWidth_ << "x" << imHeight_ << endl;

      // At this point we have orig{Prev,Cur}Frame in 8-bit greyscale and gradX and gradY in 64-bit floating point
      loadedImages_.prevGradX_ = Utils::matrixOfMat(gradX);
      loadedImages_.prevGradY_ = Utils::matrixOfMat(gradY);

      // If we have space, cache this frame
      {
        int bytesPerCacheEntry = imWidth_ * imHeight_ * (sizeof(double) * 2 + sizeof(char) * 2);
        if (cache_.size() * bytesPerCacheEntry < maxCacheBytes_)
          cache_[curFrameNum] = CachedImages(origPrevFrame, origCurFrame, loadedImages_.prevGradX_, loadedImages_.prevGradY_);
      }
    }

    // Now convert prev and cur frame to 64-bit floating point
    cv::Mat prevFrame, curFrame;
    double scale =
      prevFrame.depth() == CV_8U ? 1. / 255. :
      prevFrame.depth() == CV_16U ? 1. / 65535 :
      prevFrame.depth() == CV_32F ? 1. :
      prevFrame.depth() == CV_64F ? 1. :
      (throw invalid_argument("Unsupported image depth"), 0.);
    origPrevFrame.convertTo(prevFrame, CV_64F, scale); // Convert to floating point
    origCurFrame.convertTo(curFrame, CV_64F, scale); // Convert to floating point
    loadedImages_.prevFrame_ = Utils::matrixOfMat(prevFrame);
    loadedImages_.curFrame_ = Utils::matrixOfMat(curFrame);

    // Compute weights
    loadedImages_.weightsV_ = flow::pixelWeights(loadedImages_.prevGradX_, loadedImages_.prevGradY_, flowSigmaV, pixSigmaV);
    loadedImages_.weightsF_ = flow::pixelWeights(loadedImages_.prevGradX_, loadedImages_.prevGradY_, flowSigmaF, pixSigmaF);

    return loadedImages_;
  }
}