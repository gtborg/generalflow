/**
* @file    flowOps-inl.h
* @brief   Shared operations with optical flow
* @author  Richard Roberts
* @created August 1, 2012
*/

#pragma once

#include <generalflow_core/linearFlow.h>
#include <generalflow_flow/flowOps.h>

using namespace std;
using namespace gtsam;

using Eigen::Vector2d;
using Eigen::Vector3d;
using Eigen::RowVector2d;
using Eigen::RowVector3d;
using Eigen::RowVectorXd;
using Eigen::Map;
using Eigen::Stride;
using Eigen::Dynamic;
using Eigen::RowMajor;
using Eigen::ColMajor;

namespace generalflow {
  namespace flow {

    /* **************************************************************************************** */
    template<typename MatrixRot, typename MatrixTrans,
      typename VectorVelocity, typename VectorSigmas, typename VectorIndicators>
      Eigen::Matrix<double, VectorIndicators::RowsAtCompileTime, 1>
      fullImageErrorImpl(
      const ImagePairGrad& images, int pixX, int pixY,
      const MatrixRot& basisRot,
      const std::vector<MatrixTrans>& basisTrans,
      const VectorVelocity& velocity,
      const VectorSigmas& flowSigmas,
      const VectorSigmas& pixSigmas,
      const VectorIndicators& indicators,
      boost::optional<gtsam::Matrix&> ddWRot,
      boost::optional<std::vector<gtsam::Matrix>&> ddWk,
      boost::optional<gtsam::Matrix&> ddy,
      const boost::shared_ptr<FullImageErrorVisualization>& visualization)
    {
      // Constants
      const DenseIndex r = basisRot.cols();
      const DenseIndex q = velocity.size() - r;
      const DenseIndex kappa = basisTrans.size() + 1;
      typedef Eigen::Matrix<double, VectorIndicators::RowsAtCompileTime, 1> ColVectorKappa;

      // Compute residuals
      const RowVector2d grad = flow::grad(images.prevGradX(), images.prevGradY(), pixX, pixY);
      const double Dt = images.curFrame()(pixY, pixX) - images.prevFrame()(pixY, pixX);
      ColVectorKappa imgErrors(kappa + 1);
      auto omega = velocity.head(r);
      auto v = velocity.tail(q);
      const double IRot = (grad * basisRot * omega)(0,0); // Pre-compute rotation contribution
      imgErrors(0) = Dt;
      for(DenseIndex k = 1; k < kappa; ++k) {
        imgErrors(k) = Dt + IRot + (grad * basisTrans[k-1] * v)(0, 0);
        // Infinite values in basis cause infinite values here and indicate undefined flow, so set
        // residual to zero.
        if(!isfinite(imgErrors(k-1)))
          imgErrors(k) = 0.0;
      }
      imgErrors(kappa) = Dt + IRot; // Infinity model

      // Effective sigmas
      ColVectorKappa invSigmas = (flowSigmas.array().square() * grad.squaredNorm() + pixSigmas.array().square()).sqrt().inverse();

      ColVectorKappa robustInvSigmas = indicators.array().sqrt() * invSigmas.array();
      ColVectorKappa e = imgErrors.array() * robustInvSigmas.array();

      if(visualization)
      {
        visualization->prevFrame.at<float>(pixY, pixX) = images.prevFrame()(pixY, pixX);
        visualization->curFrame.at<float>(pixY, pixX) = images.curFrame()(pixY, pixX);
        visualization->temporalDeriv.at<float>(pixY, pixX) = images.curFrame()(pixY, pixX) - images.prevFrame()(pixY, pixX);
        Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(
                  &visualization->errs.at<float>(0,0), visualization->errs.rows,
                  visualization->errs.cols).col(pixX).segment((kappa+1)*pixY, kappa+1) = imgErrors.template cast<float>();
        Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(
                  &visualization->normalizedErrs.at<float>(0,0), visualization->normalizedErrs.rows,
                  visualization->normalizedErrs.cols).col(pixX).segment((kappa+1)*pixY, kappa+1) =
                      (invSigmas.array() * imgErrors.array()).template cast<float>();
        Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(
                  &visualization->invSigmas.at<float>(0,0), visualization->invSigmas.rows,
                  visualization->invSigmas.cols).col(pixX).segment((kappa+1)*pixY, kappa+1) = invSigmas.template cast<float>();
        Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(
          &visualization->indicators.at<float>(0,0), visualization->indicators.rows,
          visualization->indicators.cols).col(pixX).segment((kappa+1)*pixY, kappa+1) =
          indicators.template cast<float>();
        Eigen::Vector2f expectedFlow = (indicators(kappa) * basisRot * omega).template cast<float>();
        for(DenseIndex k = 1; k < kappa; ++k)
          expectedFlow += (indicators(k) * basisTrans[k-1] * v).template cast<float>();
        Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(
          &visualization->expectedFlow.at<float>(0,0), visualization->expectedFlow.rows,
          visualization->expectedFlow.cols).col(pixX).segment(2*pixY, 2) =
          expectedFlow;
      }

      // Derivatives
      if(ddWRot)
      {
        // The Jacobian w.r.t. V2 has the structure:
        // [     0     0     0 ]
        // [ de/dv de/dv de/dv ]
        // [ de/dv de/dv de/dv ]
//        cout << "robustInvSigmas: " << robustInvSigmas.transpose() << endl;
//        cout << "grad: " << grad << endl;
//        cout << "velocity: " << velocity.transpose() << endl;
//        cout << "omega: " << omega.transpose() << endl;
        ddWRot->resize(kappa+1, 2*r);
        ddWRot->row(0).setZero();
        Map<Eigen::Matrix<double, Dynamic, Dynamic, RowMajor>, 0, Stride<Dynamic, Dynamic> >(
            &(*ddWRot)(1,0), 2, r, Stride<Dynamic, Dynamic>((kappa+1)*r, (kappa+1))) =
                grad.transpose() * omega.transpose();
        ddWRot->bottomRows(kappa - 1).rowwise() = ddWRot->row(1); // Replicate Jacobian across all rows
        ddWRot->array().colwise() *= robustInvSigmas.array(); // Weight Jacobians
//        cout << "ddWRot:\n" << *ddWRot << endl;
      }

      if(ddWk)
      {
        for(DenseIndex k = 1; k < kappa; ++k)
        {
          // The Jacobian w.r.t. Wk has the structure:
          // [     0     0     0     0     0     0 ]
          // [ de/dv de/dv de/dv de/dv de/dv de/dv ]
          // [     0     0     0     0     0     0 ]
//          cout << "v: " << v.transpose() << endl;
          (*ddWk)[k-1].resize(kappa+1, 2*q);
          Map<Eigen::Matrix<double, Dynamic, Dynamic, RowMajor>, 0, Stride<Dynamic, Dynamic> >(
              &((*ddWk)[k-1])(k,0), 2, q, Stride<Dynamic, Dynamic>((kappa+1)*q, (kappa+1))) =
                  robustInvSigmas(k) * grad.transpose() * v.transpose();
          (*ddWk)[k-1].topRows(k).setZero();
          (*ddWk)[k-1].bottomRows(kappa - k).setZero();
        }
//        cout << "ddWk:\n" << (*ddWk)[0] << endl;
//        cout << "e:\n" << e.transpose() << endl;
      }

      if (ddy)
      {
        ddy->resize(kappa+1, r+q);

        // First row is zeros
        ddy->row(0).setZero();

        // Fill in rotational velocity columns
        ddy->block(1, 0, kappa, r) = robustInvSigmas.tail(kappa) * grad * basisRot;

        // Fill in translational velocity columns
        for(DenseIndex k = 1; k < kappa; ++k)
        {
          ddy->rightCols(q).row(k) = robustInvSigmas(k) * grad * basisTrans[k-1];
          if (!ddy->rightCols(q).row(k).allFinite())
            ddy->rightCols(q).row(k).setZero();
        }

        ddy->rightCols(q).bottomRows(1).setZero();
      }

      return e;
    }

  }
}
