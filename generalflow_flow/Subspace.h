/**
 * @file    Subspace.h
 * @brief   A subspace with a basis
 * @author  Richard Roberts
 * @created Dec 28, 2010
 */
#pragma once

#include <generalflow_flow/dllexport.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/nonlinear/Values.h>

#include <string>

namespace generalflow {

  class generalflow_flow_EXPORT Subspace {
  protected:
    gtsam::Matrix basis_;
    gtsam::Matrix alignment_;
    double inlierSigma2_;
    double outlierSigma2_;
    double inlierPixSigma2_;
    double outlierPixSigma2_;
    double inlierPrior_;
    size_t width_;
    size_t height_;

  public:
    Subspace(const gtsam::Matrix& basis, double inlierSigma2, double outlierSigma2,
      double inlierPixSigma2, double outlierPixSigma2, double inlierPrior, size_t width, size_t height) :
      basis_(basis), inlierSigma2_(inlierSigma2), outlierSigma2_(outlierSigma2),
      inlierPixSigma2_(inlierPixSigma2), outlierPixSigma2_(outlierPixSigma2),
      inlierPrior_(inlierPrior), width_(width), height_(height) {}
    Subspace(const gtsam::Matrix& basis, double inlierSigma2, double outlierSigma2, double inlierPrior, size_t width, size_t height) :
      basis_(basis), inlierSigma2_(inlierSigma2), outlierSigma2_(outlierSigma2),
      inlierPixSigma2_(0.0), outlierPixSigma2_(0.0), inlierPrior_(inlierPrior), width_(width), height_(height) {}
    Subspace(size_t dimSpace, size_t dimLatent, double inlierSigma2, double outlierSigma2, double inlierPrior) :
      basis_(gtsam::zeros(dimSpace, dimLatent)), inlierSigma2_(inlierSigma2), outlierSigma2_(outlierSigma2),
      inlierPixSigma2_(0.0), outlierPixSigma2_(0.0), inlierPrior_(inlierPrior) {}
    Subspace(const std::string& path, int width = 0, int height = 0);
    Subspace() : inlierSigma2_(0.0), outlierSigma2_(0.0),
      inlierPixSigma2_(0.0), outlierPixSigma2_(0.0), inlierPrior_(0.0) {}
    Subspace(const gtsam::Values& values, gtsam::Key firstKey, double inlierPrior, int width, int height);

    void write(const std::string& path) const;

    const gtsam::Matrix& basis() const { return basis_; }
    gtsam::Matrix& basis() { return basis_; }

    const gtsam::Matrix& basisAlignment() const { return alignment_; }

    size_t dimSpace() const { return basis_.rows(); }
    size_t dimLatent() const { return basis_.cols(); }

    double inlierSigma2() const { return inlierSigma2_; }
    double& inlierSigma2() { return inlierSigma2_; }
    double outlierSigma2() const { return outlierSigma2_; }
    double& outlierSigma2() { return outlierSigma2_; }
    double inlierPixSigma2() const { return inlierPixSigma2_; }
    double& inlierPixSigma2() { return inlierPixSigma2_; }
    double outlierPixSigma2() const { return outlierPixSigma2_; }
    double& outlierPixSigma2() { return outlierPixSigma2_; }

    double inlierPrior() const { return inlierPrior_; }

    size_t width() const { return width_; }
    size_t height() const { return height_; }
  };

}

