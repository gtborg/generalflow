/**
* @file    ImgOps.cpp
* @brief
* @author  Richard Roberts
* @created Jan 12, 2011
*/

#include <generalflow_flow/ImgOps.h>
#include <gtsam/base/timing.h>
#include <gtsam/base/debug.h>
#include <math.h>
#include <boost/math/special_functions/round.hpp>

using namespace std;
using namespace gtsam;
using boost::math::lround;
using boost::math::round;

namespace generalflow {

  /* **************************************************************************************** */
  const vector<double>& ImgOps::derivKernel(double subpixelOffset) const {

    int subpixelQuant = lround(subpixelOffset * double(nCached_));
    vector<double>& cached(cachedKernels_[subpixelQuant]);
    if (cached.empty()) {
      //      cout << "Creating kernel for subpixel offset " << subpixelOffset << " (quant " << subpixelQuant << ")" << endl;
      cached.resize(size_);
      int ksize = int(size_);
      int krad = (ksize - 1) >> 1;
      double sum = 0.0;
      for (int i = 0; i < ksize; ++i) {
        double pos = double(i - krad) - subpixelOffset;
        sum += (cached[i] = exp(-0.5 * pos*pos));
        cached[i] *= pos;
      }
      for (int i = 0; i < ksize; ++i) {
        cached[i] /= sum;
      }
    }
    return cached;
  }

  /* **************************************************************************************** */
  static cv::Mat makeGaussianKernel(double sigma, size_t size, double subpixelOffset) {
    int ksize = int(size);
    cv::Mat kernel(ksize, 1, CV_64F);
    int krad = (ksize - 1) >> 1;
    double sigma2 = sigma * sigma;
    double sum = 0.0;
    for (int i = 0; i < ksize; ++i) {
      double pos = double(i - krad) - subpixelOffset;
      sum += (kernel.at<double>(i, 0) = exp(-0.5 * pos*pos / sigma2));
    }
    kernel *= (1.0 / sum);
    return kernel;
  }

  /* **************************************************************************************** */
  cv::Mat ImgOps::gaussianKernel(double subpixelOffset) const {
    if (nCached_ > 0) {
      int subpixelQuant = lround(subpixelOffset * double(nCached_));
      cv::Mat& cached(cachedGaussianKernels_[subpixelQuant]);
      if (cached.empty())
        cached = makeGaussianKernel(sigma_, size_, subpixelOffset);
      return cached;
    }
    else
      return makeGaussianKernel(sigma_, size_, subpixelOffset);
  }

  /* **************************************************************************************** */
  boost::optional<std::pair<double, gtsam::Point2> > ImgOps::linear(const cv::Mat& img, const cv::Mat& gradImgX, const cv::Mat& gradImgY, const gtsam::Point2 x) const {

    pair<double, Point2> ret;

    if (ISDEBUG("ImgOps::linear")) x.print("linearizing: ");

    // Get the subpixel resampling kernel - round to the nearest integer and get subpixel offset
    gttic(kernel);
    const int ksize = size();
    const int krad = (ksize - 1) / 2;
    double xsubpix = x.x() - round(x.x());
    double ysubpix = x.y() - round(x.y());
    const cv::Mat& xkernel(gaussianKernel(xsubpix));
    const cv::Mat& ykernel(gaussianKernel(ysubpix));
    gttoc(kernel);

    if (ISDEBUG("zero error"))
      cout << "xsubpix,ysubpix = " << xsubpix << ", " << ysubpix << endl;

    // Resample each image at the requested location
    long int imx = lround(x.x());
    long int imy = lround(x.y());
    if (ISDEBUG("ImgOps::linear")) cout << "val=" << img.at<double>(imy, imx) << ", grad = (" << gradImgX.at<double>(imy, imx) << ", " << gradImgX.at<double>(imy, imx) << ")" << endl;
    if (imx >= krad && imy >= krad && imx < img.cols - krad && imy < img.rows - krad) {
      gttic(convolve);
      double gradX = 0.0;
      double gradY = 0.0;
      double curimgval = 0.0;
      for (int i = -krad; i <= krad; ++i) {
        double jresX = 0.0;
        double jresY = 0.0;
        double jresCV = 0.0;
        for (int j = -krad; j <= krad; ++j) {
          assert(i + krad >= 0 && i + krad < ksize);
          assert(j + krad >= 0 && j + krad < ksize);
          jresX += xkernel.at<double>(j + krad, 0) * gradImgX.at<double>(imy + i, imx + j);
          jresY += xkernel.at<double>(j + krad, 0) * gradImgY.at<double>(imy + i, imx + j);
          jresCV += xkernel.at<double>(j + krad, 0) * img.at<double>(imy + i, imx + j);
        }
        gradX += ykernel.at<double>(i + krad, 0) * jresX;
        gradY += ykernel.at<double>(i + krad, 0) * jresY;
        curimgval += ykernel.at<double>(i + krad, 0) * jresCV;
      }
      gttoc(convolve);

      return make_pair(curimgval, Point2(gradX, gradY));
    }
    else {
      return boost::none;
    }
  }

  /* **************************************************************************************** */
  double ImgOps::resample(const cv::Mat& image, const gtsam::Point2& location) const {

    // Get the subpixel resampling kernel - round to the nearest integer and get subpixel offset
    const int ksize = size();
    const int krad = (ksize - 1) / 2;
    double xsubpix = location.x() - round(location.x());
    double ysubpix = location.y() - round(location.y());
    const cv::Mat& xkernel(gaussianKernel(xsubpix));
    const cv::Mat& ykernel(gaussianKernel(ysubpix));

    // Resample the image at the requested location
    long int imx = lround(location.x());
    long int imy = lround(location.y());
    // If the whole kernel is in-bounds
    if (imx >= krad && imy >= krad && imx < image.cols - krad && imy < image.rows - krad) {
      double filtered = 0.0;
      // Separated filter - filter in Y and X
      for (int i = -krad; i <= krad; ++i) {
        double filteredRow = 0.0;
        for (int j = -krad; j <= krad; ++j) {
          assert(i + krad >= 0 && i + krad < ksize);
          assert(j + krad >= 0 && j + krad < ksize);
          filteredRow += xkernel.at<double>(j + krad, 0) * image.at<double>(imy + i, imx + j);
        }
        filtered += ykernel.at<double>(i + krad, 0) * filteredRow;
      }

      return filtered;
    }
    else {
      return 0.0;
    }
  }

  /* **************************************************************************************** */
  double ImgOps::resampleBilinear(const cv::Mat& image, const gtsam::Point2& location) const {

    long int x1 = (long int) floor(location.x());
    long int x2 = (long int) ceil(location.x());
    long int y1 = (long int) floor(location.y());
    long int y2 = (long int) ceil(location.y());

    cout << "x1=" << x1 << ", x2=" << x2 << ", y1=" << y1 << ", y2=" << y2 << endl;

    // If only a fraction of a pixel out of the image, use the image edge
    if (x1 < 0)
      x1 = 0;
    if (x2 >= image.cols)
      x2 = image.cols - 1;
    if (y1 < 0)
      y1 = 0;
    if (y2 >= image.rows)
      y2 = image.rows - 1;
    // If completely out of the image, return 0
    if (x2 < 0 || y2 < 0 || x1 >= image.cols || y1 >= image.rows)
      return 0.0;

    // Blend
    double x1frac = location.x() - floor(location.x());
    double xblend1 = image.at<double>(y1, x1) * (1.0 - x1frac) + image.at<double>(y1, x2) * x1frac;
    double xblend2 = image.at<double>(y2, x1) * (1.0 - x1frac) + image.at<double>(y2, x2) * x1frac;
    double yfrac = location.y() - floor(location.y());
    double yblend = xblend1 * (1.0 - yfrac) + xblend2 * yfrac;

    return yblend;
  }

  /* **************************************************************************************** */
  pair<cv::Mat, cv::Mat> ImgOps::ComputeDerivatives(const cv::Mat& img) {
    cv::Mat gradImgX, gradImgY;
    gradImgX.create(img.rows, img.cols, img.depth());
    gradImgY.create(img.rows, img.cols, img.depth());
    cv::Sobel(img, gradImgX, img.depth(), 1, 0, 1, 1.0 / 2.0);
    cv::Sobel(img, gradImgY, img.depth(), 0, 1, 1, 1.0 / 2.0);
    return make_pair(gradImgX, gradImgY);
  }

  /* **************************************************************************************** */
  std::pair<gtsam::Matrix, gtsam::Matrix> ImgOps::ComputeDerivatives(const gtsam::Matrix& img) {
    cv::Mat cvImg(img.cols(), img.rows(), CV_64F, const_cast<double*>(&img(0, 0))); // Transposed
    Matrix gradX(img.rows(), img.cols()), gradY(img.rows(), img.cols());
    // Note reversed X,Y references, because the cv image is transposed so the X-Y gradients are reversed
    cv::Mat cvGradX(img.cols(), img.rows(), CV_64F, const_cast<double*>(&gradY(0, 0))); // Transposed
    cv::Mat cvGradY(img.cols(), img.rows(), CV_64F, const_cast<double*>(&gradX(0, 0))); // Transposed
    cv::Sobel(cvImg, cvGradX, cvImg.depth(), 1, 0, 1, 1.0 / 2.0);
    cv::Sobel(cvImg, cvGradY, cvImg.depth(), 0, 1, 1, 1.0 / 2.0);
    return make_pair(gradX, gradY);
  }

}
