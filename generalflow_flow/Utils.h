/*
 * Utils.h
 *
 *  Created on: May 5, 2010
 *      Author: richard
 */

#pragma once

#include <vector>
#include <ostream>

#include <boost/filesystem.hpp>
#include <generalflow_flow/dllexport.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/geometry/Point2.h>
#include <gtsam/base/Vector.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/geometry/Rot3.h>

#include <opencv2/core/core.hpp>

namespace generalflow {

class Utils {
private:
  Utils() {};

public:
  static generalflow_flow_EXPORT cv::Mat colorize(const cv::Mat& img);

  static generalflow_flow_EXPORT void drawFlow(cv::Mat& img, const std::vector<gtsam::Point2>& flow, int flowSamplePower, gtsam::Vector indicators, double scale = 1.0);

  static generalflow_flow_EXPORT gtsam::Matrix matrixOfMat(const cv::Mat& mat);

  static generalflow_flow_EXPORT cv::Mat matOfMatrix(const gtsam::Matrix& matrix);

  static generalflow_flow_EXPORT cv::Mat mat8uOfMatrix(const gtsam::Matrix& matrix);

  static generalflow_flow_EXPORT cv::Mat matOfVector(const gtsam::Vector& vector, size_t cols);

  static generalflow_flow_EXPORT cv::Mat scale(const cv::Mat& img, double factor, int interpolation);

  static generalflow_flow_EXPORT cv::Mat gray2rgb(const cv::Mat& img);

  static generalflow_flow_EXPORT int decodedTimestamps(size_t framei, const boost::filesystem::path& imageDir);

  static generalflow_flow_EXPORT cv::Mat grayAndUndistort(const cv::Mat& img, const cv::Mat& camMatrix, const cv::Mat& distCoeffs);

  static generalflow_flow_EXPORT cv::Mat grabImage(int framei, const boost::filesystem::path& imgdir, const std::string& imgFnFormat);

  /// Rectify a rotated image
  static generalflow_flow_EXPORT cv::Mat rectify(const cv::Mat& I2, const gtsam::Cal3_S2& K, const gtsam::Rot3& c0Rc);

  template<class T, class Values, class Value, class Key>
  static void plotError(const T& func, const Values& values0, const Key& key,
      const gtsam::Vector& steps, const std::vector<int>& range, const std::vector<std::string>& axisName) {

    Values updatedValues = values0;

    Value v0 = values0[key];
    gtsam::Vector up = gtsam::zero(v0.dim());

    double error;
    for (size_t i = 0; i<v0.dim(); i++) {
      std::cout << axisName[i] << " = [";
      for (int r=-range[i]; r<=range[i]; r++) {
        up[i] = r*steps[i];
        Value newVal = v0.retract(up);
        updatedValues.update(key, newVal);
        error = func.error(updatedValues);
        std::cout << up[i] << "  " << error << "\n";
      }
      up[i] = 0;
      std::cout << "]; figure; plot(" << axisName[i] << "(:,1)," << axisName[i] << "(:,2)); axis([-0.1 0.1 1980 2000]);\n"<< std::endl;
    }
  }

  template<class T, class Values, class Key>
  static void plotError(const T& func, const Values& values0, const Key& key,
      const gtsam::Vector& ranges, const gtsam::Vector& stepSizes, const std::vector<std::string>& axisName, std::ostream& stream) {

    typedef typename Key::Value Value;

    Values updatedValues = values0;

    Value v0 = values0[key];
    gtsam::Vector up = gtsam::zero(v0.dim());

    double error;
    for (size_t i = 0; i<v0.dim(); i++) {
      stream << axisName[i].c_str() << " = [";
      for (double d=-ranges[i]; d<=ranges[i]; d+=stepSizes[i]) {
        up[i] = d;
        Value newVal = v0.retract(up);
        updatedValues.update(key, newVal);
        error = func(updatedValues);
        stream << up[i] << "  " << error << "\n";
      }
      up[i] = 0;
      stream << "];\n";
    }
  }

  template<class T, class Values, class Key>
  static void plotError(const T& func, const Values& values0, const Key& key,
      const gtsam::Vector& ranges, double stepSize, const std::vector<std::string>& axisName, std::ostream& stream) {
    gtsam::Vector stepSizes = gtsam::repeat(ranges.rows(), stepSize);
    plotError<T,Values,Key>(func,values0,key,ranges,stepSizes,axisName,stream);
  }

  /// Computes the log of the sum of several values *whose logs* are in vectorOfLogs
  template<typename T>
  static typename T::Scalar logSum(const T& vectorOfLogs)
  {
    if(vectorOfLogs.size() == 0)
      return (typename T::Scalar)0.0;
    else
    {
      // Find max element
      typename T::Index maxIndex;
      vectorOfLogs.maxCoeff(&maxIndex);

      typename T::Scalar logSum = vectorOfLogs(maxIndex);
      for(typename T::Index i = 0; i < vectorOfLogs.size(); ++i)
        if(i != maxIndex)
          logSum += std::log((typename T::Scalar)1.0 + std::exp(vectorOfLogs(i) - logSum));

      return logSum;
    }
  }
};

}

