/**
 * @file    Subspace.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Dec 28, 2010
 */

#include "Subspace.h"
#include "Utils.h"

#include <fstream>
#include <cctype>
#include <sstream>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/filesystem.hpp>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/inference/Symbol.h>

using namespace std;
using namespace gtsam;

namespace generalflow {

  /* **************************************************************************************** */
  Subspace::Subspace(const string& path, int width, int height) {

    size_t blockDim;
    size_t latentDim;
    size_t nBlocks;
    int basisWidth;
    int basisHeight;

    cout << "Reading " << path << endl;

    // Read dimensions of arrays from .basissize file
    ifstream basissize((path + ".basissize").c_str());
    if(!basissize.is_open()) throw runtime_error("Could not open .basissize file");
    if(!(basissize >> latentDim >> blockDim >> nBlocks >> basisWidth >> basisHeight))
			throw runtime_error("Error reading basissize file");
    basissize.close();
    cout << latentDim << "-dim latent space, " << blockDim << "-dim blocks, " << nBlocks << " blocks." << endl;
    cout << "Basis dimensions: " << basisWidth << "x" << basisHeight << endl;
    if(basisWidth * basisHeight != nBlocks)
      throw runtime_error("Inconsistent basis size, width*height != nBlocks");

    // Allocate basis matrix
    basis_.resize(blockDim*nBlocks, latentDim);

    ifstream basis((path + ".W").c_str());
    if(!basis.is_open()) throw runtime_error("Could not open .basis file");
    for(size_t block=0; block<nBlocks; ++block) {
      // Eat whitespace
      while(!basis.eof() && isspace(basis.peek())) basis.get();
      if(!basis.eof()) {
        Matrix Wblock(zeros(blockDim, latentDim));
        for(size_t r=0; r<blockDim; ++r)
          for(size_t j=0; j<latentDim; ++j)
            basis >> basis_(block*blockDim + r,j);
      } else
        throw runtime_error("Unexpected EOF");
    }
    basis.close();

    ifstream sigmas((path + ".sigmas").c_str());
    if(!sigmas.is_open()) throw runtime_error("Could not open .sigmas file");
    sigmas >> inlierSigma2_ >> outlierSigma2_;
    inlierPixSigma2_ = 0.0;
    outlierPixSigma2_ = 0.0; // These may not be present in the file
    sigmas >> inlierPixSigma2_ >> outlierPixSigma2_;
    sigmas.close();
    cout << "Read " << path+".sigmas" << endl;

    inlierPrior_ = 0.5;

    if(width == 0 || height == 0) {
      width = basisWidth;
      height = basisHeight;
    }

    if(width != basisWidth || height != basisHeight) {
      if(basisWidth * basisHeight * 2 != dimSpace())
        throw runtime_error("Basis size is not consistent with its dimensions");
      cout<<"origCols origRows newCols newRows "<<basisWidth<<" "<<basisHeight<<" "<< width<<" "<<height<<endl;

      // Now scale
      Matrix bigBasis(width*height*2, dimLatent());
      for(size_t k=0; k<dimLatent(); ++k) {
        cv::Mat flowField(basisWidth, basisHeight, CV_64FC2, &basis_(0,k)); // Transposed
        cv::Mat bigField;
        cv::resize(flowField, bigField, cv::Size(height, width), 0, 0, CV_INTER_AREA); // Transposed
        bigBasis.col(k) = Eigen::Map<Vector>(&bigField.at<double>(0,0), width*height*2);
      }

      basis_ = bigBasis;
    }
    width_ = width;
    height_ = height;

    // Read alignment file if present
    if(boost::filesystem::is_regular_file(path + ".alignment")) {
      ifstream alignment((path + ".alignment").c_str());
      alignment >> alignment_;
    } else {
      alignment_ = Matrix::Identity(dimLatent(), dimLatent());
    }
  }

  /* **************************************************************************************** */
  Subspace::Subspace(const gtsam::Values& values, gtsam::Key firstKey, double inlierPrior, int width, int height) {
    // Basis
    Values::const_iterator it = values.find(firstKey);
    const int q = dynamic_cast<const gtsam::LieMatrix&>(it->value).cols();
    basis_.resize(2*width*height, q);
    gtsam::Key key = firstKey;
    int row = 0;
    for(int j = 0; j < width; ++j) {
      for(int i = 0; i < height; ++i) {
        if(it == values.end() || it->key != key)
          throw std::invalid_argument("Values did not contain the expected keys");
        const gtsam::LieMatrix& v = dynamic_cast<const gtsam::LieMatrix&>(it->value);
        basis_.block(row, 0, 2, q) = v.matrix();
        row += 2;
        ++ it;
        ++ key;
      }
    }

    // Sigmas
    if(values.exists(gtsam::symbol('S',0))) {
      Vector sigmas = values.at<LieVector>(gtsam::symbol('S',0)).vector();
      inlierSigma2_ = sigmas(0) * sigmas(0);
      outlierSigma2_ = sigmas(1) * sigmas(1);
      if(sigmas.size() == 4) {
        inlierPixSigma2_ = sigmas(2) * sigmas(2);
        outlierPixSigma2_ = sigmas(3) * sigmas(3);
      } else {
        inlierPixSigma2_ = 0.0;
        outlierPixSigma2_ = 0.0;
      }
    } else {
      inlierSigma2_ = 0.0;
      outlierSigma2_ = 0.0;
      inlierPixSigma2_ = 0.0;
      outlierPixSigma2_ = 0.0;
    }

    inlierPrior_ = inlierPrior;
    width_ = width;
    height_ = height;
  }

  /* **************************************************************************************** */
  void Subspace::write(const string& path) const {

    cout << "Writing " << path << endl;

    ofstream basissize((path + ".basissize").c_str());
    if(!basissize.is_open()) throw runtime_error("Could not open .basissize file");
    basissize << dimLatent() << " " << 2 << " " << (dimSpace()/2) << " " << width() << " " << height() << "\n";
    basissize.close();

    ofstream basis((path + ".W").c_str());
    if(!basis.is_open()) throw runtime_error("Could not open .basis file");
    for(size_t row=0; row<dimSpace(); ++row) {
      for(size_t j=0; j<dimLatent(); ++j)
        basis << basis_(row, j) << " ";
      basis << "\n";
    }
    basis.close();

    ofstream sigmas((path + ".sigmas").c_str());
    if(!sigmas.is_open()) throw runtime_error("Could not open .sigmas file");
    sigmas << inlierSigma2_ << " " << outlierSigma2_ << " " << inlierPixSigma2_ << " " << outlierPixSigma2_ << "\n";
    sigmas.close();

    cout << "Wrote " << path << endl;
  }

}
