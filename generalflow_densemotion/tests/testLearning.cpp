/**
* @file    testBasisFactor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Sept 26, 2013
*/

#include <CppUnitLite/TestHarness.h>

#include <fstream>
#include <boost/random.hpp>

#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/VectorValues.h>
#include <gtsam/base/numericalDerivative.h>

#include <generalflow_densemotion/BasisFactor.h>
#include <generalflow_densemotion/VelocityFactor.h>

using namespace gtsam;
using namespace std;
using namespace generalflow;
using namespace generalflow::densemotion;
using namespace generalflow::flow;

/* ************************************************************************* */
namespace {
  struct CallEvaluateError
  {
    const BasisFactor& factor;

    CallEvaluateError(const BasisFactor& factor) : factor(factor) {}

    Vector operator()(const LieMatrix& value) const
    {
      return factor.evaluateError(value);
    }
  };
}

/* ************************************************************************* */
TEST(BasisFactor, evaluateError)
{
  Matrix img1(6,6);
  img1 <<
      1, 2, 3, 4, 5, 6,
      1, 2, 3, 4, 5, 6,
      1, 2, 3, 4, 5, 6,
      1, 2, 3, 4, 5, 6,
      1, 2, 3, 4, 5, 6,
      1, 2, 3, 4, 5, 6;

  Matrix img2(6,6);
  img2 <<
      2, 3, 4, 5, 6, 7,
      2, 3, 4, 5, 6, 7,
      2, 3, 4, 5, 6, 7,
      2, 3, 4, 5, 6, 7,
      2, 3, 4, 5, 6, 7,
      2, 3, 4, 5, 6, 7;

  ImagePairGrad imgpair(img1, img2);

  cout << "prevGradX:\n" << imgpair.prevGradX() << endl;
  cout << "prevGradY:\n" << imgpair.prevGradY() << endl;

  Matrix basisX(6,6);
  basisX <<
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5, -0.5, -0.5, -0.5;
  Matrix basisY(6,6);
  basisY <<
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0;
  Matrix basis(basisX.cols() * basisX.rows()*2, 1);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis.data(), basis.rows() / 2) =
      Eigen::Map<Vector>(basisX.data(), basis.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis.data() + 1, basis.rows() / 2) =
      Eigen::Map<Vector>(basisY.data(), basis.rows() / 2);

  cout << "basis.transpose(): " << basis.transpose() << endl;

  Vector velocity(1);
  velocity << 2.0;
  Vector sigmas(4);
  sigmas << 0.1, 1.0, 0.01, 0.01;
  DenseIndex pixX = 2, pixY = 2;
  BasisFactor factor(0, velocity, sigmas, 1.0, boost::make_shared<ImagePairGrad>(imgpair), pixX, pixY);

  Vector expectedErrorVector = Vector::Zero(1);
  Matrix ddW;
  Vector actualErrorVector = factor.evaluateError(LieMatrix(basis.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis.cols())),
    ddW);

  // Check numerical derivatives
  Matrix expectedddW = numericalDerivative11<LieMatrix>(
    boost::function<Vector(const LieMatrix&)>(CallEvaluateError(factor)),
    LieMatrix(basis.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis.cols())));

  EXPECT(assert_equal(expectedErrorVector, actualErrorVector));
  EXPECT(assert_equal(expectedddW, ddW));
}

#if 0
/* ************************************************************************* */
TEST(BasisFactor, convergence)
{
  boost::random::mt19937 rng;
  std::vector<boost::shared_ptr<ImagePairGrad> > imagePairs;
  const int wid = 50, hei = 50, npix = wid*hei;
  const int q = 2;

  for(int img = 0; img < 50; ++img)
  {
    Matrix testImg1(50, 50);
    Matrix testImg2(50, 50);

    boost::random::uniform_real_distribution<> ctr(0, 50);
    boost::random::uniform_real_distribution<> shift(-0.5, 0.5);

    double ctrx = ctr(rng), ctry = ctr(rng);
    double shiftx = shift(rng), shifty = shift(rng);

    for(int j = 0; j < wid; ++j) {
      for(int i = 0; i < hei; ++i) {
        double dy = double(i) - ctry;
        double dx = double(j) - ctrx;
        double r = sqrt(dx*dx + dy*dy);
        testImg1(i,j) = r / hei;

        dy = (double(i) - shifty) - ctry;
        dx = (double(j) - shiftx) - ctrx;
        r = sqrt(dx*dx + dy*dy);
        testImg2(i,j) = r / hei;
      }
    }

    ofstream f("testBasisFactor_convergence_img1.txt");
    f << testImg1;
    f.close();
    f.open("testBasisFactor_convergence_img2.txt");
    f << testImg2;
    f.close();

    imagePairs.push_back(boost::make_shared<ImagePairGrad>(testImg1, testImg2));
  }

  Vector sigmas(4); sigmas << 0.1, 0.5, 0.01, 0.01;
  const double inlierPrior = 0.5;

  // Random initial basis
  Matrix basis = Matrix::Random(npix * 2, q);
  Values velocities;
  for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    velocities.insert(frame, LieVector(Vector::Random(q)));

  for(int it = 1; it <= 20; ++it)
  {
    // Update velocity
    NonlinearFactorGraph graph;
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    {
      graph += VelocityFactor(frame, basis, sigmas, inlierPrior, *imagePairs[frame]);
      graph += PriorFactor<LieVector>(frame, LieVector(Vector::Zero(q)), noiseModel::Unit::Create(q));
    }
    dynamic_cast<const VelocityFactor&>(*graph[0]).inspection(true);
    velocities = velocities.retract(graph.linearize(velocities)->optimize());
    ofstream inliersfile((boost::format("testBasisFactor_convergence_inliers_%04d.txt")%it).str().c_str());
    inliersfile << dynamic_cast<const VelocityFactor&>(*graph[0]).lastInlierImage();
    inliersfile.close();

    // Store current basis in a Values
    Values basisValues;
    for(size_t pix = 0; pix < npix; ++pix)
      basisValues.insert(pix, LieMatrix(basis.block(2*pix, 0, 2, q)));

    // Update basis
    NonlinearFactorGraph basisGraph;
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    {
      size_t pix = 0;
      for(DenseIndex j = 0; j < wid; ++j) {
        for(DenseIndex i = 0; i < hei; ++i) {
          basisGraph += BasisFactor(pix, velocities.at<LieVector>(frame), sigmas, inlierPrior, imagePairs[frame], j, i);
          ++ pix;
        }
      }
    }

    //size_t pix = 0;
    //for(DenseIndex j = 0; j < wid; ++j) {
    //  for(DenseIndex i = 0; i < hei; ++i) {
    //    basisGraph += PriorFactor<LieMatrix>(pix, LieMatrix(Matrix::Zero(2, q)),
    //      noiseModel::Isotropic::Sigma(2*q, 100000.0));
    //    ++ pix;
    //  }
    //}
    
    for(DenseIndex j = 1; j < wid; ++j) {
      for(DenseIndex i = 0; i < hei; ++i) {
        size_t pix = j*hei + i;
        size_t pixL = (j-1)*hei + i;
        basisGraph += BetweenFactor<LieMatrix>(pix, pixL, LieMatrix(Matrix::Zero(2, q)),
          noiseModel::Isotropic::Sigma(2*q, 1.0));
      }
    }
    for(DenseIndex j = 0; j < wid; ++j) {
      for(DenseIndex i = 1; i < hei; ++i) {
        size_t pix = j*hei + i;
        size_t pixU = j*hei + (i-1);
        basisGraph += BetweenFactor<LieMatrix>(pix, pixU, LieMatrix(Matrix::Zero(2, q)),
          noiseModel::Isotropic::Sigma(2*q, 1.0));
      }
    }

    // Check derivatives
    //BOOST_FOREACH(const NonlinearFactor::shared_ptr& factor, basisGraph)
    //{
    //  if(const BasisFactor* basisFactor = dynamic_cast<const BasisFactor*>(factor.get()))
    //  {
    //    CallEvaluateError e(*basisFactor);
    //    Matrix expected = numericalDerivative11<LieMatrix>(e, basisValues.at<LieMatrix>(basisFactor->key()));
    //    Matrix actual;
    //    (void) basisFactor->evaluateError(basisValues.at<LieMatrix>(basisFactor->key()), actual);
    //    bool result = assert_equal(expected, actual);
    //    CHECK(result);
    //  }
    //}

    try {
      basisValues = basisValues.retract(basisGraph.linearize(basisValues)->optimize(boost::none, EliminateQR));
    }
    catch(std::exception& e)
    {
      cout << e.what() << endl;
      throw;
    }
    cout << "Total error = " << graph.error(velocities) + basisGraph.error(basisValues) << endl;

    // Reassemble basis
    for(size_t pix = 0; pix < npix; ++pix)
      basis.block(2*pix, 0, 2, q) = basisValues.at<LieMatrix>(pix);

    ofstream basisfile((boost::format("testBasisFactor_convergence_basis_%04d.txt")%it).str().c_str());
    basisfile << basis;

    ofstream velfile((boost::format("testBasisFactor_convergence_velocity_%04d.txt")%it).str().c_str());
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
      velfile << velocities.at<LieVector>(0).transpose() << "\n";

    cout << "Finished iteration " << it << endl;
  }
}
#endif

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */


