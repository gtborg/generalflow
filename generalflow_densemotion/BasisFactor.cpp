/**
* @file    BasisFactor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <generalflow_densemotion/BasisFactor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace densemotion {

    inline double sq(double x) { return x*x; }

    const gtsam::SharedNoiseModel BasisFactor::unitNoise1 = gtsam::noiseModel::Unit::Create(1);

    /* **************************************************************************************** */
    void BasisFactor::print(const string& s, const KeyFormatter& keyFormatter) const
    {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool BasisFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const
    {
      const BasisFactor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(velocity_, t->velocity_, tol) &&
        equal_with_abs_tol(sigmas_, t->sigmas_, tol) &&
        abs(inlierPrior_ - t->inlierPrior_) <= tol &&
        images_ == t->images_ && pixX_ == t->pixX_ && pixY_ == t->pixY_;
    }

    /* **************************************************************************************** */
    Vector BasisFactor::evaluateError(const LieMatrix& basis, boost::optional<Matrix&> H) const
    {
      // References to values for convenience
      const int imWidth = images_->curFrame().cols();
      const int imHeight = images_->curFrame().rows();

      double inlier;
      return flow::fullImageError(*images_, pixX_, pixY_, imWidth, imHeight, inlierPrior_, basis,
        velocity_, sigmas_(0), sigmas_(1), sigmas_(2), sigmas_(3), inlier,
        H, boost::none, boost::none, boost::none, boost::none, boost::none);
    }

  }
}
