/**
* @file    VelocityFactor.h
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#pragma once

#include <generalflow_densemotion/dllexport.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <generalflow_flow/flowOps.h>

namespace generalflow {
  namespace densemotion {

    class generalflow_densemotion_EXPORT BasisFactor : public gtsam::NoiseModelFactor1<gtsam::LieMatrix> {
    public:
      typedef gtsam::NoiseModelFactor1<gtsam::LieMatrix> Base;
      typedef BasisFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Vector velocity_;
      gtsam::Vector sigmas_;

      double inlierPrior_;
      flow::ImagePairGrad::shared_ptr images_;
      gtsam::DenseIndex pixX_;
      gtsam::DenseIndex pixY_;

      static const gtsam::SharedNoiseModel unitNoise1;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      BasisFactor(gtsam::Key basisKey, const gtsam::Vector& velocity, const gtsam::Vector& sigmas,
                     double inlierPrior, const flow::ImagePairGrad::shared_ptr& images, gtsam::DenseIndex pixX, gtsam::DenseIndex pixY)
        : Base(unitNoise1, basisKey), velocity_(velocity), sigmas_(sigmas),
        inlierPrior_(inlierPrior), images_(images), pixX_(pixX), pixY_(pixY) {}

      /** Virtual destructor */
      virtual ~BasisFactor() {}

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 1; }

      /** Evaluate error */
      virtual gtsam::Vector evaluateError(const gtsam::LieMatrix& basis, boost::optional<gtsam::Matrix&> H = boost::none) const;
    };

  }
}
