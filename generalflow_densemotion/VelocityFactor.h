/**
* @file    VelocityFactor.h
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#pragma once

#include <generalflow_densemotion/dllexport.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <generalflow_flow/flowOps.h>

namespace generalflow {
  namespace densemotion {

    class generalflow_densemotion_EXPORT VelocityFactor : public gtsam::NonlinearFactor {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef VelocityFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Matrix basis_;
      gtsam::Vector sigmas_;

      double inlierPrior_;
      flow::ImagePairGrad images_;
      
      mutable bool inspection_;
      mutable gtsam::Matrix lastErrorImage_;
      mutable gtsam::Matrix lastInlierImage_;
      mutable gtsam::Matrix lastJacobians_;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      VelocityFactor(gtsam::Key latentKey, const gtsam::Matrix& basis, const gtsam::Vector& sigmas, 
                     double inlierPrior, const flow::ImagePairGrad& images)
        : Base(gtsam::ListOfOne(latentKey)), basis_(basis), sigmas_(sigmas),
        inlierPrior_(inlierPrior), images_(images), inspection_(false) {}

      /** Virtual destructor */
      virtual ~VelocityFactor() {}

      gtsam::Key latentKey() const { return Base::keys_[0]; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 1; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;

      void inspection(bool enable) const { inspection_ = enable; }
      gtsam::Matrix lastErrorImage() const { return lastErrorImage_; }
      gtsam::Matrix lastInlierImage() const { return lastInlierImage_; }
      gtsam::Matrix lastJacobians() const { return lastJacobians_; }
    };

  }
}
