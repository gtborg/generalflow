#include <generalflow_densemotion/VelocityFactor.h>
#include <generalflow_flow/Utils.h>

#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/base/LieVector.h>

#include <generalflow_flow/flowOps.h>

#include <opencv2/highgui/highgui.hpp>

using namespace gtsam;
using namespace std;
using namespace generalflow;
using namespace generalflow::densemotion;

int main(int argc, char* argv[])
{
  Matrix testImg1(10, 10);
  Matrix testImg2(10, 10);

  for(int j = 0; j < testImg1.cols(); ++j)
	  for(int i = 0; i < testImg1.rows(); ++i) {
		  double dy = i - (testImg1.rows() / 2);
		  double dx = j - (testImg1.cols() / 2);
		  double r = sqrt(dx*dx + dy*dy);
		  testImg1(i,j) = r / testImg1.rows();

		  dy = i - (testImg1.rows() / 2);
		  dx = (j - 0.5) - (testImg1.cols() / 2);
		  r = sqrt(dx*dx + dy*dy);
		  testImg2(i,j) = r / testImg1.rows();
	  }

  cv::Mat img1(testImg1.cols(), testImg1.rows(), CV_64FC1, &testImg1(0,0));
  cv::resize(img1, img1, cv::Size(100,100));
  cv::imshow("Image1", Utils::colorize(img1));
  cv::Mat img2(testImg2.cols(), testImg2.rows(), CV_64FC1, &testImg2(0,0));
  cv::resize(img2, img2, cv::Size(100,100));
  cv::imshow("Image2", Utils::colorize(img2));


  //Create basis flow with test image size
  Matrix basis(2 * testImg1.rows() * testImg1.cols(), 2);
  Matrix block(2,2);
  block << 1,0,0,1;

  for(int index = 0; index < basis.rows() /2 ; index++){
    basis.block(2*index,0,2,2) << block;
  }
  const Matrix basic = basis;

  //Print statement
  std::cout << basis << std::endl;
  

  Vector sigmas(4);
  sigmas << 1.0, 10.0, 0.01, 0.01;
  double inlierPrior = 0.01;
 
  
  //VelocityFactor flowFactor(const gtsam::Matrix& basis, const gtsam::Vector sigmas,
                            //double inlierPrior, int frameNum, boost::shared_ptr<FrameCache> frameCache);

  //Fill in constructor arguments
  flow::ImagePairGrad images(testImg1, testImg2);
  boost::shared_ptr<VelocityFactor> flowFactor = boost::make_shared<VelocityFactor>(
		  symbol('v', 0), basic, sigmas, inlierPrior, images);
  flowFactor->inspection(true);

  NonlinearFactorGraph graph;
  graph += flowFactor;
  Values initial;
  
  initial.insert(symbol('v', 0), LieVector(Vector::Zero(2)));
  
  GaussNewtonOptimizer optimizer(graph, initial);
  for(int it = 0; it < 15; ++it)
	  optimizer.iterate();
  Values final = optimizer.values();
  
  final.print();
  
  cout << flowFactor->lastInlierImage() << endl;

  Eigen::Matrix<double, -1, -1, Eigen::RowMajor> gradXM = images.prevGradX();
  cv::Mat gradX(gradXM.cols(), gradXM.rows(), CV_64FC1, &gradXM(0,0));
  cv::resize(gradX, gradX, cv::Size(200,200));
  cv::Mat gradXcolor = Utils::colorize(gradX);
  cv::imshow("gradX", 10*gradXcolor);

  Eigen::Matrix<double, -1, -1, Eigen::RowMajor> gradYM = images.prevGradY();
  cv::Mat gradY(gradYM.cols(), gradYM.rows(), CV_64FC1, &gradYM(0,0));
  cv::resize(gradY, gradY,  cv::Size(200,200));
  cv::Mat gradYcolor = Utils::colorize(gradY);
  cv::imshow("gradY", 10*gradYcolor);

  cv::waitKey(0);

  return 0;
}
