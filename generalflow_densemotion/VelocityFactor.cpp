/**
* @file    VelocityFactor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <tbb/tbb.h>

#include <generalflow_densemotion/VelocityFactor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

#define PRINT(A) ((void)(cout << #A ": " << (A) << endl))

namespace generalflow {
  namespace densemotion {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    void VelocityFactor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool VelocityFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const VelocityFactor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(basis_, t->basis_, tol) &&
        equal_with_abs_tol(sigmas_, t->sigmas_, tol) &&
        abs(inlierPrior_ - t->inlierPrior_) <= tol &&
        images_.equals(t->images_, tol);
    }

    /* **************************************************************************************** */
    double VelocityFactor::error(const Values& x) const {

      double error = 0.0;
      // References to values for convenience
      const LieVector& latent = x.at<LieVector>(latentKey());
      const int imWidth = images_.curFrame().cols();
      const int imHeight = images_.curFrame().rows();
      const DenseIndex q = basis_.cols();

      // Loop over all pixels doing low-rank updates on the Hessian
      DenseIndex pix = 0;
      for (int j = 0; j < imWidth; ++j) {
        for (int i = 0; i < imHeight; ++i) {
          Matrix Wi = basis_.block(2*pix ,0,2,q);

          // Compute sqrt error and derivatives
          double inlier;
          Vector e = flow::fullImageError(images_, j, i, imWidth, imHeight,
            inlierPrior_, Wi, latent, sigmas_(0), sigmas_(1), sigmas_(2), sigmas_(3), inlier,
            boost::none, boost::none, boost::none, boost::none, boost::none, boost::none);
          error += e.squaredNorm();
          
          ++pix;
        }
      }
      return 0.5 * error;
    }

    typedef tbb::spin_mutex HessianMutex;

    /* **************************************************************************************** */
    namespace {
      struct LinearizeChunk
      {
        HessianMutex& mutex;
        HessianFactor& hessian;
        const Matrix& basis_;
        const flow::ImagePairGrad& images_;
        const LieVector& latent;
        const double inlierPrior_;
        const Vector& sigmas_;
        const bool inspection_;
        Matrix& lastInlierImage_;
        Matrix& lastJacobians_;

        LinearizeChunk(HessianMutex& mutex, HessianFactor& hessian, const Matrix& basis_, const flow::ImagePairGrad& images_,
            const LieVector& latent, const double inlierPrior_,
            const Vector& sigmas_, const bool inspection_, Matrix& lastInlierImage_, Matrix& lastJacobians_) :
              mutex(mutex), hessian(hessian), basis_(basis_), images_(images_), latent(latent), inlierPrior_(inlierPrior_), sigmas_(sigmas_),
              inspection_(inspection_), lastInlierImage_(lastInlierImage_), lastJacobians_(lastJacobians_) {}

        void operator()(const tbb::blocked_range<size_t>& r) const
        {
          const DenseIndex q = basis_.cols();
          const int imWidth = images_.curFrame().cols();
          const int imHeight = images_.curFrame().rows();

          HessianFactor chunkHessian = HessianFactor(JacobianFactor(hessian.keys().front(), Matrix(0, q), Vector()));
          Scatter scatter;
          scatter.insert(make_pair(hessian.keys().front(), SlotEntry(0, q)));

          Matrix Wi;
          Matrix ddy;
          Vector e;
          JacobianFactor pixJacobian(hessian.keys().front(), Matrix::Zero(1, q), Vector::Zero(1));

          for(size_t pix = r.begin(); pix != r.end(); ++pix)
          {
            int i = pix % imHeight;
            int j = pix / imHeight;

            Wi = basis_.block(2*pix ,0,2,q);

            // Compute sqrt error and derivatives
            double inlier;
            e = flow::fullImageError(images_, j, i, imWidth, imHeight,
              inlierPrior_, Wi, latent, sigmas_(0), sigmas_(1), sigmas_(2), sigmas_(3), inlier,
              boost::none, ddy, boost::none, boost::none, boost::none, boost::none);

            // Build temporary JacobianFactor to do low-rank update easily
            pixJacobian.getA(pixJacobian.begin()) = ddy;
            pixJacobian.getb() = -e;

            if (inspection_) {
              lastInlierImage_(i, j) = inlier;
              lastJacobians_.row(pix) = ddy;
            }

            // Apply low-rank update
            chunkHessian.updateATA(pixJacobian, scatter);
          }

          {
            HessianMutex::scoped_lock lock(mutex);
            hessian.updateATA(chunkHessian, scatter);
          }
        }
      };
    }

    /* **************************************************************************************** */
    boost::shared_ptr<GaussianFactor> VelocityFactor::linearize(const Values& x) const {

      const DenseIndex q = basis_.cols();
      const int imWidth = images_.curFrame().cols();
      const int imHeight = images_.curFrame().rows();

      // Create Hessian
      size_t nPix = imWidth * imHeight;
      HessianFactor::shared_ptr hessian = boost::make_shared<HessianFactor>(
        JacobianFactor(latentKey(), Matrix(0, q), Vector(), noiseModel::Unit::Create(0)));

      // References to values for convenience
      const LieVector& latent = x.at<LieVector>(latentKey());

      if (inspection_) {
        lastErrorImage_ = images_.curFrame() - images_.prevFrame();
        lastInlierImage_ = Matrix(lastErrorImage_.rows(), lastErrorImage_.cols());
        lastJacobians_ = Matrix(imWidth*imHeight, q);
      }

      // Loop over all pixels doing updates on the Hessian
      HessianMutex hessianMutex;
      tbb::parallel_for(tbb::blocked_range<size_t>(0, nPix),
          LinearizeChunk(hessianMutex, *hessian, basis_, images_, latent, inlierPrior_, sigmas_, inspection_, lastInlierImage_, lastJacobians_));

      return hessian;
    }
  }
}
