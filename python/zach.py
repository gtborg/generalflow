
import matplotlib;matplotlib.use('QT4Agg')

from numpy import *
from guiqwt.builder import make
from guiqwt.plot import CurveDialog
#from guiqwt.pyplot import *
import guiqwt
import matplotlib.pyplot as plt

u = matrix([[-.1,0]])
v = matrix([[0,0]])

def I0(x): return 0.5*x
def I1(x): return 0.5*(1.1*x)
def Ix0(x): return 0.5
def Ix1(x): return 0.5*1.1

def rho(u,x): return I1(x) + u*Ix1(x) - I0(x)
def Deltau(u): return hstack((u[:,0] - u[:,1], u[:,0] - u[:,1]))

x0 = matrix([[ 10, 11 ]])
def e(lam): return lambda u: sum(lam*abs(rho(u,x0)) + abs(Deltau(u)), axis=1)

lam = 1.0
Theta = 0.01
def ea(u,v):
    return sum(abs(Deltau(u)) + 1/(2*Theta)*power(u-v, 2) + lam*abs(rho(v,x0)), axis=1)
def eaU(v): return lambda u: ea(u, v)
def eaV(u): return lambda v: ea(u, v)

px = matrix(arange(-5.0, 5.0, 0.01)).transpose()
py = e(lam)(hstack((px, matrix(tile(0, [px.shape[0],1])))))
pyu = (lambda u1: eaU(repeat(v, size(u1,0), 0))
                    (hstack((u1, repeat(matrix(0), size(u1,0), 0))))) \
     (matrix(arange(-5.0, 5.0, 0.01)).transpose())
pyv = (lambda v1: eaV(repeat(u, size(v1,0), 0))
                    (hstack((v1, repeat(matrix(0), size(v1,0), 0))))) \
     (matrix(arange(-5.0, 5.0, 0.01)).transpose())

#import guidata
#_app = guidata.qapplication()
#win = CurveDialog(edit=True, toolbar=True)
#win.get_plot().add_item(make.curve(px, py, color='r'))
#win.get_plot().add_item(make.curve(px, pyu, color='g'))
#win.get_plot().add_item(make.curve(px, pyv, color='b'))
#win.show()
#win.exec_()

#plt.ion()

import mpldatacursor

lines = [
         plt.plot(px, py, 'r')[0],
         plt.plot(px, pyu, 'g')[0],
         plt.plot(px, pyv, 'b')[0]]
mpldatacursor.HighlightingDataCursor(lines)

plt.show()

