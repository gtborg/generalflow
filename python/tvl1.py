# Functions for TV-L1

import image
from numpy import *
import scipy.ndimage as ndimg

def imageResidual(I0, I1, dI1, x, u0, u):
    u0iRounded = round(u0[0])
    u0jRounded = round(u0[1])
    p0i = x[0] + u0iRounded
    p0j = x[1] + u0jRounded
    return I1[p0i,p0j] + (u[0]-u0iRounded)*dI1[p0i,p0j,0] + (u[1]-u0jRounded)*dI1[p0i,p0j,1] - I0[x]

def regResidual(U, x):
    i = x[0]
    j = x[1]

    if i == 0:
        graduY = 0.5 * (U[i+1, j] - U[i, j])
    elif i == U.shape[0]:
        graduY = 0.5 * (U[i, j] - U[i-1, j])
    else:
        graduY = U[i+1, j] - U[i-1, j]

    if j == 0:
        graduX = 0.5 * (U[i, j+1] - U[i, j])
    elif j == U.shape[1]:
        graduX = 0.5 * (U[i, j] - U[i, j-1])
    else:
        graduX = U[i, j+1] - U[i, j+1]

    return linalg.norm(graduX) + linalg.norm(graduY)

kernelSize = 2
if kernelSize == 2:
    divKernelHorz = image.divKernelHorz2
    divKernelVert = image.divKernelVert2
    gradKernel = image.gradKernel2
elif kernelSize == 3:
    divKernelHorz = image.divKernelHorz3
    divKernelVert = image.divKernelVert3
    gradKernel = image.gradKernel3
else:
    raise('Invalid kernel size');

def solveU(theta, tau, v, debug=False):
    p = (zeros(v.shape), zeros(v.shape))
    nIts = 15
    for i in range(0,nIts):
        p = iterateP(theta, tau, v, p, debug=debug, debugIter=(i, nIts))
    u = v + theta * dstack((image.convolveSeparableWithD(p[0], divKernelHorz, divKernelVert),
                            image.convolveSeparableWithD(p[1], divKernelHorz, divKernelVert)))
    return u

def solveV(I0, I1, dI1, lam, theta, u, debug=False):
    v = zeros(u.shape)
    I,J = meshgrid(range(I0.shape[0]), arange(I0.shape[1]))
    for x in zip(I.flat, J.flat):
        #residual = imageResidual(I0, I1, dI1, x, u[x], u[x])
        residual = imageResidual(I0, I1, dI1, x, (0.0,0.0), u[x])
        #dI1x = dI1[tuple(array(x) + around(u[x]).astype(int))]
        dI1x = dI1[x]
        dI1xSquaredNorm = vdot(dI1x, dI1x)
        threshold = lam * theta * dI1xSquaredNorm
        if residual < -threshold:
            step = lam * theta * dI1x
        elif residual > threshold:
            step = -lam * theta * dI1x
        else:
            step = -residual * (dI1x / dI1xSquaredNorm if dI1xSquaredNorm > 1e-7 else 0.0)
        v[x] = u[x] + step
    return v

def iterateP(theta, tau, v, p0, debug=False, debugIter=(0,0)):
    p = ()

    if debug:
        import matplotlib.pyplot as plt
        import matplotlib.cm as cm
        import matplotlib.gridspec as gridspec
        import flowdisplay as fd
        plt.figure(10)
        gs = gridspec.GridSpec(debugIter[1], 5)
        gs.update(left = 0.05, right = 0.95, bottom = 0.05, top = 0.95, wspace = 0.05)

    for d, p0d in enumerate(p0):
        print('Start {0}'.format(d))
        divP = image.convolveSeparableWithD(p0d, divKernelHorz, divKernelVert, origin=0)
        uc = v[:,:,d] + theta * divP
        gradKernelOrigin = -1 if gradKernel.shape[0] == 2 else 0
        gradUc = dstack((ndimg.correlate1d(uc, gradKernel, axis=1, mode='nearest', origin=gradKernelOrigin),
                         ndimg.correlate1d(uc, gradKernel, axis=0, mode='nearest', origin=gradKernelOrigin)))
        #p += ((p0d + tau * gradDivPmV) / (1 + tau * sum(gradDivPmV.flat)),)
        norms = maximum(1.0, tau / theta * sqrt(sum(gradUc * gradUc, axis=2)))
        p += ((p0d + tau / theta * gradUc) / dstack((norms,norms)),)
        print('Finish {0}'.format(d))
        
        if debug:
            plt.subplot(gs[debugIter[0], 0+d])
            plt.imshow(fd.computeColor(p0d, norm=True), interpolation='nearest')
            plt.title('p[%s]' % ('X','Y')[d])

            plt.subplot(gs[debugIter[0], 2+d])
            plt.imshow(divP, interpolation='nearest', cmap=cm.gray)
            plt.title('divP[%s]' % ('X','Y')[d])

            #plt.subplot(gs[debugIter[0], 4+d])
            #plt.imshow(fd.computeColor(uc, norm=True), interpolation='nearest')
            #plt.title('gradDivPmV[%s]' % ('X','Y')[d])

    if debug:
        u = v + theta * dstack((image.convolveSeparableWithD(p[0], divKernelHorz, divKernelVert),
                                image.convolveSeparableWithD(p[1], divKernelHorz, divKernelVert)))
        plt.subplot(gs[debugIter[0], 4])
        plt.imshow(fd.computeColor(u), interpolation='nearest')
        plt.title('u')

    return p


