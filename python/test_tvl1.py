from numpy import *
import tvl1
import flowdisplay
import image
from matplotlib.pyplot import *
from matplotlib import cm
from mpldatacursor import datacursor

testV = zeros((100,100,2))
i,j = meshgrid(range(size(testV,0)), range(size(testV,1)))
x = i - size(testV,0) / 2
y = j - size(testV,0) / 2
mag = sqrt(x*x + y*y)
testV[mag < 10] = [0.5,0]

figure(1)

subplot(2,2, 1)
imshow(flowdisplay.computeColor(testV), interpolation='nearest')
title('V')
subplot(2,2, 2)
imshow(flowdisplay.colorWheelImage(100), interpolation='nearest')

tau = 1.0 / sqrt(8.0)
theta = 1.0
p0 = (zeros(testV.shape), zeros(testV.shape))

#subplot(3,2, 3)
#imshow(flowdisplay.computeColor(p1[0]), interpolation='nearest')
#title('p1[X]')
#subplot(3,2, 4)
#imshow(flowdisplay.computeColor(p1[1]), interpolation='nearest')
#title('p1[Y]')

#u = tvl1.solveU(theta, tau, testV, debug=True)

#figure(1)
#subplot(2,2, 3)
#imshow(flowdisplay.computeColor(u), interpolation='nearest')
#title('u')




import scipy.ndimage as ndimage
import matplotlib.gridspec as gridspec

I0 = zeros((100,100))
for i in range(I0.shape[0]):
    for j in range(I0.shape[1]):
        I0[i,j] = sin(i) + sin(j)
i,j = meshgrid(range(size(I0,0)), range(size(I0,1)))
x = i - size(I0,0) / 2
y = j - size(I0,0) / 2
mag = sqrt(x*x + y*y)

I1 = zeros((100,100))
i,j = meshgrid(range(size(I1,0)), range(size(I1,1)))
x = i - size(I1,0) / 2 - 1
y = j - size(I1,0) / 2
mag = sqrt(x*x + y*y)
for i in range(I0.shape[0]):
    for j in range(I0.shape[1]):
        if mag[i,j] < 10.0:
            I1[i,j] = sin(i) + sin(j-0.5)
        else:
            I1[i,j] = sin(i) + sin(j)

savetxt('I0.txt', I0)
savetxt('I1.txt', I1)

dI1 = dstack((ndimage.correlate1d(I1, image.gradKernel3, axis=1, mode='nearest'),
              ndimage.correlate1d(I1, image.gradKernel3, axis=0, mode='nearest')))

lam = 1.0

figure(11)
nIts = 5
nFigRows = 2
skip = 1
gs = gridspec.GridSpec(2*nFigRows, int(ceil((nIts/skip)/float(nFigRows))))
gs.update(left = 0.05, right = 0.95, bottom = 0.05, top = 0.95, wspace = 0.05)
u = zeros(I0.shape + (2,))
for i in range(nIts):
    v = tvl1.solveV(I0, I1, dI1, lam, theta, u)
    u = tvl1.solveU(theta, tau, v)
    if i % skip == 0:
        subplot(gs[0+2*((i/skip)%nFigRows), i/skip/nFigRows])
        imshow(flowdisplay.computeColor(v), interpolation='nearest')
        title('v @ {0}'.format(i))
        subplot(gs[1+2*((i/skip)%nFigRows), i/skip/nFigRows])
        imshow(flowdisplay.computeColor(u), interpolation='nearest')
        title('u @ {0}'.format(i))

figure(2)
subplot(2,2, 1)
imshow(I0, interpolation='nearest', cmap=cm.gray)
subplot(2,2, 2)
imshow(I1, interpolation='nearest', cmap=cm.gray)
subplot(2,2, 3)
imshow(flowdisplay.computeColor(dI1), interpolation='nearest')
subplot(2,2, 4)
imshow(flowdisplay.computeColor(v, norm=True), interpolation='nearest')

datacursor()

show()
