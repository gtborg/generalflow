# Image functions

from numpy import *
from scipy import *
import scipy.ndimage as ndimage

divKernelHorz3 = array([[-1,1], [0,1], [1,1]])
divKernelVert3 = array([[1,-1], [1,0], [1,1]])
gradKernel3 = array([-0.5, 0.0, 0.5])

divKernelHorz2 = array([[-1,1], [1,1]])
divKernelVert2 = array([[1,-1], [1,1]])
gradKernel2 = array([-1,1])

#gradDivKernelHorzX = correlate1d(concatenate(([[0,0]],divKernelHorz,[[0,0]])),
#                                 weights=concatenate(([0],gradKernel,[0])))
#gradDivKernelVertX = concatenate(([[0,0]],divKernelVert,[[0,0]]))
#gradDivKernelHorzY = concatenate(([[0,0]],divKernelHorz,[[0,0]]))
#gradDivKernelVertY = correlate1d(concatenate(([[0,0]],divKernelVert,[[0,0]])),
#                                 weights=concatenate(([0],gradKernel,[0])))

def div(U):
    divUx = (ndimage.correlate1d(U[0], divKernelHorzX, axis=1)
             + ndimage.correlate1d(U[0], divKernelVertX, axis=0))
    divUy = (ndimage.correlate1d(U[1], divKernelHorzY, axis=1)
             + ndimage.correlate1d(U[1], divKernelVertY, axis=0))
    return dstack((divUx, divUy))

# Convolve an X-Y-separable filter in a 3D image (e.g. each XY value is a vector)
def convolveSeparableWithD(I, kHorz, kVert, origin=0):
    for d in range(0, (size(I,2) if len(I.shape) > 2 else 1)):
        r = ndimage.correlate1d(ndimage.correlate1d(I[:,:,d], weights=kHorz[:,d], axis=1, mode='nearest', origin=origin),
                                weights=kVert[:,d], axis=0, mode='nearest', origin=origin)
        if d == 0: R = r
        else:      R += r
    return R

# Check kernels
#divKernel5x5 = concatenate(([[0,0]],divKernelVert,[[0,0]]))[:,newaxis,:] * concatenate(([[0,0]],divKernelHorz,[[0,0]]))[newaxis,:,:]

#gradKernelX5x5 = column_stack((zeros((5,2)), array([0,-1,0,1,0]), zeros((5,2))))
#gradKernelY5x5 = row_stack((zeros((2,5)), array([0,-1,0,1,0]), zeros((2,5))))

#gradDivKernelXexpected = dstack((ndimage.correlate(divKernel5x5[:,:,0], weights=gradKernelX5x5),
#                         ndimage.correlate(divKernel5x5[:,:,1], weights=gradKernelX5x5)))
#gradDivKernelYexpected = dstack((ndimage.correlate(divKernel5x5[:,:,0], weights=gradKernelY5x5),
#                         ndimage.correlate(divKernel5x5[:,:,1], weights=gradKernelY5x5)))
#gradDivKernelXactual = gradDivKernelVertX[:,newaxis,:] * gradDivKernelHorzX[newaxis,:,:]
#gradDivKernelYactual = gradDivKernelVertY[:,newaxis,:] * gradDivKernelHorzY[newaxis,:,:]

# Test images
testVF1 = array([ [ [1,0], [1,1], [1,0] ],
                  [ [0,0], [1,0], [2,0] ],
                  [ [1,0], [1,-1], [1,0] ] ])
if 0 != convolveSeparableWithD(testVF1, divKernelHorz3, divKernelVert3)[1,1]:
    raise('convolveSeparableWithD test failed')

