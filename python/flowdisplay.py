# Flow display functions

from numpy import *

# Adapted from the MATLAB and C++ code of:
#   Daniel Scharstein 
#   Contact: schar@middlebury.edu
# and
#   Deqing Sun, Department of Computer Science, Brown University
#   Contact: dqsun@cs.brown.edu
def makeColorWheel(ncols=55):
    # color encoding scheme
    # adapted from the color circle idea described at
    # http://members.shaw.ca/quadibloc/other/colint.htm

    RY = int(round(15. / 55 * ncols))
    YG = int(round(6. / 55 * ncols))
    GC = int(round(4. / 55 * ncols))
    CB = int(round(11. / 55 * ncols))
    BM = int(round(13. / 55 * ncols))
    MR = int(round(6. / 55 * ncols))

    ncols = RY + YG + GC + CB + BM + MR

    colorwheel = zeros((ncols, 3), uint8) # r g b

    col = 0
    #RY
    colorwheel[0:RY, 0] = 255
    colorwheel[0:RY, 1] = floor(255*arange(0,RY)/RY)
    col = col + RY

    #YG
    colorwheel[col:(col+YG), 0] = 255 - floor(255*arange(0,YG)/YG)
    colorwheel[col:(col+YG), 1] = 255
    col = col + YG

    #GC
    colorwheel[col:(col+GC), 1] = 255
    colorwheel[col:(col+GC), 2] = floor(255*arange(0,GC)/GC)
    col = col + GC

    #CB
    colorwheel[col:(col+CB), 1] = 255 - floor(255*arange(0,CB)/CB)
    colorwheel[col:(col+CB), 2] = 255
    col = col + CB

    #BM
    colorwheel[col:(col+BM), 2] = 255
    colorwheel[col:(col+BM), 0] = floor(255*arange(0,BM)/BM)
    col = col + BM

    #MR
    colorwheel[col:(col+MR), 2] = 255 - floor(255*arange(0,MR)/MR)
    colorwheel[col:(col+MR), 0] = 255

    return colorwheel

def computeColor(U, norm=False):
    img = zeros(concatenate((U.shape[0:2], [3])))
    magnitude = sqrt(sum(U * U, axis=2))
    if norm:
        m = max(magnitude.flat)
        if m > 1e-7:
            magnitude = magnitude / m
    ncols = size(colorwheel, 0)
    a = arctan2(U[:,:,0], U[:,:,1]) / pi
    idx = floor((a + 1) / 2 * (ncols - 1))

    for i in range(0,ncols):
        tmp = (idx == i)
        mag = magnitude[tmp]
        if len(mag.flat) > 0:
            img[tmp,:] = mag[:,newaxis] * colorwheel[i,:]

    # Correct for out of range
    oor = any(img > 255, axis=2)
    img[oor] = img[oor] * 0.25 + 128

    return img.astype(uint8, copy=False)

def colorWheelImage(imSize):
    rng = 1.04
    s2 = round(imSize / 2)
    
    x,y = meshgrid(arange(0,imSize), arange(0,imSize))
    u = x * rng / s2 - rng
    v = y * rng / s2 - rng

    return computeColor(dstack((u,v)))

colorwheel = makeColorWheel(200)

