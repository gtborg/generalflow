# Builds 3rd party libraries
cmake_minimum_required(VERSION 2.6)

if(GENERALFLOW_BUILD_generalflow_flow)

	# SLIC
	set(slic_sources
	  "SLIC/SLICSuperpixels/SLIC.h"
	  "SLIC/SLICSuperpixels/SLIC.cpp")
	add_library(SLIC SHARED ${slic_sources})
	source_group("" FILES ${slic_sources}) # Create MSVC structure
	install(TARGETS SLIC EXPORT GeneralFlow-exports RUNTIME DESTINATION lib LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
	list(APPEND GeneralFlow_EXPORTED_TARGETS SLIC)

	#gSLIC
	#find_package(CUDA)
	#if(CUDA_FOUND)
	#  file(GLOB gslic_h "gSLIC/gSLIC/*.h")
	#  file(GLOB gslic_cpp "gSLIC/gSLIC/*.cpp")
	#  file(GLOB gslic_cu "gSLIC/gSLIC/*.cu")
	#  set(gslic_sources ${gslic_h} ${gslic_cpp} ${gslic_cu})
	#  cuda_add_library(gSLIC ${gslic_sources} SHARED)
	#  source_group("" FILES ${gslic_sources}) # Create MSVC structure
	#  install(TARGETS gSLIC EXPORT GeneralFlow-exports RUNTIME DESTINATION lib LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
	#  list(APPEND GeneralFlow_EXPORTED_TARGETS gSLIC)
	#endif()
	
	# libMRF
	file(GLOB MRF_sources "MRF2.2/*.cpp")
	file(GLOB MRF_headers "MRF2.2/*.h")
	source_group("" FILES ${MRF_sources} ${MRF_headers})
	
	set(MRF_sources ${MRF_sources} ${MRF_headers})

endif()

# Export targets
set(GeneralFlow_EXPORTED_TARGETS "${GeneralFlow_EXPORTED_TARGETS}" PARENT_SCOPE)

# 3rdparty sources
set(MRF_sources ${MRF_sources} PARENT_SCOPE)
