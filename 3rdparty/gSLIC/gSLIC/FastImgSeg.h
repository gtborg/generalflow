#ifndef __CUDA_SUPERPIXELSEG__
#define __CUDA_SUPERPIXELSEG__

//////////////////////////////////////////////////////////////////////
// Inserted by Richard Roberts for building a DLL on Windows
#ifdef _WIN32
#ifdef gSLIC_EXPORTS
#define gSLIC_EXPORT __declspec(dllexport)
#else
#define gSLIC_EXPORT __declspec(dllimport)
#endif /* gSLIC_EXPORTS */
#else /* _WIN32 */
#define gSLIC_EXPORT
#endif
// End of inserted code for building a DLL on Windows
//////////////////////////////////////////////////////////////////////

#include "cudaUtil.h"
#include "cudaSegSLIC.h"

class gSLIC_EXPORT FastImgSeg
{

public:
	unsigned char* sourceImage;
	unsigned char* markedImg;
	int* segMask;

private:

	int width;
	int height;
	int nSeg;

	bool bSegmented;
	bool bImgLoaded;
	bool bMaskGot;

public:
	FastImgSeg();
	FastImgSeg(int width,int height,int dim,int nSegments);
	~FastImgSeg();

	void initializeFastSeg(int width,int height,int nSegments);
	void clearFastSeg();
	void changeClusterNum(int nSegments);

	void LoadImg(unsigned char* imgP);
	void DoSegmentation(SEGMETHOD eMethod, double weight);
	void Tool_GetMarkedImg();
	void Tool_WriteMask2File(char* outFileName, bool writeBinary);
};

#endif
