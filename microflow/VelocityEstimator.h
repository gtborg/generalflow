/**
 * @file microflow.h
 * @brief microflow library for simple camera velocity estimation
 * @author Richard Roberts
 * @date May 24, 2013
 */
#pragma once

#include <utility>
#include <microflow/Image.h>

// Forward declarations
class VelocityEstimatorJacobiansTest;

namespace microflow {

  // Forward declarations
  class VelocityEstimator;

  /* ************************************************************************* */
  /** Parameters for the VelocityEstimator */
  struct VelocityEstimatorParams {
    int imageSubsampleFactor;
    float focalLenth_pixels;

    VelocityEstimatorParams() :
      imageSubsampleFactor(1), focalLenth_pixels(1.f) {}

    VelocityEstimatorParams(int subsampleFactor, float focalLength) :
      imageSubsampleFactor(subsampleFactor), focalLenth_pixels(focalLength) {}
  };

  /* ************************************************************************* */
  /** Class to reuse calculations during sequential velocity estimation.
   * Constructing this class from an image computes its gradients. */
  class microflow_EXPORT VelocityEstimatorGradients {
    Image8s gradX_;
    Image8s gradY_;

  public:
    // Construct this class by computing gradients for the given image.
    VelocityEstimatorGradients(const Image8uPointer& image, int subsampleFactor);

    friend class VelocityEstimator;
  };

  /* ************************************************************************* */
  /** Simple camera velocity estimation for a downward-facing, narrow field-of-view
    * camera.  Assumes the camera sees the ground plane and is close to level. */
  class microflow_EXPORT VelocityEstimator {
  protected:
    VelocityEstimatorParams params_;

  public:

    /** Construct VelocityEstimator object from parameters */
    VelocityEstimator(const VelocityEstimatorParams& params) :
      params_(params) {}

    /** Estimate velocity for an image pair with pre-computed spatial gradients
     * in the first image.  Returns the spatial gradients in the second image
     * for use in the next call to this function.  Omitting the spatial gradients
     * argument, or passing 0 for them, will cause them to be computed from
     * scratch, which should only be done for the first image pair. */
    Eigen::Vector2f estimate(const Image8uPointer& prevFrame, const Image8uPointer& curFrame,
      const Eigen::Vector2f& omegaXY, const float altitude, VelocityEstimatorGradients& imageGradients) const;

  private:
    Eigen::Matrix<float,1,3> calculateAugmentedJacobian(uint8_t prevImg, uint8_t curImg,
      int8_t gradX, int8_t gradY, const Eigen::Vector2f& uOmega) const;
    
    friend class ::VelocityEstimatorJacobiansTest;
  };

}
