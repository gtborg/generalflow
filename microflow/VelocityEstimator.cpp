/**
 * @file microflow.cpp
 * @brief microflow library for simple camera velocity estimation
 * @author Richard Roberts
 * @date May 24, 2013
 */

#include <microflow/VelocityEstimator.h>

#include <stdexcept>

namespace microflow {

  /* ************************************************************************* */
  VelocityEstimatorGradients::VelocityEstimatorGradients(const Image8uPointer& image, int subsampleFactor)
  {
    GridSamplingBounds gridBounds = computeGridSampling(image.cols(), image.rows(), subsampleFactor);
    gradX_.resize(gridBounds.subsampledHeight, gridBounds.subsampledWidth);
    gradY_.resize(gridBounds.subsampledHeight, gridBounds.subsampledWidth);
    computeGradients(image, subsampleFactor, gradX_, gradY_);
  }

  /* ************************************************************************* */
  Eigen::Vector2f VelocityEstimator::estimate(const Image8uPointer& prevFrame, const Image8uPointer& curFrame,
    const Eigen::Vector2f& omegaXY, const float altitude, VelocityEstimatorGradients& imageGradients) const
  {
    // Check input
    GridSamplingBounds gridBounds = computeGridSampling(prevFrame.cols(), prevFrame.rows(), params_.imageSubsampleFactor);
    if(prevFrame.cols() != curFrame.cols() || prevFrame.rows() != curFrame.rows())
      throw std::invalid_argument("Previous and current frame are different sizes");
    if(gridBounds.subsampledWidth != imageGradients.gradX_.cols() || gridBounds.subsampledHeight != imageGradients.gradX_.rows()
      || gridBounds.subsampledWidth != imageGradients.gradY_.cols() || gridBounds.subsampledHeight != imageGradients.gradY_.rows())
      throw std::invalid_argument("Provided image gradients were computed from an image of a different size");

    // Compute rotational flow
    Eigen::Vector2f uOmega(params_.focalLenth_pixels * omegaXY(0), params_.focalLenth_pixels * omegaXY(1));

    // Build Hessian matrix for computing translational flow vector (we'll only use the upper triangle)
    Eigen::Matrix3f hessian = Eigen::Matrix3f::Zero();
    for(Image8s::Index fullY = gridBounds.firstPixel, smallY = 0;
      smallY < gridBounds.subsampledHeight; fullY += params_.imageSubsampleFactor, ++smallY)
    {
      for(Image8s::Index fullX = gridBounds.firstPixel, smallX = 0;
        smallX < gridBounds.subsampledWidth; fullX += params_.imageSubsampleFactor, ++smallX)
      {
        // Calculate Jacobian
        const Eigen::Matrix<float,1,3> Ab = calculateAugmentedJacobian(prevFrame(fullY, fullX), curFrame(fullY, fullX),
          imageGradients.gradX_(smallY, smallX), imageGradients.gradY_(smallY, smallX), uOmega);
        // Update Hessian - low-rank update to symmetric matrix
        hessian.selfadjointView<Eigen::Upper>().rankUpdate(Ab.adjoint());
      }
    }

    // Solve with Cholesky to compute average flow vector.  Stores the result
    // in the last column of the hessian.
    // A'Ax - A'b = 0
    Eigen::Block<Eigen::Matrix3f, 2, 1> v = hessian.topRightCorner<2,1>();
    hessian.topLeftCorner<2,2>().selfadjointView<Eigen::Upper>().llt().solveInPlace(v);
    v *= altitude / params_.focalLenth_pixels; // Convert to meters/second
    v = Eigen::Vector2f(v(1), -v(0)); // Convert to Drone coordinate frame motion

    // Compute gradients for current frame for use next time
    computeGradients(curFrame, params_.imageSubsampleFactor, imageGradients.gradX_, imageGradients.gradY_);

    return v;
  }

  /* ************************************************************************* */
  Eigen::Matrix<float,1,3> VelocityEstimator::calculateAugmentedJacobian(uint8_t prevImg, uint8_t curImg,
    int8_t gradX, int8_t gradY, const Eigen::Vector2f& uOmega) const
  {
    // Residual is e = I1 - grad*(ut+uw) - I2
    // Ax - b = 0, where b = -(I1 - grad*uw - I2), and A = -grad
    const Eigen::Matrix<float,1,2> grad(gradX, gradY);
    return (Eigen::Matrix<float,1,3>() << -grad, -(prevImg - grad.dot(uOmega) - curImg)).finished();
  }
}
