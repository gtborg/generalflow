/**
 * @file cameraDemo
 * @author Richard Roberts
 */

#include <microflow/VelocityEstimator.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace microflow;
using namespace std;

int main(int argc, char *argv[])
{
  // Construct velocity estimator
  VelocityEstimatorParams params;
  params.imageSubsampleFactor = 4;
  VelocityEstimator estimator(params);

  // Create video capture
  cv::Mat frame, img2grey;
  cv::VideoCapture cap(0);
  if(!cap.isOpened()) {
    cout << "Could not open camera" << endl;
    return -1;
  }
  cap >> frame; // Get first frame
  cv::cvtColor(frame, img2grey, CV_BGR2GRAY);

  // Compute first frame gradients
  VelocityEstimatorGradients gradients(Image8uPointer(img2grey.data, img2grey.rows, img2grey.cols), params.imageSubsampleFactor);

  // Loop
  cv::Mat img1grey;
  while(true)
  {
    // Swap img1 and img 2
    {
      cv::Mat tmp = img2grey;
      img2grey = img1grey;
      img1grey = tmp;
    }
    cap >> frame; // Grab next frame
    cv::cvtColor(frame, img2grey, CV_BGR2GRAY); // Convert to greyscale

    // Estimate velocity
    Eigen::Vector2f u = estimator.estimate(
      Image8uPointer(img1grey.data, img1grey.rows, img1grey.cols),
      Image8uPointer(img2grey.data, img2grey.rows, img2grey.cols),
      Eigen::Vector2f::Zero(), 1.0, gradients);

    //cout << "u: " << u.transpose() << endl;

    // Draw result
    const cv::Point center(frame.cols/2, frame.rows/2);
    cv::line(frame, 8*center, 8*center + cv::Point(int(8.f*3.f*u(0)), int(8.f*3.f*u(1))), cv::Scalar(255, 255, 0), 2, CV_AA, 3);
    cv::imshow("Flow", frame);

    if(cv::waitKey(1) == 'q')
      break;
  }

  return 0;
}
