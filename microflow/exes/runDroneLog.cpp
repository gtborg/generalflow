/**
 * @file runDroneLog
 * @author Richard Roberts
 */

#include <microflow/VelocityEstimator.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

using namespace microflow;
using namespace std;

int main(int argc, char *argv[])
{
  if(argc < 2) {
    cout << "Usage: " << argv[0] << " LOG_DIRECTORY" << endl;
    return 1;
  }

  const string logDir = argv[1];
  const int imgFrameLag = 0;
  const int firstFrameToUse = 2 + imgFrameLag;
  const int imageDownScale = 1;
  const int imageSubsample = 1;

  // Check log format
  enum { V1, V2 } logFormat;
  if(boost::filesystem::exists(logDir + "/forward3"))
    logFormat = V1;
  else if(boost::filesystem::exists(logDir + "/log"))
    logFormat = V2;
  else {
    cout << "Could not determine log format" << endl;
    return 1;
  }

  // Open log file
  const string logFilename =
    logFormat == V1 ?
    logDir + "/forward3/1__logFlow.txt" :
    logDir + "/log/1__logFlow.txt";
  ifstream logFile(logFilename.c_str(), ios::binary);
  char logBuf[2048];

  // Open output file
  const string outputFileName =
    logFormat == V1 ?
    logDir + "/forward3/velocityEstimated.txt" :
    logDir + "/log/velocityEstimated.txt";
  ofstream outputFile(outputFileName.c_str(), ios::binary);

  // Construct velocity estimator
  VelocityEstimatorParams params;
  //params.focalLenth_pixels = 6.8699099106029587e+02f / 4.f / float(imageDownScale);
  params.focalLenth_pixels = 306.358543272f / float(imageDownScale);
  params.imageSubsampleFactor = imageSubsample;
  VelocityEstimator estimator(params);

  // Create video capture
  cv::Mat img2grey = cv::imread(logDir + (boost::format("/img_%010d.pgm") % firstFrameToUse).str(), CV_LOAD_IMAGE_GRAYSCALE);
  if(img2grey.rows == 0) {
    cout << "Missing image for frame " << firstFrameToUse << endl;
    return 1;
  }
  cv::resize(img2grey, img2grey, cv::Size(), 1.0 / imageDownScale, 1.0 / imageDownScale);
  for(int i=0; i<firstFrameToUse; ++i)
    logFile.getline(logBuf, 2048); // Discard first lines
  string logBufStr(logBuf);
  boost::tokenizer<boost::char_separator<char> > tok = boost::tokenizer<boost::char_separator<char> >(logBufStr, boost::char_separator<char>(" "));
  boost::tokenizer<boost::char_separator<char> >::const_iterator tokIt = tok.begin();
  double lastTime = boost::lexical_cast<double>(*tokIt);
  int lastFrame = firstFrameToUse;

  // Compute first frame gradients
  VelocityEstimatorGradients gradients(Image8uPointer(img2grey.data, img2grey.rows, img2grey.cols), params.imageSubsampleFactor);

  // Loop
  cv::Mat img1grey;
  cv::Mat frame;
  while(true)
  {
    // Read log data
    logFile.getline(logBuf, 2048);
    string logBufStr(logBuf);
    if(logBufStr.empty())
      break;
    boost::tokenizer<boost::char_separator<char> > tok = boost::tokenizer<boost::char_separator<char> >(logBufStr, boost::char_separator<char>(" "));
    boost::tokenizer<boost::char_separator<char> >::const_iterator tokIt = tok.begin();
    const double time = boost::lexical_cast<double>(*tokIt);
    if(logFormat == V1)
      advance(tokIt, 14); // Field 14 - gyroXFiltCorrected
    else
      ++ tokIt;
    float gyroX = boost::lexical_cast<float>(*tokIt);
    ++ tokIt; // Field 15 - gyroYFiltCorrected
    float gyroY = boost::lexical_cast<float>(*tokIt);
    if(logFormat == V1)
      advance(tokIt, 4); // Field 20 - altitude
    else
      advance(tokIt, 2);
    const float altitude = boost::lexical_cast<float>(*tokIt);
    ++ tokIt; // Field 21 - sequence
    const int seq = boost::lexical_cast<int>(*tokIt);

    // Get angle between frames
    gyroX *= float(time - lastTime);
    gyroY *= float(time - lastTime);

    cout << "Frame " << seq << ".  Gyro: " << gyroX << ", " << gyroY << ".  Altitude: " << altitude << endl;

    // Swap img1 and img 2 and read next image
    {
      cv::Mat tmp = img2grey;
      img2grey = img1grey;
      img1grey = tmp;
    }
    img2grey = cv::imread(logDir + (boost::format("/img_%010d.pgm") % (seq - imgFrameLag)).str(), CV_LOAD_IMAGE_GRAYSCALE);
    if(img2grey.rows == 0)
      img2grey = img1grey;
    cv::resize(img2grey, img2grey, cv::Size(), 1.0 / imageDownScale, 1.0 / imageDownScale);

    // Estimate velocity
    Eigen::Vector2f u;
    if(seq - lastFrame == 1)
      u = estimator.estimate(
      Image8uPointer(img1grey.data, img1grey.rows, img1grey.cols),
      Image8uPointer(img2grey.data, img2grey.rows, img2grey.cols),
      /*Eigen::Vector2f::Zero()*/Eigen::Vector2f(gyroX, gyroY), altitude, gradients);
    else
      u = Eigen::Vector2f::Zero();

    // Save last time
    lastTime = time;
    lastFrame = seq;

    outputFile.precision(20);
    outputFile << time << " " << u(0) << " " << u(1) << " " << seq << " " << gyroX << " " << gyroY << " " << altitude << endl;
    //cout << "u: " << u.transpose() << endl;

    Eigen::Vector2f uScaled = 2000.f * u;

    // Draw result
    cv::cvtColor(img2grey, frame, CV_GRAY2BGR);
    const cv::Point center(frame.cols/2, frame.rows/2);
    cv::line(frame, 8*center, 8*center + cv::Point(int(8.f*uScaled(1)), int(8.f*(-1.f)*uScaled(0))), cv::Scalar(255, 255, 0), 2, CV_AA, 3);
    //cv::line(frame, 8*center, 8*center + cv::Point(int(8.f*uScaled(0)), int(8.f*uScaled(1))), cv::Scalar(255, 255, 0), 2, CV_AA, 3);
    cv::imshow("Flow", frame);

    if(cv::waitKey(1) == 'q')
      break;
  }

  return 0;
}
