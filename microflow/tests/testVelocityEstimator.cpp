/**
 * @file testImage
 * @author Richard Roberts
 */

#include <CppUnitLite/TestHarness.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
//#include <boost/timer/timer.hpp>

#include <microflow/VelocityEstimator.h>

using namespace std;
using namespace microflow;

/* ************************************************************************* */
TEST(VelocityEstimator, Jacobians)
{
  const uint8_t prevImg = 100;
  const uint8_t curImg = 99;
  const int8_t gradX = 1;
  const int8_t gradY = 0;
  const Eigen::Vector2f uOmega = Eigen::Vector2f::Zero();

  const Eigen::RowVector3f expectedAb(-1.f, 0.f, -1.f);

  VelocityEstimator velocityEstimator = VelocityEstimator(VelocityEstimatorParams());
  const Eigen::RowVector3f actualAb = velocityEstimator.calculateAugmentedJacobian(
    prevImg, curImg, gradX, gradY, uOmega);

  //cout << "expectedAb: \n" << expectedAb << endl;
  //cout << "actualAb: \n" << actualAb << endl;

  EXPECT(expectedAb == actualAb);
}

/* ************************************************************************* */
TEST(VelocityEstimator, EstimateVelocity)
{
  cv::Mat imgCV = cv::imread(SOURCE_DIR "/generalflow_basisflow/tests/kitchen.jpg", CV_LOAD_IMAGE_GRAYSCALE);
  CHECK(imgCV.data);
  Image8uPointer img(imgCV.data, imgCV.rows, imgCV.cols);

  const int vx = 1;
  const int vy = 1;

  Image8u img1 = img.topLeftCorner(img.rows() - vy, img.cols() - vx);
  Image8u img2 = img.bottomRightCorner(img.rows() - vy, img.cols() - vx);

  for(int subsampleFactor = 1; subsampleFactor <= 32; ++subsampleFactor) {
    VelocityEstimatorParams params;
    params.imageSubsampleFactor = subsampleFactor;
    VelocityEstimator velocityEstimator(params);
    Eigen::Vector2f omega = Eigen::Vector2f::Zero();
    VelocityEstimatorGradients gradients(&img1, params.imageSubsampleFactor);
    //boost::timer::cpu_timer timer;
    Eigen::Vector2f u = velocityEstimator.estimate(&img1, &img2, omega, 1.0, gradients);
    //timer.stop();

    Eigen::Vector2f errs = (u - Eigen::Vector2f((float)vx, (float)vy)).cwiseAbs();
    //cout << "subsampleFactor " << subsampleFactor << ":  err = " << errs.transpose() <<
    //  ",  time = " << timer.elapsed().wall / 1000 << " microseconds" << endl;
  }
}

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr); }
/* ************************************************************************* */

