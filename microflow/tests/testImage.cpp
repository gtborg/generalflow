/**
 * @file testImage
 * @author Richard Roberts
 */

#include <CppUnitLite/TestHarness.h>
#include <iostream>

#include <microflow/Image.h>

using namespace std;
using namespace microflow;

/* ************************************************************************* */
TEST(Image, computeGradients)
{
  Image8u testImage(3,3);
  testImage <<
    0, 255, 0,
    255, 0, 0,
    0, 0, 0;

  Image8s gradXExpected(3,3);
  gradXExpected <<
    0, 0, 0,
    0, -127, 0,
    0, 0, 0;

  Image8s gradYExpected(3,3);
  gradYExpected <<
    0, 0, 0,
    0, -127, 0,
    0, 0, 0;

  Image8s gradXActual(3,3);
  Image8s gradYActual(3,3);
  computeGradients(&testImage, 1, gradXActual, gradYActual);

  EXPECT(gradXExpected == gradXActual);
  EXPECT(gradYExpected == gradYActual);
}

/* ************************************************************************* */
TEST(Image, computeGradientsSampling)
{
  Image8u testImage(6,6);
  testImage <<
    0, 2, 3, 4, 5, 6,
    0, 2, 3, 4, 5, 6,
    0, 2, 3, 4, 5, 6,
    0, 2, 3, 4, 5, 6,
    0, 2, 3, 4, 5, 6,
    0, 2, 3, 4, 5, 6;

  Image8s gradXExpected(3,3);
  gradXExpected <<
    0, 0, 0,
    0, 1, 1,
    0, 1, 1;

  Image8s gradYExpected(3,3);
  gradYExpected <<
    0, 0, 0,
    0, 0, 0,
    0, 0, 0;

  Image8s gradXActual(3,3);
  Image8s gradYActual(3,3);
  computeGradients(&testImage, 2, gradXActual, gradYActual);

  EXPECT(gradXExpected == gradXActual);
  EXPECT(gradYExpected == gradYActual);
}

/* ************************************************************************* */
int main() { TestResult tr; return TestRegistry::runAllTests(tr); }
/* ************************************************************************* */

