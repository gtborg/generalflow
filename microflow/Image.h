/**
 * @file Image.h
 * @brief Images and image utilities for the microflow library
 * @author Richard Roberts
 * @date May 24, 2013
 */
#pragma once

//#include <Eigen/Dense>
#include <microflow/dllexport.h>
#include <gtsam/3rdparty/Eigen/Eigen/Dense>
//#include <cstdint>
#include <stdint.h>

namespace microflow {

  /* ************************************************************************* */
  /** This is an image that uses external data instead of storing its own data. */
  template<typename SCALAR>
  class ImagePointer :
    public Eigen::Map<Eigen::Matrix<SCALAR, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >
  {
  public:
    typedef SCALAR Scalar;
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> PlainObjectType;

  private:
    typedef Eigen::Map<PlainObjectType> Base;

  public:
    /** Construct to point to raw image data */
    ImagePointer(Scalar* data, int rows, int cols) :
      Base(data, rows, cols) {}

    /** Construct to point to an existing image, in this case the original image
     * must not be deallocated as long as this ImagePointer exists! */
    ImagePointer(PlainObjectType* image) :
      Base(image->data(), image->rows(), image->cols()) {}
  };

  /* ************************************************************************* */
  /// An 8-bit unsigned image
  typedef Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Image8u;
  /// A pointer to an 8-bit unsigned image - actually an image class with externally-managed data
  typedef ImagePointer<uint8_t> Image8uPointer;
  /// An 8-bit signed image (for gradients)
  typedef Eigen::Matrix<int8_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Image8s;
  /// A pointer to an 8-bit signed image - actually an image class with externally-managed data
  typedef ImagePointer<int8_t> Image8sPointer;

  /* ************************************************************************* */
  struct GridSamplingBounds {
    Image8s::Index subsampledWidth;
    Image8s::Index subsampledHeight;
    Image8s::Index firstPixel;
  };

  /* ************************************************************************* */
  /** Compute bounds for a uniform grid sampling centered in the image */
  microflow_EXPORT GridSamplingBounds computeGridSampling(Image8s::Index imgWidth, Image8s::Index imgHeight, Image8s::Index subsampleFactor);

  /* ************************************************************************* */
  /** Compute spatial gradients of \c image, writes results in-place into \c gradX
   * and \c gradY.  If \c subsampleFactor is greater than 1, gradients will be
   * computed only at every subsampleFactor'th pixel, but the gradient kernel will
   * still be applied to the pixels from the original image at full resolution. */
  microflow_EXPORT void computeGradients(const Image8uPointer& image, int subsampleFactor,
    Image8s& gradX, Image8s& gradY);

}
