/**
 * @file Image.cpp
 * @brief Images and image utilities for the microflow library
 * @author Richard Roberts
 * @date May 24, 2013
 */

#include <microflow/Image.h>

#include <utility>
#include <stdexcept>

namespace microflow {

  /* ************************************************************************* */
  GridSamplingBounds computeGridSampling(Image8s::Index imgWidth, Image8s::Index imgHeight, Image8s::Index subsampleFactor)
  {
    GridSamplingBounds bounds;
    bounds.subsampledWidth = imgWidth / subsampleFactor;
    bounds.subsampledHeight = imgHeight / subsampleFactor;
    bounds.firstPixel = (subsampleFactor - 1) / 2;
    return bounds;
  }

  /* ************************************************************************* */
  void computeGradients(const Image8uPointer& image, int subsampleFactor,
    Image8s& gradX, Image8s& gradY)
  {
    // Get expected gradient image sizes.  For example:
    // image 10x10, subsampleFactor 1  ->  subsampled 10x10
    // image 10x10, subsampleFactor 2  ->  subsampled 5x5, sample at 0, 2, 4, 6, 8
    // image 10x10, subsampleFactor 3  ->  subsampled 3x3, sample at 1, 4, 7
    GridSamplingBounds gridBounds = computeGridSampling(image.cols(), image.rows(), subsampleFactor);

    // Check arguments
    if(subsampleFactor < 1 || subsampleFactor > image.rows() || subsampleFactor > image.cols())
      throw std::invalid_argument("subsampleFactor is out of range");
    if(gradX.rows() != gridBounds.subsampledHeight || gradX.cols() != gridBounds.subsampledWidth)
      throw std::invalid_argument("gradX image is not sized correctly for the given image size and subsampleFactor");
    if(gradY.rows() != gridBounds.subsampledHeight || gradY.cols() != gridBounds.subsampledWidth)
      throw std::invalid_argument("gradY image is not sized correctly for the given image size and subsampleFactor");

    static const Image8s::Index kernelSize = 3;
    static const Image8s::Index kernelHalf = (kernelSize - 1) / 2;

    // Decide which pixels to use.  If subsampleFactor>1, we try to center the
    // sampling within the bounds of the image.
    Image8s::Index firstDestX = 0;
    Image8s::Index firstSourceX = gridBounds.firstPixel;
    while(firstSourceX - kernelHalf < 0) { ++firstDestX; firstSourceX+=subsampleFactor; }
    Image8s::Index firstDestY = 0;
    Image8s::Index firstSourceY = gridBounds.firstPixel;
    while(firstSourceY - kernelHalf < 0) { ++firstDestY; firstSourceY+=subsampleFactor; }
    Image8s::Index lastDestX = gridBounds.subsampledWidth - 1;
    Image8s::Index lastSourceX = gridBounds.firstPixel + subsampleFactor*(gridBounds.subsampledWidth - 1);
    while(lastSourceX + kernelHalf >= image.cols()) { --lastDestX; lastSourceX-=subsampleFactor; }
    Image8s::Index lastDestY = gridBounds.subsampledHeight - 1;
    Image8s::Index lastSourceY = gridBounds.firstPixel + subsampleFactor*(gridBounds.subsampledHeight - 1);
    while(lastSourceY + kernelHalf >= image.rows()) { --lastDestY; lastSourceY-=subsampleFactor; }

    // Compute gradients by convolution.
    for(Image8s::Index destY = firstDestY, sourceY = firstSourceY; destY <= lastDestY; ++destY, sourceY += subsampleFactor) {
      for(Image8s::Index destX = firstDestX, sourceX = firstSourceX; destX <= lastDestX; ++destX, sourceX += subsampleFactor)
      {
        // Compute simple [-1 0 1] gradients.  This relies on twos-complement
        // math - the final division by 2 before casting to signed int ensures
        // that the results are within range.
        gradX(destY, destX) = int8_t((image(sourceY, sourceX+1) - image(sourceY, sourceX-1)) / 2);
        gradY(destY, destX) = int8_t((image(sourceY+1, sourceX) - image(sourceY-1, sourceX)) / 2);
      }
    }

    // Zero out border
    gradX.block(0, 0, firstDestY, gridBounds.subsampledWidth).setZero(); // Top strip (including corners)
    gradY.block(0, 0, firstDestY, gridBounds.subsampledWidth).setZero(); // Top strip (including corners)
    gradX.block(firstDestY, 0, lastDestY+1-firstDestY, firstDestX).setZero(); // Left strip (excluding corners)
    gradY.block(firstDestY, 0, lastDestY+1-firstDestY, firstDestX).setZero(); // Left strip (excluding corners)
    gradX.block(firstDestY, lastDestX+1, lastDestY+1-firstDestY, gridBounds.subsampledWidth-(lastDestX+1)).setZero(); // Right strip (excluding corners)
    gradY.block(firstDestY, lastDestX+1, lastDestY+1-firstDestY, gridBounds.subsampledWidth-(lastDestX+1)).setZero(); // Right strip (excluding corners)
    gradX.block(lastDestY+1, 0, gridBounds.subsampledHeight-(lastDestY+1), gridBounds.subsampledWidth).setZero(); // Bottom strip (including corners)
    gradY.block(lastDestY+1, 0, gridBounds.subsampledHeight-(lastDestY+1), gridBounds.subsampledWidth).setZero(); // Bottom strip (including corners)
  }

}
