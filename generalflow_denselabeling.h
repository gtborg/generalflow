virtual class gtsam::Rot3;
virtual class gtsam::Cal3_S2;
virtual class gtsam::NonlinearFactor;

namespace generalflow {
  namespace denselabeling {

#include <generalflow_denselabeling/Velocity2ClassFactor.h>
    virtual class Velocity2ClassFactor : gtsam::NonlinearFactor
    {
      Velocity2ClassFactor(size_t velocityKey, const gtsam::Rot3& attitude, double altitude,
        const gtsam::Cal3_S2& camCalibration, double inlierSigma, double outlierSigma,
        double inlierPixSigma, double outlierPixSigma,
        double groundClassPrior, double distantClassPrior, const generalflow::flow::ImagePairGrad* images);

      Matrix indicators() const;
    };

  }
}

