/*
 * FlyCap.cpp
 *
 *  Created on: Apr 28, 2012
 *      Author: muri
 */

#include "FlyCap.h"
#include <flycapture/FlyCapture2.h>
#include <stdexcept>

using namespace FlyCapture2;

namespace generalflow {

Error error;
Camera cam;


FlyCap::~FlyCap() {
  endCapture();
}

FlyCap::FlyCap() {

  BusManager busMgr;
  unsigned int numCameras;
  error = busMgr.GetNumOfCameras(&numCameras);
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());

  PGRGuid guid;
  error = busMgr.GetCameraFromIndex(0, &guid);
  // Connect to a camera
  error = cam.Connect(&guid);
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());

  startCapture();
  calibrate();
  turnOffAuto();

  cam.SetVideoModeAndFrameRate (VIDEOMODE_1024x768Y8, FRAMERATE_60);  //best resolution at 60fps
  //cam.SetVideoModeAndFrameRate (VIDEOMODE_640x480Y8, FRAMERATE_120);  //best resolution at 120fps

}

cv::Mat FlyCap::grabImage() const {
  Image rawImage;
  error = cam.RetrieveBuffer( &rawImage );
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());
  // Create a converted image
  Image convertedImage;

  // Convert the raw image
  error = rawImage.Convert( PIXEL_FORMAT_MONO8, &convertedImage );
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());

  cv::Mat cvImage(convertedImage.GetRows(), convertedImage.GetCols(), CV_8U);
  memcpy(cvImage.data, convertedImage.GetData(), convertedImage.GetRows() * convertedImage.GetCols());

  return cvImage;

}

void FlyCap::startCapture(){
  // Start capturing images
  error = cam.StartCapture();
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());
}

void FlyCap::endCapture(){
  // Stop capturing images
  error = cam.StopCapture();
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());

  // Disconnect the camera
  error = cam.Disconnect();
  if (error != PGRERROR_OK)
    throw std::runtime_error(error.GetDescription());
}

///////////////////////////
//
//  Calibration Stuff
//
///////////////////////////

void FlyCap::turnOffAuto(){
  Property gain;
  gain.type = GAIN;
  error = cam.GetProperty(&gain);
  gain.autoManualMode = false;
  error = cam.SetProperty(&gain);

  Property exposure;
  exposure.type = AUTO_EXPOSURE;
  cam.GetProperty(&exposure);
  exposure.autoManualMode = false;
  cam.SetProperty(&exposure);

  Property framerate;
  cam.GetProperty(&framerate);
  framerate.type = FRAME_RATE;
  framerate.autoManualMode = false;
  cam.SetProperty(&framerate);

  Property shutter;
  shutter.type = SHUTTER;
  error = cam.GetProperty(&shutter);
  shutter.autoManualMode = false;
  cam.SetProperty(&shutter);

  Property sharpness;
  sharpness.type = SHARPNESS;
  error = cam.GetProperty(&sharpness);
  sharpness.autoManualMode = false;
  cam.SetProperty(&sharpness);
}

void FlyCap::calibrate(){
  // turns on the auto-adjustment for the camera and lets it calibrate for 180 frames
  // make sure the camera is set to start capture before calling this

  Property gain;
  gain.type = GAIN;
  error = cam.GetProperty(&gain);
  gain.autoManualMode = true;
  error = cam.SetProperty(&gain);

  Property exposure;
  exposure.type = AUTO_EXPOSURE;
  cam.GetProperty(&exposure);
  exposure.autoManualMode = true;
  cam.SetProperty(&exposure);

  Property framerate;
  cam.GetProperty(&framerate);
  framerate.type = FRAME_RATE;
  framerate.autoManualMode = true;
  cam.SetProperty(&framerate);

  Property shutter;
  shutter.type = SHUTTER;
  error = cam.GetProperty(&shutter);
  shutter.autoManualMode = true;
  cam.SetProperty(&shutter);

  Property sharpness;
  sharpness.type = SHARPNESS;
  error = cam.GetProperty(&sharpness);
  sharpness.autoManualMode = true;
  cam.SetProperty(&sharpness);

  //grab 180 frames and do nothing with them
  for(int frame = 0; frame < 180; frame++){
    Image rawImage;
    error = cam.RetrieveBuffer( &rawImage );
    if (error != PGRERROR_OK)
      throw std::runtime_error(error.GetDescription());
  }
}

} /* namespace generalflow */
