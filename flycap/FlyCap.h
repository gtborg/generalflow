/*
 * FlyCap.h
 *
 *  Created on: Apr 28, 2012
 *      Author: muri
 */

#pragma once

#include <opencv2/core/core.hpp>

namespace generalflow {

class FlyCap {
public:
  virtual ~FlyCap();
  FlyCap();

  cv::Mat grabImage() const;

  void calibrate();

  void turnOffAuto();

  void startCapture();

  void endCapture();
};

} /* namespace generalflow */
