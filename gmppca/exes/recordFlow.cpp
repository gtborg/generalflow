/*
 * recordFlow.cpp
 *
 *  Created on: May 4, 2010
 *      Author: richard
 */

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/math/special_functions/fpclassify.hpp>
#include <iostream>
#include <fstream>
#include <limits>
#include <signal.h>
#include <gtsam/base/timing.h>

using namespace std;
namespace fs = boost::filesystem;
using boost::math::round;
using boost::math::lround;
using boost::math::isfinite;

static bool stay = true;

void sighandler(int sig) {
  stay = false;
}

cv::Mat grabFrame(int frameNum, const string& format){
  const string file = (boost::format(format)%frameNum).str();
  if(fs::is_regular_file(file))
    return cv::imread(file);
  else
    return cv::Mat();
}

int main(int argc, char* argv[]) {

  if(argc < 2) {
    cout << "Please specify an output file\n";
    cout << "Usage:  " << argv[0] << "  OUTFILE  [INFILE]" << endl;
    cout << "If OUTFILE is a single underscore _ no flow file will be written\n"
        "and instead annotated video frames will be written to frames/%08d.jpg\n"
        "if the frames directory already exists." << endl;
    return 1;
  }

  ofstream outfile, infofile;
  if(string(argv[1]) != "_") {
    outfile.open(argv[1], ios::binary);
    if(!outfile.is_open()) {
      cout << "Error opening output file " << argv[1] << endl;
      return 1;
    }
    infofile.open((string(argv[1])+".txt").c_str());
    if(!infofile.is_open()) {
      cout << "Error opening info file " << string(argv[1])+".txt" << endl;
      return 1;
    }
  }

  int fileFrameNum = 0;
  bool usingCapture = string(argv[2]).find('%') == string::npos;
  cv::VideoCapture cap;
  if(usingCapture){
    if(argc >= 3)
      cap.open(argv[2]);
    else
      cap.open(0);

    if(!cap.isOpened()) {
      cout << "Could not create capture object" << endl;
      return 1;
    }
  }
//  cvSetCaptureProperty(cap, CV_CAP_PROP_FRAME_WIDTH, 320);
//  cvSetCaptureProperty(cap, CV_CAP_PROP_FRAME_HEIGHT, 240);
  cv::Mat curframe, prevframe;
  cv::Mat frame;
  if(usingCapture){
    cap >> frame;
  } else{
    // If frame 0 does not exist, try frame 1 instead
    frame = grabFrame(fileFrameNum, argv[2]);
    if(frame.empty()) {
      ++ fileFrameNum;
      frame = grabFrame(fileFrameNum, argv[2]);
    }
  }

  int imageScalePower = 0;
  int flowSamplePower = 3;
  size_t nVert = frame.rows >> imageScalePower >> flowSamplePower;
  size_t nHorz = frame.cols>> imageScalePower >> flowSamplePower;
  double hstep = double(1 << flowSamplePower);
  double vstep = double(1 << flowSamplePower);
  double hoffset = hstep / 2.0;
  double voffset = vstep / 2.0;
  vector<pair<size_t, size_t> > imgXYs;
  imgXYs.reserve(nVert*nHorz);
  for(size_t j=0; j<nVert; ++j)
    for(size_t i=0; i<nHorz; ++i) {
      long int pimx = lround(hoffset + double(i) * hstep);
      long int pimy = lround(voffset + double(j) * vstep);
      assert(pimx >= 0.0 && pimy >= 0.0);
      imgXYs.push_back(make_pair(pimx,pimy));
    }

  cout << "Original image: " << frame.cols << "x" << frame.rows << endl;
  cout << "Flow: " << nHorz << "x" << nVert << endl;

  signal(SIGINT, &sighandler);
  cout << "Recording optical flow, press Ctrl-C to exit" << endl;

  // Set up flow variables
  cv::vector<cv::Point2f> prevPoints;
  cv::vector<cv::Point2f> nextPoints;
  cv::vector<uchar> status;
  cv::vector<float> errs;
  nextPoints.reserve(nVert*nHorz);
  status.reserve(nVert*nHorz);
  errs.reserve(nVert*nHorz);
  cv::Mat flow(nVert, nHorz, CV_64FC2);
  int skip = 1 << flowSamplePower;
  int offset = skip >> 1;
  for(size_t i=0; i<nVert; i++)
    for(size_t j=0; j<nHorz; j++)
      prevPoints.push_back(cv::Point2f((j<<flowSamplePower)+offset, (i<<flowSamplePower)+offset));

  bool wroteinfo = false;
  int framenum = 0;
  double lastpos = usingCapture ? cap.get(CV_CAP_PROP_POS_AVI_RATIO) : fileFrameNum;
  do {

    double curpos = usingCapture ? cap.get(CV_CAP_PROP_POS_AVI_RATIO) : fileFrameNum;
    if(curpos < lastpos)
      break;
    lastpos = curpos;

    if(usingCapture){
      cap >> frame;
    } else{
      frame = grabFrame(fileFrameNum++, argv[2]);
    }
    if(frame.empty())
      break;

    gttic(copy_to_prev);
    // Store last frame
    curframe.copyTo(prevframe);
    gttoc(copy_to_prev);

    // Convert to grayscale and resize
    gttic(convert_gray);
    cv::Mat gray;
    cvtColor(frame, gray, CV_BGR2GRAY);
    curframe.create(gray.rows>>imageScalePower, gray.cols>>imageScalePower, CV_8UC1);
    resize(gray, curframe, curframe.size());
    gttoc(convert_gray);

    if(framenum > 0) {
      gttic(flow);
      // Compute optical flow
      cv::calcOpticalFlowPyrLK(prevframe, curframe, cv::vector<cv::Point2f>(prevPoints), nextPoints, status, errs, cv::Size(7,7), 4, cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 15, 0.05));
      gttoc(flow);
      gttic(result);
      size_t l = 0;
      for(int i=0; i<flow.rows; i++) {
        double* row = flow.ptr<double>(i);
        for(int j=0; j<flow.cols; j++) {
          if(status[l] != 0) {
            row[2*j] = nextPoints[l].x - prevPoints[l].x;
            row[2*j+1] = nextPoints[l].y - prevPoints[l].y;
          } else {
            row[2*j] = numeric_limits<double>::infinity();
            row[2*j+1] = numeric_limits<double>::infinity();
          }
          ++ l;
        }
      }
      gttoc(result);

      int nHorz = frame.cols >> flowSamplePower >> imageScalePower;
      int nVert = frame.rows >> flowSamplePower >> imageScalePower;
      double scale = (double)((1 << flowSamplePower) << (imageScalePower-0));
      double offset = scale / 2.0;
  //    size_t mIndex = 0;
      for(int i=0; i<nVert; i+=1) {
        for(int j=0; j<nHorz; j+=1) {
          double x0 = scale * (double)j + offset;
          double y0 = scale * (double)i + offset;
          if(std::isfinite(flow.at<complex<double> >(i,j).real())) {
            double x = x0 + flow.at<complex<double> >(i,j).real() * (1<<imageScalePower);
            double y = y0 + flow.at<complex<double> >(i,j).imag() * (1<<imageScalePower);
            cv::line(frame, cv::Point((int)round(4.0*x0), (int)round(4.0*y0)),
                cv::Point((int)round(4.0*x), (int)round(4.0*y)), cv::Scalar(255,0,0), 1.25, CV_AA, 2);
          }
        }
      }

      imshow("Flow", frame);
      cv::waitKey(5);
      cv::imwrite((boost::format("frames/%08d.jpg")%framenum).str(), frame);

      gttic(write);
      if(outfile.is_open()) {
        double *data = (double*)flow.data;
        outfile.write((char*)data, 2*flow.rows*flow.cols*sizeof(*data));
        if(!outfile.good()) {
          cout << "Error writing to file" << endl;
          exit(1);
        }
        outfile.flush();
        if(!outfile.good()) {
          cout << "Error writing to file" << endl;
          exit(1);
        }
      }
      gttoc(write);
    }

    if(!wroteinfo && infofile.is_open()) {
      infofile << flow.rows << "\t" << flow.cols << endl;
      infofile.close();
      wroteinfo = true;
    }

    if(framenum % 100 == 0) {
      cout << "frame " << framenum << endl;
      cout << "Pos " << (usingCapture ? cap.get(CV_CAP_PROP_POS_AVI_RATIO) : fileFrameNum) << endl;
    }

    framenum++;
//    cout << "Frame " << framenum << endl;

    //tictoc_print();

  } while(stay);

  outfile.close();
  cout << "\nRecorded " << framenum << " frames" << endl;
  cout << "Closed output file, exiting" << endl;

  return 0;

}
