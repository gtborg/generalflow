# this one is important
SET(CMAKE_SYSTEM_NAME Linux)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
SET(CMAKE_C_COMPILER   /home/uuv/overo-oe/tmp/cross/armv7a/bin/arm-angstrom-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER /home/uuv/overo-oe/tmp/cross/armv7a/bin/arm-angstrom-linux-gnueabi-g++)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH /home/uuv/overo-oe/tmp/staging/i686-linux /home/uuv/flowroot /home/uuv/Desktop/coaxcv/api /home/uuv/overo-oe/tmp/staging/armv7a-angstrom-linux-gnueabi)

SET(OpenCV_ROOT_DIR /home/uuv/flowroot)
SET(BOOST_ROOT /home/uuv/flowroot/usr)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

