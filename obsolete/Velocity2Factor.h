/**
* @file    Velocity2Factor.h
* @brief   Factor on velocity with 2 bases and images as measurements
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#pragma once

#include <generalflow_denselabeling/dllexport.h>
#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

namespace generalflow {
  namespace denselabeling {

    class generalflow_denselabeling_EXPORT Velocity2Factor : public gtsam::NonlinearFactor {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef Velocity2Factor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Matrix basis1_;
      gtsam::Matrix basis2_;
      Sigmas sigmas_;
      boost::optional<gtsam::Matrix> indicators_;
      boost::shared_ptr<const ClassPriors> classPriors_;
      boost::shared_ptr<flow::ImagePairGrad> images_;
      
      mutable bool inspection_;
      mutable gtsam::Matrix lastErrorImage_;
      mutable gtsam::Matrix lastInlierImage_;
      mutable gtsam::Matrix lastJacobians_;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      Velocity2Factor(gtsam::Key latentKey, const gtsam::Matrix& basis1, const gtsam::Matrix& basis2,
        const Sigmas& sigmas, boost::shared_ptr<const ClassPriors> classPriors, const boost::shared_ptr<flow::ImagePairGrad>& images) :
      Base(gtsam::ListOfOne(latentKey)), basis1_(basis1), basis2_(basis2), sigmas_(sigmas),
        classPriors_(classPriors), images_(images), inspection_(false) {}

      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      Velocity2Factor(gtsam::Key latentKey, const gtsam::Matrix& basis1, const gtsam::Matrix& basis2,
        const Sigmas& sigmas, const gtsam::Matrix& indicators, const boost::shared_ptr<flow::ImagePairGrad>& images) :
      Base(gtsam::ListOfOne(latentKey)), basis1_(basis1), basis2_(basis2), sigmas_(sigmas),
      indicators_(indicators), images_(images), inspection_(false) {}

      gtsam::Key latentKey() const { return Base::keys_[0]; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 2; }

      /// Basis 1
      const gtsam::Matrix& basis1() const { return basis1_; }

      /// Basis 1
      const gtsam::Matrix& basis2() const { return basis2_; }

      /// Sigmas
      const Sigmas& sigmas() const { return sigmas_; }

      /// Class priors
      const boost::shared_ptr<const ClassPriors> classPriors() const { return classPriors_; }

      /// Indicators
      const boost::optional<gtsam::Matrix>& indicators() const { return indicators_; }

      /// Images
      const boost::shared_ptr<flow::ImagePairGrad>& images() const { return images_; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;

      void inspection(bool enable) const { inspection_ = enable; }
      gtsam::Matrix lastErrorImage() const { return lastErrorImage_; }
      gtsam::Matrix lastInlierImage() const { return lastInlierImage_; }
      gtsam::Matrix lastJacobians() const { return lastJacobians_; }
    };

  }
}
