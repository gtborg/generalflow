/**
 * @file    Velocity2ClassFactor.h
 * @brief   Optical flow factor for 2 basis flow models on only the latent variables
 * @author  Richard Roberts
 * @created Jan 16, 2013
 */

#pragma once

#include <generalflow_denselabeling/dllexport.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/nonlinear/NonlinearFactor.h>

class TestDenseLabeling;

namespace generalflow
{
  namespace denselabeling
  {
    /* **************************************************************************************** */
    class generalflow_denselabeling_EXPORT Velocity2ClassFactor : public gtsam::NonlinearFactor
    {
    public:
      typedef gtsam::NonlinearFactor Base;
      typedef Velocity2ClassFactor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Key key_;
      gtsam::Matrix basis1_;
      gtsam::Matrix basis2_;
      double inlierSigma_;
      double outlierSigma_;
      double inlierPixSigma_;
      double outlierPixSigma_;
      double prior1_;
      double prior2_;
      boost::shared_ptr<flow::ImagePairGrad> images_;
      mutable gtsam::Matrix indicators_;

    public:
      /** Constructor, does non-trivial calculation of basis flows.
      * @param velocityKey The key of the 6D velocity vector variable
      * @param attitude The camera attitude using aerospace coordinate convention (X front, Y right,
      *        Z down)
      * @param altitude The camera altitude above the ground plane
      * @param inlierSigma The standard deviation (equal in X and Y) of the image feature locations
      * @param groundClassPrior The prior probability of a pixel belonging to the ground plane (0.33
      *        means that ground plane, distant, and outlier classes will have equal prior
      *        probabilities).
      * @param distantClassPrior The prior probability of a pixel belonging to distant structure
      *        (0.33 means that ground plane, distant, and outlier classes will have equal prior
      *        probabilities).
      * @param measurements The sparse flow measurements. */
      Velocity2ClassFactor(gtsam::Key velocityKey, const gtsam::Rot3& attitude, double altitude,
        const gtsam::Cal3_S2& camCalibration, double inlierSigma, double outlierSigma,
        double inlierPixSigma, double outlierPixSigma,
        double groundClassPrior, double distantClassPrior, const boost::shared_ptr<flow::ImagePairGrad>& images);

      /** Virtual destructor */
      virtual ~Velocity2ClassFactor() {}

      /** Return the velocity key. */
      gtsam::Key key() const { return key_; }

      /** Access the indicator variables. */
      const gtsam::Matrix& indicators() const { return indicators_; }

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 2; }

      /** Compute the error of the factor for the given variable assignments */
      virtual double error(const gtsam::Values& c) const;

      /** Linearize the factor to a JacobianFactor */
      virtual boost::shared_ptr<gtsam::GaussianFactor> linearize(const gtsam::Values& x) const;

    private:
      Eigen::Vector2d evaluateError(const gtsam::LieVector& y, size_t i, Eigen::Vector3d& indicators,
        boost::optional<gtsam::Matrix&> ddy = boost::none) const;

      friend class ::TestDenseLabeling;
    };

  }
}
