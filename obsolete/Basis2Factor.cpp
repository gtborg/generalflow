/**
* @file    Basis2Factor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <generalflow_denselabeling/Basis2Factor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace denselabeling {

    inline double sq(double x) { return x*x; }

    const gtsam::SharedNoiseModel Basis2Factor::unitNoise2 = gtsam::noiseModel::Unit::Create(2);

    /* **************************************************************************************** */
    void Basis2Factor::print(const string& s, const KeyFormatter& keyFormatter) const
    {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool Basis2Factor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const
    {
      const Basis2Factor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(velocity_, t->velocity_, tol) &&
        sigmas_.equals(t->sigmas_, tol) &&
        equal_with_abs_tol(classPriors_->get<0>(), t->classPriors_->get<0>()) &&
        equal_with_abs_tol(classPriors_->get<1>(), t->classPriors_->get<1>()) &&
        equal_with_abs_tol(classPriors_->get<2>(), t->classPriors_->get<2>()) &&
        images_ == t->images_ && pixX_ == t->pixX_ && pixY_ == t->pixY_;
    }

    /* **************************************************************************************** */
    Vector Basis2Factor::evaluateError(const LieMatrix& basis1, const LieMatrix& basis2,
      boost::optional<Matrix&> H1, boost::optional<Matrix&> H2) const
    {
      if(indicators_)
      {
        return flow::fullImageError(*images_, pixX_, pixY_, basis1, basis2, velocity_,
          sigmas_.flowSigmaF, sigmas_.flowSigmaB1, sigmas_.flowSigmaB2, sigmas_.pixSigmaF, sigmas_.pixSigmaB1, sigmas_.pixSigmaB2,
          *indicators_, H1, H2, boost::none, visualization_);
      }
      else
      {
        Eigen::Vector3d indicators;
        return flow::fullImageError(*images_, pixX_, pixY_, basis1, basis2, velocity_,
            classPriors_->get<0>()(pixY_, pixX_), classPriors_->get<1>()(pixY_, pixX_), classPriors_->get<2>()(pixY_, pixX_),
            sigmas_.flowSigmaF, sigmas_.flowSigmaB1, sigmas_.flowSigmaB2, sigmas_.pixSigmaF, sigmas_.pixSigmaB1, sigmas_.pixSigmaB2,
            indicators, H1, H2, boost::none, visualization_);
      }
    }

  }
}
