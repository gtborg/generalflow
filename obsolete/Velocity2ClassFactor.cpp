/**
 * @file    Velocity2ClassFactor.h
 * @brief   Optical flow factor for 2 basis flow models on only the latent variables
 * @author  Richard Roberts
 * @created Jan 16, 2013
 */

#include <generalflow_denselabeling/Velocity2ClassFactor.h>
#include <generalflow_core/geometry.h>
#include <generalflow_core/linearFlow.h>
#include <generalflow_core/SparseFlowField.h>
#include <generalflow_flow/flowOps.h>
#include <generalflow_flow/ImgOps.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/linear/HessianFactor.h>

using namespace gtsam;
using namespace generalflow;

namespace generalflow
{
  namespace denselabeling
  {
    namespace {
      const int VDimRot = 3;
    }

    /* **************************************************************************************** */
    Velocity2ClassFactor::Velocity2ClassFactor(gtsam::Key velocityKey, const gtsam::Rot3& attitude, double altitude,
      const gtsam::Cal3_S2& camCalibration, double inlierSigma, double outlierSigma,
      double inlierPixSigma, double outlierPixSigma,
      double groundClassPrior, double distantClassPrior, const boost::shared_ptr<flow::ImagePairGrad>& images) :
      key_(velocityKey), inlierSigma_(inlierSigma), outlierSigma_(outlierSigma),
      inlierPixSigma_(inlierPixSigma), outlierPixSigma_(outlierPixSigma),
      prior1_(groundClassPrior), prior2_(distantClassPrior),
      images_(images), indicators_(3, images_->prevFrame().cols() * images_->prevFrame().rows())
    {
      // Calculate basis flows:

      // Camera pose (aerospace coordinates - X forward, Y right, Z down)
      const Pose3 cameraPose(attitude, Point3(0.0, 0.0, -altitude));

      // Velocities for each basis component
      const Matrix6 velocities6 = Matrix6::Identity();
      const Eigen::Matrix<double, VDimRot, VDimRot> velocities3 = Eigen::Matrix<double, VDimRot, VDimRot>::Identity();

      DenseIndex npix = images_->prevFrame().cols() * images_->prevFrame().rows();
      basis1_.resize(npix * 2, 6);
      basis2_.resize(npix * 2, VDimRot);

      DenseIndex pix = 0;
      for (DenseIndex j = 0; j < images_->prevFrame().cols(); ++j)
        for (DenseIndex i = 0; i < images_->prevFrame().rows(); ++i)
      {
        const Point2 pixel(j, i);
        basis1_.block<2, 6>(2 * pix, 0) = geometry::opticalFlowGroundPlane(cameraPose, camCalibration, pixel, velocities6);
        basis2_.block<2, VDimRot>(2 * pix, 0) = geometry::opticalFlowInfinity(camCalibration, pixel, velocities3);
        ++ pix;
      }
    }

    /* **************************************************************************************** */
    void Velocity2ClassFactor::print(const std::string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool Velocity2ClassFactor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const This *t = dynamic_cast<const This*>(&f);
      if (t && Base::equals(f))
        return key_ == t->key_ && inlierSigma_ == t->inlierSigma_ && prior1_ == t->prior1_ && prior2_ == t->prior2_
        && gtsam::equal_with_abs_tol(basis1_, t->basis1_, tol) && gtsam::equal_with_abs_tol(basis2_, t->basis2_, tol)
        && images_ == t->images_;
      else
        return false;
    }

    /* **************************************************************************************** */
    double Velocity2ClassFactor::error(const gtsam::Values& x) const
    {
      const LieVector& y = x.at<LieVector>(key_);
      // Error = 1/2 * e^2
      double error = 0.0;
      for (DenseIndex i = 0; i < basis1_.rows() / 2; ++i)
      {
        Eigen::Vector3d indicators;
        error += evaluateError(y, i, indicators).squaredNorm();
      }
      return 0.5 * error;
    }

    /* **************************************************************************************** */
    boost::shared_ptr<gtsam::GaussianFactor>
      Velocity2ClassFactor::linearize(const gtsam::Values& x) const
    {
      const LieVector& y = x.at<LieVector>(key_);

      Eigen::Matrix<double, 7, 7> hessian = Eigen::Matrix<double, 7, 7>::Zero();

      // Evaluate residual and Jacobian for each measurement and apply low-rank updates to Hessian
      Matrix ddy; // Avoid reallocating this
      for (DenseIndex i = 0; i < basis1_.rows() / 2; ++i)
      {
        Eigen::Vector3d indicators;
        const Eigen::Vector2d b = -evaluateError(y, i, indicators, ddy);
        indicators_.col(i) = indicators;
        const Eigen::Matrix<double, 2, 7> J = (Eigen::Matrix<double, 2, 7>() <<
          ddy, b).finished();
        hessian.selfadjointView<Eigen::Upper>().rankUpdate(J.adjoint());
      }

      // Construct HessianFactor
      return boost::shared_ptr<GaussianFactor>(
        boost::make_shared<HessianFactor>(key_, hessian.topLeftCorner(6, 6), hessian.topRightCorner(6, 1), hessian(6, 6)));
    }

    /* **************************************************************************************** */
    Eigen::Vector2d Velocity2ClassFactor::evaluateError(const LieVector& y, size_t i,
      Eigen::Vector3d& indicators, boost::optional<Matrix&> ddy) const
    {
      int pixX = i / images_->prevFrame().rows();
      int pixY = i % images_->prevFrame().rows();
      return flow::fullImageError(
        *images_, pixX, pixY,
        LieMatrix(basis1_.block<2, 6>(2 * i, 0)), LieMatrix(basis2_.block<2, VDimRot>(2 * i, 0)),
        y, (1.0 - prior1_ - prior2_), prior1_, prior2_,
        outlierSigma_, inlierSigma_, inlierSigma_,
        outlierPixSigma_, inlierPixSigma_, inlierPixSigma_,
        indicators,
        boost::none, boost::none, ddy);
    }

  }
}
