/**
* @file    Basis2Factor.h
* @brief   Factor on 2 bases with images as measurements
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#pragma once

#include <generalflow_denselabeling/dllexport.h>
#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>

#include <boost/tuple/tuple.hpp>

namespace generalflow {
  namespace denselabeling {

    /* **************************************************************************************** */
    class generalflow_denselabeling_EXPORT Basis2Factor :
      public gtsam::NoiseModelFactor2<gtsam::LieMatrix, gtsam::LieMatrix> {
    public:
      typedef gtsam::NoiseModelFactor2<gtsam::LieMatrix, gtsam::LieMatrix> Base;
      typedef Basis2Factor This;
      typedef boost::shared_ptr<This> shared_ptr;

    private:
      gtsam::Vector velocity_;
      Sigmas sigmas_;
      boost::optional<Eigen::Vector3d> indicators_;

      boost::shared_ptr<const ClassPriors> classPriors_;
      flow::ImagePairGrad::shared_ptr images_;
      gtsam::DenseIndex pixX_;
      gtsam::DenseIndex pixY_;

      boost::shared_ptr<flow::FullImageErrorVisualization> visualization_;

      static const gtsam::SharedNoiseModel unitNoise2;

    public:
      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      Basis2Factor(gtsam::Key basis1Key, gtsam::Key basis2Key, const gtsam::Vector& velocity,
          const Sigmas& sigmas, const boost::shared_ptr<const ClassPriors>& classPriors,
          const flow::ImagePairGrad::shared_ptr& images,
          gtsam::DenseIndex pixX, gtsam::DenseIndex pixY,
          const boost::shared_ptr<flow::FullImageErrorVisualization>& visualization = boost::shared_ptr<flow::FullImageErrorVisualization>())
        : Base(unitNoise2, basis1Key, basis2Key), velocity_(velocity), sigmas_(sigmas),
        classPriors_(classPriors), images_(images), pixX_(pixX), pixY_(pixY), visualization_(visualization) {}

      /** Constructor
      * @param basisKey Key for basis (n x q LieMatrix)
      * @param latentKey Key for latent variable (q LieVector)
      * @param inlierKey Key for inlier indicators for each pixel (n LieVector)
      * @param flowSigmaVKey Key for inlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param flowSigmaFKey Key for outlier flow vector sigmas for each pixel (n x 2 LieMatrix OR 1 x 2 LieMatrix)
      * @param pixSigmaVKey Key for inlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      * @param pixSigmaFKey Key for outlier pixel intensity sigmas for each pixel (n LieVector OR 1 LieVector)
      */
      Basis2Factor(gtsam::Key basis1Key, gtsam::Key basis2Key, const gtsam::Vector& velocity,
          const Sigmas& sigmas, const Eigen::Vector3d& indicators,
          const flow::ImagePairGrad::shared_ptr& images,
          gtsam::DenseIndex pixX, gtsam::DenseIndex pixY,
          const boost::shared_ptr<flow::FullImageErrorVisualization>& visualization = boost::shared_ptr<flow::FullImageErrorVisualization>())
        : Base(unitNoise2, basis1Key, basis2Key), velocity_(velocity), sigmas_(sigmas),
        indicators_(indicators), images_(images), pixX_(pixX), pixY_(pixY), visualization_(visualization) {}

      /** Virtual destructor */
      virtual ~Basis2Factor() {}

      /** Print */
      virtual void print(const std::string& s = "", const gtsam::KeyFormatter& keyFormatter = gtsam::DefaultKeyFormatter) const;

      /** Compare two factors */
      virtual bool equals(const gtsam::NonlinearFactor& f, double tol = 1e-9) const;

      /** Error vector dimension */
      virtual size_t dim() const { return 1; }

      /** Evaluate error */
      virtual gtsam::Vector evaluateError(const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2,
        boost::optional<gtsam::Matrix&> H1 = boost::none,
        boost::optional<gtsam::Matrix&> H2 = boost::none) const;
    };

  }
}
