/**
* @file    testDenseLabeling.cpp
* @brief
* @author  Richard Roberts
* @created Sept 5, 2013
*/

#include <CppUnitLite/TestHarness.h>

#include <generalflow_denselabeling/Velocity2ClassFactor.h>

#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>

#include <iostream>

using namespace generalflow::denselabeling;
using namespace generalflow::flow;
using namespace generalflow;
using namespace gtsam;
using namespace std;

/* ************************************************************************* */
class TestDenseLabeling
{
public:
  static Eigen::Vector2d evaluateError(const Velocity2ClassFactor& factor, const LieVector& y, size_t i,
    Eigen::Vector3d& indicators, boost::optional<Matrix&> ddy)
  {
    return factor.evaluateError(y, i, indicators, ddy);
  }
};

/* ************************************************************************* */
TEST(DenseLabeling, evaluateError)
{
  // Create test images
  ImagePairGrad images;
  images.prevFrame_.resize(3, 3);
  images.curFrame_.resize(3, 3);
  images.prevGradX_.resize(3, 3);
  images.prevGradY_.resize(3, 3);
  images.prevFrame_(1, 1) = 0.5;
  images.curFrame_(1, 1) = 0.6;
  images.prevGradX_(1, 1) = -0.1;
  images.prevGradY_(1, 1) = 0.0;

  // Create calibration and camera pose
  Rot3 attitude = Rot3::Ry(-M_PI / 2.0); // Looking downwards to the ground plane
  double altitude = 1.0;
  Cal3_S2 calibration;
  double inlierSigma = 0.1, outlierSigma = 1.0;
  double inlierPixSigma = 0.01, outlierPixSigma = 0.01;
  double groundClassPrior = 0.4, distantClassPrior = 0.4;

  // Create factor
  Velocity2ClassFactor factor(0, attitude, altitude, calibration, inlierSigma, outlierSigma,
    inlierPixSigma, outlierPixSigma, groundClassPrior, distantClassPrior,
    boost::make_shared<ImagePairGrad>(images));

  // Evaluate error with zero velocity
  Vector y = Vector::Zero(6);
  size_t i = 4; // Center pixel at 1,1
  Eigen::Vector3d indicators;
  Matrix ddy;
  Eigen::Vector2d e = TestDenseLabeling::evaluateError(factor, y, i, indicators, ddy);

  // Expected velocity is 1 to the left (image appears to shift to the right)
  y << 0, 0, 0, 0, -1, 0;
  e = TestDenseLabeling::evaluateError(factor, y, i, indicators, ddy);

  // Constrain velocity in all directions except left-right
  PriorFactor<LieVector> prior(0, LieVector(Vector::Zero(6)),
    noiseModel::Constrained::MixedSigmas((Vector(6) << 0.0, 0.0, 0.0, 0.0, 1e6, 0.0)));

  NonlinearFactorGraph graph;
  graph += prior, factor;

  Values initial;
  initial.insert(0, LieVector(Vector::Zero(6)));
  Values soln = GaussNewtonOptimizer(graph, initial).optimize();
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

