/**
* @file    testDenseLabeling.cpp
* @brief
* @author  Richard Roberts
* @created Sept 5, 2013
*/

#include <CppUnitLite/TestHarness.h>

#include <generalflow_denselabeling/Basis2Factor.h>

#include <gtsam/slam/PriorFactor.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>

#include <iostream>

using namespace generalflow::denselabeling;
using namespace generalflow::flow;
using namespace generalflow;
using namespace gtsam;
using namespace std;

/* ************************************************************************* */
namespace {
  struct CallEvaluateError
  {
    const Basis2Factor& factor;

    CallEvaluateError(const Basis2Factor& factor) : factor(factor) {}

    Vector operator()(const LieMatrix& value1, const LieMatrix& value2) const
    {
      return factor.evaluateError(value1, value2);
    }
  };
}

/* ************************************************************************* */
TEST(Basis2Factor, evaluateError)
{
  Matrix img1(6,6);
  img1 <<
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6;

  Matrix img2(6,6);
  img2 <<
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7;

  ImagePairGrad imgpair(img1, img2);

  Matrix basisX(6,6);
  Matrix basisY(6,6);
  basisX <<
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5;
  basisY <<
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0;
  Matrix basis1(basisX.cols() * basisX.rows() * 2, 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(&basis1(0,0), basis1.rows() / 2) =
    Eigen::Map<Vector>(basisX.data(), basis1.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(&basis1(1,0), basis1.rows() / 2) =
    Eigen::Map<Vector>(basisY.data(), basis1.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(&basis1(0,1), basis1.rows() / 2) =
    Eigen::Map<Vector>(basisX.data(), basis1.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(&basis1(1,1), basis1.rows() / 2) =
    Eigen::Map<Vector>(basisY.data(), basis1.rows() / 2);

  basisX <<
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5;
  basisY <<
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
    0.3, 0.3, 0.3, 0.3, 0.3, 0.3;
  Matrix basis2(basisX.cols() * basisX.rows() * 2, 1);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis2.data(), basis2.rows() / 2) =
    Eigen::Map<Vector>(basisX.data(), basis2.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis2.data() + 1, basis2.rows() / 2) =
    Eigen::Map<Vector>(basisY.data(), basis2.rows() / 2);

  Matrix classPrior1 = Matrix::Constant(6, 6, 0.5);
  Matrix classPrior2 = Matrix::Constant(6, 6, 0.5);
  boost::shared_ptr<ClassPriors> classPriors =
    boost::make_shared<ClassPriors>(classPrior2, classPrior1, classPrior2);

  Vector velocity(2);
  velocity << 1.0, 1.0;
  Sigmas sigmas;
  sigmas.flowSigmaB1 = 0.1;
  sigmas.flowSigmaB2 = 0.1;
  sigmas.flowSigmaF = 1.0;
  sigmas.pixSigmaB1 = 0.01;
  sigmas.pixSigmaB2 = 0.01;
  sigmas.pixSigmaF = 0.01;
  DenseIndex pixX = 2, pixY = 2;
  Basis2Factor factor(1, 2, velocity, sigmas, classPriors, boost::make_shared<ImagePairGrad>(imgpair), pixX, pixY);

  Vector expectedErrorVector = Vector::Zero(2);
  Matrix ddW1, ddW2;
  Vector actualErrorVector = factor.evaluateError(
    LieMatrix(basis1.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis1.cols())),
    LieMatrix(basis2.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis2.cols())),
    ddW1, ddW2);

  // Check numerical derivatives
  Matrix expectedddW1 = numericalDerivative21<LieMatrix, LieMatrix>(
    boost::function<Vector(const LieMatrix&, const LieMatrix&)>(CallEvaluateError(factor)),
    LieMatrix(basis1.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis1.cols())),
    LieMatrix(basis2.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis2.cols())));
  Matrix expectedddW2 = numericalDerivative22<LieMatrix, LieMatrix>(
    boost::function<Vector(const LieMatrix&, const LieMatrix&)>(CallEvaluateError(factor)),
    LieMatrix(basis1.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis1.cols())),
    LieMatrix(basis2.block(pixX * 2*basisX.rows() + 2*pixY, 0, 2, basis2.cols())));

  cout << "numerical W1:\n" << expectedddW1 << "\nnumerical W2:\n" << expectedddW2 << endl;

  EXPECT(assert_equal(expectedErrorVector, actualErrorVector));
  EXPECT(assert_equal(expectedddW1, ddW1, 1e-4));
  EXPECT(assert_equal(expectedddW2, ddW2, 1e-4));
}

/* ************************************************************************* */
TEST(Basis2Factor, MatrixMaps)
{
  Matrix toFill(2, 3);
  toFill <<
      1, 2, 3,
      4, 5, 6;

  Matrix D(2, 6);

  Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>, 0, Eigen::Stride<Eigen::Dynamic,Eigen::Dynamic> >(
      &D(0,0), 2, 3, Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(2*toFill.cols(), 2)) = toFill;
  D.row(1).setZero();

  Matrix expected(2, 6);
  expected <<
      1, 2, 3, 4, 5, 6,
      0, 0, 0, 0, 0, 0;

  EXPECT(assert_equal(expected, D));
}

/* ************************************************************************* */
TEST(Basis2Factor, MatrixMaps2)
{
  using Eigen::Map;
  using Eigen::Dynamic;
  using Eigen::RowMajor;
  using Eigen::Stride;
  const DenseIndex kappa = 2;
  const DenseIndex r = 3;
  Matrix ddWRot = Matrix::Constant(kappa, 2*r, -1);
  Eigen::RowVector2d grad(1, 2);
  Eigen::Vector3d omega(3, 4, 5);
  Map<Eigen::Matrix<double, Dynamic, Dynamic, RowMajor>, 0, Stride<Dynamic, Dynamic> >(
      &ddWRot(0,0), 2, r, Stride<Dynamic, Dynamic>(kappa*r, kappa)) = grad.transpose() * omega.transpose();

  Matrix expected(2, 6);
  expected <<
      3, 4, 5, 6, 8, 10,
      -1,-1,-1,-1,-1,-1;

  EXPECT(assert_equal(expected, ddWRot));
}


/* ************************************************************************* */
TEST(Basis2Factor, MatrixMaps3)
{
  using Eigen::Map;
  using Eigen::Dynamic;
  using Eigen::RowMajor;
  using Eigen::Stride;
  const DenseIndex kappa = 3;
  const DenseIndex q = 3;
  const DenseIndex k = 2;
  Matrix ddWk = Matrix::Constant(kappa, 2*q, -1);
  Eigen::RowVector2d grad(1, 2);
  Eigen::Vector3d v(3, 4, 5);
  Map<Eigen::Matrix<double, Dynamic, Dynamic, RowMajor>, 0, Stride<Dynamic, Dynamic> >(
      &ddWk(k-1,0), 2, q, Stride<Dynamic, Dynamic>(kappa*q, kappa)) = grad.transpose() * v.transpose();
  ddWk.topRows(k - 1).setZero();
  ddWk.bottomRows(kappa - k).setZero();

  Matrix expected(3, 6);
  expected <<
      0, 0, 0, 0, 0, 0,
      3, 4, 5, 6, 8, 10,
      0, 0, 0, 0, 0, 0;

  EXPECT(assert_equal(expected, ddWk));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

