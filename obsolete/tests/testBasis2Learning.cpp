/**
 * @file    testBasis2Learning.cpp
 * @brief   Test learning of multiple bases
 * @author  Richard Roberts
 * @created Oct 7, 2013
 */

#include <CppUnitLite/TestHarness.h>

#include <fstream>
#include <boost/random.hpp>
#include <boost/filesystem.hpp>
#include <tbb/tbb.h>

#include <generalflow_denselabeling/Basis2Factor.h>
#include <generalflow_denselabeling/Velocity2Factor.h>
#include <generalflow_core/linearFlow.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/VectorValues.h>
#include <gtsam/nonlinear/Symbol.h>

using namespace std;
using namespace gtsam;
using namespace generalflow;
using namespace generalflow::denselabeling;
using namespace generalflow::flow;

//tbb::task_scheduler_init init(1);

double updateBases(vector<Matrix>& bases, const Values& velocities, const Sigmas& sigmas,
    const boost::shared_ptr<ClassPriors>& classPriors,
    const vector<boost::shared_ptr<ImagePairGrad> >& imagePairs)
{
  const DenseIndex wid = imagePairs.front()->prevFrame().cols();
  const DenseIndex hei = imagePairs.front()->prevFrame().rows();
  const DenseIndex q = bases.front().cols();

  // Store current basis in a Values
  Values basisValues;
  for(size_t k = 0; k < bases.size(); ++k)
    for(DenseIndex pix = 0; pix < bases.front().rows() / 2; ++pix)
      basisValues.insert(symbol('a' + k, pix), LieMatrix(bases[k].block(2*pix, 0, 2, bases[k].cols())));

  // Update basis
  NonlinearFactorGraph basisGraph;
  for(size_t frame = 0; frame < imagePairs.size(); ++frame)
  {
    size_t pix = 0;
    for(DenseIndex j = 0; j < wid; ++j) {
      for(DenseIndex i = 0; i < hei; ++i) {
        basisGraph += Basis2Factor(symbol('a', pix), symbol('b', pix),
            velocities.at<LieVector>(frame), sigmas, classPriors, imagePairs[frame], j, i);
        ++ pix;
      }
    }
  }

//    size_t pix = 0;
//    for(DenseIndex j = 0; j < wid; ++j) {
//      for(DenseIndex i = 0; i < hei; ++i) {
//        basisGraph += PriorFactor<LieMatrix>(symbol('a', pix), LieMatrix(Matrix::Zero(2, q)),
//          noiseModel::Isotropic::Sigma(2*q, 1000000000.0));
//        basisGraph += PriorFactor<LieMatrix>(symbol('b', pix), LieMatrix(Matrix::Zero(2, q)),
//          noiseModel::Isotropic::Sigma(2*q, 1000000000.0));
//        ++ pix;
//      }
//    }

  const double betweenSigma = 5000;
  for(DenseIndex j = 1; j < wid; ++j) {
    for(DenseIndex i = 0; i < hei; ++i) {
      size_t pix = j*hei + i;
      size_t pixL = (j-1)*hei + i;
      basisGraph += BetweenFactor<LieMatrix>(symbol('a', pix), symbol('a', pixL), LieMatrix(Matrix::Zero(2, q)),
        noiseModel::Isotropic::Sigma(2*q, betweenSigma));
      basisGraph += BetweenFactor<LieMatrix>(symbol('b', pix), symbol('b', pixL), LieMatrix(Matrix::Zero(2, q)),
        noiseModel::Isotropic::Sigma(2*q, betweenSigma));
    }
  }
  for(DenseIndex j = 0; j < wid; ++j) {
    for(DenseIndex i = 1; i < hei; ++i) {
      size_t pix = j*hei + i;
      size_t pixU = j*hei + (i-1);
      basisGraph += BetweenFactor<LieMatrix>(symbol('a', pix), symbol('a', pixU), LieMatrix(Matrix::Zero(2, q)),
        noiseModel::Isotropic::Sigma(2*q, betweenSigma));
      basisGraph += BetweenFactor<LieMatrix>(symbol('b', pix), symbol('b', pixU), LieMatrix(Matrix::Zero(2, q)),
        noiseModel::Isotropic::Sigma(2*q, betweenSigma));
    }
  }

  // Check derivatives
  //BOOST_FOREACH(const NonlinearFactor::shared_ptr& factor, basisGraph)
  //{
  //  if(const BasisFactor* basisFactor = dynamic_cast<const BasisFactor*>(factor.get()))
  //  {
  //    CallEvaluateError e(*basisFactor);
  //    Matrix expected = numericalDerivative11<LieMatrix>(e, basisValues.at<LieMatrix>(basisFactor->key()));
  //    Matrix actual;
  //    (void) basisFactor->evaluateError(basisValues.at<LieMatrix>(basisFactor->key()), actual);
  //    bool result = assert_equal(expected, actual);
  //    CHECK(result);
  //  }
  //}

  try {
    basisValues = basisValues.retract(basisGraph.linearize(basisValues)->optimize(boost::none, EliminateQR));
  }
  catch(std::exception& e)
  {
    cout << e.what() << endl;
    throw;
  }

  // Reassemble basis
  for(size_t k = 0; k < bases.size(); ++k)
    for(DenseIndex pix = 0; pix < bases.front().rows() / 2; ++pix)
      bases[k].block(2*pix, 0, 2, bases.front().cols()) = basisValues.at<LieMatrix>(symbol('a' + k, pix));

  return basisGraph.error(basisValues);
}

double updateVelocity(const vector<Matrix>& bases, Values& velocities, const Sigmas& sigmas,
    const boost::shared_ptr<ClassPriors>& classPriors,
    const vector<boost::shared_ptr<ImagePairGrad> >& imagePairs)
{
  const DenseIndex q = bases.front().cols();

  // Update velocity
  NonlinearFactorGraph graph;
  for(size_t frame = 0; frame < imagePairs.size(); ++frame)
  {
    graph += Velocity2Factor(frame, bases[0], bases[1], sigmas, classPriors, imagePairs[frame]);
    graph += PriorFactor<LieVector>(frame, LieVector(Vector::Zero(q)), noiseModel::Isotropic::Sigma(q, 1e6));
  }
  velocities = velocities.retract(graph.linearize(velocities)->optimize(boost::none, EliminateQR));

  return graph.error(velocities);
}

/* ************************************************************************* */
vector<Matrix> createBasesConvergence(DenseIndex wid, DenseIndex hei)
{
  vector<Matrix> result(2, Matrix(2*wid*hei, 1));
  for(int j = 0; j < wid; ++j) {
    for(int i = 0; i < hei; ++i)
    {
      double ux_1_1 = (double(i) - hei/2) / wid;
      double uy_1_1 = -(double(j) - wid/2) / wid;

      double ux_2_1 = 1.0;
      double uy_2_1 = 0.0;

      DenseIndex pix = j * hei + i;
      result[0].middleRows(2*pix, 2) <<
          ux_1_1,
          uy_1_1;
      result[1].middleRows(2*pix, 2) <<
          ux_2_1,
          uy_2_1;
    }
  }
  return result;
}

#if 0
/* ************************************************************************* */
TEST_UNSAFE(Basis2Learning, convergence)
{
  boost::random::mt19937 rng;
  vector<boost::shared_ptr<ImagePairGrad> > imagePairs;
  const int wid = 50, hei = 50, npix = wid*hei;
  const int q = 1;

  boost::filesystem::create_directory("convergence");

  ofstream velfile("convergence/velocity.txt");
  ofstream flowfile("convergence/flow.txt");
  ofstream basisfile1("convergence2/basis_1.txt");

  vector<Vector> trueVelocities;
  vector<Matrix> trueBases = createBasesConvergence(wid, hei);

  basisfile1 << trueBases[0] << endl;
  basisfile1.close();

  for(int img = 0; img < 100; ++img)
  {
    Matrix testImg1(hei, wid);
    Matrix testImg2(hei, wid);

    boost::random::uniform_real_distribution<> ctr(0, wid);
    boost::random::uniform_real_distribution<> shift(-0.5, 0.5);
    boost::random::uniform_int_distribution<> split(0, hei);
    boost::random::uniform_int_distribution<> direction(0, 1);

    double ctrx = ctr(rng), ctry = ctr(rng);
    double v = shift(rng);
    int splitv = split(rng);
    int directionv = direction(rng);

    velfile << v << "\n";

    for(int j = 0; j < wid; ++j) {
      for(int i = 0; i < hei; ++i) {
        double dy = double(i) - ctry;
        double dx = double(j) - ctrx;
        double r = sqrt(dx*dx + dy*dy);
        testImg1(i,j) = r / hei;

        double dx2, dy2;
        if((directionv == 0 && i < splitv) || (directionv == 1 && i >= splitv)) {
          double ux = trueBases[0].row(2*pix).dot(v);
          double uy = trueBases[0].row(2*pix + 1).dot(v);
          dy2 = (double(i) - (-uy)) - ctry;
          dx2 = (double(j) - (-ux)) - ctrx;
          flowfile << ux << " " << uy << " ";
        } else {
          double ux = trueBases[1].row(2*pix).dot(v);
          double uy = trueBases[1].row(2*pix + 1).dot(v);
          dy2 = (double(i) - uy) - ctry;
          dx2 = (double(j) - ux) - ctrx;
          flowfile << ux << " " << uy << " ";
        }
        r = sqrt(dx2*dx2 + dy2*dy2);
        testImg2(i,j) = r / hei;
      }
    }

    flowfile << "\n";

    imagePairs.push_back(boost::make_shared<ImagePairGrad>(testImg1, testImg2));
    trueVelocities.push_back((Vector(1) << v).finished());
  }

  velfile.close();
  flowfile.close();


  Sigmas sigmas;
  sigmas.flowSigmaB1 = 0.01;
  sigmas.flowSigmaB2 = 0.01;
  sigmas.flowSigmaF = 0.05;
  sigmas.pixSigmaB1 = 0.001;
  sigmas.pixSigmaB2 = 0.001;
  sigmas.pixSigmaF = 0.005;

  // Set up class priors
  int split = hei / 2;
  boost::shared_ptr<ClassPriors> classPriors = boost::make_shared<ClassPriors>();
  classPriors->get<0>() = Matrix::Constant(hei, wid, 0.1);
  classPriors->get<1>() = Matrix(hei, wid);
  classPriors->get<1>().topRows(split).setConstant(0.45);
  classPriors->get<1>().bottomRows(hei - split).setConstant(0.45);
  classPriors->get<2>() = Matrix(hei, wid);
  classPriors->get<2>().topRows(split).setConstant(0.45);
  classPriors->get<2>().bottomRows(hei - split).setConstant(0.45);

  cout << "classPriors<1>:\n" << classPriors->get<1>() << endl;

  // Initialize with random basis and random velocity
  vector<Matrix> bases;
  bases.push_back(Matrix::Random(2 * npix, q));
  bases.push_back(Matrix::Random(2 * npix, q));
  Values velocities;
  for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    velocities.insert(frame, LieVector(Vector::Random(q)));
    //velocities.insert(frame, LieVector(trueVelocities[frame]));

  for(int it = 1; it <= 20; ++it)
  {
    // Update bases then velocity
    double error = 0.0;
    error += updateBases(bases, velocities, sigmas, classPriors, imagePairs);
    error += updateVelocity(bases, velocities, sigmas, classPriors, imagePairs);
    cout << "Total error = " << error << endl;

    for(size_t k = 0; k < bases.size(); ++k)
    {
      ofstream basisfile((boost::format("convergence/basis_%d_%04d.txt")%(k+1)%it).str().c_str());
      basisfile << bases[k];
    }

    ofstream velfile((boost::format("convergence/velocity_%04d.txt")%it).str().c_str());
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
      velfile << velocities.at<LieVector>(frame).transpose() << "\n";

    // Write out error details
    ofstream err1file((boost::format("convergence/errors_1_%04d.txt")%it).str().c_str());
    ofstream err2file((boost::format("convergence/errors_2_%04d.txt")%it).str().c_str());
    ofstream indfile((boost::format("convergence/indicators_%04d.txt")%it).str().c_str());
    ofstream dtfile((boost::format("convergence/Dt_%04d.txt")%it).str().c_str());
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    {
      for(DenseIndex pixX = 0; pixX < wid; ++pixX) {
        for(DenseIndex pixY = 0; pixY < hei; ++pixY)
        {
          const DenseIndex pix = pixX * hei + pixY;
          const ImagePairGrad& imgPair = *imagePairs[frame];
          const Eigen::RowVector2d grad = flow::grad(imgPair.prevGradX(), imgPair.prevGradY(), pixX, pixY);
          const double Dt = imgPair.curFrame()(pixY, pixX) - imgPair.prevFrame()(pixY, pixX);
          const Eigen::RowVector3d imgErrors = (Eigen::RowVector3d() <<
            Dt,
            flow::imgError(Dt, grad, bases[0].middleRows(2*pix, 2), velocities.at<LieVector>(frame)),
            flow::imgError(Dt, grad, bases[1].middleRows(2*pix, 2), velocities.at<LieVector>(frame).head(bases[1].cols()))
            ).finished();
          err1file << imgErrors(1) << " ";
          err2file << imgErrors(2) << " ";
          const Eigen::RowVector3d priors(
              classPriors->get<0>()(pixY,pixX),
              classPriors->get<1>()(pixY,pixX),
              classPriors->get<2>()(pixY,pixX));
          const Eigen::RowVector3d sigmasv =
            (Eigen::RowVector3d(sigmas.flowSigmaF, sigmas.flowSigmaB1, sigmas.flowSigmaB2).array().square() *
            grad.squaredNorm()
            + Eigen::RowVector3d(sigmas.pixSigmaF, sigmas.pixSigmaB1, sigmas.pixSigmaB2).array().square())
            .cwiseSqrt();
          const Eigen::Vector3d indicators = linearFlow::indicatorProbabilities<1,3>(imgErrors, priors, sigmasv.cwiseInverse());
          indfile << indicators.transpose() << " ";
          dtfile << Dt << " ";
        }
      }
      err1file << "\n";
      err2file << "\n";
      indfile << "\n";
      dtfile << "\n";
    }

    cout << "Finished iteration " << it << endl;
  }
}
#endif

/* ************************************************************************* */
vector<Matrix> createBasesConvergence2(DenseIndex wid, DenseIndex hei)
{
  vector<Matrix> result(2, Matrix(2*wid*hei, 2));
  for(int j = 0; j < wid; ++j) {
    for(int i = 0; i < hei; ++i)
    {
      double ux_1_1 = (double(i) - hei/2) / wid;
      double ux_1_2 = 0.0;
      double uy_1_1 = -(double(j) - wid/2) / wid;
      double uy_1_2 = 1.0;

      double ux_2_1 = (double(j) - wid/2) / (wid/2);
      double ux_2_2 = 0.0;
      double uy_2_1 = 0.0;
      double uy_2_2 = (double(i) - hei/2) / (hei/2);

      DenseIndex pix = j * hei + i;
      result[0].middleRows(2*pix, 2) <<
          ux_1_1, ux_1_2,
          uy_1_1, uy_1_2;
      result[1].middleRows(2*pix, 2) <<
          ux_2_1, ux_2_2,
          uy_2_1, uy_2_2;
    }
  }
  return result;
}

/* ************************************************************************* */
TEST_UNSAFE(Basis2Learning, convergence2)
{
  boost::random::mt19937 rng;
  vector<boost::shared_ptr<ImagePairGrad> > imagePairs;
  const int wid = 24, hei = 24, npix = wid*hei;
  const int q = 2;

  boost::filesystem::create_directory("convergence2");

  ofstream velfile("convergence2/velocity.txt");
  ofstream flowfile("convergence2/flow.txt");
  ofstream basisfile1("convergence2/basis_1.txt");
  ofstream basisfile2("convergence2/basis_2.txt");

  vector<Vector> trueVelocities;
  vector<Matrix> trueBases = createBasesConvergence2(wid, hei);

  basisfile1 << trueBases[0] << endl;
  basisfile1.close();
  basisfile2 << trueBases[1] << endl;
  basisfile2.close();

  for(int img = 0; img < 100; ++img)
  {
    Matrix testImg1(hei, wid);
    Matrix testImg2(hei, wid);

    boost::random::uniform_real_distribution<> ctr(0, wid);
    boost::random::uniform_int_distribution<> split(0, hei);
    boost::random::uniform_real_distribution<> shift(-0.5, 0.5);

    Eigen::Vector2d v;
    v << shift(rng), shift(rng);

    double ctrx = ctr(rng), ctry = ctr(rng);
    int splitv = split(rng);

    velfile << v.transpose() << "\n";

    for(int j = 0; j < wid; ++j) {
      for(int i = 0; i < hei; ++i) {
        double dy = double(i) - ctry;
        double dx = double(j) - ctrx;
        double r = sqrt(dx*dx + dy*dy);
        testImg1(i,j) = r / hei;

        const DenseIndex pix = j * hei + i;

        double dx2, dy2;
        if(i < splitv) {
          double ux = trueBases[0].row(2*pix).dot(v);
          double uy = trueBases[0].row(2*pix + 1).dot(v);
          dy2 = (double(i) - (-uy)) - ctry;
          dx2 = (double(j) - (-ux)) - ctrx;
          flowfile << ux << " " << uy << " ";
        } else {
          double ux = trueBases[1].row(2*pix).dot(v);
          double uy = trueBases[1].row(2*pix + 1).dot(v);
          dy2 = (double(i) - uy) - ctry;
          dx2 = (double(j) - ux) - ctrx;
          flowfile << ux << " " << uy << " ";
        }
        r = sqrt(dx2*dx2 + dy2*dy2);
        testImg2(i,j) = r / hei;
      }
    }

    flowfile << "\n";

    imagePairs.push_back(boost::make_shared<ImagePairGrad>(testImg1, testImg2));
    trueVelocities.push_back((Vector(1) << v).finished());
  }

  velfile.close();
  flowfile.close();


  Sigmas sigmas;
  sigmas.flowSigmaB1 = 0.01;
  sigmas.flowSigmaB2 = 0.01;
  sigmas.flowSigmaF = 0.05;
  sigmas.pixSigmaB1 = 0.001;
  sigmas.pixSigmaB2 = 0.001;
  sigmas.pixSigmaF = 0.005;

  // Set up class priors
  int split = hei / 2;
  boost::shared_ptr<ClassPriors> classPriors = boost::make_shared<ClassPriors>();
  classPriors->get<0>() = Matrix::Constant(hei, wid, 0.1);
  classPriors->get<1>() = Matrix(hei, wid);
  classPriors->get<1>().topRows(split).setConstant(0.45);
  classPriors->get<1>().bottomRows(hei - split).setConstant(0.45);
  classPriors->get<2>() = Matrix(hei, wid);
  classPriors->get<2>().topRows(split).setConstant(0.45);
  classPriors->get<2>().bottomRows(hei - split).setConstant(0.45);

  cout << "classPriors<1>:\n" << classPriors->get<1>() << endl;

  // Initialize with random basis and random velocity
  vector<Matrix> bases;
  bases.push_back(Matrix::Random(2 * npix, q));
  bases.push_back(Matrix::Random(2 * npix, q));
  Values velocities;
  for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    velocities.insert(frame, LieVector(Vector::Random(q)));
    //velocities.insert(frame, LieVector(trueVelocities[frame]));

  for(int it = 1; it <= 200; ++it)
  {
    // Update bases then velocity
    double error = 0.0;
    error += updateBases(bases, velocities, sigmas, classPriors, imagePairs);
    error += updateVelocity(bases, velocities, sigmas, classPriors, imagePairs);
    cout << "Total error = " << error << endl;

    for(size_t k = 0; k < bases.size(); ++k)
    {
      ofstream basisfile((boost::format("convergence2/basis_%d_%04d.txt")%(k+1)%it).str().c_str());
      basisfile << bases[k];
    }

    ofstream velfile((boost::format("convergence2/velocity_%04d.txt")%it).str().c_str());
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
      velfile << velocities.at<LieVector>(frame).transpose() << "\n";

    // Write out error details
    ofstream err1file((boost::format("convergence2/errors_1_%04d.txt")%it).str().c_str());
    ofstream err2file((boost::format("convergence2/errors_2_%04d.txt")%it).str().c_str());
    ofstream indfile((boost::format("convergence2/indicators_%04d.txt")%it).str().c_str());
    ofstream dtfile((boost::format("convergence2/Dt_%04d.txt")%it).str().c_str());
    for(size_t frame = 0; frame < imagePairs.size(); ++frame)
    {
      for(DenseIndex pixX = 0; pixX < wid; ++pixX) {
        for(DenseIndex pixY = 0; pixY < hei; ++pixY)
        {
          const DenseIndex pix = pixX * hei + pixY;
          const ImagePairGrad& imgPair = *imagePairs[frame];
          const Eigen::RowVector2d grad = flow::grad(imgPair.prevGradX(), imgPair.prevGradY(), pixX, pixY);
          const double Dt = imgPair.curFrame()(pixY, pixX) - imgPair.prevFrame()(pixY, pixX);
          const Eigen::RowVector3d imgErrors = (Eigen::RowVector3d() <<
            Dt,
            flow::imgError(Dt, grad, bases[0].middleRows(2*pix, 2), velocities.at<LieVector>(frame)),
            flow::imgError(Dt, grad, bases[1].middleRows(2*pix, 2), velocities.at<LieVector>(frame).head(bases[1].cols()))
            ).finished();
          err1file << imgErrors(1) << " ";
          err2file << imgErrors(2) << " ";
          const Eigen::RowVector3d priors(
              classPriors->get<0>()(pixY,pixX),
              classPriors->get<1>()(pixY,pixX),
              classPriors->get<2>()(pixY,pixX));
          const Eigen::RowVector3d sigmasv =
            (Eigen::RowVector3d(sigmas.flowSigmaF, sigmas.flowSigmaB1, sigmas.flowSigmaB2).array().square() *
            grad.squaredNorm()
            + Eigen::RowVector3d(sigmas.pixSigmaF, sigmas.pixSigmaB1, sigmas.pixSigmaB2).array().square())
            .cwiseSqrt();
          const Eigen::Vector3d indicators = linearFlow::indicatorProbabilities<1,3>(imgErrors, priors, sigmasv.cwiseInverse());
          indfile << indicators.transpose() << " ";
          dtfile << Dt << " ";
        }
      }
      err1file << "\n";
      err2file << "\n";
      indfile << "\n";
      dtfile << "\n";
    }

    cout << "Finished iteration " << it << endl;
  }
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
