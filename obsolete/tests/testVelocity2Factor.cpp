/**
* @file    testVelocity2Factor.cpp
* @brief
* @author  Richard Roberts
* @created Oct 4, 2013
*/

#include <CppUnitLite/TestHarness.h>

#include <generalflow_denselabeling/Velocity2Factor.h>

#include <gtsam/slam/PriorFactor.h>
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>

#include <iostream>
#include <tbb/tbb.h>

using namespace generalflow::denselabeling;
using namespace generalflow::flow;
using namespace generalflow;
using namespace gtsam;
using namespace std;

tbb::task_scheduler_init init(1);

/* ************************************************************************* */
namespace {
  struct CallError
  {
    const Velocity2Factor& factor;

    CallError(const Velocity2Factor& factor) : factor(factor) {}

    double operator()(const LieVector& value) const
    {
      Values values;
      values.insert(factor.latentKey(), value);
      return factor.error(values);
    }
  };

  struct LinearError
  {
    const HessianFactor& factor;

    LinearError(const HessianFactor& factor) : factor(factor) {}

    double operator()(const Vector& value) const
    {
      VectorValues values;
      values.insert(factor.keys().front(), value);
      return factor.error(values);
    }
  };
}

/* ************************************************************************* */
TEST(Velocity2Factor, evaluateError)
{
  Matrix img1(6,6);
  img1 <<
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6,
    1, 2, 3, 4, 5, 6;

  Matrix img2(6,6);
  img2 <<
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7,
    2, 3, 4, 5, 6, 7;

  ImagePairGrad imgpair(img1, img2);

  // Hack:  remove zero boundaries from image gradients so that error really evaluates to zero
  imgpair.prevGradX_.setConstant(imgpair.prevGradX_(3,3));
  imgpair.prevGradY_.setConstant(imgpair.prevGradY_(3,3));

  Matrix basisX(6,6);
  basisX <<
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
    -0.5, -0.5, -0.5, -0.5, -0.5, -0.5;
  Matrix basisY(6,6);
  basisY <<
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0;
  Matrix basis(basisX.cols() * basisX.rows()*2, 1);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis.data(), basis.rows() / 2) =
    Eigen::Map<Vector>(basisX.data(), basis.rows() / 2);
  Eigen::Map<Vector, 0, Eigen::InnerStride<2> >(basis.data() + 1, basis.rows() / 2) =
    Eigen::Map<Vector>(basisY.data(), basis.rows() / 2);

  Matrix classPrior1 = Matrix::Constant(6, 6, 1.0);
  Matrix classPrior2 = Matrix::Constant(6, 6, 0.0);
  boost::shared_ptr<ClassPriors> classPriors =
    boost::make_shared<ClassPriors>(classPrior2, classPrior1, classPrior2);

  const Vector velocity = Vector::Constant(1, 2.0);
  Values values;
  values.insert(0, LieVector(velocity));
  Sigmas sigmas;
  sigmas.flowSigmaB1 = 0.1;
  sigmas.flowSigmaB2 = 0.1;
  sigmas.flowSigmaF = 1.0;
  sigmas.pixSigmaB1 = 0.01;
  sigmas.pixSigmaB2 = 0.01;
  sigmas.pixSigmaF = 0.01;
  Velocity2Factor factor(0, basis, basis, sigmas, classPriors, boost::make_shared<ImagePairGrad>(imgpair));

  Matrix expectedHessian = numericalHessian(
    boost::function<double(const LieVector&)>(CallError(factor)), LieVector(velocity));
  Vector expectedGradient = numericalGradient(
    boost::function<double(const LieVector&)>(CallError(factor)), LieVector(velocity));
  double expectedError = 0.0;

  HessianFactor hf = dynamic_cast<const HessianFactor&>(*factor.linearize(values));
  Matrix actualHessian = hf.info(hf.begin(), hf.begin());
  Vector actualGradient = -hf.linearTerm();
  double actualError = factor.error(values);

  Matrix linearHessian = numericalHessian(
    boost::function<double(const LieVector&)>(LinearError(hf)), LieVector(Vector::Zero(velocity.size())));
  Vector linearGradient = numericalGradient(
    boost::function<double(const LieVector&)>(LinearError(hf)), LieVector(Vector::Zero(velocity.size())));
  double linearError1 = 0.5 * hf.constantTerm();
  double linearError2 = hf.error(values.zeroVectors());

  EXPECT(assert_equal(expectedHessian, actualHessian, 1e-2));
  EXPECT(assert_equal(expectedHessian, linearHessian, 1e-2));
  EXPECT(assert_equal(expectedGradient, actualGradient, 1e-4));
  EXPECT(assert_equal(expectedGradient, linearGradient, 1e-4));
  EXPECT_DOUBLES_EQUAL(expectedError, actualError, 1e-9);
  EXPECT_DOUBLES_EQUAL(expectedError, linearError1, 1e-9);
  EXPECT_DOUBLES_EQUAL(expectedError, linearError2, 1e-9);
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

