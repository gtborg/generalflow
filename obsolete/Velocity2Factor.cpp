/**
* @file    Velocity2Factor.cpp
* @brief   Image factor on only the latent variables
* @author  Richard Roberts
* @created Aug 4, 2012
*/

#include <tbb/tbb.h>

#include <generalflow_denselabeling/Velocity2Factor.h>
#include <generalflow_flow/flowOps.h>

#include <gtsam/base/LieMatrix.h>
#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieScalar.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/linear/HessianFactor.h>
#include <boost/format.hpp>
#include <boost/assign/list_of.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace denselabeling {

    inline double sq(double x) { return x*x; }

    /* **************************************************************************************** */
    void Velocity2Factor::print(const string& s /* = "" */, const KeyFormatter& keyFormatter /* = gtsam::DefaultKeyFormatter */) const {
      Base::print(s, keyFormatter);
    }

    /* **************************************************************************************** */
    bool Velocity2Factor::equals(const NonlinearFactor& f, double tol /* = 1e-9 */) const {
      const Velocity2Factor *t = dynamic_cast<const This*>(&f);
      return t && Base::equals(f) &&
        equal_with_abs_tol(basis1_, t->basis1_, tol) &&
        equal_with_abs_tol(basis2_, t->basis2_, tol) &&
        sigmas_.equals(t->sigmas_, tol) &&
        equal_with_abs_tol(classPriors_->get<0>(), t->classPriors_->get<0>()) &&
        equal_with_abs_tol(classPriors_->get<1>(), t->classPriors_->get<1>()) &&
        equal_with_abs_tol(classPriors_->get<2>(), t->classPriors_->get<2>()) &&
        images_->equals(*t->images_, tol);
    }

    /* **************************************************************************************** */
    namespace
    {
      struct AccumulateErrorChunk
      {
        const Velocity2Factor& velocity2Factor;
        const LieVector& latent;
        double error;

        AccumulateErrorChunk(const Velocity2Factor& velocity2Factor, const LieVector& latent) :
          velocity2Factor(velocity2Factor), latent(latent), error(0.0) {}

        AccumulateErrorChunk(const AccumulateErrorChunk& original, tbb::split) :
          velocity2Factor(original.velocity2Factor), latent(original.latent), error(0.0) {}

        void join(const AccumulateErrorChunk& other)
        {
          error += other.error;
        }

        void operator()(const tbb::blocked_range<size_t>& range)
        {
          const DenseIndex imHeight = velocity2Factor.images()->curFrame().rows();

          Matrix W1i, W2i;

          for(size_t pix = range.begin(); pix != range.end(); ++pix)
          {
            DenseIndex i = pix % imHeight;
            DenseIndex j = pix / imHeight;

            // Store blocks in matrices - we copy here to avoid copying every
            // time we would pass a block into fullImageError.
            // TODO:  Modify fullImageError to avoid copy altogether.
            W1i = velocity2Factor.basis1().middleRows(2*pix, 2);
            W2i = velocity2Factor.basis2().middleRows(2*pix, 2);

            // Compute sqrt error and derivatives
            Eigen::Vector2d e;
            if(velocity2Factor.indicators())
            {
              e = flow::fullImageError(*velocity2Factor.images(), j, i, W1i, W2i, latent,
                  velocity2Factor.sigmas().flowSigmaF, velocity2Factor.sigmas().flowSigmaB1, velocity2Factor.sigmas().flowSigmaB2,
                  velocity2Factor.sigmas().pixSigmaF, velocity2Factor.sigmas().pixSigmaB1, velocity2Factor.sigmas().pixSigmaB2,
                  velocity2Factor.indicators()->col(pix), boost::none, boost::none, boost::none);
            }
            else
            {
              Eigen::Vector3d indicators;
              e = flow::fullImageError(*velocity2Factor.images(), j, i, W1i, W2i, latent,
                  velocity2Factor.classPriors()->get<0>()(i, j),
                  velocity2Factor.classPriors()->get<1>()(i, j),
                  velocity2Factor.classPriors()->get<2>()(i, j),
                  velocity2Factor.sigmas().flowSigmaF, velocity2Factor.sigmas().flowSigmaB1, velocity2Factor.sigmas().flowSigmaB2,
                  velocity2Factor.sigmas().pixSigmaF, velocity2Factor.sigmas().pixSigmaB1, velocity2Factor.sigmas().pixSigmaB2,
                  indicators, boost::none, boost::none, boost::none);
            }

            // Update error
            error += e.squaredNorm();
          }
        }
      };
    }

    /* **************************************************************************************** */
    double Velocity2Factor::error(const Values& x) const
    {
      AccumulateErrorChunk errorAccumulator(*this, x.at<LieVector>(latentKey()));
      const size_t nPix = images_->curFrame().cols() * images_->curFrame().rows();
      tbb::parallel_reduce(tbb::blocked_range<size_t>(0, nPix), errorAccumulator);
      return 0.5 * errorAccumulator.error;
    }

    /* **************************************************************************************** */
    namespace
    {
      struct AccumulateHessianChunk
      {
        const Velocity2Factor& velocity2Factor;
        const LieVector& latent;
        Matrix hessian;

        AccumulateHessianChunk(const Velocity2Factor& velocity2Factor, const LieVector& latent) :
          velocity2Factor(velocity2Factor), latent(latent),
          hessian(Matrix::Zero(latent.size()+1, latent.size()+1)) {}

        AccumulateHessianChunk(const AccumulateHessianChunk& original, tbb::split) :
          velocity2Factor(original.velocity2Factor), latent(original.latent),
          hessian(Matrix::Zero(latent.size()+1, latent.size()+1)) {}

        void join(const AccumulateHessianChunk& other)
        {
          hessian += other.hessian;
        }

        void operator()(const tbb::blocked_range<size_t>& range)
        {
          const int imHeight = velocity2Factor.images()->curFrame().rows();

          Matrix W1i, W2i;
          Matrix ddy;

          for(size_t pix = range.begin(); pix != range.end(); ++pix)
          {
            int i = pix % imHeight;
            int j = pix / imHeight;

            // Store blocks in matrices - we copy here to avoid copying every
            // time we would pass a block into fullImageError.
            // TODO:  Modify fullImageError to avoid copy altogether.
            W1i = velocity2Factor.basis1().middleRows(2*pix, 2);
            W2i = velocity2Factor.basis2().middleRows(2*pix, 2);

            // Compute sqrt error and derivatives
            Eigen::Vector2d e;
            if(velocity2Factor.indicators())
            {
              e = flow::fullImageError(*velocity2Factor.images(), j, i, W1i, W2i, latent,
                  velocity2Factor.sigmas().flowSigmaF, velocity2Factor.sigmas().flowSigmaB1, velocity2Factor.sigmas().flowSigmaB2,
                  velocity2Factor.sigmas().pixSigmaF, velocity2Factor.sigmas().pixSigmaB1, velocity2Factor.sigmas().pixSigmaB2,
                  velocity2Factor.indicators()->col(pix), boost::none, boost::none, ddy);
            }
            else
            {
              Eigen::Vector3d indicators;
              e = flow::fullImageError(*velocity2Factor.images(), j, i, W1i, W2i, latent,
                  velocity2Factor.classPriors()->get<0>()(i, j),
                  velocity2Factor.classPriors()->get<1>()(i, j),
                  velocity2Factor.classPriors()->get<2>()(i, j),
                  velocity2Factor.sigmas().flowSigmaF, velocity2Factor.sigmas().flowSigmaB1, velocity2Factor.sigmas().flowSigmaB2,
                  velocity2Factor.sigmas().pixSigmaF, velocity2Factor.sigmas().pixSigmaB1, velocity2Factor.sigmas().pixSigmaB2,
                  indicators, boost::none, boost::none, ddy);
            }

            // Update hessian (only the upper triangle, which is all that GTSAM uses)
            // The factors of 2 here are because this is the Hessian without the 1/2 factor applied in error().
            hessian.topLeftCorner(latent.size(), latent.size()).selfadjointView<Eigen::Upper>()
                .rankUpdate(ddy.transpose());
            hessian.col(latent.size()).head(latent.size()) -= ddy.transpose() * e;
            hessian(latent.size(), latent.size()) += e.squaredNorm();
          }
        }
      };
    }

    /* **************************************************************************************** */
    boost::shared_ptr<GaussianFactor> Velocity2Factor::linearize(const Values& x) const
    {
      // Values for convenience
      const LieVector& latent = x.at<LieVector>(latentKey());
      const DenseIndex q = latent.size();
      const int imWidth = images_->curFrame().cols();
      const int imHeight = images_->curFrame().rows();
      const size_t nPix = imWidth * imHeight;

      // Allocate Hessian (matrix is uninitialized)
      HessianFactor::shared_ptr hessian = boost::make_shared<HessianFactor>(
        ListOfOne(latentKey()), SymmetricBlockMatrix(boost::assign::list_of(q)(1)));

      if (inspection_) {
        lastErrorImage_ = images_->curFrame() - images_->prevFrame();
        lastInlierImage_ = Matrix(lastErrorImage_.rows(), lastErrorImage_.cols());
        lastJacobians_ = Matrix(imWidth*imHeight, q);
      }

      // Loop over all pixels doing updates on the Hessian
      AccumulateHessianChunk hessianAccumulator(*this, latent);
      tbb::parallel_reduce(tbb::blocked_range<size_t>(0, nPix), hessianAccumulator);

      // Copy matrix into HessianFactor, dividing by two to account for the 1/2 factor in error()
      hessian->info() = hessianAccumulator.hessian;

      return hessian;
    }
  }
}
