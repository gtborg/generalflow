import processing.opengl.*;
import processing.opengl.PGraphicsOpenGL;
import java.nio.*; // for FloatBuffer
import javax.media.opengl.*; // for GL

public void setup() {
  size(500, 500, OPENGL);
}


float time = 0;
float eyeX = 0;
float eyeY = 0;
float eyeZ = 10;
float nearClip = 0.01f;
float farClip = 200;

public void draw() {
  resetMatrix(); // set the transformation matrix to the identity

  background(0); // clear the screen to black

  // set up for perspective projection
  perspective(PI * 0.333f, 1.0f, nearClip, farClip);

  // place the camera in the scene (just like gluLookAt())
  camera(eyeX, eyeY, eyeZ, 0.0f, 0.0f, -0.0f, 0.0f, 1.0f, 0.0f);

  scale(1.0f, -1.0f, 1.0f); // change to right-handed coordinate system

  // create an ambient light source
  ambientLight(102, 102, 102);

  // create two directional light sources
  lightSpecular(204, 204, 204);
  directionalLight(102f, 102f, 102f, -0.7f, -0.7f, -1f);
  directionalLight(152, 152, 152, 0, 0, -1);

  // Right wall
  pushMatrix();

  fill(100, 100, 200);
  ambient(100, 100, 200);
  specular(0, 0, 0);
  shininess(1.0f);

  translate(25, 0, 10);
  // rotate(-time, 1.0f, 0.0f, 0.0f);
  box(1, 50, 2000);

  popMatrix();

  // Left Wall
  pushMatrix();

  fill(100, 100, 200);
  ambient(100, 100, 200);
  specular(0, 0, 0);
  shininess(1.0f);

  translate(-25, 0, 10);
  box(1, 50, 2000);

  popMatrix();

  // Floor
  pushMatrix();

  fill(100, 100, 200);
  ambient(100, 100, 200);
  specular(0, 0, 0);
  shininess(1.0f);

  translate(0, -24, 12);
  box(47, 1, 2000);

  popMatrix();

  // Test sphere
  pushMatrix();

  fill(100, 100, 200);
  ambient(100, 100, 200);
  specular(0, 0, 0);
  shininess(1.0f);
  noStroke();

  translate(10, 0, -50);

  sphereDetail(40);
  sphere(8);

  stroke(0);
  popMatrix();


  //float cameraZ = ((height / 2.0f) / tan(PI * 60.0f / 360.0f));
  //float near = cameraZ / 10.0f;
  //float far = cameraZ * 10.0f;

  // openGL for processing
  PGraphicsOpenGL pogl;
  FloatBuffer zbuff;
  float z;
  float worldZ;
  float realZ;
  loadPixels();


  // 1/2 inch sensor for 640 * 480 res used for conversion to meters
  // change fLength, realZ, and v
  
  //focal length in mm
  float fLength = 6; 
  //change mm to pixels
  fLength = .0006*METERSTOPIXELS;
  //conversion from 1 meter to 1 inch
  float METERTOINCH = 39.3700787;
  //either the height or width of the screen size
  float SCREENRES = 640;
  //conversion from meters to pixels
  float METERSTOPIXELS = METERTOINCH*SCREENRES*2;
  
  //change meters to pixels
  float vx = 1;
  float vy = 1;
  float vz = 1;
  
  //change meters to pixels
  vx *= METERSTOPIXELS;
  vy *= METERSTOPIXELS;
  vz += METERSTOPIXELS;

  float wx = 1;
  float wy = 1;
  float wz = 1;
  
  
  

  Matrix A = new Matrix(2, 3);
  Matrix B = new Matrix(2, 3);
  Matrix v = new Matrix(3, 1);
  Matrix w = new Matrix(3, 1);
  Matrix AZ = new Matrix(2, 3);
  Matrix AZv = new Matrix(2, 1);
  Matrix Bw = new Matrix(2, 1);
  Matrix U = new Matrix(2, 1);

  for (int i = 0; i < width; i++)
    for (int j = 0; j < height; j++) {
      // Find the z-buffer for each pixel with openGL commands
      pogl = (PGraphicsOpenGL) g;
      zbuff = FloatBuffer.allocate(1);
      pogl.gl.glReadPixels(i, j, 1, 1, GL.GL_DEPTH_COMPONENT, GL.GL_FLOAT, zbuff);
      z = zbuff.get();
      //System.out.println("zbuff: " + z);

      // conversion to perspective mode
      worldZ = 2.0f * farClip * nearClip / (((z - 0.5f) * 2.0f) * (farClip - nearClip) - (farClip + nearClip));

      // find real world depth (worldZ + distance to camera)
      realZ = -(worldZ + eyeZ);
      
      //convert realZ from meters to pixels
      



      // create A
      A.data[0][0] = -fLength;
      A.data[0][2] = j;
      A.data[1][1] = -fLength;
      A.data[1][2] = i; 

      // create B
      B.data[0][0] = j*i/fLength;
      B.data[0][1] = -(fLength + (j*j)/fLength);
      B.data[0][2] = i;
      B.data[1][0] = fLength + (i*i)/fLength;
      B.data[1][1] = -(j*i)/fLength;
      B.data[1][2] = -j;

      // create v
      v.data[0][0] = vx;
      v.data[1][0] = vy;
      v.data[2][0] = vz;

      // create w
      w.data[0][0] = wx;
      w.data[1][0] = wy;
      w.data[2][0] = wz;

      // multiply A by scalar z
      AZ = A.newcopy();
      AZ.multf(1/realZ);

      // multiply AZ by v
      AZv.thisprod(AZ, v);

      // multiply B by w
      Bw.thisprod(B, w);

      // U = AZv + Bw
      U.thissum(AZv, Bw);




      // color pixels based on depth
      color c = color(0, 0, 0);
      if (realZ > 100)
        c = color(50, 50, 50);
      else if (realZ > 75)
        c = color(0, 200 * realZ/100, 0);
      else if (realZ > 50)
        c = color(0, 0, 200 * realZ/100);
      else if (realZ > 25)
        c = color(400 * realZ/100, 0, 0);


      pixels[j * width + i] = color(realZ + 100, realZ + 100, realZ + 100);
      //pixels[j * width + i] = c;
      //System.out.println("real world depth: " + realZ);
    }
  U.printm();
  updatePixels();
  time += 0.05;
}

