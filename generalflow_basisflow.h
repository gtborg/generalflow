virtual class gtsam::LieVector;
virtual class gtsam::Rot2;
virtual class gtsam::Rot3;
virtual class gtsam::Pose2;
virtual class gtsam::Pose3;
virtual class gtsam::Cal3_S2;
virtual class gtsam::Cal3DS2;
virtual class gtsam::Point2;
virtual class gtsam::Point3;
virtual class gtsam::NonlinearFactor;
class gtsam::NonlinearFactorGraph;
class gtsam::Values;
virtual class gtsam::noiseModel::Base;
class gtsam::Symbol;

#include <gtsam/geometry/Pose2.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/nonlinear/Symbol.h>

namespace gtsam {

#include <gtsam/base/timing.h>
/** print the timing outline */
void tictoc_print_();
void tictoc_reset_();

}

namespace basisflow {

#include <basisflow/GMPPCALucasKanade.h>
class GMPPCALucasKanade {
  GMPPCALucasKanade(const basisflow::Subspace& subspace, const basisflow::GMPPCALKParams& params, size_t imWidth);
  void setNextFrame(const cv::Mat& frame);
  basisflow::Subspace subspace(size_t level) const;
  cv::Mat prevFrame(size_t level) const;
  cv::Mat curFrame(size_t level) const;
};

class GMPPCALKParams {
  GMPPCALKParams(size_t imageScalePower, size_t flowSamplePower, double blurSigma, size_t nPyrLevels, size_t firstPyrLevel);
};

#include <basisflow/OnlyBasisImageFactor.h>
virtual class OnlyBasisImageFactor : gtsam::NonlinearFactor {
  OnlyBasisImageFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
    gtsam::Values* latentValues, gtsam::Values* sigmaValues, double inlierPrior,
    const basisflow::IntVector& frameNums, basisflow::FrameCache* frameCache);
};

#include <basisflow/OnlyLatentImageFactor.h>
virtual class OnlyLatentImageFactor : gtsam::NonlinearFactor {
  OnlyLatentImageFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
    gtsam::Values* basisValues, gtsam::Values* sigmaValues, double inlierPrior,
    int frameNum, basisflow::FrameCache* frameCache);
  void inspection(bool enable) const;
  Matrix lastErrorImage() const;
  Matrix lastInlierImage() const;
  Matrix lastJacobians() const;
};

#include <basisflow/OnlySigmaImageFactor.h>
virtual class OnlySigmaImageFactor : gtsam::NonlinearFactor {
  OnlySigmaImageFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
    gtsam::Values *basisValues, gtsam::Values *latentValues,
    double inlierPrior, const basisflow::IntVector& frameNums,
    basisflow::FrameCache *frameCache);
};

#include <basisflow/OnlyBasisFlowFactor.h>
virtual class OnlyBasisFlowFactor : gtsam::NonlinearFactor {
  //OnlyBasisFlowFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
  //  gtsam::Values* latentValues, gtsam::Values* sigmaValues,
  //  double inlierPrior, const basisflow::SparseFlowFieldMeasurement& measurement);
  static gtsam::NonlinearFactorGraph CreateWholeFrame(const basisflow::SparseFlowField& measurements,
    size_t firstBasisKey, size_t latentKey, size_t sigmaKey,
    gtsam::Values* latentValues, gtsam::Values* sigmaValues, double inlierPrior);
};

#include <basisflow/OnlyLatentFlowFactor.h>
virtual class OnlyLatentFlowFactor : gtsam::NonlinearFactor {
  //OnlyLatentFlowFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
  //  gtsam::Values* basisValues, gtsam::Values* sigmaValues,
  //  double inlierPrior, const basisflow::SparseFlowFieldMeasurement& measurement);
  static gtsam::NonlinearFactorGraph CreateWholeFrame(const basisflow::SparseFlowField& measurements,
    size_t firstBasisKey, size_t latentKey, size_t sigmaKey,
    gtsam::Values* basisValues, gtsam::Values* sigmaValues, double inlierPrior);
};

#include <basisflow/OnlyLatent2FlowFactor.h>
virtual class OnlyLatent2FlowFactor : gtsam::NonlinearFactor {
  static gtsam::NonlinearFactorGraph CreateWholeFrame(const basisflow::SparseFlowField& measurements,
    size_t firstBasis1Key, size_t firstBasis2Key, size_t latentKey, size_t sigmaKey, size_t indicatorKey,
    gtsam::Values* basisValues, gtsam::Values* sigmaValues, gtsam::Values* indicatorValues,
    double comp1Prior, double comp2Prior);
};

#include <basisflow/BasisAlignmentFactor.h>
template<POSE = {gtsam::Pose2, gtsam::Pose3}>
virtual class BasisAlignmentFactor : gtsam::NonlinearFactor {
	BasisAlignmentFactor(size_t j1, size_t j2, size_t jM, Vector measuredY, const gtsam::noiseModel::Base* relativePoseModel);
	BasisAlignmentFactor(const gtsam::Symbol& j1, const gtsam::Symbol& j2, const gtsam::Symbol& jM, Vector measuredY, const gtsam::noiseModel::Base* relativePoseModel);
};

#include <basisflow/ScalarInequality.h>
virtual class ScalarInequality : gtsam::NonlinearFactor {
  ScalarInequality(size_t key, int element, double threshold, bool isGreaterThan, double mu);
  ScalarInequality(size_t key, int element, double threshold, bool isGreaterThan);
  double value(const gtsam::LieVector& x) const;
};

#include <basisflow/PartialDiagonalSchurSolver.h>
class PartialDiagonalSchurSolver {
  PartialDiagonalSchurSolver(int dimI, int dimy);
  void updateI(int i, double L_IiIi, double eta_Ii, double c);
  void updatey(Matrix L_yy, Vector eta_y, double c);
  void updateIy(int i, double L_IiIi, Vector L_yIi, Matrix L_yy, double eta_Ii, Vector eta_y, double c);
  double logLikelihood(Vector I) const;
};

#include <basisflow/ModelSelection.h>
class SubspaceVector {
  SubspaceVector();
  void push_back(const basisflow::Subspace& subspace);
  basisflow::Subspace at(size_t i) const;
  size_t size() const;
};
class ModelSelection {
  ModelSelection(const basisflow::SubspaceVector& subspaces, basisflow::FrameCache* frameCache);
  Vector evaluateLogLikelihood(int frameNum) const;
  Matrix lastRightVectors() const;
  Matrix lastEtaBars() const;
};


#include <basisflow/CostMap.h>
class CostMap {
  CostMap(double width, double height, double cellWidth);
  pair<int, int> mapFromWorld(double x, double y) const;
  pair<double, double> worldFromMap(int x, int y) const;
  void update(const gtsam::Pose3& pose, const gtsam::Cal3_S2& K, Matrix imageLabelsFree, Matrix imageLabelsOccupied,
    int gridWidth, int gridHeight, int imgWidth, int imgHeight);
  Matrix mapFree() const;
  Matrix mapOccupied() const;
};

}
