/**
 * @file    wrappedContainers.h
 * @brief   Containers wrapped for MATLAB
 * @author  Richard Roberts
 * @created February 28, 2012
 */
#pragma once

#include <vector>
#include <utility>

namespace generalflow {

  typedef std::vector<int> IntVector;

  class IntPairVector : public std::vector<std::pair<int,int> > {
  private:
    typedef std::vector<std::pair<int,int> > Base;
  public:
    IntPairVector() {}
    IntPairVector(const Base& rhs) : Base(rhs) {}
    IntPairVector& operator=(const Base& rhs) { Base::operator=(rhs); return *this; }
    void push_back(int a, int b) { Base::push_back(std::make_pair(a,b)); }
  };

}
