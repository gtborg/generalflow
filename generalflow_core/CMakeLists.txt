
# If we have GTSAM, use Eigen from GTSAM, and otherwise look for Eigen
if(TARGET gtsam)
	include_directories("${GTSAM_INCLUDE_DIR}/gtsam/3rdparty/Eigen")
else()
	find_package(Eigen3 REQUIRED)
	include_directories("${EIGEN3_INCLUDE_DIR}")
endif()

# Build library
generalflow_add_subdirectory_library(gtsam)
GeneralFlowWrapThisSubdirectory("")

if(GENERALFLOW_BUILD_TESTS AND GENERALFLOW_BUILD_generalflow_core)
	include_directories(${OpenCV_INCLUDE_DIRS})
	gtsam_add_subdir_tests("core" "generalflow_core;${OpenCV_LIBS}" "generalflow_core;${OpenCV_LIBS}" "")
endif()

# Export targets
set(GeneralFlow_EXPORTED_TARGETS "${GeneralFlow_EXPORTED_TARGETS}" PARENT_SCOPE)
