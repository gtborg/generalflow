/**
 * @file    testLinearFlow.cpp
 * @brief
 * @author  Richard Roberts
 * @created Feb 11, 2011
 */

#include <CppUnitLite/TestHarness.h>
#include <gtsam/base/numericalDerivative.h>
#include <generalflow_core/linearFlow.h>

using namespace generalflow;
using namespace gtsam;
using namespace std;

/* ************************************************************************* */
class CallFullFlowError {
  const SparseFlowField::Measurement& m;
  const double inlierPrior;
  const double flowSigmaV;
  const double flowSigmaF;
public:
  CallFullFlowError(const SparseFlowField::Measurement& _m, double _inlierPrior, double _flowSigmaV, double _flowSigmaF) :
    m(_m), inlierPrior(_inlierPrior), flowSigmaV(_flowSigmaV), flowSigmaF(_flowSigmaF) {}
  Vector operator()(const LieMatrix& basis, const LieVector& latent) const {
    double inlier;
    return linearFlow::fullFlowError(m, inlierPrior, basis, latent, flowSigmaV, flowSigmaF, inlier, boost::none, boost::none);
  }
};

/* ************************************************************************* */
class CallFullFlowError2 {
  const SparseFlowField::Measurement& m;
  const double indicatorPrior1;
  const double indicatorPrior2;
  const double flowSigmaB1;
  const double flowSigmaB2;
  const double flowSigmaF;
public:
  CallFullFlowError2(const SparseFlowField::Measurement& _m, double _indPrior1, double _indPrior2, double _flowSigmaB1, double _flowSigmaB2, double _flowSigmaF) :
    m(_m), indicatorPrior1(_indPrior1), indicatorPrior2(_indPrior2), flowSigmaB1(_flowSigmaB1), flowSigmaB2(_flowSigmaB2), flowSigmaF(_flowSigmaF) {}
  Vector operator()(const LieMatrix& basis1, const LieMatrix& basis2, const LieVector& latent) const {
    Eigen::Vector3d indicators;
    return linearFlow::fullFlowError(m, indicatorPrior1, indicatorPrior2, basis1, basis2, latent, flowSigmaB1, flowSigmaB2, flowSigmaF, indicators);
  }
};

/* ************************************************************************* */
TEST(sparseFlow, fullFlowError)
{
  // Set up basis
  Matrix basis(2,3); basis <<
    1, 0.5, 0,
    0, 0.25, 1;
  Vector latent(3); latent << 1, 2, 0.5;

  // Set up measurement
  SparseFlowField::Measurement measurement;
  measurement.ux_ = 1.5;
  measurement.uy_ = 1.0;

  // Parameters
  const double inlierPrior = 1.0;
  const double flowSigmaV = 0.5;
  const double flowSigmaF = 2.0;

  // Compute numerical derivatives
  boost::function<Vector(const LieMatrix&, const LieVector&)> callFullFlowError(CallFullFlowError(measurement, inlierPrior, flowSigmaV, flowSigmaF));
  Matrix ddVExpected = numericalDerivative21(callFullFlowError, LieMatrix(basis), LieVector(latent));
  Matrix ddyExpected = numericalDerivative22(callFullFlowError, LieMatrix(basis), LieVector(latent));

  // Call fullFlowError
  Matrix ddVActual, ddyActual;
  double inlier;
  Vector e = linearFlow::fullFlowError(measurement, inlierPrior, basis, latent, flowSigmaV, flowSigmaF,
    inlier, ddVActual, ddyActual);

  EXPECT(assert_equal(ddVExpected, ddVActual));
  EXPECT(assert_equal(ddyExpected, ddyActual));
}

/* ************************************************************************* */
TEST(sparseFlow, fullFlowError2)
{
  // Set up basis
  Matrix basis1(2,3); basis1 <<
    1, 0.5,  0,
    0, 0.25, 1;
  Matrix basis2(2,3); basis2 <<
    1,   0.5,  0.5,
    1.5, 0.75,   1;
  Vector latent(3); latent << 1, 2, 0.5;

  // Set up measurement
  SparseFlowField::Measurement measurement1(0, 0, 2.0, 1.0);
  SparseFlowField::Measurement measurement2(0, 0, 2.25, 3.5);

  // Parameters
  const double comp1Prior = 0.4;
  const double comp2Prior = 0.4;
  const double flowSigmaB1 = 0.5;
  const double flowSigmaB2 = 0.5;
  const double flowSigmaF = 2.0;

  // Compute numerical derivatives (measurement 1)
  boost::function<Vector(const LieMatrix&, const LieMatrix&, const LieVector&)> callFullFlowErrorA(
    CallFullFlowError2(measurement1, comp1Prior, comp2Prior, flowSigmaB1, flowSigmaB2, flowSigmaF));
  Matrix ddV1Expected1 = numericalDerivative31(callFullFlowErrorA, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));
  Matrix ddV2Expected1 = numericalDerivative32(callFullFlowErrorA, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));
  Matrix ddyExpected1 = numericalDerivative33(callFullFlowErrorA, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));

  // Compute numerical derivatives (measurement 2)
  boost::function<Vector(const LieMatrix&, const LieMatrix&, const LieVector&)> callFullFlowErrorB(
    CallFullFlowError2(measurement2, comp1Prior, comp2Prior, flowSigmaB1, flowSigmaB2, flowSigmaF));
  Matrix ddV1Expected2 = numericalDerivative31(callFullFlowErrorB, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));
  Matrix ddV2Expected2 = numericalDerivative32(callFullFlowErrorB, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));
  Matrix ddyExpected2 = numericalDerivative33(callFullFlowErrorB, LieMatrix(basis1), LieMatrix(basis2), LieVector(latent));

  // Call fullFlowError (measurement 1)
  Matrix ddV1Actual1, ddV2Actual1, ddyActual1;
  Eigen::Vector3d indicators1;
  Vector e1 = linearFlow::fullFlowError(measurement1, comp1Prior, comp2Prior, basis1, basis2, latent, flowSigmaB1, flowSigmaB2, flowSigmaF, indicators1,
    ddV1Actual1, ddV2Actual1, ddyActual1);

  // Call fullFlowError (measurement 2)
  Matrix ddV1Actual2, ddV2Actual2, ddyActual2;
  Eigen::Vector3d indicators2;
  Vector e2 = linearFlow::fullFlowError(measurement2, comp1Prior, comp2Prior, basis1, basis2, latent, flowSigmaB1, flowSigmaB2, flowSigmaF, indicators2,
    ddV1Actual2, ddV2Actual2, ddyActual2);

  EXPECT(assert_equal(ddV1Expected1, ddV1Actual1, 1e-4));
  EXPECT(assert_equal(ddV2Expected1, ddV2Actual1, 1e-4));
  EXPECT(assert_equal(ddyExpected1, ddyActual1, 1e-4));

  EXPECT(assert_equal(ddV1Expected2, ddV1Actual2, 1e-4));
  EXPECT(assert_equal(ddV2Expected2, ddV2Actual2, 1e-4));
  EXPECT(assert_equal(ddyExpected2, ddyActual2, 1e-4));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
