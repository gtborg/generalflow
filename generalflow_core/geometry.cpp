/**
 * @file    flowGeometry.cpp
 * @brief   Operations for parametrized optical flow fields and their derivatives
 * @author  Richard Roberts
 * @created January 11, 2013
 */

#include <gtsam/base/LieMatrix.h>
#include <generalflow_core/geometry.h>
#include <limits>

using namespace std;
using namespace gtsam;

namespace generalflow {
  namespace geometry {

//#pragma optimize("", off)
    /* **************************************************************************************** */
    Point3 planeLineIntersection(const Eigen::Vector4d& plane, const Point3& a, const Point3& b) {
      Eigen::Vector4d A; A << a.vector(), 1.0; // Homogeneous point coordinates
      Eigen::Vector4d B; B << b.vector(), 1.0;
      // Compute x = L*plane, where L = AB' - BA' (see Hartley&Zisserman p. 70, Pl�cker matrix line representation)
      // The grouping A(B'*plane) - B(A'*plane) is more efficient to compute:
      const Eigen::Vector4d x = A * (B.transpose() * plane) - B * (A.transpose() * plane);
      // Normalize x and return
      return Point3(x(0)/x(3), x(1)/x(3), x(2)/x(3));
    }

    /* **************************************************************************************** */
    Point3 cameraGroundPlaneIntersection(const Pose3& camera, const Cal3_S2& K, const Point2& x) {
      const Point3 a = camera.translation();
      const Point2 calibrated = K.calibrate(x);
      const Point3 b = camera.transform_from(Point3(1.0, calibrated.x(), calibrated.y()));
      const Eigen::Vector4d xyPlane(0.0, 0.0, 1.0, 0.0);
      return planeLineIntersection(xyPlane, a, b);
    }

    /* **************************************************************************************** */
    namespace {
      template<int N>
      Eigen::Matrix<double,2,N> _opticalFlowGroundPlane(const Pose3& camera, const Cal3_S2& K, const Point2& pixel,
        const Eigen::Matrix<double,6,N>& v)
      {
        if(K.skew() != 0.0)
          throw std::invalid_argument("flowGeometry::opticalFlowGroundPlane only supports zero skew in calibration");
        const Point3 X = cameraGroundPlaneIntersection(camera, K, pixel);
        const Point3 Xc = camera.transform_to(X);
        const Point2 centeredPixel = pixel - K.principalPoint();
        const double x = centeredPixel.x(), y = centeredPixel.y();
        const double fx = K.fx(), fy = K.fy();
        if(Xc.x() > 0.0) {
          const double p = 1.0 / Xc.x();
          //const Eigen::Matrix<double,2,6> W = (Eigen::Matrix<double,2,6>() <<
          //  x*y/fx,      -(fx + x*x/fx),  y,  -p*fx,  0,    p*x,
          //  fy + y*y/fy, -x*y/fy,        -x,   0,    -p*fy, p*y).finished();
          const Eigen::Matrix<double,2,6> W = (Eigen::Matrix<double,2,6>() <<
            y,  x*y/fx,      -(fx + x*x/fx),   p*x, -p*fx, 0,
            -x, fy + y*y/fy, -x*y/fy,          p*y,  0,   -p*fy).finished();
          return W * v;
        } else {
          return Eigen::Matrix<double,2,N>::Constant(2, v.cols(), numeric_limits<double>::infinity());
        }
      }
    }

    /* **************************************************************************************** */
    Point2 opticalFlowGroundPlane(const Pose3& camera, const Cal3_S2& K, const Point2& pixel, const Vector6& v) {
      const Eigen::Vector2d u = _opticalFlowGroundPlane<1>(camera, K, pixel, v);
      return Point2(u(0), u(1));
    }

    /* **************************************************************************************** */
    Eigen::Matrix<double,2,6>
      opticalFlowGroundPlane(const Pose3& camera, const Cal3_S2& K, const Point2& pixel, const Matrix6& v)
    {
      return _opticalFlowGroundPlane<6>(camera, K, pixel, v);
    }

    /* **************************************************************************************** */
    gtsam::Vector
      opticalFlowGroundPlane(const Pose3& camera, const Cal3_S2& K, const Point2& pixel, const Vector& v)
    {
      return _opticalFlowGroundPlane<1>(camera, K, pixel, v);
    }

    /* **************************************************************************************** */
    namespace {
      template<int N>
      Eigen::Matrix<double,2,N> _opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel,
        const Eigen::Matrix<double,3,N>& v)
      {
        if(K.skew() != 0.0)
          throw std::invalid_argument("flowGeometry::opticalFlowInfinity only supports zero skew in calibration");
        const Point2 centeredPixel = pixel - K.principalPoint();
        const double x = centeredPixel.x(), y = centeredPixel.y();
        const double fx = K.fx(), fy = K.fy();
        const Eigen::Matrix<double,2,3> W = (Eigen::Matrix<double,2,3>() <<
          y,  x*y/fx,      -(fx + x*x/fx),
          -x, fy + y*y/fy, -x*y/fy).finished();
        return W * v;
      }
    }

    /* **************************************************************************************** */
    gtsam::Point2 opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const Eigen::Vector3d& v) {
      const Eigen::Vector2d u = _opticalFlowInfinity<1>(K, pixel, v);
      return Point2(u(0), u(1));
    }

    /* **************************************************************************************** */
    Eigen::Matrix<double,2,3>
      opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const Eigen::Matrix3d& v)
    {
      return _opticalFlowInfinity<3>(K, pixel, v);
    }

    /* **************************************************************************************** */
    gtsam::Vector
      opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const gtsam::Vector& v)
    {
      return _opticalFlowInfinity<1>(K, pixel, v);
    }

    /* **************************************************************************************** */
    gtsam::Matrix flowFieldGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const Vector6& v,
      int gridWidth, int gridHeight, int imgWidth, int imgHeight)
    {
      // Step sizes in pixels for grid
      const double xstep = double(imgWidth) / double(gridWidth);
      const double ystep = double(imgHeight) / double(gridHeight);
      // Allocate result flow field
      Matrix flow(2*gridHeight, gridWidth);
      // Loop over pixels
      double x = 0.5*xstep;
      for(int j = 0; j < gridWidth; ++j) {
        double y = 0.5*ystep;
        for(int i = 0; i < gridHeight; ++i) {
          // Compute flow at each pixel
          const Point2 pixel(x, y);
          flow.block(2*i, j, 2, 1) = opticalFlowGroundPlane(camera, K, pixel, v).vector();
          y += ystep;
        }
        x += xstep;
      }
      return flow;
    }

    /* **************************************************************************************** */
    gtsam::Matrix flowFieldInfinity(const gtsam::Cal3_S2& K, const Eigen::Vector3d& v,
      int gridWidth, int gridHeight, int imgWidth, int imgHeight)
    {
      // Step sizes in pixels for grid
      const double xstep = double(imgWidth) / double(gridWidth);
      const double ystep = double(imgHeight) / double(gridHeight);
      // Allocate result flow field
      Matrix flow(2*gridHeight, gridWidth);
      // Loop over pixels
      double x = 0.5*xstep;
      for(int j = 0; j < gridWidth; ++j) {
        double y = 0.5*ystep;
        for(int i = 0; i < gridHeight; ++i) {
          // Compute flow at each pixel
          const Point2 pixel(x, y);
          flow.block(2*i, j, 2, 1) = opticalFlowInfinity(K, pixel, v).vector();
          y += ystep;
        }
        x += xstep;
      }
      return flow;
    }

    /* **************************************************************************************** */
    gtsam::Values sparseBasisGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, double scale,
      int imgWidth, int imgHeight, Key firstBasisKey, const std::vector<std::pair<int,int> >& locations)
    {
      Values result;
      for(size_t i = 0; i < locations.size(); ++i) {
        Vector6 v = Vector6::Zero();
        Matrix V(2, v.size());
        for(int k = 0; k < v.size(); ++k) {
          v(k) = scale;
          V.col(k) = opticalFlowGroundPlane(camera, K, Point2(locations[i].first, locations[i].second), v).vector();
          v(k) = 0.0;
        }
        result.insert(firstBasisKey + i, LieMatrix(V));
      }
      return result;
    }

    /* **************************************************************************************** */
    gtsam::Values sparseBasisInfinity(const gtsam::Cal3_S2& K, double scale,
      int imgWidth, int imgHeight, Key firstBasisKey, const std::vector<std::pair<int,int> >& locations)
    {
      Values result;
      for(size_t i = 0; i < locations.size(); ++i) {
        Eigen::Vector3d v = Eigen::Vector3d::Zero();
        Matrix V(2, v.size());
        for(int k = 0; k < v.size(); ++k) {
          v(k) = scale;
          V.col(k) = opticalFlowInfinity(K, Point2(locations[i].first, locations[i].second), v).vector();
          v(k) = 0.0;
        }
        result.insert(firstBasisKey + i, LieMatrix(V));
      }
      return result;
    }

//#pragma optimize("", on)

  }
}