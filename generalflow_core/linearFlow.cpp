/**
 * @file    linearFlow.cpp
 * @brief   Linear optical flow / basis flow model
 * @author  Richard Roberts
 * @created November 23, 2012
 */

#include <generalflow_core/linearFlow.h>

using namespace gtsam;
using namespace std;

namespace generalflow {
  namespace linearFlow {

    //#pragma optimize("", off)
    /* **************************************************************************************** */
    Eigen::Vector2d flowResidual(double ux, double uy, const gtsam::Matrix& basis,
      const gtsam::Vector& latent, boost::optional<gtsam::Matrix&> ddV,
      boost::optional<gtsam::Matrix&> ddy)
    {
      // Compute measurement error (W*y - u)
      const Eigen::Vector2d e = basis * latent - (Eigen::Vector2d() << ux, uy).finished();

      // Compute Jacobians
      const int q = latent.size();
      if(ddV)
        *ddV = (Matrix(2,2*q) <<
        latent.transpose(), Matrix::Zero(1,q), Matrix::Zero(1,q), latent.transpose()).finished();
      if(ddy)
        *ddy = basis;

      return e;
    }

    /* **************************************************************************************** */
    Eigen::Vector2d fullFlowError(const SparseFlowField::Measurement& measurement,
      double inlierPrior, const gtsam::LieMatrix& basis, const gtsam::LieVector& latent,
      double flowSigmaV, double flowSigmaF, double& inlier,
      boost::optional<gtsam::Matrix&> ddV,
      boost::optional<gtsam::Matrix&> ddy)
    {
      // Compute measurement error (W*y - u)
      const Eigen::Vector2d ev = flowResidual(measurement.ux(), measurement.uy(), basis, latent, ddV, ddy);
      const Eigen::Vector2d ef = Eigen::Vector2d(measurement.ux(), measurement.uy());

      // Compute inlier indicator
      const Eigen::RowVector2d invSigmas(1.0/flowSigmaV, 1.0/flowSigmaF);
      const Eigen::Vector2d inlierProbs = indicatorProbabilities<2,2>(
        (Eigen::Matrix<double,2,2>() << ev, ef).finished(),
        Eigen::RowVector2d(inlierPrior, 1.0-inlierPrior),
        invSigmas);
      inlier = inlierProbs(0);

      // Compute effective sigma
      //const double robustFlowInvSigma = sqrt((inlier*invSigmas(0)*invSigmas(0) + (1.-inlier)*invSigmas(1)*invSigmas(1)));
      const double robustInvSigma = sqrt(inlier) * invSigmas(0);

      // Whiten measurement error
      const Eigen::Vector2d b = ev.array() * robustInvSigma;

      // Compute Jacobians
      if(ddV)
        *ddV = ddV->array() * robustInvSigma;
      if(ddy)
        *ddy = ddy->array() * robustInvSigma;

      return b;
    }

    /* **************************************************************************************** */
    Eigen::Vector4d fullFlowError(const SparseFlowField::Measurement& measurement,
      double comp1Prior, double comp2Prior,
      const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2,
      const gtsam::LieVector& latent,
      double flowSigmaB1, double flowSigmaB2, double flowSigmaF, Eigen::Vector3d& indicators,
      boost::optional<gtsam::Matrix&> ddV1, boost::optional<gtsam::Matrix&> ddV2,
      boost::optional<gtsam::Matrix&> ddy)
    {
      // Compute measurement errors
      Matrix ddy1, ddy2;
      boost::optional<Matrix&> _ddy1; if(ddy) _ddy1 = ddy1;
      boost::optional<Matrix&> _ddy2; if(ddy) _ddy2 = ddy2;
      Eigen::Vector2d e1 = flowResidual(measurement.ux(), measurement.uy(), basis1, latent, ddV1, _ddy1);
      Eigen::Vector2d e2 = flowResidual(measurement.ux(), measurement.uy(), basis2, latent.head(basis2.cols()), ddV2, _ddy2);
      const Eigen::Vector2d ef = Eigen::Vector2d(measurement.ux(), measurement.uy());

      // Compute indicator probabilities
      const Eigen::Matrix<double,2,3> residuals = (Eigen::Matrix<double,2,3>() << e1, e2, ef).finished();
      const Eigen::RowVector3d priors(comp1Prior, comp2Prior, (1.0 - comp1Prior - comp2Prior));
      const Eigen::RowVector3d invSigmas(1.0/flowSigmaB1, 1.0/flowSigmaB2, 1.0/flowSigmaF);
      indicators = indicatorProbabilities<2,3>(residuals, priors, invSigmas);

      // Compute effective sigmas
      const double robustInvSigma1 = sqrt(indicators(0)) * invSigmas(0);
      const double robustInvSigma2 = sqrt(indicators(1)) * invSigmas(1);

      if(!e1.allFinite())
        e1.setZero();
      if(!e2.allFinite())
        e2.setZero();
      if(ddy && !ddy1.allFinite())
        ddy1.setZero();
      if(ddy && !ddy2.allFinite())
        ddy2.setZero();

      // Whiten measurement error
      Eigen::Vector4d b; b << e1.array() * robustInvSigma1, e2.array() * robustInvSigma2;

      // Compute Jacobians
      const int q = latent.size();
      if(ddV1)
        *ddV1 = (Matrix(4,2*q) << ddV1->array() * robustInvSigma1, Matrix::Zero(2,2*q)).finished();
      if(ddV2)
        *ddV2 = (Matrix(4,2*q) << Matrix::Zero(2,2*q), ddV2->array() * robustInvSigma2).finished();
      if(ddy) {
        *ddy = (Matrix(4,q) << ddy1 * robustInvSigma1,
          ddy2 * robustInvSigma2, Eigen::Matrix<double,2,Eigen::Dynamic>::Zero(2,latent.size()-basis2.cols())).finished();
        Matrix ddyc = *ddy;
      }

      return b;
    }

    //#pragma optimize("", on)

  }

}
