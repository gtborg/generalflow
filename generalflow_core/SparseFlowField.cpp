/**
* @file    SparseFlowField.cpp
* @brief   A container for sparse flow measurements on a grid
* @author  Richard Roberts
* @created November 23, 2012
*/

#include <limits>
#include <boost/foreach.hpp>
#include <boost/math/special_functions/round.hpp>

#include "SparseFlowField.h"

using namespace std;
using namespace gtsam;

namespace generalflow {

  /* **************************************************************************************** */
  SparseFlowField SparseFlowField::FromPairedField(const gtsam::Matrix& flow, int imgWidth, int imgHeight) {
    const float gridCellWidth = float(imgWidth) / float(flow.cols());
    const float gridCellHeight = float(imgHeight) / float(flow.rows() / 2);
    if(2*(flow.rows()/2) != flow.rows())
      throw invalid_argument("Flow field does not have an even number of rows (u,v should be paired)");
    SparseFlowField sparseFlow(imgWidth, imgHeight);

    // Fill in sparse flow measurements
    for(int j=0; j<flow.cols(); ++j)
      for(int i=0; i<(flow.rows()/2); ++i)
        if(isfinite(flow(2*i, j)) && isfinite(flow(2*i+1, j))) {
          const pair<float,float> xy = calcPixel(j, i, gridCellWidth, gridCellHeight);
          sparseFlow.push_back(Measurement(xy.first, xy.second, (float)flow(2*i,j), (float)flow(2*i+1,j)));
        }

    return sparseFlow;
  }

  /* **************************************************************************************** */
  bool SparseFlowField::equals(const SparseFlowField& other, double tol) const {
    if(imgWidth_ != other.imgWidth_ || imgHeight_ != other.imgHeight_ || this->size() != other.size())
      return false;
    for(size_t i = 0; i < this->size(); ++i) {
      if(!measurements_[i].equals(other.measurements_[i], tol))
        return false;
    }
    return true;
  }

  /* **************************************************************************************** */
  gtsam::Matrix SparseFlowField::sampleOnGrid(int gridWidth, int gridHeight) const {
    Matrix result = Matrix::Zero(2*gridHeight, gridWidth);
    Matrix counts = Matrix::Zero(2*gridHeight, gridWidth);
    const float gridCellWidth = float(this->imgWidth()) / float(gridWidth);
    const float gridCellHeight = float(this->imgHeight()) / float(gridHeight);
    BOOST_FOREACH(const SparseFlowField::Measurement& m, *this) {
      const pair<int,int> gridCell = calcGridCell(m.x(), m.y(), gridCellWidth, gridCellHeight);
      result.block<2,1>(2*gridCell.second, gridCell.first) += Eigen::Vector2d(m.ux(), m.uy());
      counts.block<2,1>(2*gridCell.second, gridCell.first) += Eigen::Vector2d::Ones();
    }
    result.array() /= counts.array(); // Average
    return result;
  }

  /* **************************************************************************************** */
  void SparseFlowField::Measurement::print(const std::string& str) const {
    cout << str << "x: (" << x_ << ", " << y_ << "),  u: (" << ux_ << ", " << uy_ << ")" << endl;
  }

  /* **************************************************************************************** */
  std::vector<std::pair<int,int> > SparseFlowField::getPixels() const {
    vector<pair<int,int> > points;
    BOOST_FOREACH(const Measurement& m, *this) {
      points.push_back(make_pair((int)boost::math::round(m.x()), (int)boost::math::round(m.y())));
    }
    return points;
  }

  /* **************************************************************************************** */
  gtsam::Matrix flowOnGrid(const SparseFlowField& flow, int gridWidth, int gridHeight) {
    Matrix result = Matrix::Zero(2*gridHeight, gridWidth);
    Matrix counts = Matrix::Zero(2*gridHeight, gridWidth);
    const float gridCellWidth = float(flow.imgWidth()) / float(gridWidth);
    const float gridCellHeight = float(flow.imgHeight()) / float(gridHeight);
    BOOST_FOREACH(const SparseFlowField::Measurement& m, flow) {
      const pair<int,int> gridCell = calcGridCell(m.x(), m.y(), gridCellWidth, gridCellHeight);
      result.block<2,1>(2*gridCell.second, gridCell.first) += Eigen::Vector2d(m.ux(), m.uy());
      counts.block<2,1>(2*gridCell.second, gridCell.first) += Eigen::Vector2d::Ones();
    }
    result.array() /= counts.array(); // Average
    return result;
  }

}
