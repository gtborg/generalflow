/**
* @file    SparseFlowField.h
* @brief   A container for sparse flow measurements on a grid
* @author  Richard Roberts
* @created November 23, 2012
*/
#pragma once

#include <generalflow_core/dllexport.h>
#include <vector>
#include <gtsam/base/Matrix.h>

namespace generalflow {

  /**
   * A class representing a flow field stored as sparse flow measurements.
   */
  class generalflow_core_EXPORT SparseFlowField {

  public:

    /**
     * A single flow measurement
     */
    struct generalflow_core_EXPORT Measurement {
      float x_;	///< The x grid coordinate
      float y_;	///< The y grid coordinate
      float ux_; ///< The horizontal flow component
      float uy_; ///< The vertical flow component
      Measurement() : x_(-1), y_(-1), ux_(0.0), uy_(0.0) {}
      Measurement(float x_, float y_, float ux_, float uy_) : x_(x_), y_(y_), ux_(ux_), uy_(uy_) {}

      bool equals(const Measurement& o, double tol = 1e-9) const {
        return abs(x_ - o.x_) <= tol && abs(y_ - o.y_) <= tol && abs(ux_ - o.ux_) <= tol && abs(uy_ - o.uy_) <= tol;
      }

      void print(const std::string& str = "") const;

      float x() const { return x_; }
      float y() const { return y_; }
      float ux() const { return ux_; }
      float uy() const { return uy_; }
    };

  private:
    int imgWidth_; ///< The width of the original image
    int imgHeight_; ///< The height of the original image
    typedef std::vector<Measurement> Measurements;
    Measurements measurements_; ///< The measurements

  public:

    typedef Measurements::const_iterator const_iterator;
    typedef Measurements::const_iterator iterator;

    /// Default constructor creates an empty flow field with zero image size
    SparseFlowField() : imgWidth_(0), imgHeight_(0) {}

    /// Create an empty flow field with the specified image size
    SparseFlowField(int imgWidth, int imgHeight) :
      imgWidth_(imgWidth), imgHeight_(imgHeight) {}

    /// Create a flow field with the specified image size and measurements
    SparseFlowField(int imgWidth, int imgHeight,
      const std::vector<Measurement>& measurements) : imgWidth_(imgWidth), imgHeight_(imgHeight), measurements_(measurements) {}

    /// Create a sparse flow field from a paired flow field.  A paired flow field is a matrix with
    /// twice the height of the actual flow field, and each pair of rows contains the horizontal and
    /// vertical components of the flow vectors.  It is assumed that the paired flow field is
    /// already subsampled from a larger image, so the original image size may be specified in
    /// imgWidth and imgHeight to properly set up the x and y coordinates of the sparse flow
    /// measurements to correspond to pixels in the original image. */
    static SparseFlowField FromPairedField(const gtsam::Matrix& flow, int imgWidth, int imgHeight);

    /// Test for equality
    bool equals(const SparseFlowField& other, double tol = 1e-9) const;
    
    /** Fill in a paired flow field using the given sparse flow measurements,
     * sampling the flow on a regular grid.  Multiple sparse flow measurements falling into the same
     * grid cell are averaged. */
    gtsam::Matrix sampleOnGrid(int gridWidth, int gridHeight) const;

    /// Iterator to the first flow measurement.
    const_iterator begin() const { return measurements_.begin(); }

    /// Iterator to the last flow measurement.
    const_iterator end() const { return measurements_.end(); }

    /// Retrieve the image width.
    int imgWidth() const { return imgWidth_; }

    /// Retrieve the image height.
    int imgHeight() const { return imgHeight_; }

    /// Retrieve the number of sparse flow measurements.
    size_t size() const { return measurements_.size(); }

    /// Retrieve a measurement by index, synonym for at(size_t i).
    const Measurement& operator[](size_t i) const { return measurements_[i]; }

    /// Retrieve a measurement by index, synonym for operator[](size_t i).
    const Measurement& at(size_t i) const { return measurements_[i]; }

    /// Add a new sparse flow measurement
    void push_back(const Measurement& m) { measurements_.push_back(m); }

    /// Build a vector of the x,y coordinates of each sparse flow measurement (their positions, not
    /// their flow vectors).
    std::vector<std::pair<int,int> > getPixels() const;
  };

  typedef SparseFlowField::Measurement SparseFlowFieldMeasurement;

  /** Calculate the grid cell corresponding to a pixel */
  inline std::pair<int,int> calcGridCell(float pixelX, float pixelY, float gridCellWidth, float gridCellHeight) {
    return std::make_pair(int(pixelX / gridCellWidth), int(pixelY / gridCellHeight)); }

  /** Calculate the pixel corresponding to a grid cell */
  inline std::pair<float,float> calcPixel(int gridX, int gridY, float gridCellWidth, float gridCellHeight) {
    return std::make_pair((float(gridX) + 0.5f) * gridCellWidth, (float(gridY) + 0.5f) * gridCellHeight); }

  /** Fill in a paired flow field using the given sparse flow measurements,
   * sampling the flow on a regular grid. */
  generalflow_core_EXPORT gtsam::Matrix flowOnGrid(const SparseFlowField& flow, int gridWidth, int gridHeight);

}
