/**
 * @file    linearFlow.h
 * @brief   Linear optical flow / basis flow model
 * @author  Richard Roberts
 * @created November 23, 2012
 */
#pragma once

#include <generalflow_core/dllexport.h>
#include <generalflow_core/SparseFlowField.h>

#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>

namespace generalflow {
  namespace linearFlow {

    /** The flow prediction residual and its derivatives */
    generalflow_core_EXPORT Eigen::Vector2d flowResidual(double ux, double uy, const gtsam::Vector& basis,
      const gtsam::Vector& latent, boost::optional<gtsam::Matrix&> ddV, boost::optional<gtsam::Matrix&> ddy);

    /** Indicator variable probabilities */
    template<int M, int N>
    Eigen::Matrix<double,N,1> indicatorProbabilities(const Eigen::Matrix<double,M,N>& residuals,
      const Eigen::Matrix<double,1,N>& priors, const Eigen::Matrix<double,1,N>& invSigmas);

    /**
    * Compute the prediction error of a flow vector measurement, and optionally, its derivatives.
    */
    generalflow_core_EXPORT Eigen::Vector2d fullFlowError(const SparseFlowField::Measurement& measurement,
      double inlierPrior, const gtsam::LieMatrix& basis, const gtsam::LieVector& latent,
      double flowSigmaV, double flowSigmaF, double& inlier,
      boost::optional<gtsam::Matrix&> ddV = boost::none,
      boost::optional<gtsam::Matrix&> ddy = boost::none);

    /**
    * Compute the prediction error of a flow vector measurement, and optionally, its derivatives.
    */
    generalflow_core_EXPORT Eigen::Vector4d fullFlowError(const SparseFlowField::Measurement& measurement,
      double comp1Prior, double comp2Prior,
      const gtsam::LieMatrix& basis1, const gtsam::LieMatrix& basis2,
      const gtsam::LieVector& latent,
      double flowSigmaB1, double flowSigmaB2, double flowSigmaF,
      Eigen::Vector3d& indicators,
      boost::optional<gtsam::Matrix&> ddV1 = boost::none,
      boost::optional<gtsam::Matrix&> ddV2 = boost::none,
      boost::optional<gtsam::Matrix&> ddy = boost::none);


    //#pragma optimize("", off)
    /* **************************************************************************************** */
    template<int M, int N>
    Eigen::Matrix<double,N,1> indicatorProbabilities(const Eigen::Matrix<double,M,N>& residuals,
      const Eigen::Matrix<double,1,N>& priors, const Eigen::Matrix<double,1,N>& invSigmas)
    {
      const gtsam::DenseIndex K = (N == Eigen::Dynamic ? priors.size() : N);
      Eigen::Matrix<double,N,1> p(K, 1);
      double sumP = 0.0;
      for(int i=0; i<K; ++i) {
        assert(priors(i) >= 0.0 && priors(i) <= 1.0);
        const double eSqNorm = residuals.col(i).squaredNorm();
        if(std::isfinite(eSqNorm))
          p(i) = priors(i) * invSigmas(i) * exp(-0.5 * eSqNorm * invSigmas(i) * invSigmas(i));
        else
          p(i) = 0.0;
        sumP += p(i);
      }

      if(sumP < 1e-10)
        for(int i=0; i<K; ++i)
          p(i) = priors(i);
      else
        for(int i=0; i<K; ++i)
          p(i) /= sumP;

      // Bump up near-zero probabilities
      const double minP = 0.3 / double(K);
      sumP = 0.0;
      if(1.0 - p(0) < minP)
        p(0) = 1.0 - minP;
      for(int i=0; i<K; ++i) {
        sumP += p(i);
      }
      for(int i=0; i<K; ++i)
        p(i) /= sumP;

      return p;
    }
    //#pragma optimize("", on)

  }

}
