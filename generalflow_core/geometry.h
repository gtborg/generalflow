/**
 * @file    flowGeometry.h
 * @brief   Operations for parametrized optical flow fields and their derivatives
 * @author  Richard Roberts
 * @created January 11, 2013
 */
#pragma once

#include <generalflow_core/dllexport.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/geometry/PinholeCamera.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/nonlinear/Values.h>

namespace generalflow {

  namespace geometry {

    generalflow_core_EXPORT
      gtsam::Point3
      planeLineIntersection(const Eigen::Vector4d& plane, const gtsam::Point3& a, const gtsam::Point3& b);

    generalflow_core_EXPORT
      gtsam::Point3
      cameraGroundPlaneIntersection(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& x);

    generalflow_core_EXPORT
      gtsam::Point2
      opticalFlowGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const gtsam::Vector6& v);
    
    generalflow_core_EXPORT
      Eigen::Matrix<double,2,6>
      opticalFlowGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const gtsam::Matrix6& v);

    generalflow_core_EXPORT
      gtsam::Vector
      opticalFlowGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const gtsam::Vector& v);

    generalflow_core_EXPORT
      gtsam::Point2
      opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const Eigen::Vector3d& v);

    generalflow_core_EXPORT
      Eigen::Matrix<double,2,3>
      opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const Eigen::Matrix3d& v);

    generalflow_core_EXPORT
      gtsam::Vector
      opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, const gtsam::Vector& v);

    generalflow_core_EXPORT
      gtsam::Matrix
      flowFieldGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Vector6& v,
      int gridWidth, int gridHeight, int imgWidth, int imgHeight);

    generalflow_core_EXPORT
      gtsam::Matrix
      flowFieldInfinity(const gtsam::Cal3_S2& K, const Eigen::Vector3d& v,
      int gridWidth, int gridHeight, int imgWidth, int imgHeight);

    generalflow_core_EXPORT
      gtsam::Values
      sparseBasisGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, double scale,
      int imgWidth, int imgHeight, gtsam::Key firstBasisKey, const std::vector<std::pair<int,int> >& locations);

    generalflow_core_EXPORT
      gtsam::Values
      sparseBasisInfinity(const gtsam::Cal3_S2& K, double scale,
      int imgWidth, int imgHeight, gtsam::Key firstBasisKey, const std::vector<std::pair<int,int> >& locations);

  }
}
