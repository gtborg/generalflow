virtual class gtsam::LieVector;
virtual class gtsam::Rot2;
virtual class gtsam::Rot3;
virtual class gtsam::Pose2;
virtual class gtsam::Pose3;
virtual class gtsam::Cal3_S2;
virtual class gtsam::Cal3DS2;
virtual class gtsam::Point2;
virtual class gtsam::Point3;
virtual class gtsam::NonlinearFactor;
class gtsam::NonlinearFactorGraph;
class gtsam::Values;
virtual class gtsam::noiseModel::Base;
class gtsam::Symbol;

#include <gtsam/geometry/Pose2.h>
#include <gtsam/geometry/Pose3.h>

namespace gtsam {

#include <gtsam/base/timing.h>
/** print the timing outline */
void tictoc_print_();
void tictoc_reset_();

}

namespace generalflow {

#include <generalflow_core/wrappedContainers.h>
class IntVector {
  IntVector();
  size_t size() const;
  void push_back(int a);
  int at(size_t i) const;
};

class IntPairVector {
  IntPairVector();
  size_t size() const;
  void push_back(int a, int b);
  pair<int,int> at(size_t i) const;
};

#include <generalflow_core/SparseFlowField.h>
class SparseFlowField {
  SparseFlowField();
  SparseFlowField(int imgWidth, int imgHeight);
  static generalflow::SparseFlowField FromPairedField(Matrix flow, int imgWidth, int imgHeight);
  int imgWidth() const;
  int imgHeight() const;
  size_t size() const;
  generalflow::SparseFlowFieldMeasurement at(size_t i) const;
  void push_back(const generalflow::SparseFlowFieldMeasurement& m);
  generalflow::IntPairVector getPixels() const;
};

class SparseFlowFieldMeasurement {
  SparseFlowFieldMeasurement(double x, double y, double ux, double uy);
  void print(string str) const;
  void print() const;
  double x() const;
  double y() const;
  double ux() const;
  double uy() const;
};

pair<int,int> calcGridCell(double pixelX, double pixelY, double gridCellWidth, double gridCellHeight);
pair<double,double> calcPixel(int gridX, int gridY, double gridCellWidth, double gridCellHeight);
Matrix flowOnGrid(const generalflow::SparseFlowField& flow, int gridWidth, int gridHeight);

#include <generalflow_core/geometry.h>
namespace geometry {
  gtsam::Point3 planeLineIntersection(Vector plane, const gtsam::Point3& a, const gtsam::Point3& b);
  gtsam::Point3 cameraGroundPlaneIntersection(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& x);
  Vector opticalFlowGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, Vector v);
  Vector opticalFlowInfinity(const gtsam::Cal3_S2& K, const gtsam::Point2& pixel, Vector v);
  Matrix flowFieldGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, Vector v,
    int gridWidth, int gridHeight, int imgWidth, int imgHeight);
  Matrix flowFieldInfinity(const gtsam::Cal3_S2& K, Vector v,
    int gridWidth, int gridHeight, int imgWidth, int imgHeight);
  gtsam::Values sparseBasisGroundPlane(const gtsam::Pose3& camera, const gtsam::Cal3_S2& K, double scale,
    int imgWidth, int imgHeight, size_t firstBasisKey, const generalflow::IntPairVector& locations);
  gtsam::Values sparseBasisInfinity(const gtsam::Cal3_S2& K, double scale,
    int imgWidth, int imgHeight, size_t firstBasisKey, const generalflow::IntPairVector& locations);
}

}
