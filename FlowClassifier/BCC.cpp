/**
 * @file BCC (Balancing cats controller)
 * @author Richard Roberts
 */

#include <FlowClassifier/BCC.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/slam/PriorFactor.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace generalflow::flowlabeling;

namespace FlowClassifier {

  /* **************************************************************************************** */
  namespace {
    struct Balanced {
      double obstacleDensityLeft;
      double obstacleDensityRight;

      Balanced(
        const std::vector<Point2>& calibratedPoints,
        const Eigen::Matrix<double,3,-1>& indicators,
        double offCenterFactor, bool countGround, bool countDistant)
      {
        // Sum obstacle and non-obstacle probabilities
        double obstacleLeft = 0.0, clearLeft = 0.0;
        double obstacleRight = 0.0, clearRight = 0.0;
        for(int i = 0; i < indicators.cols(); ++i) {
          if(calibratedPoints[i].x() < 0.0) {
            const double factor = 1.0 + offCenterFactor * (1.0 - -calibratedPoints[i].x());
            if(countGround)
              clearLeft += factor * indicators(0, i);
            if(countDistant)
              clearLeft += factor * indicators(1, i);
            obstacleLeft += factor * indicators(2, i);
          } else {
            const double factor = 1.0 + offCenterFactor * (1.0 - calibratedPoints[i].x());
            if(countGround)
              clearRight += factor * indicators(0, i);
            if(countDistant)
              clearRight += factor * indicators(1, i);
            obstacleRight += factor * indicators(2, i);
          }
        }

        // Normalize densities
        obstacleDensityLeft = obstacleLeft / (obstacleLeft + clearLeft);
        obstacleDensityRight = obstacleRight / (obstacleRight + clearRight);

        // Fix if no features were observed on one or both sides
        if(isfinite(obstacleDensityLeft) && !isfinite(obstacleDensityRight))
          obstacleDensityRight = obstacleDensityLeft;
        else if(!isfinite(obstacleDensityLeft) && isfinite(obstacleDensityRight))
          obstacleDensityLeft = obstacleDensityRight;
        else if(!isfinite(obstacleDensityLeft) && !isfinite(obstacleDensityRight))
          obstacleDensityLeft = obstacleDensityRight = 0.5;
      }
    };
  }

  /* **************************************************************************************** */
  BCC::BCC(const BCCParams& params) :
    params_(params)
  {
    calibrationUndistorted_ = Cal3_S2(
      params_.calibration.fx(), params_.calibration.fy(), params_.calibration.skew(),
      params_.calibration.px(), params_.calibration.py()); // Create undistorted version of calibration
    cvCameraMatrix_.create(3, 3, CV_64F); // Allocate OpenCV camera matrix
    Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor> >(&cvCameraMatrix_.at<double>(0,0))
      = calibrationUndistorted_.matrix(); // Copy calibration matrix into OpenCV camera matrix
    cvDistCoeffs_.create(1, params_.calibration.k().size(), CV_64F); // Allocate OpenCV distortion coeffs
    Eigen::Map<Eigen::VectorXd>(&cvDistCoeffs_.at<double>(0,0), params_.calibration.k().size())
      = params_.calibration.k(); // Copy distortion coeffs into OpenCV matrix
  }

  /* **************************************************************************************** */
  Eigen::Vector2d BCC::balance(const gtsam::Rot3& attitude, double altitude, const Eigen::Vector3d& deltaAxisAngle,
    const std::vector<std::pair<gtsam::Point2, gtsam::Point2> >& measurements) const
  {
    // Undistort measurements
    typedef std::pair<gtsam::Point2, gtsam::Point2> Point2Pair;
    std::vector<Point2Pair> measurementsUndistorted;
    measurementsUndistorted.reserve(measurements.size());
    std::vector<Point2> calibratedPoints;
    calibratedPoints.reserve(measurements.size());

    BOOST_FOREACH(const Point2Pair& m, measurements)
    {
      boost::array<Eigen::Vector2d, 2> distorted;
      distorted[0] = Eigen::Vector2d(m.first.x(), m.first.y());
      distorted[1] = Eigen::Vector2d(m.second.x(), m.second.y());
      boost::array<Eigen::Vector2d, 2> undistorted;
      cv::undistortPoints(
        cv::Mat(1, 2, CV_64FC2, &distorted[0]),
        cv::Mat(1, 2, CV_64FC2, &undistorted[0]),
        cvCameraMatrix_, cvDistCoeffs_);
      const Point2 calibrated1(undistorted[0]);
      const Point2 calibrated2(undistorted[1]);
      const Point2 undistorted1 = calibrationUndistorted_.uncalibrate(calibrated1);
      const Point2 undistorted2 = calibrationUndistorted_.uncalibrate(calibrated2);
      measurementsUndistorted.push_back(make_pair(undistorted1, undistorted2));
      calibratedPoints.push_back(calibrated2);
    }

    // Build graph
    NonlinearFactorGraph graph;
    Velocity2ClassFactor::shared_ptr factor = boost::make_shared<Velocity2ClassFactor>(
      0, attitude, altitude, calibrationUndistorted_, params_.inlierSigma, params_.outlierSigma,
      params_.priorGround, params_.priorDistant, measurementsUndistorted);
    graph.push_back(factor);

    // Add angular rates prior
    Vector6 prior;
    prior.head<3>() = deltaAxisAngle;
    prior.tail<3>() = Eigen::Vector3d::Zero();
    Vector6 sigmas; sigmas << params_.gyroPriorSigma, params_.gyroPriorSigma, params_.gyroPriorSigma, 1e2, 1e2, 1e2;
    sigmas.head<3>().setConstant(params_.gyroPriorSigma);
    sigmas.tail<3>().setConstant(1e2);
    graph += PriorFactor<LieVector>(0, LieVector(prior), noiseModel::Diagonal::Sigmas(sigmas));

    // Optimize
    Values initial;
    initial.insert(0, LieVector(prior));
    GaussNewtonOptimizer opt(graph, initial);
    if(params_.nIterations < 0) {
      opt.optimize();
    } else {
      for(int it = 0; it < params_.nIterations; ++it) {
        opt.iterate();
      }
    }

    // Balance features
    const Balanced balanced(calibratedPoints, factor->indicators(),
      params_.offCenterFactor, params_.countGround, params_.countDistant);
    return Eigen::Vector2d(balanced.obstacleDensityLeft, balanced.obstacleDensityRight);
  }

}
