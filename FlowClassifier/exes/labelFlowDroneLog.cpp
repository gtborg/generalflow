/**
 * @file labelFlowDroneLog
 * @author Richard Roberts
 */

#include <generalflow_flowlabeling/Velocity2ClassFactor.h>
#include <FlowClassifier/BCC.h>
#include <iostream>
#include <fstream>
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <gtsam/geometry/Point2.h>
#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/slam/PriorFactor.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace FlowClassifier;
using namespace generalflow::flowlabeling;
using namespace std;
using namespace gtsam;

struct LogData {
  double time;
  double roll;
  double pitch;
  double yaw;
  double rollUncorrected;
  double pitchUncorrected;
  double yawUncorrected;
  double altitude;
  int curFrame;
  int prevFrame;
  int nFeatures;
};

boost::optional<LogData> readNextLine(ifstream& logFile) {
  LogData logData;

  // Read log data
  char logBuf[2048];
  logFile.getline(logBuf, 2048);
  string logBufStr(logBuf);
  if(logBufStr.empty())
    return boost::none;

  // Tokenize
  boost::tokenizer<boost::char_separator<char> > tok = boost::tokenizer<boost::char_separator<char> >(logBufStr, boost::char_separator<char>(" "));
  boost::tokenizer<boost::char_separator<char> >::const_iterator tokIt = tok.begin();

  // Read nav data
  logData.time = boost::lexical_cast<double>(*(tokIt++));
  logData.roll = boost::lexical_cast<double>(*(tokIt++));
  logData.pitch = boost::lexical_cast<double>(*(tokIt++));
  logData.yaw = boost::lexical_cast<double>(*(tokIt++));
  logData.rollUncorrected = boost::lexical_cast<double>(*(tokIt++));
  logData.pitchUncorrected = boost::lexical_cast<double>(*(tokIt++));
  logData.yawUncorrected = boost::lexical_cast<double>(*(tokIt++));
  logData.altitude = boost::lexical_cast<double>(*(tokIt++));
  logData.curFrame = boost::lexical_cast<int>(*(tokIt++));
  logData.prevFrame = boost::lexical_cast<int>(*(tokIt++));
  logData.nFeatures = boost::lexical_cast<int>(*(tokIt++));

  return logData;
}

struct Correspondence {
  double x1;
  double y1;
  double x2;
  double y2;
};

Correspondence readNextCorrespondence(ifstream& logFile) {
  Correspondence correspondence;

  // Read log data
  char logBuf[2048];
  logFile.getline(logBuf, 2048);
  string logBufStr(logBuf);
  if(logBufStr.empty())
    throw std::runtime_error("Unexpected EOF");

  // Tokenize
  boost::tokenizer<boost::char_separator<char> > tok = boost::tokenizer<boost::char_separator<char> >(logBufStr, boost::char_separator<char>(" "));
  boost::tokenizer<boost::char_separator<char> >::const_iterator tokIt = tok.begin();

  // Read nav data
  correspondence.x1 = boost::lexical_cast<double>(*(tokIt++));
  correspondence.y1 = boost::lexical_cast<double>(*(tokIt++));
  correspondence.x2 = boost::lexical_cast<double>(*(tokIt++));
  correspondence.y2 = boost::lexical_cast<double>(*(tokIt++));

  return correspondence;
}

class DebugVelocity2ClassFactor
{
  Velocity2ClassFactor::shared_ptr factor_;

public:
  DebugVelocity2ClassFactor(const Velocity2ClassFactor::shared_ptr& factor) : factor_(factor) {}

  Matrix& basis1() const { return factor_->basis1_; }
  Matrix& basis2() const { return factor_->basis2_; }
  vector<pair<Point2,Point2> >& measurements() const { return factor_->measurements_; }
};

int main(int argc, char *argv[])
{
  if(argc < 2) {
    cout << "Usage: " << argv[0] << " LOG_DIRECTORY" << endl;
    return 1;
  }

  const string logDir = argv[1];
  const int imgFrameLag = 0;
  const int firstFrameToUse = 2 + imgFrameLag;
  const int imageDownScale = 1;
  const int imageSubsample = 1;
  const double pixelSigma = 0.5;
  const double outlierSigma = 2.0;
  const double groundClassPrior = 0.33;
  const double distantClassPrior = 0.33;
  const bool useGyroPrior = true;
  const double gryoPrior = 1e0;
  const bool calcPlain = false;

  const Cal3DS2 calibration(0.5*1123.47872, 0.5*1107.22609, 0., 0.5*655.74421, 0.5*324.27659, -0.51179, 0.26092, 0.00498, 0.00216);
  const Cal3_S2 calUndistorted(calibration.fx(), calibration.fy(), calibration.skew(), calibration.px(), calibration.py());
  const Eigen::Matrix<double,3,3,Eigen::RowMajor> cameraMatrix = calUndistorted.matrix();
  cv::Mat cvCameraMatrix(3, 3, CV_64F, (void*)&cameraMatrix(0,0));
  const Vector distCoeffs = calibration.k();
  cv::Mat cvDistCoeffs(1, distCoeffs.size(), CV_64F, (void*)&distCoeffs(0));

  // BCC
  BCCParams bccParams = BCCParams(calibration);
  //bccParams.imageDownscale = imageDownScale;
  //bccParams.imageSubsample = imageSubsample;
  //bccParams.inlierSigma = pixelSigma;
  //bccParams.outlierSigma = outlierSigma;
  //bccParams.priorGround = groundClassPrior;
  //bccParams.priorDistant = distantClassPrior;
  //bccParams.gyroPriorSigma = gryoPrior;
  //bccParams.nIterations = 10;
  BCC bcc(bccParams);

  // Open log file
  const string logFilename = logDir + "/1__logFlowClassifier.txt";
  ifstream logFile(logFilename.c_str(), ios::binary);

  // Open output file
  const string outputFileName_frames = logDir + "/labeledFlow_frames.txt";
  ofstream outputFile_frames(outputFileName_frames.c_str(), ios::binary);
  const string outputFileName_features = logDir + "/labeledFlow_features.txt";
  ofstream outputFile_features(outputFileName_features.c_str(), ios::binary);
  const string outputFileName_balance = logDir + "/labeledFlow_balance.txt";
  ofstream outputFile_balance(outputFileName_balance.c_str(), ios::binary);

  // Loop
  Rot3 lastAttitude;
  while(true)
  {
    boost::optional<LogData> _data = readNextLine(logFile);
    if(!_data)
      break;
    const LogData& data = *_data;

    // Read features
    vector<pair<Point2, Point2> > correspondencesDistorted;
    vector<pair<Point2, Point2> > correspondences;
    for(int i = 0; i < data.nFeatures; ++i)
    {
      Correspondence correspondence = readNextCorrespondence(logFile);

      // Undistort
      correspondencesDistorted.push_back(make_pair(
        Point2(correspondence.x1, correspondence.y1), Point2(correspondence.x2, correspondence.y2)));
      cv::Mat distorted(1, 2, CV_64FC2);
      distorted.at<cv::Vec2d>(0,0) = cv::Vec2d(correspondence.x1, correspondence.y1);
      distorted.at<cv::Vec2d>(0,1) = cv::Vec2d(correspondence.x2, correspondence.y2);
      cv::Mat undistorted;
      cv::undistortPoints(distorted, undistorted, cvCameraMatrix, cvDistCoeffs);
      const Point2 calibrated1(undistorted.at<cv::Vec2d>(0,0)[0], undistorted.at<cv::Vec2d>(0,0)[1]);
      const Point2 calibrated2(undistorted.at<cv::Vec2d>(0,1)[0], undistorted.at<cv::Vec2d>(0,1)[1]);
      const Point2 undistorted1 = calUndistorted.uncalibrate(calibrated1);
      const Point2 undistorted2 = calUndistorted.uncalibrate(calibrated2);
      correspondences.push_back(make_pair(undistorted1, undistorted2));
    }

    Vector v = Vector6::Zero();
    Eigen::Vector2d balance;
    const int nFeaturesUsed = 
      correspondences.size() >= 1 ? data.nFeatures : 0;
    if(nFeaturesUsed != 0) {
      try {

        Rot3 attitudeUncorrected = Rot3::RzRyRx(data.rollUncorrected, data.pitchUncorrected, data.yawUncorrected);
        Rot3 attitude = Rot3::RzRyRx(data.roll, data.pitch, data.yaw);

        if(calcPlain)
        {
          // Build graph
          NonlinearFactorGraph graph;
          Velocity2ClassFactor::shared_ptr factor = boost::make_shared<Velocity2ClassFactor>(0, attitude, data.altitude,
            calUndistorted, pixelSigma, outlierSigma, groundClassPrior, distantClassPrior, correspondences);
          graph.push_back(factor);

          // Add strong attitude prior if this mode is enabled
          if(useGyroPrior) {
            Vector prior(6);
            prior.segment(0, 3) = lastAttitude.localCoordinates(attitudeUncorrected, Rot3::EXPMAP);
            prior.segment(3, 3) = Eigen::Vector3d::Zero();
            Vector sigmas(6); sigmas << gryoPrior, gryoPrior, gryoPrior, 1e2, 1e2, 1e2;
            graph.add(PriorFactor<LieVector>(0, prior, noiseModel::Diagonal::Sigmas(sigmas)));
          }

          // Optimize
          Values initial;
          initial.insert(0, LieVector(Vector::Zero(6)));
          GaussNewtonOptimizer opt(graph, initial);
          for(int it = 0; it < 10; ++it) {
            opt.iterate();
          }
          v = opt.values().at<LieVector>(0);

          // Write log file
          for(int i = 0; i < data.nFeatures; ++i) {
            outputFile_features.precision(20);
            outputFile_features << data.curFrame << " "
              << correspondencesDistorted[i].first.x() << " " << correspondencesDistorted[i].first.y() << " "
              << correspondencesDistorted[i].second.x() << " " << correspondencesDistorted[i].second.y() << " "
              << factor->indicators()(0,i) << " " << factor->indicators()(1,i) << " " << factor->indicators()(2,i) << endl;
          }

          // Write debug info
          boost::filesystem::create_directory(logDir + "/debug");
          ofstream debugFile1((boost::format(logDir + "/debug/basis1_%08d.txt") % data.curFrame).str().c_str(), ios::binary);
          debugFile1 << DebugVelocity2ClassFactor(factor).basis1() << endl;
          ofstream debugFile2((boost::format(logDir + "/debug/basis2_%08d.txt") % data.curFrame).str().c_str(), ios::binary);
          debugFile2 << DebugVelocity2ClassFactor(factor).basis2() << endl;
        }

        // Run BCC
        balance = bcc.balance(attitude, data.altitude,
          lastAttitude.localCoordinates(attitudeUncorrected, Rot3::EXPMAP),
          correspondencesDistorted);
        outputFile_balance << data.curFrame << " " << balance(0) << " " << balance(1) << endl;

        lastAttitude = attitudeUncorrected;

      } catch(std::exception& e) {
        cout << e.what() << endl;
        return 1;
      }
    } else {
      v = Vector::Zero(6);
    }

    // Write results
    if(calcPlain) {
      cout << "v = [ " << v.transpose() << " ]" << endl;
      outputFile_frames.precision(20);
      outputFile_frames << data.time << " " << v(0) << " " << v(1) << " " << v(2) << " "
        << v(3) << " " << v(4) << " " << v(5) << " " << data.curFrame << " " << nFeaturesUsed << endl;
    }
  }

  return 0;
}
