/**
 * @file BCC (Balancing cats controller)
 * @author Richard Roberts
 */

#include <FlowClassifier/dllexport.h>
#include <generalflow_flowlabeling/Velocity2ClassFactor.h>
#include <gtsam/geometry/Cal3DS2.h>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace gtsam;

namespace FlowClassifier
{

  struct BCCParams
  {
    int imageDownscale;           ///< Factor to downscale the image
    int imageSubsample;           ///< Only use every nth pixel from the downscaled image
    double gyroPriorSigma;        ///< Sigma for a prior from the rotation rate measured by the gyro
    gtsam::Cal3DS2 calibration;   ///< Camera calibration
    double inlierSigma;           ///< Expected sigma on ground plane and distant flow
    double outlierSigma;          ///< Expected sigma on obstacle flow
    double priorGround;           ///< Prior for the ground plane class
    double priorDistant;          ///< Prior for the distant class
    double offCenterFactor;       ///< Factor by which to multiply abs(x), where x is the x image
                                  ///  coordinate, to weight feature contributions
    int nIterations;              ///< Number of iterations to run when solving for labels, *or* set
                                  ///  to -1 to stop based on a quality threshold.
    bool countGround;             ///< Whether to count the ground features as non-obstacles, if
                                  ///  false, they do not affect the controller.
    bool countDistant;            ///< Whether to count distant features as non-obstacles, if false,
                                  ///  they do not affect the controller.

    /** Construct the parameters with defaults for everything except the calibration */
    BCCParams(const gtsam::Cal3DS2& calibration) :
      imageDownscale(1), imageSubsample(1), gyroPriorSigma(1e-3), calibration(calibration),
      inlierSigma(0.5), outlierSigma(2.0), priorGround(0.333), priorDistant(0.333),
      offCenterFactor(100.0), nIterations(-1), countGround(true), countDistant(true) {}
  };

  class FlowClassifier_EXPORT BCC
  {
    BCCParams params_;
    gtsam::Cal3_S2 calibrationUndistorted_;
    cv::Mat cvCameraMatrix_;
    cv::Mat cvDistCoeffs_;

  public:
    BCC(const BCCParams& params);

    Eigen::Vector2d balance(const gtsam::Rot3& attitude, double altitude, const Eigen::Vector3d& deltaAxisAngle,
      const std::vector<std::pair<gtsam::Point2, gtsam::Point2> >& measurements) const;
  };

}
