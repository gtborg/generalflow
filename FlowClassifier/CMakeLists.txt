# Build library
generalflow_add_subdirectory_library("gtsam;generalflow_core;generalflow_flowlabeling;${OpenCV_LIBS}")

if(GENERALFLOW_BUILD_FlowClassifier)
	# If we have GTSAM, use Eigen from GTSAM, and otherwise look for Eigen
	if(TARGET gtsam)
		include_directories("${GTSAM_INCLUDE_DIR}/gtsam/3rdparty/Eigen")
	else()
		find_package(Eigen3 REQUIRED)
		include_directories("${EIGEN3_INCLUDE_DIR}")
	endif()

	# Build executables
	file(GLOB exes "exes/*.cpp")
	set(libs ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY})
	gtsam_add_executables("${exes}" "FlowClassifier;${libs}" "FlowClassifier;${libs}" "")

	#if(GENERALFLOW_BUILD_TESTS)
	#	gtsam_add_subdir_tests("FlowClassifier" "FlowClassifier" "FlowClassifier" "")
	#endif()
endif()

# Export targets
set(GeneralFlow_EXPORTED_TARGETS "${GeneralFlow_EXPORTED_TARGETS}" PARENT_SCOPE)
