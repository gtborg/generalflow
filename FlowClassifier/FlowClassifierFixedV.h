﻿/**
 * @file FlowClassifier.h
 * @brief Class for classifying flow into ground plane or non-ground-plane
 * @author Richard Roberts
 * @date June 20, 2013
 */
#pragma once

#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/geometry/Rot3.h>
#include <generalflow_core/SparseFlowField.h>

#include <vector>

namespace flowclassifier
{

  typedef Eigen::Matrix<double,6,1> Vector6;

  /** Parameters for FlowClassifier */
  struct FlowClassifierFixedVParams
  {
    gtsam::Cal3_S2 cameraCalibration;
    double flowSigmaDistant;
    double flowSigmaGround;
    double flowSigmaOutlier;
    double distantClassPrior;
    double groundClassPrior;
    double outlierClassPrior;
  };

  /** Class for classifying flow into ground plane or non-ground-plane */
  class FlowClassifierFixedV
  {
  protected:
    FlowClassifierFixedVParams params_; /// Our parameters

  public:
    /// Construct a FlowClassifier with the specified parameters
    FlowClassifierFixedV(const FlowClassifierFixedVParams& params) : params_(params) {}

    /// Classify sparse flow measurements.  The velocity vector v has components
    /// [ ωx ωy ωz vx vz vz ]
    std::vector<Eigen::Vector3d> classify(const generalflow::SparseFlowField& measurements,
      const gtsam::Rot3& attitude, double altitude, const Vector6& v) const;

  };

}