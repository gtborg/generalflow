/**
 * @file FlowClassifier.cpp
 * @brief Class for classifying flow into ground plane or non-ground-plane
 * @author Richard Roberts
 * @date June 20, 2013
 */

#include <FlowClassifier/FlowClassifierFixedV.h>
#include <generalflow_core/geometry.h>
#include <generalflow_core/linearFlow.h>
#include <gtsam/geometry/Pose3.h>

using namespace generalflow;
using namespace gtsam;

namespace flowclassifier {

  /* **************************************************************************************** */
  std::vector<Eigen::Vector3d> FlowClassifierFixedV::classify(const generalflow::SparseFlowField& measurements,
    const gtsam::Rot3& attitude, double altitude, const Vector6& v) const
  {
    // Allocate result
    std::vector<Eigen::Vector3d> result(measurements.size());

    // Camera pose (aerospace coordinates - X forward, Y right, Z down)
    const Pose3 cameraPose(attitude, Point3(0.0, 0.0, -altitude));

    for(size_t i = 0; i < measurements.size(); ++i)
    {
      const Point2 pixel(measurements[i].x(), measurements[i].y());
      const Eigen::Vector2d observedFlow(measurements[i].ux(), measurements[i].uy());

      // Compute flow residuals at this pixel for each model
      Eigen::Matrix<double, 2, 3> residuals; residuals <<

        // Residual for structure at infinity
        geometry::opticalFlowInfinity(
        params_.cameraCalibration, pixel, Eigen::Vector3d(v.head<3>())).vector() - observedFlow,

        // Residual for structure on the ground plane
        geometry::opticalFlowGroundPlane(
        cameraPose, params_.cameraCalibration, pixel, v).vector() - observedFlow,

        // Residual for outlier flow
        observedFlow;

      // Priors and sigmas
      const Eigen::RowVector3d priors(params_.distantClassPrior, params_.groundClassPrior, params_.outlierClassPrior);
      const Eigen::RowVector3d invSigmas(1.0/params_.flowSigmaDistant, 1.0/params_.flowSigmaGround, 1.0/params_.flowSigmaOutlier);

      // Compute indicators
      result[i] = linearFlow::indicatorProbabilities<2,3>(residuals, priors, invSigmas);
    }

    return result;
  }

}
