class gtsam::Values;
class gtsam::NonlinearFactorGraph;
virtual class gtsam::NonlinearFactor;
class generalflow::SparseFlowField;

namespace generalflow {
  namespace flowlabeling {

#include <generalflow_flowlabeling/OnlyLatentFlowFactor.h>
    virtual class OnlyLatentFlowFactor : gtsam::NonlinearFactor {
      //OnlyLatentFlowFactor(size_t basisKey, size_t latentKey, size_t sigmaKey,
      //  gtsam::Values* basisValues, gtsam::Values* sigmaValues,
      //  double inlierPrior, const basisflow::SparseFlowFieldMeasurement& measurement);
      static gtsam::NonlinearFactorGraph CreateWholeFrame(const generalflow::SparseFlowField& measurements,
        size_t firstBasisKey, size_t latentKey, size_t sigmaKey,
        gtsam::Values* basisValues, gtsam::Values* sigmaValues, double inlierPrior);
    };

#include <generalflow_flowlabeling/OnlyLatent2FlowFactor.h>
    virtual class OnlyLatent2FlowFactor : gtsam::NonlinearFactor {
      static gtsam::NonlinearFactorGraph CreateWholeFrame(const generalflow::SparseFlowField& measurements,
        size_t firstBasis1Key, size_t firstBasis2Key, size_t latentKey, size_t sigmaKey, size_t indicatorKey,
        gtsam::Values* basisValues, gtsam::Values* sigmaValues, gtsam::Values* indicatorValues,
        double comp1Prior, double comp2Prior);
    };

  }
}

