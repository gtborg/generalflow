class generalflow::SparseFlowField;
class gtsam::Values;
virtual class gtsam::Cal3DS2;

#include <generalflow_flow/sparseFlow.h>
#include <generalflow_flow/flowOps.h>

namespace generalflow {

#include <generalflow_flow/Subspace.h>
  class Subspace {
    Subspace(string path);
    Subspace(string path, int width, int height);
    Subspace(Matrix basis, double inlierSigma2, double outlierSigma2,
      double inlierPixSigma2, double outlierPixSigma2, double inlierPrior, size_t width, size_t height);
    Subspace(Matrix basis, double inlierSigma2, double outlierSigma2, double inlierPrior, size_t width, size_t height);
    Subspace(const gtsam::Values& values, size_t firstKey, double inlierPrior, int width, int height);
    void write(string path) const;
    Matrix basis() const;
    size_t width() const;
    size_t height() const;
    size_t dimSpace() const;
    size_t dimLatent() const;
    double inlierSigma2() const;
    double outlierSigma2() const;
    double inlierPixSigma2() const;
    double outlierPixSigma2() const;
    double inlierPrior() const;
  };

  namespace flow {

    class ImagePairGrad {
      Matrix prevFrame() const;
      Matrix curFrame() const;
      Matrix prevGradX() const;
      Matrix prevGradY() const;

      ImagePairGrad();
      ImagePairGrad(Matrix prevFrame, Matrix curFrame);
      ImagePairGrad(const generalflow::flow::ImagePairGrad& lastPair, Matrix newCurFrame);
    };

    // LK
    generalflow::SparseFlowField computeFlow(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight, double minEigThreshold, const gtsam::Cal3DS2& calibration, Vector initialization);
    generalflow::SparseFlowField computeFlow(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight, double minEigThreshold, const gtsam::Cal3DS2& calibration);
    generalflow::SparseFlowField computeFlow(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight, double minEigThreshold);
    generalflow::SparseFlowField computeFlow(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight);

    // Farneback
    generalflow::SparseFlowField computeFlowFarneback(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      int pyrLevels, double pyrScale, int winSize, int numIters, int polyN, int polySigma, const gtsam::Cal3DS2& calibration);
    generalflow::SparseFlowField computeFlowFarneback(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      int pyrLevels, double pyrScale, int winSize, int numIters, int polyN, int polySigma);
    generalflow::SparseFlowField computeFlowFarneback(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight);

    // TVL1
    generalflow::SparseFlowField computeFlowTVL1(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      bool useSuperpixels, bool normalizeBrightness,
      double tau, double lambda, double theta, int nscales, int warps, double epsilon, int iterations, const gtsam::Cal3DS2& calibration);
    generalflow::SparseFlowField computeFlowTVL1(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      bool useSuperpixels, bool normalizeBrightness,
      double tau, double lambda, double theta, int nscales, int warps, double epsilon, int iterations);
    generalflow::SparseFlowField computeFlowTVL1(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      bool useSuperpixels, bool normalizeBrightness);
    generalflow::SparseFlowField computeFlowTVL1(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight,
      bool useSuperpixels);
    generalflow::SparseFlowField computeFlowTVL1(const cv::Mat& prevFrame, const cv::Mat& curFrame, int gridWidth, int gridHeight);

    cv::Mat getLastSuperpixelLabels();
    Matrix fillSuperpixels(const cv::Mat& superpixelLabels, Vector values);
    void writeIndicatorValuesMATLAB(gtsam::Values& values, Matrix indicators);
    Matrix readIndicatorValuesMATLAB(const gtsam::Values& values, int n);

    void drawFlow(const cv::Mat& image, const generalflow::SparseFlowField& flow, int gridWidth, int gridHeight, double scale);
    void drawFlow(const cv::Mat& image, const generalflow::SparseFlowField& flow, int gridWidth, int gridHeight);

    pair<Matrix, Matrix> warpMaps(const generalflow::Subspace& subspace, Vector y);
    Matrix warpWithBasis(Matrix frame, const generalflow::Subspace& subspace, Vector y);
  }

#include <generalflow_flow/Utils.h>
  class Utils {
    static Matrix matrixOfMat(const cv::Mat& m);
    static cv::Mat matOfMatrix(Matrix m);
    static cv::Mat mat8uOfMatrix(Matrix m);
    static cv::Mat gray2rgb(const cv::Mat& img);
  };

  size_t vKey(char chr, size_t imgSetNum, size_t frameNum);
  char vKeyChr(size_t key);
  size_t vKeyImgSet(size_t key);
  size_t vKeyFrameNum(size_t key);

#include <generalflow_flow/ImgOps.h>
  class ImgOps {
    ImgOps(double sigma, size_t size, size_t nCached);
    ImgOps(double sigma, size_t size);

    static pair<cv::Mat, cv::Mat> ComputeDerivatives(const cv::Mat& img);
    static pair<Matrix, Matrix> ComputeDerivatives(Matrix img);
  };

#include <generalflow_flow/FrameCache.h>
  class FrameCache {
    FrameCache(string filenameFormat, int pixelSkip, int width, int height, int maxCacheBytes);
    generalflow::FrameCacheLoadedImages loadImages(int curFrameNum,
      double flowSigmaV, double flowSigmaF, double pixSigmaV, double pixSigmaF);
  };

  class FrameCacheLoadedImages {
    Matrix prevFrame() const;
    Matrix curFrame() const;
    Matrix prevGradX() const;
    Matrix prevGradY() const;
    Matrix weightsV() const;
    Matrix weightsF() const;
  };

}

namespace cv {

#include <opencv2/core/core.hpp>
  class Mat {
    Mat();
  };
  class Size {
    Size();
    Size(int width, int height);
  };

#include <opencv2/imgproc/imgproc.hpp>
  void resize(const cv::Mat& src, cv::Mat& dst, const cv::Size& dsize, double fx, double fy);

#include <opencv2/highgui/highgui.hpp>
  cv::Mat imread(string filename, int flags);
  cv::Mat imread(string filename);
  void imshow(string winname, const cv::Mat& img);
  int waitKey(int delay);
  void destroyWindow(string winname);

}
